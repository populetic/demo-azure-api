<?php

namespace App\Entity;

class EmailCustom
{
    public function __construct($data)
    {
        foreach ($data as $key => $value) {
            $this->{$key} = $value;
        }
    }

    public $id;
    public $claimId;
    public $email;
    public $status;
    public $subject;
    public $body;
    public $docsIds = [];
    public $createdAt;
}
