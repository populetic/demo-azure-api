<?php

namespace App\Entity;

class CompanyData
{
    public function __construct($data)
    {
        foreach ($data as $key => $value) {
            $this->{$key} = $value;
        }
    }

    public $countryId;
    public $fiscalName;
    public $businessName;
    public $address;
    public $phone;
    public $businessEmail;
    public $adminEmail;
    public $adwordsCookiesExpireDays;
    public $fee;
}
