<?php

namespace App\Entity;

class Demand
{
    public function __construct($data)
    {
        foreach ($data as $key => $value) {
            $this->{$key} = $value;
        }
    }

    public $id;
    public $procedureNumber;
    public $judgementNumber;
    public $courtNumberId;
    public $amountRequested;
    public $amountReceived;
    public $amountInterests;
    public $comments;
    public $isNoShow;
    public $isOverseas;
    public $hasFlightstats;
    public $presentedAt;
    public $admitedAt;
    public $judgedAt;
    public $cancellatedAt;
    public $createdAt;
    public $updatedAt;
}
