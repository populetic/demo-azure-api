<?php

namespace App\Entity;

class Flight
{
    public function __construct($data)
    {
        foreach ($data as $key => $value) {
            $this->{$key} = $value;
        }
    }

    public $id;
    public $number;
    public $date;
    public $status;
    public $airlineId;
    public $airportDepartureId;
    public $departureScheduledTime;
    public $departureActualTime;
    public $airportArrivalId;
    public $arrivalScheduledTime;
    public $arrivalActualTime;
    public $delay;
    public $isDisrupted;
    public $flightDataOriginId;
    public $distance;
    public $createdAt;
}
