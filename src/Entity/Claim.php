<?php

namespace App\Entity;

class Claim
{
    public function __construct($data)
    {
        foreach ($data as $key => $value) {
            $this->{$key} = $value;
        }
    }

    public $id;
    public $ref;
    public $clientId;
    public $claimStatusId;
    public $claimDocumentationStatusId;
    public $flightId;
    public $isOnboardingFlight;
    public $reservationNumber;
    public $disruptionTypeId;
    public $amountEstimated;
    public $amountTickets;
    public $amountReceived;
    public $fee;
    public $hasConnectionFlight;
    public $delayOptionId;
    public $airlineReasonId;
    public $lawsuiteTypeId;
    public $description;
    public $mainClaimId;
    public $isRemarketingEnabled;
    public $isFirstRemarketingSent;
    public $isClaimable;
    public $isExtrajudicialAirlineNegative;
    public $resolutionWayId;
    public $lawFirmId;
    public $userId;
    public $finishedAt;
    public $extrajudicialSentAt;
    public $createdAt;
    public $updatedAt;
    public $additionalInformation;
}
