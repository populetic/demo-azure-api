<?php

namespace App\Entity;

class Note
{
    public function __construct($data)
    {
        foreach ($data as $key => $value) {
            $this->{$key} = $value;
        }
    }

    public $id;
    public $claimId;
    public $content;
    public $userId;
    public $noteTypeId;
    public $departmentForId;
    public $socialNetworkTypeId;
    public $clientSocialNetworkId;
    public $createdAt;
}
