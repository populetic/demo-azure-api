<?php

namespace App\Entity;

class Airline
{
    public function __construct($data)
    {
        foreach ($data as $key => $value) {
            $this->{$key} = $value;
        }
    }

    public $id;
    public $name;
    public $iata;
    public $icao;
    public $flightstatsIata;
    public $isBankruptcy;
    public $hasCommunityLicense;
    public $isActive;
    public $createdAt;
    public $updatedAt;
}
