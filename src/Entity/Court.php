<?php

namespace App\Entity;

class Court
{
    public function __construct($data)
    {
        foreach ($data as $key => $value) {
            $this->{$key} = $value;
        }
    }

    public $id;
    public $courtTypeId;
    public $address;
    public $postalCode;
    public $cityId;
    public $countryId;
    public $createdAt;
    public $updatedAt;
}
