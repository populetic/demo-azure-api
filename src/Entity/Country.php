<?php

namespace App\Entity;

class Country
{
    public function __construct($data)
    {
        foreach ($data as $key => $value) {
            $this->{$key} = $value;
        }
    }

    public $id;
    public $iso;
    public $name;
    public $regionId;
    public $regionName;
    public $isUe;
    public $competencePriority;
    public $isActive;
}
