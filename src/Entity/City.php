<?php

namespace App\Entity;

class City
{
    public function __construct($data)
    {
        foreach ($data as $key => $value) {
            $this->{$key} = $value;
        }
    }

    public $id;
    public $countryId;
    public $name;
    public $isActive;
}
