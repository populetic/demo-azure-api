<?php

namespace App\Entity;

class Partner
{
    public function __construct($data)
    {
        foreach ($data as $key => $value) {
            $this->{$key} = $value;
        }
    }

    public $id;
    public $fiscalName;
    public $businessName;
    public $fee;
    public $isActive;
    public $createdAt;
    public $updatedAt;
}
