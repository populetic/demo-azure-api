<?php

namespace App\Entity;

class Client
{
    public function __construct($data)
    {
        foreach ($data as $key => $value) {
            $this->{$key} = $value;
        }
    }

    public $id;
    public $name;
    public $surnames;
    public $email;
    public $phone;
    public $city;
    public $address;
    public $countryProvinceId;
    public $countryId;
    public $languageId;
    public $idCard;
    public $documentationTypeId;
    public $idCardExpiresAt;
    public $legalGuardianId;
    public $gender;
    public $ip;
    public $userAgent;
    public $createdAt;
    public $updatedAt;
}
