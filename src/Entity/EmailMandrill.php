<?php

namespace App\Entity;

class EmailMandrill
{
    public function __construct($data = [])
    {
        foreach ($data as $key => $value) {
            $this->{$key} = $value;
        }
    }

    public $id;
    public $claimId;
    public $status;
    public $emailMandrillTemplateId;
    public $opens;
    public $clicks;
    public $mandrillId;
    public $docsIds = [];
    public $createdAt;
}
