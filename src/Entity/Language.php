<?php

namespace App\Entity;

class Language
{
    public function __construct($data)
    {
        foreach ($data as $key => $value) {
            $this->{$key} = $value;
        }
    }

    public $id;
    public $code;
    public $name;
    public $claimReference;
}
