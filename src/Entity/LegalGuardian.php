<?php

namespace App\Entity;

class LegalGuardian
{
    public function __construct($data)
    {
        foreach ($data as $key => $value) {
            $this->{$key} = $value;
        }
    }

    public $id;
    public $name;
    public $surnames;
    public $idCard;
    public $documentationTypeId;
    public $idCardExpiresAt;
}
