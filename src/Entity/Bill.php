<?php

namespace App\Entity;

class Bill
{
    public function __construct($data)
    {
        foreach ($data as $key => $value) {
            $this->{$key} = $value;
        }
    }

    public $id;
    public $number;
    public $claimId;
    public $claimBillingDataId;
    public $billTypeId;
    public $base;
    public $ivaTotal;
    public $total;
    public $comments;
    public $parentBillId;
    public $billPreferenceId;
    public $expiresAt;
    public $createdAt;
}
