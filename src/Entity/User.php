<?php

namespace App\Entity;

class User
{
    public function __construct($data)
    {
        foreach ($data as $key => $value) {
            $this->{$key} = $value;
        }
    }

    public $id;
    public $username;
    public $email;
    public $password;
    public $name;
    public $surnames;
    public $countryId;
    public $languageId;
    public $roleId;
    public $lawFirmId;
    public $isSystem;
    public $isActive;
    public $createdAt;
    public $updatedAt;
}
