<?php

namespace App\Entity;

class Documentation
{
    public function __construct($data)
    {
        foreach ($data as $key => $value) {
            $this->{$key} = $value;
        }
    }

    public $id;
    public $claimId;
    public $documentationTypeId;
    public $isValid;
    public $toDelete;
    public $notAvailable;
    public $extension;
    public $createdAt;
    public $updatedAt;
}
