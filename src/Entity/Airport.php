<?php

namespace App\Entity;

class Airport
{
    public function __construct($data)
    {
        foreach ($data as $key => $value) {
            $this->{$key} = $value;
        }
    }

    public $id;
    public $name;
    public $iata;
    public $icao;
    public $countryId;
    public $cityId;
    public $isMain;
    public $latitude;
    public $longitude;
    public $createdAt;
    public $updatedAt;
}
