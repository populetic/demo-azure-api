<?php

namespace App\Entity;

class Region
{
    public function __construct($data)
    {
        foreach ($data as $key => $value) {
            $this->{$key} = $value;
        }
    }

    public $id;
    public $name;
    public $code;
    public $shortname;
    public $metaregion;
    public $isActive;
}
