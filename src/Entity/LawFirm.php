<?php

namespace App\Entity;

class LawFirm
{
    public function __construct($data)
    {
        foreach ($data as $key => $value) {
            $this->{$key} = $value;
        }
    }

    public $id;
    public $name;
    public $countryId;
    public $email;
    public $phone;
    public $fax;
    public $address;
    public $website;
    public $createdAt;
    public $updatedAt;
}
