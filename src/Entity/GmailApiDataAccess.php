<?php

namespace App\Entity;

class GmailApiDataAccess
{
    public function __construct($data)
    {
        foreach ($data as $key => $value) {
            $this->{$key} = $value;
        }
    }

    public $id;
    public $email;
    public $accessToken;
    public $refreshToken;
    public $clientId;
    public $lastScanAt;
}
