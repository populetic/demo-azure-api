<?php

namespace App\Repository;

class MarketingRepository extends BaseRepository
{
    /*********************
     ** ADWORDS TRACKING **
     *********************/

    private $adwordsTrackingFieldsToGet = '
        SELECT
            id,
            email,
            fk_claim as claimId,
            gclid,
            utm_medium as utmMedium,
            utm_source as utmSource,
            utm_campaign as utmCampaign,
            ip,
            user_agent as userAgent,
            created_at as createdAt,
            updated_at as updatedAt
        FROM adwords_tracking
    ';

    public function getAdwordsTrackingsByPageAndItemsPerPage($page, $itemsPerPage): array
    {
        $offset = $this->getOffset($page, $itemsPerPage);

        $query = $this->db->prepare($this->adwordsTrackingFieldsToGet . ' LIMIT ' . $offset . ',' . $itemsPerPage);
        $query->execute();

        return $query->fetchAll();
    }

    public function getAdwordsTrackingById($id): array
    {
        $query = $this->db->prepare($this->adwordsTrackingFieldsToGet . ' WHERE id = :id');
        $query->execute([ 'id' => $id ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function createAdwordsTracking($data): int
    {
        $query = $this->db->prepare('
            INSERT INTO adwords_tracking (email, fk_claim, gclid, utm_medium, utm_source, utm_campaign, ip, user_agent) 
            VALUES (:email, :claimId, :gclid, :utmMedium, :utmSource, :utmCampaign, :ip, :userAgent)
        ');
        $query->execute([
            'email'       => $data['email'],
            'claimId'     => $data['claimId'],
            'gclid'       => $data['gclid'],
            'utmMedium'   => $data['utmMedium'],
            'utmSource'   => $data['utmSource'],
            'utmCampaign' => $data['utmCampaign'],
            'ip'          => $data['ip'],
            'userAgent'   => $data['userAgent']
        ]);

        return $this->db->lastInsertId();
    }

    public function editAdwordsTracking($data)
    {
        $query = $this->db->prepare('
            UPDATE adwords_tracking 
            SET
                email = :email,
                fk_claim = :claimId,
                gclid = :gclid,
                utm_medium = :utmMedium,
                utm_source = :utmSource,
                utm_campaign = :utmCampaign,
                ip = :ip,
                user_agent = :userAgent
            WHERE id = :id
        ');
        $query->execute([
            'id'          => $data['id'],
            'email'       => $data['email'],
            'claimId'     => $data['claimId'],
            'gclid'       => $data['gclid'],
            'utmMedium'   => $data['utmMedium'],
            'utmSource'   => $data['utmSource'],
            'utmCampaign' => $data['utmCampaign'],
            'ip'          => $data['ip'],
            'userAgent'   => $data['userAgent']
        ]);
    }

    public function deleteAdwordsTracking($id)
    {
        $query = $this->db->prepare('DELETE FROM adwords_tracking WHERE id = :id');
        $query->execute([ 'id' => $id ]);
    }



    /***************************
     * AIRLINES REVENUE IMPACT *
     ***************************/

    private $revenueImpactFieldsToGet = '
        SELECT
            ri.id,
            ri.fk_airline as airlineId,
            a.name as airlineName,
            ri.fk_country as countryId,
            c.name as countryName,
            ri.revenue_impact as revenueImpact,
            ri.created_at as createdAt
        FROM airlines_revenue_impact ri
        LEFT JOIN airlines a ON a.id = ri.fk_airline
        LEFT JOIN countries c ON c.id = ri.fk_country
    ';

    public function getRevenueImpactsByAirlineAndCountry($airlineId, $countryId, $onlyLast = false): array
    {
        $limit = '';
        if ($onlyLast) {
            $limit = ' LIMIT 1';
        }

        $query = $this->db->prepare(
            $this->revenueImpactFieldsToGet . ' 
            WHERE ri.fk_airline = :airlineId AND ri.fk_country = :countryId
            ORDER BY ri.id DESC' . $limit
        );
        $query->execute([ 'airlineId' => $airlineId, 'countryId' => $countryId ]);

        return $query->fetchAll();
    }

    public function getRevenueImpactById($id): array
    {
        $query = $this->db->prepare($this->revenueImpactFieldsToGet . ' WHERE ri.id = :id');
        $query->execute([ 'id' => $id ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function createRevenueImpact($data): int
    {
        $query = $this->db->prepare('
            INSERT INTO airlines_revenue_impact (fk_airline, fk_country, revenue_impact) 
            VALUES (:airlineId, :countryId, :revenueImpact)
        ');
        $query->execute([
            'airlineId'     => $data['airlineId'],
            'countryId'     => $data['countryId'],
            'revenueImpact' => $data['revenueImpact']
        ]);

        return $this->db->lastInsertId();
    }

    public function deleteRevenueImpact($id)
    {
        $query = $this->db->prepare('DELETE FROM airlines_revenue_impact WHERE id = :id');
        $query->execute([ 'id' => $id ]);
    }

    public function airlineRevenueImpactExists($airlineId, $countryId): bool
    {
        $query = $this->db->prepare('
            SELECT COUNT(id) as counter FROM airlines_revenue_impact
            WHERE fk_airline = :airlineId AND fk_country = :countryId
        ');
        $query->execute([ 'airlineId' => $airlineId, 'countryId' => $countryId ]);

        return (int)$query->fetch()['counter'] > 0;
    }
}