<?php

namespace App\Repository;

use App\Entity\Partner;

class PartnersRepository extends BaseRepository
{
    /*********************
     ****** PARTNERS ******
     *********************/

    private $fieldsToGet = '
        SELECT
            id,
            fiscal_name as fiscalName,
            business_name as businessName,
            fee,
            is_active as isActive,
            created_at as createdAt,
            updated_at as updatedAt
        FROM partners
    ';

    public function getAll(): array
    {
        $query = $this->db->prepare($this->fieldsToGet);
        $query->execute();

        return $query->fetchAll();
    }

    public function getById($id): array
    {
        $query = $this->db->prepare($this->fieldsToGet . ' WHERE id = :id;');
        $query->execute([ 'id' => $id ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function getByClaimId($claimId): array
    {
        $query = $this->db->prepare('
            SELECT
                p.id,
                p.fiscal_name as fiscalName,
                p.business_name as businessName,
                p.fee,
                p.is_active as isActive,
                p.created_at as createdAt,
                p.updated_at as updatedAt
            FROM partners p
            LEFT JOIN partners_claims pc ON pc.fk_partner = p.id
            WHERE pc.fk_claim = :claimId;
        ');
        $query->execute([ 'claimId' => $claimId ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function create(Partner $partner): int
    {
        $query = $this->db->prepare('
            INSERT INTO partners (fiscal_name, business_name, fee) VALUES (:fiscalName, :businessName, :fee);
        ');
        $query->execute([
            'fiscalName'    => $partner->fiscalName,
            'businessName'  => $partner->businessName,
            'fee'           => $partner->fee
        ]);

        return $this->db->lastInsertId();
    }

    public function edit(Partner $partner)
    {
        $query = $this->db->prepare('
            UPDATE partners
            SET
                fiscal_name     = :fiscalName,
                business_name   = :businessName,
                fee             = :fee,
                is_active       = :isActive
            WHERE id = :id
        ');
        $query->execute([
            'id'            => $partner->id,
            'fiscalName'    => $partner->fiscalName,
            'businessName'  => $partner->businessName,
            'fee'           => $partner->fee,
            'isActive'      => $partner->isActive
        ]);
    }

    public function delete($id)
    {
        $query = $this->db->prepare('DELETE FROM partners WHERE id = :id;');
        $query->execute([ 'id' => $id ]);
    }



    /*********************
     ** PARTNERS CLAIMS **
     *********************/

    private $partnersClaimsFieldsToGet = '
        SELECT
            fk_claim as claimId,
            fk_partner as partnerId,
            affiliate_id as affiliateId,
            is_valid_claim as isValidClaim
        FROM partners_claims
    ';

    public function getAllPartnersClaimsByPartnerId($partnerId): array
    {
        $query = $this->db->prepare($this->partnersClaimsFieldsToGet . ' WHERE fk_partner = :partnerId');
        $query->execute([ 'partnerId' => $partnerId ]);

        return $query->fetchAll();
    }

    public function getPartnerClaimByClaimAndPartner($claimId, $partnerId): array
    {
        $query = $this->db->prepare(
            $this->partnersClaimsFieldsToGet . '
            WHERE
                fk_claim = :claimId AND
                fk_partner = :partnerId;
        ');
        $query->execute([ 'claimId' => $claimId, 'partnerId' => $partnerId ]);

        return $query->fetchAll();
    }

    public function createPartnerClaim($partnerClaimData)
    {
        $query = $this->db->prepare('
            INSERT INTO partners_claims (fk_claim, fk_partner, affiliate_id, is_valid_claim)
            VALUES (:claimId, :partnerId, :affiliateId, :isValidClaim);
        ');
        $query->execute([
            'claimId'       => $partnerClaimData['claimId'],
            'partnerId'     => $partnerClaimData['partnerId'],
            'affiliateId'   => $partnerClaimData['affiliateId'],
            'isValidClaim'  => $partnerClaimData['isValidClaim']
        ]);
    }

    public function editPartnerClaim($partnerClaimData)
    {
        $query = $this->db->prepare('
            UPDATE partners_claims SET affiliate_id = :affiliateId, is_valid_claim = :isValidClaim
            WHERE fk_claim = :claimId AND fk_partner = :partnerId;
        ');
        $query->execute([
            'claimId'       => $partnerClaimData['claimId'],
            'partnerId'     => $partnerClaimData['partnerId'],
            'affiliateId'   => $partnerClaimData['affiliateId'],
            'isValidClaim'  => $partnerClaimData['isValidClaim']
        ]);
    }

    public function deletePartnerClaim($claimId, $partnerId)
    {
        $query = $this->db->prepare('DELETE FROM partners_claims WHERE fk_claim = :claimId AND fk_partner = :partnerId;');
        $query->execute([ 'claimId' => $claimId, 'partnerId' => $partnerId ]);
    }
}