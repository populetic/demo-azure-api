<?php

namespace App\Repository;

use App\Entity\Note;

class NotesRepository extends BaseRepository
{
    /*********************
     ******* NOTES *******
     *********************/

    public function getAllByClaimId($claimId): array
    {
        $claimsRepo    = new ClaimsRepository($this->db);
        $relatedClaims = $claimsRepo->getAllRelatedClaimIds($claimId);

        $query = $this->db->prepare('
            SELECT 
                n.id,
                n.fk_claim as claimId,
                n.content,
                n.fk_user as userId,
                n.fk_note_type as noteTypeId,
                nt.name as noteTypeName,
                n.fk_department_for as departmentForId,
                d.name as departmentForName,
                n.fk_social_network_type as socialNetworkTypeId,
                snt.name as socialNetworkTypeName,
                n.id_client_social_network as clientSocialNetworkId,
                n.created_at as createdAt
            FROM notes n
            LEFT JOIN note_types nt ON nt.id = n.fk_note_type
            LEFT JOIN statia.departments d ON d.id = n.fk_department_for
            LEFT JOIN social_network_types snt ON snt.id = n.fk_social_network_type
            WHERE n.fk_claim IN (' . implode(',', $relatedClaims) . ');
        ');
        $query->execute();

        return $query->fetchAll();
    }

    public function getById($id): array
    {
        $query = $this->db->prepare('
            SELECT
                id, 
                fk_claim as claimId,
                content,
                fk_user as userId,
                fk_note_type as noteTypeId,
                fk_department_for as departmentForId,
                fk_social_network_type as socialNetworkTypeId,
                id_client_social_network as clientSocialNetworkId,
                created_at as createdAt
            FROM notes
            WHERE id = :id;
        ');
        $query->execute([ 'id' => $id ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function create(Note $note): int
    {
        $query = $this->db->prepare('
            INSERT INTO notes
            (fk_claim, content, fk_user, fk_note_type, fk_department_for, fk_social_network_type, id_client_social_network)
            VALUES 
            (:claimId, :content, :userId, :noteTypeId, :departmentForId, :socialNetworkTypeId, :clientSocialNetworkId);
        ');
        $query->execute([
            'claimId'               => $note->claimId,
            'content'               => $note->content,
            'userId'                => $note->userId,
            'noteTypeId'            => $note->noteTypeId,
            'departmentForId'       => $note->departmentForId,
            'socialNetworkTypeId'   => $note->socialNetworkTypeId,
            'clientSocialNetworkId' => $note->clientSocialNetworkId
        ]);

        return $this->db->lastInsertId();
    }

    public function edit(Note $note)
    {
        $query = $this->db->prepare('
            UPDATE notes SET 
                content = :content,
                fk_note_type = :noteTypeId,
                fk_department_for = :departmentForId,
                fk_social_network_type = :socialNetworkTypeId,
                id_client_social_network = :clientSocialNetworkId
            WHERE id = :id;
        ');
        $query->execute([
            'id'                    => $note->id,
            'content'               => $note->content,
            'noteTypeId'            => $note->noteTypeId,
            'departmentForId'       => $note->departmentForId,
            'socialNetworkTypeId'   => $note->socialNetworkTypeId,
            'clientSocialNetworkId' => $note->clientSocialNetworkId
        ]);
    }

    public function delete($id)
    {
        $query = $this->db->prepare('DELETE FROM notes WHERE id = :id;');
        $query->execute([ 'id' => $id ]);
    }



    /**********************
     ***** NOTE TYPES *****
     **********************/

    public function getAllNoteTypes(): array
    {
        $query = $this->db->prepare('SELECT id, name, is_active as isActive FROM note_types;');
        $query->execute();

        return $query->fetchAll();
    }

    public function getNoteTypeById($id): array
    {
        $query = $this->db->prepare('SELECT id, name, is_active as isActive FROM note_types WHERE id = :id;');
        $query->execute([ 'id' => $id ]);

        $noteType = $query->fetch();

        return $noteType ? $noteType : [];
    }

    public function createNoteType($noteType): int
    {
        $query = $this->db->prepare('INSERT INTO note_types (name) VALUES (:name);');
        $query->execute([ 'name' => $noteType['name'] ]);

        return $this->db->lastInsertId();
    }

    public function editNoteType($noteType)
    {
        $query = $this->db->prepare('UPDATE note_types SET name = :name, is_active = :isActive WHERE id = :id;');
        $query->execute([
            'id'        => $noteType['id'],
            'name'      => $noteType['name'],
            'isActive'  => array_key_exists('isActive', $noteType) ? $noteType['isActive'] : 1
        ]);
    }

    public function deleteNoteType($id)
    {
        $query = $this->db->prepare('DELETE FROM note_types WHERE id = :id;');
        $query->execute([ 'id' => $id ]);
    }



    /************************
     * SOCIAL NETWORK TYPES *
     ************************/

    public function getAllSocialNetworkTypes(): array
    {
        $query = $this->db->prepare('SELECT id, name, is_active as isActive FROM social_network_types;');
        $query->execute();

        return $query->fetchAll();
    }

    public function getSocialNetworkTypeById($id): array
    {
        $query = $this->db->prepare('SELECT id, name, is_active as isActive FROM social_network_types WHERE id = :id;');
        $query->execute([ 'id' => $id ]);

        $socialNetworkType = $query->fetch();

        return $socialNetworkType ? $socialNetworkType : [];
    }

    public function createSocialNetworkType($socialNetworkType): int
    {
        $query = $this->db->prepare('INSERT INTO social_network_types (name) VALUES (:name);');
        $query->execute([ 'name' => $socialNetworkType['name'] ]);

        return $this->db->lastInsertId();
    }

    public function editSocialNetworkType($socialNetworkType)
    {
        $query = $this->db->prepare('UPDATE social_network_types SET name = :name, is_active = :isActive WHERE id = :id;');
        $query->execute([
            'id'        => $socialNetworkType['id'],
            'name'      => $socialNetworkType['name'],
            'isActive'  => array_key_exists('isActive', $socialNetworkType) ? $socialNetworkType['isActive'] : 1
        ]);
    }

    public function deleteSocialNetworkType($id)
    {
        $query = $this->db->prepare('DELETE FROM social_network_types WHERE id = :id;');
        $query->execute([ 'id' => $id ]);
    }
}