<?php

namespace App\Repository;

use App\Entity\Airport;

class AirportsRepository extends BaseRepository
{
    private $fieldsToGet = '
        SELECT
            a.id,
            a.name,
            a.iata,
            CONCAT(a.name, " (", a.iata, ")") as nameAndIata,
            a.icao,
            a.fk_country as countryId,
            c.name as countryName,
            a.fk_city as cityId,
            ci.name as cityName,
            a.is_main as isMain,
            a.latitude,
            a.longitude,
            a.created_at as createdAt,
            a.updated_at as updatedAt
        FROM airports a
        LEFT JOIN countries c ON c.id = a.fk_country
        LEFT JOIN cities ci ON ci.id = a.fk_city 
    ';

    public function getAll(): array
    {
        $query = $this->db->prepare($this->fieldsToGet);
        $query->execute();

        return $query->fetchAll();
    }

    public function getById($id): array
    {
        $query = $this->db->prepare($this->fieldsToGet . ' WHERE a.id = :id');
        $query->execute([ 'id' => $id ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function getLatLongByAirportId($airportId): array
    {
        $query = $this->db->prepare('SELECT latitude, longitude FROM airports WHERE id = :airportId');
        $query->execute([ 'airportId' => $airportId ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function isAirportInUE($airportId): bool
    {
        $query = $this->db->prepare('
            SELECT c.is_ue as isUe
            FROM airports a
            LEFT JOIN countries c ON c.id = a.fk_country
            WHERE a.id = :airportId
        ');
        $query->execute([ 'airportId' => $airportId ]);

        $data = $query->fetch();

        return (array_key_exists('isUe', $data) && $data['isUe'] == 1) ? true : false;
    }

    public function create(Airport $airport): int
    {
        $query = $this->db->prepare('
            INSERT INTO airports (name, iata, icao, fk_country, fk_city, is_main, latitude, longitude)
            VALUES (:name, :iata, :icao, :countryId, :cityId, :isMain, :latitude, :longitude);
        ');
        $query->execute([
            'name'      => $airport->name,
            'iata'      => $airport->iata,
            'icao'      => $airport->icao,
            'countryId' => $airport->countryId,
            'cityId'    => $airport->cityId,
            'isMain'    => $airport->isMain,
            'latitude'  => $airport->latitude,
            'longitude' => $airport->longitude
        ]);

        return $this->db->lastInsertId();
    }

    public function edit(Airport $airport)
    {
        $query = $this->db->prepare('
            UPDATE airports
            SET
                name = :name,
                iata = :iata,
                icao = :icao,
                fk_country = :countryId,
                fk_city = :cityId,
                is_main = :isMain,
                latitude = :latitude,
                longitude = :longitude
            WHERE id = :id
        ');
        $query->execute([
            'id'        => $airport->id,
            'name'      => $airport->name,
            'iata'      => $airport->iata,
            'icao'      => $airport->icao,
            'countryId' => $airport->countryId,
            'cityId'    => $airport->cityId,
            'isMain'    => $airport->isMain,
            'latitude'  => $airport->latitude,
            'longitude' => $airport->longitude
        ]);
    }

    public function delete($id)
    {
        $query = $this->db->prepare('DELETE FROM airports WHERE id = :id;');
        $query->execute([ 'id' => $id ]);
    }
}