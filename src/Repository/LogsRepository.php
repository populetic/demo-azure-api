<?php

namespace App\Repository;

use App\Model\LogsModel;

class LogsRepository extends BaseRepository
{
    /****************
     ***** LOGS *****
     ****************/

    public function getAllByYear($year): array
    {
        $query = $this->db->prepare('
            SELECT id, fk_user as userId, action, changes, created_at as createdAt 
            FROM ' . LogsModel::LOGS_TABLE . '_' . $year . '
            ORDER BY id DESC
        ');
        $query->execute();

        return $query->fetchAll();
    }

    public function insert($userId, $action, $changes)
    {
        $query = $this->db->prepare('
            INSERT INTO logs_' . date('Y') . ' (fk_user, action, changes) VALUES (:userId, :action, :changes)
        ');
        $query->execute([
            'userId'  => $userId,
            'action'  => $action,
            'changes' => $changes
        ]);
    }

    public function createTableLogsForCurrentYear()
    {
        $year  = date('Y');
        $query = $this->db->prepare('
            CREATE TABLE logs_' . $year . ' (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `fk_user` int(11) DEFAULT NULL,
              `action` varchar(255) DEFAULT NULL,
              `changes` longtext,
              `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
              PRIMARY KEY (`id`),
              KEY `idx_logs` (`fk_user`,`created_at`),
              CONSTRAINT `fk_logs_' . $year . '_1` FOREIGN KEY (`fk_user`) REFERENCES `users` (`id`) ON UPDATE NO ACTION
            );
        ');
        $query->execute();
    }



    /*********************
     * LOGS CLAIM STATUS *
     *********************/

    private $claimStatusFieldsToGet = '
        SELECT 
            id,
            fk_claim as claimId,
            fk_claim_status as claimStatusId,
            fk_user as userId, 
            action,
            changes,
            created_at as createdAt
        FROM logs_claim_status
    ';

    public function getAllClaimStatus(): array
    {
        $query = $this->db->prepare($this->claimStatusFieldsToGet . ' ORDER BY id DESC');
        $query->execute();

        return $query->fetchAll();
    }

    public function getAllLogsClaimStatusByClaimId($claimId): array
    {
        $query = $this->db->prepare(
            $this->claimStatusFieldsToGet . ' 
            WHERE fk_claim = ' . $claimId . ' ORDER BY id DESC'
        );
        $query->execute();

        return $query->fetchAll();
    }

    public function insertClaimStatus($claimId, $claimStatusId, $userId, $action, $changes)
    {
        $query = $this->db->prepare('
            INSERT INTO logs_claim_status (fk_claim, fk_claim_status, fk_user, action, changes) 
            VALUES (:claimId, :claimStatusId, :userId, :action, :changes)
        ');
        $query->execute([
            'claimId'       => $claimId,
            'claimStatusId' => $claimStatusId,
            'userId'        => $userId,
            'action'        => $action,
            'changes'       => $changes
        ]);
    }

    public function insertClaimStatusByActualClaimStatus($claimId, $userId, $action, $changes)
    {
        $query = $this->db->prepare('
            INSERT INTO logs_claim_status (fk_claim, fk_claim_status, fk_user, action, changes) 
            VALUES (:claimId, (SELECT fk_claim_status from claims where id = :claimId), :userId, :action, :changes)
        ');
        $query->execute([
            'claimId' => $claimId,
            'userId'  => $userId,
            'action'  => $action,
            'changes' => $changes
        ]);
    }
}