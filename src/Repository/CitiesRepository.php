<?php

namespace App\Repository;

use App\Entity\City;

class CitiesRepository extends BaseRepository
{
    private $fieldsToGet = '
        SELECT
            c.id,
            c.fk_country as countryId,
            co.name as countryName,
            c.name,
            c.is_active as isActive
        FROM cities c
        LEFT JOIN countries co ON co.id = c.fk_country
    ';

    public function getAll(): array
    {
        $query = $this->db->prepare($this->fieldsToGet);
        $query->execute();

        return $query->fetchAll();
    }

    public function getById($id): array
    {
        $query = $this->db->prepare($this->fieldsToGet . ' WHERE c.id = :id');
        $query->execute([ 'id' => $id ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function create(City $city): int
    {
        $query = $this->db->prepare('
            INSERT INTO cities (fk_country, name) 
            VALUES (:countryId, :name);
        ');
        $query->execute([ 'countryId' => $city->countryId, 'name' => $city->name ]);

        return $this->db->lastInsertId();
    }

    public function edit(City $city)
    {
        $query = $this->db->prepare('
            UPDATE cities
            SET
                fk_country  = :countryId,
                name        = :name,
                is_active   = :isActive
            WHERE id = :id
        ');
        $query->execute([
            'id'        => $city->id,
            'countryId' => $city->countryId,
            'name'      => $city->name,
            'isActive'  => $city->isActive
        ]);
    }

    public function delete($id)
    {
        $query = $this->db->prepare('DELETE FROM cities WHERE id = :id;');
        $query->execute([ 'id' => $id ]);
    }
}