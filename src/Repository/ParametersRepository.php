<?php

namespace App\Repository;

class ParametersRepository extends BaseRepository
{
    /**
     * Repository para tablas auxiliares con campos id y name
     */

    public function getAll($table): array
    {
        $query = $this->db->prepare('SELECT * FROM ' . $table);
        $query->execute();

        return $query->fetchAll();
    }

    public function getById($table, $id): array
    {
        $query = $this->db->prepare('SELECT * FROM ' . $table . ' WHERE id = :id');
        $query->execute([ 'id' => $id ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function create($table, $data): int
    {
        $query = $this->db->prepare('INSERT INTO ' . $table . ' (name) VALUES (:name)');
        $query->execute([ 'name' => $data['name'] ]);

        return $this->db->lastInsertId();
    }

    public function edit($table, $data)
    {
        $query = $this->db->prepare('UPDATE ' . $table . ' SET name = :name WHERE id = :id');
        $query->execute([ 'id' => $data['id'], 'name' => $data['name'] ]);
    }

    public function delete($table, $id)
    {
        $query = $this->db->prepare('DELETE FROM ' . $table . ' WHERE id = :id');
        $query->execute([ 'id' => $id ]);
    }
}