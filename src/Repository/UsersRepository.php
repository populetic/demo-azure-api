<?php

namespace App\Repository;

use App\Entity\User;

class UsersRepository extends BaseRepository
{
    private $fieldsToGet = '
        SELECT
            u.id,
            u.username,
            u.email,
            u.name,
            u.surnames,
            CONCAT(u.name, " ", u.surnames) as completeName,
            u.fk_country as countryId,
            u.fk_language as languageId,
            u.fk_role as roleId,
            u.fk_law_firm as lawFirmId,
            IF(lfa.fk_law_firm > 0, 1, 0) as isLawFirmAdmin,
            u.is_system as isSystem,
            u.is_active as isActive,
            u.created_at as createdAt,
            u.updated_at as updatedAt
    ';

    private $fromAndJoinsData = '
        FROM users u
        LEFT JOIN law_firm_administrators lfa ON lfa.fk_user = u.id
    ';

    private $fieldsToGetExtraData = '
        SELECT
            u.id,
            u.username,
            u.email,
            u.name,
            u.surnames,
            CONCAT(u.name, " ", u.surnames) as completeName,
            u.fk_country as countryId,
            c.name as countryName,
            u.fk_language as languageId,
            l.name as languageName,
            l.code as languageCode,
            u.fk_role as roleId,
            r.name as roleName,
            r.is_admin as isAdmin,
            r.fk_main_menu as mainMenuId,
            u.fk_law_firm as lawFirmId,
            lf.name as lawFirmName,
            IF(lfa.fk_law_firm > 0, 1, 0) as isLawFirmAdmin,
            u.is_system as isSystem,
            u.is_active as isActive,
            u.created_at as createdAt,
            u.updated_at as updatedAt
    ';

    private $fromAndJoinsExtraData = '
        FROM users u
        LEFT JOIN countries c ON c.id = u.fk_country
        LEFT JOIN translation_system.languages l ON l.id = u.fk_language
        LEFT JOIN statia.roles r ON r.id = u.fk_role
        LEFT JOIN law_firms lf ON lf.id = u.fk_law_firm
        LEFT JOIN law_firm_administrators lfa ON lfa.fk_user = u.id
    ';

    public function getAll(): array
    {
        $query = $this->db->prepare($this->fieldsToGet . $this->fromAndJoinsData);
        $query->execute();

        return $query->fetchAll();
    }

    public function getAllByDepartment($departmentId): array
    {
        $query = $this->db->prepare(
            $this->fieldsToGet . $this->fromAndJoinsData . '
            LEFT JOIN statia.roles r ON r.id = u.fk_role
            WHERE r.fk_department = :departmentId
        ');
        $query->execute([ 'departmentId' => $departmentId ]);

        return $query->fetchAll();
    }

    public function getAllByLawFirm($lawFirmId): array
    {
        $query = $this->db->prepare(
            $this->fieldsToGet . $this->fromAndJoinsData . '
            LEFT JOIN law_firms lf ON lf.id = u.fk_law_firm
            WHERE lf.id = :lawFirmId
        ');
        $query->execute([ 'lawFirmId' => $lawFirmId ]);

        return $query->fetchAll();
    }

    public function getById($id, $getExtraData = false): array
    {
        if ($getExtraData) {
            $sql = $this->fieldsToGetExtraData . $this->fromAndJoinsExtraData . ' WHERE u.id = :id';
        } else {
            $sql = $this->fieldsToGet . $this->fromAndJoinsData . ' WHERE u.id = :id';
        }

        $query = $this->db->prepare($sql);
        $query->execute([ 'id' => $id ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function getByUsername($username, $getPassword): array
    {
        if ($getPassword) {
            $this->fieldsToGetExtraData .= ', u.password';
        }

        $query = $this->db->prepare(
            $this->fieldsToGetExtraData . ' ' . $this->fromAndJoinsExtraData .
            ' WHERE u.username = :username'
        );
        $query->execute([ 'username' => $username ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function create(User $user): int
    {
        $query = $this->db->prepare('
            INSERT INTO users (username, email, password, name, surnames, fk_country, fk_language, fk_role, fk_law_firm, is_system)
            VALUES (:username, :email, :password, :name, :surnames, :countryId, :languageId, :roleId, :lawFirmId, :isSystem);
        ');
        $query->execute([
            'username'      => $user->username,
            'email'         => $user->email,
            'password'      => $user->password,
            'name'          => $user->name,
            'surnames'      => $user->surnames,
            'countryId'     => $user->countryId,
            'languageId'    => $user->languageId,
            'roleId'        => $user->roleId,
            'lawFirmId'     => $user->lawFirmId,
            'isSystem'      => $user->isSystem
        ]);

        return $this->db->lastInsertId();
    }

    public function edit(User $user)
    {
        $query = $this->db->prepare('
            UPDATE users
            SET
                username    = :username,
                email       = :email,
                name        = :name,
                surnames    = :surnames,
                fk_country  = :countryId,
                fk_language = :languageId,
                fk_role     = :roleId,
                fk_law_firm = :lawFirmId,
                is_system   = :isSystem,
                is_active   = :isActive
            WHERE id = :id;
        ');
        $query->execute([
            'id'            => $user->id,
            'username'      => $user->username,
            'email'         => $user->email,
            'name'          => $user->name,
            'surnames'      => $user->surnames,
            'countryId'     => $user->countryId,
            'languageId'    => $user->languageId,
            'roleId'        => $user->roleId,
            'lawFirmId'     => $user->lawFirmId,
            'isSystem'      => $user->isSystem,
            'isActive'      => $user->isActive
        ]);

        if ($user->password) {
            $this->changePassword($user->username, $user->password);
        }
    }

    public function delete($id)
    {
        $query = $this->db->prepare('DELETE FROM users WHERE id = :id;');
        $query->execute([ 'id' => $id ]);
    }

    public function usernameExists($username, $userId = ''): bool
    {
        $query = $this->db->prepare('
            SELECT COUNT(id) as countUsernames FROM users WHERE username = :username AND id != :userId;
        ');
        $query->execute([
            'username'  => $username,
            'userId'    => $userId === null ? '' : $userId
        ]);

        return (int)$query->fetch()['countUsernames'] > 0;
    }

    public function changePassword($username, $newPassword)
    {
        $query = $this->db->prepare('UPDATE users SET password = :newPassword WHERE username = :username');
        $query->execute([ 'username' => $username, 'newPassword' => $newPassword ]);
    }
}