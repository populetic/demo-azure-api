<?php

namespace App\Repository;

use App\Entity\EmailCustom;
use App\Entity\EmailMandrill;
use App\Model\EmailsModel;

class EmailsRepository extends BaseRepository
{
    /*********************
     ****** EMAILS *******
     *********************/

    private $mandrillFieldsToGet = '
        em.id,
        em.fk_claim as claimId,
        em.status,
        em.fk_email_mandrill_template as emailMandrillTemplateId,
        em.opens,
        em.clicks,
        em.mandrill_id as mandrillId,
        em.created_at as createdAt
    ';
    private $customFieldsToGet = '
        em.id,
        em.fk_claim as claimId,
        em.email,
        em.status,
        em.subject,
        em.created_at as createdAt
    ';

    public function getAllByType($type): array
    {
        $fieldsToGet = ($type === EmailsModel::MANDRILL) ? $this->mandrillFieldsToGet : $this->customFieldsToGet;

        $query = $this->db->prepare('
            SELECT ' . $fieldsToGet . ' FROM emails_' . $type . ' em 
            ORDER BY em.id DESC
        ');
        $query->execute();

        return $query->fetchAll();
    }

    public function getAllByTypeAndClaimId($type, $claimId): array
    {
        $fieldsToGet = ($type === EmailsModel::MANDRILL) ? $this->mandrillFieldsToGet : $this->customFieldsToGet;

        $query = $this->db->prepare('
            SELECT ' . $fieldsToGet . ' FROM emails_' . $type . ' em 
            WHERE em.fk_claim = ' . $claimId . ' ORDER BY em.id DESC'
        );
        $query->execute();

        return $query->fetchAll();
    }

    public function getByIdAndType($id, $type): array
    {
        $fieldsToGet = ($type === EmailsModel::MANDRILL) ?
                            $this->mandrillFieldsToGet :
                            ($this->customFieldsToGet . ', em.body');

        $query = $this->db->prepare('
            SELECT ' . $fieldsToGet . ' FROM emails_'. $type . ' em
            WHERE id = :id ORDER BY em.id DESC
        ');
        $query->execute([ 'id' => $id ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function getEmailMandrillByMandrillId($mandrillId): array
    {
        $query = $this->db->prepare('
            SELECT ' . $this->mandrillFieldsToGet . ' FROM emails_mandrill em
            WHERE mandrill_id = :mandrillId ORDER BY em.id DESC
        ');
        $query->execute([ 'mandrillId' => $mandrillId ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function insertMandrill(EmailMandrill $email): int
    {
        $query = $this->db->prepare('
            INSERT INTO emails_mandrill (fk_claim, status, fk_email_mandrill_template, mandrill_id)
            VALUES (:claimId, :status, :emailMandrillTemplateId, :mandrillId)
        ');
        $query->execute([
            'claimId'                 => $email->claimId,
            'status'                  => $email->status,
            'emailMandrillTemplateId' => $email->emailMandrillTemplateId,
            'mandrillId'              => $email->mandrillId
        ]);

        $id = $this->db->lastInsertId();
        $this->insertEmailAttachments($id, $email->docsIds, EmailsModel::MANDRILL);

        return $id;
    }

    public function insertCustom(EmailCustom $email): int
    {
        $query = $this->db->prepare('
            INSERT INTO emails_custom (fk_claim, email, status, subject, body)
            VALUES (:claimId, :email, :status, :subject, :body)
        ');
        $query->execute([
            'claimId'   => $email->claimId,
            'email'     => $email->email,
            'status'    => $email->status,
            'subject'   => $email->subject,
            'body'      => $email->body
        ]);

        $id = $this->db->lastInsertId();
        $this->insertEmailAttachments($id, $email->docsIds, EmailsModel::CUSTOM);

        return $id;
    }

    private function insertEmailAttachments($emailId, $docsIds, $emailType)
    {
        foreach ($docsIds as $docId) {
            $this->addEmailAttachment($emailId, $emailType, $docId);
        }
    }

    /** Actualiza la información de dicho email (status, opens y clicks). */
    public function updateMandrillInfo(EmailMandrill $email)
    {
        $query = $this->db->prepare('
            UPDATE emails_mandrill
            SET status = :status, opens = :opens, clicks = :clicks
            WHERE mandrill_id = :mandrillId
        ');
        $query->execute([
            'status'        => $email->status,
            'opens'         => $email->opens,
            'clicks'        => $email->clicks,
            'mandrillId'    => $email->mandrillId
        ]);
    }



    /****************************
     * EMAIL MANDRILL TEMPLATES *
     ****************************/

    private $mandrillTemplatesFieldsToGet = '
        SELECT 
            id,
            template_name as templateName,
            subject,
            created_at as createdAt,
            updated_at as updatedAt
        FROM email_mandrill_templates
    ';

    public function getAllMandrillTemplates(): array
    {
        $query = $this->db->prepare($this->mandrillTemplatesFieldsToGet);
        $query->execute();

        return $query->fetchAll();
    }

    public function getMandrillTemplateById($id): array
    {
        $query = $this->db->prepare($this->mandrillTemplatesFieldsToGet . ' WHERE id = :id');
        $query->execute([ 'id' => $id ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function getMandrillTemplateByName($name): array
    {
        $query = $this->db->prepare($this->mandrillTemplatesFieldsToGet . ' WHERE template_name = :name');
        $query->execute([ 'name' => $name ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function createMandrillTemplate($data): int
    {
        $query = $this->db->prepare('
            INSERT INTO email_mandrill_templates (template_name, subject) VALUES (:templateName, :subject)
        ');
        $query->execute([ 'templateName' => $data['templateName'], 'subject' => $data['subject'] ]);

        return $this->db->lastInsertId();
    }

    public function editMandrillTemplate($data)
    {
        $query = $this->db->prepare('
            UPDATE email_mandrill_templates SET template_name = :templateName, subject = :subject WHERE id = :id
        ');
        $query->execute([
            'id'           => $data['id'],
            'templateName' => $data['templateName'],
            'subject'      => $data['subject']
        ]);
    }

    public function deleteMandrillTemplate($id)
    {
        $query = $this->db->prepare('DELETE FROM email_mandrill_templates WHERE id = :id');
        $query->execute([ 'id' => $id ]);
    }

    public function mandrillTemplateNameExists($name, $idToAvoid = null): bool
    {
        $query = $this->db->prepare(
            'SELECT COUNT(id) as counter FROM email_mandrill_templates WHERE template_name = :name AND id != :idToAvoid'
        );
        $query->execute([
            'name'      => $name,
            'idToAvoid' => $idToAvoid === null ? '' : $idToAvoid
        ]);

        return (int)$query->fetch()['counter'] > 0;
    }



    /*********************
     **** EMAIL TASKS ****
     *********************/

    private $emailTasksFieldsToGet = '
        SELECT 
            et.id,
            et.fk_email_mandrill_template as emailMandrillTemplateId,
            emt.template_name as emailMandrillTemplateName,
            emt.subject as emailMandrillSubject,
            et.date_field as dateField,
            et.hours_to_send as hoursToSend,
            et.is_Active as isActive,
            et.created_at as createdAt,
            et.updated_at as updatedAt
        FROM email_tasks et
        LEFT JOIN email_mandrill_templates emt ON emt.id = et.fk_email_mandrill_template
    ';

    public function getAllEmailTasks(): array
    {
        $query = $this->db->prepare($this->emailTasksFieldsToGet);
        $query->execute();

        return $query->fetchAll();
    }

    public function getEmailTaskById($id): array
    {
        $query = $this->db->prepare($this->emailTasksFieldsToGet . ' WHERE et.id = :id');
        $query->execute([ 'id' => $id ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function createEmailTask($data): int
    {
        $query = $this->db->prepare('
            INSERT INTO email_tasks (fk_email_mandrill_template, date_field, hours_to_send)
            VALUES (:emailMandrillTemplateId, :dateField, :hoursToSend)
        ');
        $query->execute([
            'emailMandrillTemplateId' => $data['emailMandrillTemplateId'],
            'dateField'               => $data['dateField'],
            'hoursToSend'             => $data['hoursToSend']
        ]);

        return $this->db->lastInsertId();
    }

    public function editEmailTask($data)
    {
        $query = $this->db->prepare('
            UPDATE email_tasks
            SET
                fk_email_mandrill_template = :emailMandrillTemplateId,
                date_field = :dateField,
                hours_to_send = :hoursToSend,
                is_Active = :isActive
            WHERE id = :id
        ');
        $query->execute([
            'id'                      => $data['id'],
            'emailMandrillTemplateId' => $data['emailMandrillTemplateId'],
            'dateField'               => $data['dateField'],
            'hoursToSend'             => $data['hoursToSend'],
            'isActive'                => $data['isActive']
        ]);
    }

    public function deleteEmailTask($id)
    {
        $query = $this->db->prepare('DELETE FROM email_tasks WHERE id = :id');
        $query->execute([ 'id' => $id ]);
    }



    /*********************
     * EMAIL ATTACHMENTS *
     *********************/

    public function addEmailAttachment($emailId, $emailType, $documentationId)
    {
        $query = $this->db->prepare('
            INSERT INTO email_attachments (fk_email, email_type, fk_documentation)
            VALUES (:emailId, :emailType, :documentationId)
        ');
        $query->execute([
            'emailId'         => $emailId,
            'emailType'       => $emailType,
            'documentationId' => $documentationId
        ]);
    }

    public function deleteEmailAttachment($data)
    {
        $query = $this->db->prepare('
            DELETE FROM email_attachments 
            WHERE
                fk_email = :emailId AND
                email_type = :emailType AND
                fk_documentation = :documentationId
        ');
        $query->execute([
            'emailId'         => $data['emailId'],
            'emailType'       => $data['emailType'],
            'documentationId' => $data['documentationId']
        ]);
    }

    public function emailAttachmentExists($data): bool
    {
        $query = $this->db->prepare('
            SELECT COUNT(fk_email) as counter FROM email_attachments 
            WHERE
                fk_email = :emailId AND
                email_type = :emailType AND
                fk_documentation = :documentationId
        ');
        $query->execute([
            'emailId'         => $data['emailId'],
            'emailType'       => $data['emailType'],
            'documentationId' => $data['documentationId']
        ]);

        return (int)$query->fetch()['counter'] > 0;
    }
}