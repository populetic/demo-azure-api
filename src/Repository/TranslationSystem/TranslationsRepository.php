<?php

namespace App\Repository\TranslationSystem;

use App\Repository\BaseRepository;

class TranslationsRepository extends BaseRepository
{
    public function hasAppTranslationSystem($app): bool
    {
        $query = $this->db->prepare('SELECT COUNT(id) as appCounter FROM apps WHERE name = :appName');
        $query->execute([ 'appName' => $app ]);

        return (int)$query->fetch()['appCounter'] > 0;
    }

    public function getLangCodeById($langId): string
    {
        $query = $this->db->prepare('SELECT code FROM languages WHERE id = :langId');
        $query->execute([ 'langId' => $langId ]);

        return $query->fetch()['code'];
    }

    public function getLangIdByCode($langCode): int
    {
        $query = $this->db->prepare('SELECT id FROM languages WHERE code = :langCode');
        $query->execute([ 'langCode' => $langCode ]);

        return $query->fetch()['id'];
    }

    public function getTranslationsByAppAndLangIdAndTagId($app, $langId, $tagId): array
    {
        $query = $this->db->prepare('
            SELECT
                l.code as lang,
                t.tag_id as tagId,
                t.subtag_order as subtagOrder,
                t.translation            
            FROM translations_' . $app . ' t
            LEFT JOIN languages l ON l.id = t.fk_language
            WHERE
                t.tag_id = :tagId AND
                t.fk_language = :langId;
        ');
        $query->execute([ 'tagId' => $tagId, 'langId' => $langId ]);

        return $query->fetchAll();
    }

    public function getDefaultTranslationByAppAndTag($app, $tagId): array
    {
        $query = $this->db->prepare('
            SELECT
                l.code as lang,
                t.tag_id as tagId,
                t.subtag_order as subtagOrder,
                t.translation            
            FROM translations_' . $app . ' t
            LEFT JOIN languages l ON l.id = t.fk_language
            WHERE 
            t.tag_id = :tagId AND
            t.fk_language = (
                SELECT fk_language FROM languages_by_app WHERE is_default = 1 AND fk_app = (
                    SELECT id FROM apps WHERE name = :app
                )
            );
        ');
        $query->execute(['app' => $app, 'tagId' => $tagId ]);

        return $query->fetchAll();
    }

    public function getAllActiveLanguagesByApp($app): array
    {

        $query = $this->db->prepare('
            SELECT
                l.id,
                l.code
            FROM languages l
            LEFT JOIN languages_by_app lba ON lba.fk_language = l.id
            LEFT JOIN apps a ON a.id = lba.fk_app
            WHERE
                a.name = :appName
        ');
        $query->execute([ 'appName' => $app ]);

        return $query->fetchAll();
    }

    public function getAllTranslationsForAppByLangId($app, $langId): array
    {
        $query = $this->db->prepare('
            SELECT
                tag_id as tagId,
                subtag_order as subtagOrder,
                translation
            FROM translations_' . $app . '
            WHERE
                fk_language = :langId AND
                translation is not NULL;
        ');
        $query->execute([ 'langId' => $langId ]);

        return $query->fetchAll();
    }
}