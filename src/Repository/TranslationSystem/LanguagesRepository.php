<?php

namespace App\Repository\TranslationSystem;

use App\Entity\Language;
use App\Repository\BaseRepository;

class LanguagesRepository extends BaseRepository
{
    /*********************
     ***** LANGUAGES *****
     *********************/

    private $fieldsToGet = 'SELECT id, code, name, claim_reference as claimReference FROM languages';

    public function getAll(): array
    {
        $query = $this->db->prepare($this->fieldsToGet);
        $query->execute();

        return $query->fetchAll();
    }

    public function getById($id): array
    {
        $query = $this->db->prepare($this->fieldsToGet . ' WHERE id = :id');
        $query->execute([ 'id' => $id ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function create(Language $language): int
    {
        $query = $this->db->prepare('INSERT INTO languages (code, name, claim_reference) VALUES (:code, :name, :claimReference)');
        $query->execute([ 'code' => $language->code, 'name' => $language->name, 'claimReference' => $language->claimReference]);

        return $this->db->lastInsertId();
    }

    public function edit(Language $language)
    {
        $query = $this->db->prepare('UPDATE languages SET code = :code, name = :name, claim_reference = :claimReference WHERE id = :id');
        $query->execute([
            'id'                => $language->id,
            'code'              => $language->code,
            'name'              => $language->name,
            'claimReference'    => $language->claimReference
        ]);
    }

    public function delete($id)
    {
        $query = $this->db->prepare('DELETE FROM languages WHERE id = :id');
        $query->execute([ 'id' => $id ]);
    }

    public function codeExists($code, $idToAvoid): bool
    {
        $query = $this->db->prepare('
            SELECT COUNT(id) as counter
            FROM languages
            WHERE
                code = :code AND
                id != :idToAvoid
        ');
        $query->execute([
            'code'      => $code,
            'idToAvoid' => empty($idToAvoid) ? '' : $idToAvoid
        ]);

        return (int)$query->fetch()['counter'] > 0;
    }

    public function claimReferenceExists($claimReference, $idToAvoid): bool
    {
        $query = $this->db->prepare('
            SELECT COUNT(id) as counter
            FROM languages
            WHERE
                claim_reference = :claimReference AND
                id != :idToAvoid
        ');
        $query->execute([
            'claimReference' => $claimReference,
            'idToAvoid'      => empty($idToAvoid) ? '' : $idToAvoid
        ]);

        return (int)$query->fetch()['counter'] > 0;
    }


    /*********************
     ** LANGUAGES BY APP **
     *********************/

    public function getAllLanguagesByApp($appId): array
    {
        $query = $this->db->prepare('
            SELECT
                lba.fk_app as appId,
                a.name as appName,
                lba.fk_language as languageId,
                l.code as languageCode,
                l.name as languageName,
                l.claim_reference as languageClaimReference,
                lba.is_default as isDefault
            FROM languages_by_app lba
            LEFT JOIN apps a ON a.id = lba.fk_app
            LEFT JOIN languages l ON l.id = lba.fk_language WHERE lba.fk_app = :appId
        ');
        $query->execute([ 'appId' => $appId ]);

        return $query->fetchAll();
    }

    public function editLanguagesByApp($appId, $languageId, $isDefault): int
    {
        $query = $this->db->prepare('
            INSERT INTO languages_by_app (fk_app, fk_language, is_default)
            VALUES (:appId, :languageId, :isDefault)
        ');
        $query->execute([
            'appId'      => $appId,
            'languageId' => $languageId,
            'isDefault'  => $isDefault
        ]);

        return $this->db->lastInsertId();
    }

    public function deleteLanguagesByApp($appId)
    {
        $query = $this->db->prepare('DELETE FROM languages_by_app WHERE fk_app = :appId');
        $query->execute([ 'appId' => $appId ]);
    }
}