<?php

namespace App\Repository;

use App\Entity\Client;
use App\Entity\LegalGuardian;
use App\Model\ConstantsModel;

class ClientsRepository extends BaseRepository
{
    /*********************
     ****** CLIENTS ******
     *********************/

    private $fieldsToGet = '
        SELECT
            c.id,
            c.name,
            c.surnames,
            CONCAT(c.name, " ", c.surnames) as completeName,
            c.email,
            c.phone,
            c.city,
            c.address,
            c.fk_country_province as countryProvinceId,
            cp.name as countryProvinceName,
            c.fk_country as countryId,
            co.name as countryName,
            c.fk_language as languageId,
            l.name as languageName,
            c.id_card as idCard,
            c.fk_documentation_type as documentationTypeId,
            dt.name as documentationTypeName,
            c.id_card_expires_at as idCardExpiresAt,
            c.fk_legal_guardian as legalGuardianId,
            lg.name as legalGuardianName,
            lg.surnames as legalGuardianSurnames,
            lg.id_card as legalGuardianIdCard,
            lg.fk_documentation_type as legalGuardianDocumentationTypeId,
            lg.id_card_expires_at as legalGuardianIdCardExpiresAt,
            c.gender,
            c.ip,
            c.user_agent as userAgent,
            c.created_at as createdAt,
            c.updated_at as updatedAt
        FROM clients c
        LEFT JOIN country_provinces cp ON cp.id = c.fk_country_province
        LEFT JOIN countries co ON co.id = c.fk_country
        LEFT JOIN translation_system.languages l ON l.id = c.fk_language
        LEFT JOIN documentation_types dt ON dt.id = c.fk_documentation_type
        LEFT JOIN legal_guardians lg ON lg.id = c.fk_legal_guardian
    ';

    public function getAll($filters): array
    {
        $sql = $this->fieldsToGet;

        if ($filters['finished'] !== null) {
            $claimsJoin = ' LEFT JOIN claims cla ON cla.fk_client = c.id ';
            $where      = $filters['finished'] ? ' WHERE cla.finished_at IS NOT NULL' : ' WHERE cla.finished_at IS NULL';

            $sql = $sql . $claimsJoin . $where;
        }

        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function getById($id): array
    {
        $query = $this->db->prepare($this->fieldsToGet . ' WHERE c.id = :id');
        $query->execute([ 'id' => $id ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function getByClaimId($claimId): array
    {
        $query = $this->db->prepare(
            $this->fieldsToGet . '
            LEFT JOIN claims cla ON cla.fk_client = c.id
            WHERE cla.id = :claimId
        ');
        $query->execute([ 'claimId' => $claimId ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function getRelatedClaimsById($id): array
    {
        $claims = [];

        $query = $this->db->prepare('SELECT name, surnames, email FROM clients WHERE id = :id');
        $query->execute([ 'id' => $id ]);

        $data = $query->fetch();

        if ($data) {
            $query = $this->db->prepare('
                SELECT
                    c.id as claimId,
                    c.fk_disruption_type as disruptionTypeId,
                    f.date as flightDate,
                    f.number as flightNumber,
                    IF(c.finished_at is null, 0, 1) as claimIsFinished,
                    IF(c.main_claim_id is null, 0, 1) as isCompanion,
                    IF((SELECT count(*) FROM claims WHERE id = c.main_claim_id) > 0, 1, 0) as hasCompanion,
                    IF(cs.closed, 0, 1) as isActive
                FROM claims c
                LEFT JOIN flights f ON f.id = c.fk_flight
                LEFT JOIN clients cli ON cli.id = c.fk_client
                LEFT JOIN claim_status cs ON cs.id = c.fk_claim_status
                WHERE
                    cli.name = :name AND
                    cli.surnames = :surnames AND
                    cli.email = :email
            ');
            $query->execute([
                'name'     => $data['name'],
                'surnames' => $data['surnames'],
                'email'    => $data['email']
            ]);

            $claims = $query->fetchAll();
        }

        return $claims;
    }

    public function getRelatedDemandsByClaimsIds($claimsIds): array
    {
        $demands = [];

        if ($claimsIds) {
            $query = $this->db->prepare('
                SELECT
                    d.id as demandId,
                    d.presented_at as presentedAt,
                    ct.id as courtTypeId,
                    ct.name as courtTypeName,
                    ct.fk_country as courtTypeCountryId,
                    cn.number as courtNumber,
                    c.fk_city as courtCityId,
                    ci.name as courtCityName,
                    d.amount_requested as amountRequested,
                    d.cancellated_at as cancellatedAt
                FROM demands d
                LEFT JOIN demand_related_claims drc ON drc.fk_demand = d.id
                LEFT JOIN courts_number cn ON cn.id = d.fk_court_number
                LEFT JOIN courts c ON c.id = cn.fk_court
                LEFT JOIN court_types ct ON ct.id = c.fk_court_type
                LEFT JOIN cities ci ON ci.id = c.fk_city
                WHERE drc.fk_claim IN (' . implode(',', $claimsIds) . ')
            ');
            $query->execute();

            $demands = $query->fetchAll();
        }

        return $demands;
    }

    public function getRelatedBillsByClaimsIds($claimsIds): array
    {
        $bills = [];

        if ($claimsIds) {
            $query = $this->db->prepare('
                SELECT
                    b.id,
                    bt.series,
                    b.number,
                    b.created_at as createdAt,
                    b.total
                FROM bills b
                LEFT JOIN claims c ON c.id = b.fk_claim
                LEFT JOIN bill_types bt ON bt.id = b.fk_bill_type
                WHERE b.fk_claim IN (' . implode(',', $claimsIds) . ')
            ');
            $query->execute();

            $bills = $query->fetchAll();
        }

        return $bills;
    }

    public function create(Client $client): int
    {
        $query = $this->db->prepare('
            INSERT INTO clients
                (name, surnames, email, phone, city, address, fk_country_province, fk_country, fk_language,
                 id_card, fk_documentation_type, id_card_expires_at, fk_legal_guardian, gender, ip, user_agent)
            VALUES
                (:name, :surnames, :email, :phone, :city, :address, :countryProvinceId, :countryId, :languageId,
                 :idCard, :documentationTypeId, :idCardExpiresAt, :legalGuardianId, :gender, :ip, :userAgent);
        ');
        $query->execute([
            'name'                => $client->name,
            'surnames'            => $client->surnames,
            'email'               => $client->email,
            'phone'               => $client->phone,
            'city'                => $client->city,
            'address'             => $client->address,
            'countryProvinceId'   => $client->countryProvinceId,
            'countryId'           => $client->countryId,
            'languageId'          => $client->languageId,
            'idCard'              => $client->idCard,
            'documentationTypeId' => $client->documentationTypeId,
            'idCardExpiresAt'     => $client->idCardExpiresAt,
            'legalGuardianId'     => $client->legalGuardianId,
            'gender'              => $client->gender,
            'ip'                  => $client->ip,
            'userAgent'           => $client->userAgent
        ]);

        return $this->db->lastInsertId();
    }

    public function edit(Client $client)
    {
        $query = $this->db->prepare('
            UPDATE clients
            SET
                name = :name,
                surnames = :surnames,
                email = :email,
                phone = :phone,
                city = :city,
                address = :address,
                fk_country_province = :countryProvinceId,
                fk_country = :countryId,
                fk_language = :languageId,
                id_card = :idCard,
                fk_documentation_type = :documentationTypeId,
                id_card_expires_at = :idCardExpiresAt,
                fk_legal_guardian = :legalGuardianId,
                gender = :gender,
                ip = :ip,
                user_agent = :userAgent
            WHERE id = :id
        ');
        $query->execute([
            'id'                  => $client->id,
            'name'                => $client->name,
            'surnames'            => $client->surnames,
            'email'               => $client->email,
            'phone'               => $client->phone,
            'city'                => $client->city,
            'address'             => $client->address,
            'countryProvinceId'   => $client->countryProvinceId,
            'countryId'           => $client->countryId,
            'languageId'          => $client->languageId,
            'idCard'              => $client->idCard,
            'documentationTypeId' => $client->documentationTypeId,
            'idCardExpiresAt'     => $client->idCardExpiresAt,
            'legalGuardianId'     => $client->legalGuardianId,
            'gender'              => $client->gender,
            'ip'                  => $client->ip,
            'userAgent'           => $client->userAgent
        ]);
    }

    public function delete($id)
    {
        $query = $this->db->prepare('DELETE FROM clients WHERE id = :id;');
        $query->execute([ 'id' => $id ]);
    }

    public function getAllVIPPaymentClients(): array
    {
        $query = $this->db->prepare('
            SELECT
                c.id as claimId,
                IFNULL(c.ref, "") ref,
                cli.id_card AS nif,
                dt.name AS idCardType,
                CONCAT(cli.name, " ", cli.surnames) AS name,
                IF(cli.fk_legal_guardian is NULL, 0, 1) AS isMinor,
                c.fk_client AS id,
                cli.email,
                lang.code AS languageCode,
                c.amount_received AS amountReceived,
                IF(c.main_claim_id is NULL, 1, 0)  AS isMainClaim,
                al.name AS airlineName
            FROM claims c
            LEFT JOIN flights f ON f.id = c.fk_flight
            LEFT JOIN airlines al ON al.id = f.fk_airline
            INNER JOIN clients cli ON cli.id = c.fk_client
            LEFT JOIN translation_system.languages lang ON lang.id = cli.fk_language
            LEFT JOIN documentation_types dt ON dt.id = cli.fk_documentation_type
            WHERE
                c.fk_claim_status = :claimStatusVipPayment
                OR (
                    cli.email IN (
                        SELECT cli2.email
                        FROM claims c2
                        LEFT JOIN clients cli2 ON cli2.id = c2.fk_client
                        WHERE c2.fk_claim_status = :claimStatusVipPayment
                    )
                    AND c.fk_claim_status = :claimStatusPopuleticCollected
                )
            ORDER BY amountReceived
        ');
        $query->execute([
            'claimStatusVipPayment'         => ConstantsModel::CLAIM_STATUS_VIP_PAYMENT,
            'claimStatusPopuleticCollected' => ConstantsModel::CLAIM_STATUS_POPULETIC_COLLECTED
        ]);

        return $query->fetchAll();
    }

    public function getPaymentClientsBetweenDates($start, $end): array
    {
        $query = $this->db->prepare('
            SELECT
                c.id as claimId,
                IFNULL(c.ref, "") ref,
                cli.id_card AS nif,
                dt.name AS idCardType,
                CONCAT(cli.name, " ", cli.surnames) AS name,
                IF(cli.fk_legal_guardian is NULL, 0, 1) AS isMinor,
                c.fk_client AS id,
                cli.email,
                lang.code AS languageCode,
                c.amount_received AS amountReceived,
                IF(c.main_claim_id is NULL, 1, 0)  AS isMainClaim,
                lcs.created_at as `date`,
                al.name AS airlineName,
                lcs.fk_claim_status as claimStatusId
            FROM claims c
            LEFT JOIN logs_claim_status lcs ON lcs.fk_claim = c.id
            LEFT JOIN flights f ON f.id = c.fk_flight
            LEFT JOIN airlines al ON al.id = f.fk_airline
            INNER JOIN clients cli ON cli.id = c.fk_client
            LEFT JOIN translation_system.languages lang ON lang.id = cli.fk_language
            LEFT JOIN documentation_types dt ON dt.id = cli.fk_documentation_type
            WHERE
                c.fk_claim_status = :popuCollected AND
                lcs.fk_claim_status = :popuCollected AND
                lcs.created_at BETWEEN :start AND :end
            GROUP BY lcs.fk_claim, lcs.fk_claim_status
            ORDER BY lcs.created_at, c.amount_received
        ');
        $query->execute([
            'popuCollected' => ConstantsModel::CLAIM_STATUS_POPULETIC_COLLECTED,
            'start'         => $start,
            'end'           => $end
        ]);

        return $query->fetchAll();
    }



    /*********************
     ** LEGAL GUARDIANS **
     *********************/

    private $legalGuardianFieldsToGet = '
        SELECT
            lg.id,
            lg.name,
            lg.surnames,
            lg.id_card as idCard,
            lg.fk_documentation_type as documentationTypeId,
            dt.name as documentationTypeName,
            lg.id_card_expires_at as idCardExpiresAt
        FROM legal_guardians lg
        LEFT JOIN documentation_types dt ON dt.id = lg.fk_documentation_type
    ';

    public function getAllLegalGuardians(): array
    {
        $query = $this->db->prepare($this->legalGuardianFieldsToGet);
        $query->execute();

        return $query->fetchAll();
    }

    public function getLegalGuardianById($id): array
    {
        $query = $this->db->prepare($this->legalGuardianFieldsToGet . ' WHERE lg.id = :id');
        $query->execute([ 'id' => $id ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function createLegalGuardian(LegalGuardian $legalGuardian): int
    {
        $query = $this->db->prepare('
            INSERT INTO legal_guardians (name, surnames, id_card, fk_documentation_type, id_card_expires_at)
            VALUES (:name, :surnames, :idCard, :documentationTypeId, :idCardExpiresAt);
        ');
        $query->execute([
            'name'                => $legalGuardian->name,
            'surnames'            => $legalGuardian->surnames,
            'idCard'              => $legalGuardian->idCard,
            'documentationTypeId' => $legalGuardian->documentationTypeId,
            'idCardExpiresAt'     => $legalGuardian->idCardExpiresAt
        ]);

        return $this->db->lastInsertId();
    }

    public function editLegalGuardian(LegalGuardian $legalGuardian)
    {
        $query = $this->db->prepare('
            UPDATE legal_guardians
            SET
                name = :name,
                surnames = :surnames,
                id_card = :idCard,
                fk_documentation_type = :documentationTypeId,
                id_card_expires_at = :idCardExpiresAt
            WHERE id = :id
        ');
        $query->execute([
            'id'                  => $legalGuardian->id,
            'name'                => $legalGuardian->name,
            'surnames'            => $legalGuardian->surnames,
            'idCard'              => $legalGuardian->idCard,
            'documentationTypeId' => $legalGuardian->documentationTypeId,
            'idCardExpiresAt'     => $legalGuardian->idCardExpiresAt
        ]);
    }

    public function deleteLegalGuardian($id)
    {
        $query = $this->db->prepare('DELETE FROM legal_guardians WHERE id = :id;');
        $query->execute([ 'id' => $id ]);
    }
}