<?php

namespace App\Repository;

use App\Entity\Country;

class CountriesRepository extends BaseRepository
{
    private $fieldsToGet = '
        SELECT
            c.id,
            c.iso,
            c.name,
            c.fk_region as regionId,
            r.name as regionName,
            c.is_ue as isUe,
            c.competence_priority as competencePriority,
            c.is_active as isActive
        FROM countries c
        LEFT JOIN regions r ON r.id = c.fk_region
    ';

    public function getAll(): array
    {
        $query = $this->db->prepare($this->fieldsToGet);
        $query->execute();

        return $query->fetchAll();
    }

    public function getById($id): array
    {
        $query = $this->db->prepare($this->fieldsToGet . ' WHERE c.id = :id');
        $query->execute([ 'id' => $id ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function create(Country $country): int
    {
        $query = $this->db->prepare('
            INSERT INTO countries (iso, name, fk_region, is_ue, competence_priority) 
            VALUES (:iso, :name, :regionId, :isUe, :competencePriority);
        ');
        $query->execute([
            'iso'                => $country->iso,
            'name'               => $country->name,
            'regionId'           => $country->regionId,
            'isUe'               => $country->isUe,
            'competencePriority' => $country->competencePriority
        ]);

        return $this->db->lastInsertId();
    }

    public function edit(Country $country)
    {
        $query = $this->db->prepare('
            UPDATE countries
            SET
                iso                 = :iso,
                name                = :name,
                fk_region           = :regionId,
                is_ue               = :isUe,
                competence_priority = :competencePriority,
                is_active           = :isActive
            WHERE id = :id
        ');
        $query->execute([
            'id'                 => $country->id,
            'iso'                => $country->iso,
            'name'               => $country->name,
            'regionId'           => $country->regionId,
            'isUe'               => $country->isUe,
            'competencePriority' => $country->competencePriority,
            'isActive'           => $country->isActive
        ]);
    }

    public function delete($id)
    {
        $query = $this->db->prepare('DELETE FROM countries WHERE id = :id;');
        $query->execute([ 'id' => $id ]);
    }

    public function isoAlreadyExists($iso, $idToAvoid = null): bool
    {
        $query = $this->db->prepare('SELECT COUNT(id) as counter FROM countries WHERE iso = :iso AND id != :idToAvoid');
        $query->execute([
            'iso'       => $iso,
            'idToAvoid' => $idToAvoid === null ? '' : $idToAvoid
        ]);

        return (int)$query->fetch()['counter'] > 0;
    }

    public function getCountriesWeHaveCompetence(): array
    {
        $query = $this->db->prepare('
            SELECT id FROM countries WHERE competence_priority > 0 AND is_active = 1 ORDER BY competence_priority
        ');
        $query->execute();

        return $query->fetchAll();
    }
}