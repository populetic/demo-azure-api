<?php

namespace App\Repository;

use Doctrine\DBAL\Connection;

class BaseRepository
{
    /** @var Connection */
    protected $db;

    public function __construct(Connection $db)
    {
        $this->db = $db;
    }

    public function idExistsInTable($tableName, $id): bool
    {
        $query = $this->db->prepare('SELECT COUNT(id) as counter FROM ' . $tableName . ' WHERE id = :id');
        $query->execute([ 'id' => $id ]);

        return (int)$query->fetch()['counter'] > 0;
    }

    public function nameExistsInTable($tableName, $name, $idToAvoid = null): bool
    {
        $query = $this->db->prepare('
            SELECT COUNT(id) as counter FROM ' . $tableName . ' WHERE name = :name AND id != :idToAvoid
        ');
        $query->execute([
            'name'      => $name,
            'idToAvoid' => $idToAvoid === null ? '' : $idToAvoid
        ]);

        return (int)$query->fetch()['counter'] > 0;
    }

    public function emailExistsInTable($tableName, $email, $idToAvoid = null): bool
    {
        $query = $this->db->prepare('
            SELECT COUNT(id) as counter FROM ' . $tableName . ' WHERE email = :email AND id != :idToAvoid
        ');
        $query->execute([
            'email'      => $email,
            'idToAvoid' => $idToAvoid === null ? '' : $idToAvoid
        ]);

        return (int)$query->fetch()['counter'] > 0;
    }

    public function idIsActiveInTable($tableName, $id): bool
    {
        $query = $this->db->prepare('
            SELECT COUNT(id) as counter FROM ' . $tableName . ' WHERE id = :id AND is_active = 1
        ');
        $query->execute([ 'id' => $id ]);

        return (int)$query->fetch()['counter'] > 0;
    }

    public function tableExists($tableName): bool
    {
        $query = $this->db->prepare('SHOW TABLES LIKE :tableName;');
        $query->execute([ 'tableName' => $tableName ]);

        return !empty($query->fetchAll());
    }

    public function getTotalCountByTable($tableName, $where = ''): int
    {
        $query = $this->db->prepare('SELECT COUNT(*) as counter FROM ' . $tableName . ' ' . $where);
        $query->execute();

        return (int)$query->fetch()['counter'];
    }

    public function getPagination($page, $itemsPerPage): string
    {
        $pagination = '';
        if ($page && $itemsPerPage) {
            $pagination = ' LIMIT ' . $this->getOffset($page, $itemsPerPage) . ',' . $itemsPerPage;
        }

        return $pagination;
    }

    public function getOffset($page, $itemsPerPage): int
    {
        return ($page - 1) * $itemsPerPage;
    }
}