<?php

namespace App\Repository;

use App\Model\ConstantsModel;

class CronsRepository extends BaseRepository
{
    public function getYesterdayStatusToClientCollectOrCancelWithCharge(): array
    {
        $query = $this->db->prepare('
            SELECT
                c.id as claimId,
                cli.name as clientName,
                cli.surnames as clientSurnames,
                cli.email
            FROM claims c 
            LEFT JOIN clients cli ON cli.id = c.fk_client
            LEFT JOIN logs_claim_status lcs ON lcs.fk_claim = c.id
            WHERE
                c.fk_claim_status IN (:customerCollected, :cancelWithCharge50e, :cancelWithCharge25perc) AND 
                lcs.fk_claim_status IN (:customerCollected, :cancelWithCharge50e, :cancelWithCharge25perc) AND
                DATE(lcs.created_at) = DATE(NOW() - INTERVAL 1 DAY)
                
        ');
        $query->execute([
            'customerCollected'      => ConstantsModel::CLAIM_STATUS_CUSTOMER_COLLECTED,
            'cancelWithCharge50e'    => ConstantsModel::CLAIM_STATUS_CANCELED_WITH_50_E,
            'cancelWithCharge25perc' => ConstantsModel::CLAIM_STATUS_CANCELED_WITH_25_PERC
        ]);

        return $query->fetchAll();
    }
}