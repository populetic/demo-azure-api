<?php

namespace App\Repository\Statia;

use App\Repository\BaseRepository;

class OptionsRepository extends BaseRepository
{
    private $fieldsToGet = '
        SELECT
            op.id,
            op.name,
            op.icon,
            op.on_header_menu as onHeaderMenu,
            op.on_table_menu as onTableMenu
        FROM options op
    ';

    /*********************
     ****** OPTIONS ******
     *********************/

    public function getAll(): array
    {
        $query = $this->db->prepare($this->fieldsToGet);
        $query->execute();

        return $query->fetchAll();
    }

    public function getAllMenus(): array
    {
        $query = $this->db->prepare('SELECT id FROM menus ORDER BY id');
        $query->execute();

        return $query->fetchAll();
    }

    public function getAllMenusByRole($roleId): array
    {
        $query = $this->db->prepare('SELECT fk_menu as id FROM menus_roles WHERE fk_role = :roleId ORDER BY fk_menu');
        $query->execute([ 'roleId' => $roleId ]);

        return $query->fetchAll();
    }

    public function getById($id): array
    {
        $query = $this->db->prepare($this->fieldsToGet . ' WHERE op.id = :id;');
        $query->execute([ 'id' => $id ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function getByMenuAndRole($menuId, $roleId): array
    {
        $query = $this->db->prepare(
            $this->fieldsToGet . '
            LEFT JOIN menus_roles_options mop ON mop.fk_option = op.id
            WHERE
                mop.fk_menu = :menu AND
                mop.fk_role = :role;
        ');
        $query->execute([ 'menu' => $menuId, 'role' => $roleId ]);

        return $query->fetchAll();
    }

    public function create($data): int
    {
        $query = $this->db->prepare('
            INSERT INTO options (name, icon, on_header_menu, on_table_menu)
            VALUES (:name, :icon, :onHeaderMenu, :onTableMenu);
        ');
        $query->execute([
            'name'          => $data['name'],
            'icon'          => $data['icon'],
            'onHeaderMenu'  => $data['onHeaderMenu'],
            'onTableMenu'   => $data['onTableMenu']
        ]);

        return $this->db->lastInsertId();
    }

    public function edit($data)
    {
        $query = $this->db->prepare('
            UPDATE options
            SET name = :name, icon = :icon, on_header_menu = :onHeaderMenu, on_table_menu = :onTableMenu
            WHERE id = :id;
        ');
        $query->execute([
            'id'            => $data['id'],
            'name'          => $data['name'],
            'icon'          => $data['icon'],
            'onHeaderMenu'  => $data['onHeaderMenu'],
            'onTableMenu'   => $data['onTableMenu']
        ]);
    }

    public function delete($id)
    {
        $query = $this->db->prepare('DELETE FROM options WHERE id = :id;');
        $query->execute([ 'id' => $id ]);
    }



    /*********************
     * MENU ROLE OPTIONS *
     *********************/

    public function createMenuRole($menuId, $roleId, $quickaccess)
    {
        $query = $this->db->prepare('
            INSERT INTO menus_roles (fk_menu, fk_role, quickaccess) VALUES (:menuId, :roleId, :quickaccess)
        ');
        $query->execute([
            'menuId'      => $menuId,
            'roleId'      => $roleId,
            'quickaccess' => $quickaccess
        ]);
    }

    public function createMenuRoleOption($menuId, $roleId, $optionId)
    {
        $query = $this->db->prepare('
            INSERT INTO menus_roles_options (fk_menu, fk_role, fk_option) VALUES (:menuId, :roleId, :optionId)
        ');
        $query->execute([
            'menuId'   => $menuId,
            'roleId'   => $roleId,
            'optionId' => $optionId
        ]);
    }

    public function deleteAllMenusRolesOptionsByRoleId($roleId)
    {
        $query = $this->db->prepare('DELETE FROM menus_roles_options WHERE fk_role = :roleId;');
        $query->execute([ 'roleId' => $roleId ]);
    }

    public function deleteAllMenusRolesByRoleId($roleId)
    {
        $query = $this->db->prepare('DELETE FROM menus_roles WHERE fk_role = :roleId;');
        $query->execute([ 'roleId' => $roleId ]);
    }
}