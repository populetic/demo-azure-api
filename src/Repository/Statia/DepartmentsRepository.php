<?php

namespace App\Repository\Statia;

use App\Repository\BaseRepository;

class DepartmentsRepository extends BaseRepository
{
    public function getAll(): array
    {
        $query = $this->db->prepare('SELECT id, name FROM departments;');
        $query->execute();

        return $query->fetchAll();
    }

    public function getById($id): array
    {
        $query = $this->db->prepare('SELECT id, name FROM departments WHERE id = :id;');
        $query->execute([ 'id' => $id ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function create($name): int
    {
        $query = $this->db->prepare('INSERT INTO departments (name) VALUES (:name);');
        $query->execute([ 'name' => $name ]);

        return $this->db->lastInsertId();
    }

    public function edit($data)
    {
        $query = $this->db->prepare('UPDATE departments SET name = :name WHERE id = :id;');
        $query->execute([ 'id' => $data['id'], 'name' => $data['name'] ]);
    }

    public function delete($id)
    {
        $query = $this->db->prepare('DELETE FROM departments WHERE id = :id;');
        $query->execute([ 'id' => $id ]);
    }
}