<?php

namespace App\Repository\Statia;

use App\Repository\BaseRepository;

class MenusRepository extends BaseRepository
{
    private $fieldsToGet = '
        SELECT
            m.id,
            m.name,
            m.parent_id as parentId,
            (SELECT name FROM menus WHERE id = m.parent_id) as parentName,
            m.position,
            m.icon,
            m.has_checkbox as hasCheckbox,
            m.has_table_menu as hasTableMenu,
            m.has_multiple_delete as hasMultipleDelete
        FROM menus m
    ';

    /*********************
     ******* MENUS *******
     *********************/

    public function getAll(): array
    {
        $query = $this->db->prepare($this->fieldsToGet);
        $query->execute();

        return $query->fetchAll();
    }

    public function getById($id): array
    {
        $query = $this->db->prepare($this->fieldsToGet . ' WHERE m.id = :id;');
        $query->execute([ 'id' => $id ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function getByRole($roleId): array
    {
        $query = $this->db->prepare(
            $this->fieldsToGet . '
            LEFT JOIN menus_roles mr ON mr.fk_menu = m.id
            WHERE mr.fk_role = :roleId;
        ');
        $query->execute([ 'roleId' => $roleId ]);

        return $query->fetchAll();
    }

    public function create($data): int
    {
        $query = $this->db->prepare('
            INSERT INTO menus (name, parent_id, position, icon, has_checkbox, has_table_menu, has_multiple_delete)
            VALUES (:name, :parentId, :position, :icon, :hasCheckbox, :hasTableMenu, :hasMultipleDelete);
        ');
        $query->execute([
            'name'              => $data['name'],
            'parentId'          => $data['parentId'],
            'position'          => $data['position'],
            'icon'              => $data['icon'],
            'hasCheckbox'       => $data['hasCheckbox'],
            'hasTableMenu'      => $data['hasTableMenu'],
            'hasMultipleDelete' => $data['hasMultipleDelete']
        ]);

        return $this->db->lastInsertId();
    }

    public function edit($data)
    {
        $query = $this->db->prepare('
            UPDATE menus
            SET
                name            = :name,
                parent_id       = :parentId,
                position        = :position,
                icon            = :icon,
                has_checkbox    = :hasCheckbox,
                has_table_menu  = :hasTableMenu,
                has_multiple_delete = :hasMultipleDelete
            WHERE id = :id;
        ');
        $query->execute([
            'id'                => $data['id'],
            'name'              => $data['name'],
            'parentId'          => $data['parentId'],
            'position'          => $data['position'],
            'icon'              => $data['icon'],
            'hasCheckbox'       => $data['hasCheckbox'],
            'hasTableMenu'      => $data['hasTableMenu'],
            'hasMultipleDelete' => $data['hasMultipleDelete']
        ]);
    }

    public function delete($id)
    {
        $query = $this->db->prepare('DELETE FROM menus WHERE id = :id;');
        $query->execute([ 'id' => $id ]);
    }



    /*********************
     **** MENU OPTIONS ****
     *********************/

    public function getMenuOptionsByMenuId($id): array
    {
        $query = $this->db->prepare('
            SELECT
                o.id,
                o.name,
                o.icon,
                o.on_header_menu as onHeaderMenu,
                o.on_table_menu as onTableMenu
            FROM options o
            LEFT JOIN menus_options mo ON mo.fk_option = o.id
            WHERE mo.fk_menu = :id;
        ');
        $query->execute([ 'id' => $id ]);

        return $query->fetchAll();
    }

    public function createMenuOption($menuId, $optionId)
    {
        $query = $this->db->prepare('INSERT INTO menus_options (fk_menu, fk_option) VALUES (:menuId, :optionId);');
        $query->execute([ 'menuId' => $menuId, 'optionId' => $optionId ]);
    }

    public function deleteAllMenuOptions($menuId)
    {
        $query = $this->db->prepare('DELETE FROM menus_options WHERE fk_menu = :menuId;');
        $query->execute([ 'menuId' => $menuId ]);
    }



    /*********************
     ** MENU PROPERTIES **
     *********************/

    public function getMenuPropertiesByMenuId($id): array
    {
        $query = $this->db->prepare('
            SELECT
                   field,
                   sort,
                   filter,
                   is_advanced_filter as isAdvancedFilter,
                   is_filter_selected as isFilterSelected,
                   is_boolean as isBoolean,
                   is_number as isNumber,
                   is_date as isDate,
                   can_be_translated as canBeTranslated,
                   only_for_admin as onlyForAdmin,
                   is_simple_filter as isSimpleFilter
            FROM menus_properties
            WHERE fk_menu = :id
        ');
        $query->execute([ 'id' => $id ]);

        return $query->fetchAll();
    }

    public function createMenuProperty($newMenuProperty)
    {
        $query = $this->db->prepare('
            INSERT INTO menus_properties
            (fk_menu, field, sort, filter, is_advanced_filter, is_filter_selected, is_boolean,
            is_number, is_date, can_be_translated, only_for_admin, is_simple_filter)
            VALUES
            (:menuId, :field, :sort, :filter, :isAdvancedFilter, :isFilterSelected, :isBoolean,
            :isNumber, :isDate, :canBeTranslated, :onlyForAdmin, :isSimpleFilter);
        ');
        $query->execute([
            'menuId'           => $newMenuProperty['menuId'],
            'field'            => $newMenuProperty['field'],
            'sort'             => $newMenuProperty['sort'],
            'filter'           => $newMenuProperty['filter'],
            'isAdvancedFilter' => $newMenuProperty['isAdvancedFilter'],
            'isFilterSelected' => $newMenuProperty['isFilterSelected'],
            'isBoolean'        => $newMenuProperty['isBoolean'],
            'isNumber'         => $newMenuProperty['isNumber'],
            'isDate'           => $newMenuProperty['isDate'],
            'canBeTranslated'  => $newMenuProperty['canBeTranslated'],
            'onlyForAdmin'     => $newMenuProperty['onlyForAdmin'],
            'isSimpleFilter'   => $newMenuProperty['isSimpleFilter']
        ]);
    }

    public function editMenuProperty($menuProperty)
    {
        $query = $this->db->prepare('
            UPDATE menus_properties
            SET
                field  = :fieldNewName,
                sort   = :sort,
                filter = :filter,
                is_advanced_filter = :isAdvancedFilter,
                is_filter_selected = :isFilterSelected,
                is_boolean = :isBoolean,
                is_number = :isNumber,
                is_date = :isDate,
                can_be_translated = :canBeTranslated,
                only_for_admin = :onlyForAdmin,
                is_simple_filter = :isSimpleFilter
            WHERE
                fk_menu = :menuId AND
                field = :field;
        ');
        $query->execute([
            'menuId'           => $menuProperty['menuId'],
            'field'            => $menuProperty['field'],
            'fieldNewName'     => $menuProperty['fieldNewName'],
            'sort'             => $menuProperty['sort'],
            'filter'           => $menuProperty['filter'],
            'isAdvancedFilter' => $menuProperty['isAdvancedFilter'],
            'isFilterSelected' => $menuProperty['isFilterSelected'],
            'isBoolean'        => $menuProperty['isBoolean'],
            'isNumber'         => $menuProperty['isNumber'],
            'isDate'           => $menuProperty['isDate'],
            'canBeTranslated'  => $menuProperty['canBeTranslated'],
            'onlyForAdmin'     => $menuProperty['onlyForAdmin'],
            'isSimpleFilter'   => $menuProperty['isSimpleFilter']
        ]);
    }

    public function getMenuPropertyByFieldName($fieldName): array
    {
        $query = $this->db->prepare('
            SELECT
                   fk_menu as menuId,
                   field,
                   sort,
                   filter,
                   is_advanced_filter as isAdvancedFilter,
                   is_filter_selected as isFilterSelected,
                   is_boolean as isBoolean,
                   is_number as isNumber,
                   is_date as isDate,
                   can_be_translated as canBeTranslated,
                   only_for_admin as onlyForAdmin,
                   is_simple_filter as isSimpleFilter
            FROM menus_properties
            WHERE field = :fieldName;
        ');
        $query->execute([ 'fieldName' => $fieldName ]);

        $menuProperty = $query->fetch();

        return $menuProperty ? $menuProperty : [];
    }

    public function deleteMenuProperty($menuId, $field)
    {
        $query = $this->db->prepare('DELETE FROM menus_properties WHERE fk_menu = :menuId AND field = :field;');
        $query->execute([ 'menuId' => $menuId, 'field' => $field ]);
    }

    public function fieldExistsForMenuProperty($menuId, $field): bool
    {
        $query = $this->db->prepare(
            'SELECT COUNT(field) as counterFields FROM menus_properties WHERE fk_menu = :menuId AND field = :field;'
        );
        $query->execute([ 'menuId' => $menuId, 'field' => $field ]);

        return (int)$query->fetch()['counterFields'] > 0;
    }
}