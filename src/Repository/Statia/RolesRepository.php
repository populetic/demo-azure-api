<?php

namespace App\Repository\Statia;

use App\Repository\BaseRepository;

class RolesRepository extends BaseRepository
{
    public function getAll(): array
    {
        $query = $this->db->prepare('
            SELECT
                r.id,
                r.name,
                d.name as departmentName,
                m.name as mainMenuName,
                r.is_admin as isAdmin
            FROM roles r
            LEFT JOIN departments d on d.id = r.fk_department
            LEFT JOIN menus m on m.id = r.fk_main_menu;
        ');
        $query->execute();

        return $query->fetchAll();
    }

    public function getById($id): array
    {
        $query = $this->db->prepare('
            SELECT
                r.id,
                r.name,
                d.id as departmentId,
                d.name as departmentName,
                m.id as mainMenuId,
                m.name as mainMenuName,
                r.is_admin as isAdmin
            FROM roles r
            LEFT JOIN departments d on d.id = r.fk_department
            LEFT JOIN menus m on m.id = r.fk_main_menu
            WHERE r.id = :id;
        ');
        $query->execute([ 'id' => $id ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function create($data): int
    {
        $query = $this->db->prepare('
            INSERT INTO roles (name, fk_department, fk_main_menu, is_admin) VALUES (:name, :departmentId, :mainMenuId, :isAdmin);
        ');
        $query->execute([
            'name'          => $data['name'],
            'departmentId'  => $data['departmentId'],
            'mainMenuId'    => $data['mainMenuId'],
            'isAdmin'       => $data['isAdmin']
        ]);

        return $this->db->lastInsertId();
    }

    public function edit($data)
    {
        $query = $this->db->prepare('
            UPDATE roles 
            SET 
                name            = :name,
                fk_department   = :departmentId,
                fk_main_menu    = :mainMenuId,
                is_admin        = :isAdmin
            WHERE id = :id;
        ');
        $query->execute([
            'id'            => $data['id'],
            'name'          => $data['name'],
            'departmentId'  => $data['departmentId'],
            'mainMenuId'    => $data['mainMenuId'],
            'isAdmin'       => $data['isAdmin']
        ]);
    }

    public function delete($id)
    {
        $query = $this->db->prepare('DELETE FROM roles WHERE id = :id;');
        $query->execute([ 'id' => $id ]);
    }
}