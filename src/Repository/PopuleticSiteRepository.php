<?php

namespace App\Repository;

class PopuleticSiteRepository extends BaseRepository
{
    /*********************
     **** ACCESS LOGS ****
     *********************/

    private $accessLogsFieldsToGet = '
        SELECT
            al.id,
            al.ip,
            al.user_agent as userAgent,
            al.domain_url as domainUrl,
            al.last_page as lastPage,
            al.fk_language as languageId,
            lang.code as languageCode,            
            al.fk_disruption_type as disruptionTypeId,
            dr.name as disruptionTypeName,
            al.email,
            al.name,
            al.surnames,
            al.flight_number as flightNumber,
            al.flight_date as flightDate,
            al.is_disrupted as isDisrupted,
            al.amount_estimated as amountEstimated,
            al.fk_claim as claimId,
            al.is_remarketing as isRemarketing,
            al.finished_at as finishedAt,
            al.created_at as createdAt
        FROM access_logs al
        LEFT JOIN translation_system.languages lang ON lang.id = al.fk_language
        LEFT JOIN populetic.disruption_types dr ON dr.id = al.fk_disruption_type
    ';

    public function getAllAccessLogs(): array
    {
        $query = $this->db->prepare($this->accessLogsFieldsToGet);
        $query->execute();

        return $query->fetchAll();
    }

    public function getAccessLogsById($id): array
    {
        $query = $this->db->prepare($this->accessLogsFieldsToGet . ' WHERE al.id = :id');
        $query->execute([ 'id' => $id ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function createAccessLogs($data): int
    {
        $query = $this->db->prepare('
            INSERT INTO access_logs 
                (ip, user_agent, domain_url, last_page, fk_language, fk_disruption_type, email, name, surnames, 
                 flight_number, flight_date, is_disrupted, amount_estimated, fk_claim, is_remarketing, finished_at) 
            VALUES
                (:ip, :userAgent, :domainUrl, :lastPage, :languageId, :disruptionTypeId, :email, :name, :surnames, 
                 :flightNumber, :flightDate, :isDisrupted, :amountEstimated, :claimId, :isRemarketing, :finishedAt)
        ');
        $query->execute([
            'ip'               => $data['ip'],
            'userAgent'        => $data['userAgent'],
            'domainUrl'        => $data['domainUrl'],
            'lastPage'         => $data['lastPage'],
            'languageId'       => $data['languageId'],
            'disruptionTypeId' => $data['disruptionTypeId'],
            'email'            => $data['email'],
            'name'             => $data['name'],
            'surnames'         => $data['surnames'],
            'flightNumber'     => $data['flightNumber'],
            'flightDate'       => $data['flightDate'],
            'isDisrupted'      => $data['isDisrupted'],
            'amountEstimated'  => $data['amountEstimated'],
            'claimId'          => $data['claimId'],
            'isRemarketing'    => $data['isRemarketing'],
            'finishedAt'       => $data['finishedAt']
        ]);

        return $this->db->lastInsertId();
    }

    public function editAccessLogs($data)
    {
        $query = $this->db->prepare('
            UPDATE access_logs 
            SET 
                ip = :ip,
                user_agent = :userAgent,
                domain_url = :domainUrl,
                last_page = :lastPage,
                fk_language = :languageId,
                fk_disruption_type = :disruptionTypeId,
                email = :email,
                name = :name,
                surnames = :surnames,
                flight_number = :flightNumber,
                flight_date = :flightDate,
                is_disrupted = :isDisrupted,
                amount_estimated = :amountEstimated,
                fk_claim = :claimId,
                is_remarketing = :isRemarketing,
                finished_at = :finishedAt
            WHERE id = :id
        ');
        $query->execute([
            'id'               => $data['id'],
            'ip'               => $data['ip'],
            'userAgent'        => $data['userAgent'],
            'domainUrl'        => $data['domainUrl'],
            'lastPage'         => $data['lastPage'],
            'languageId'       => $data['languageId'],
            'disruptionTypeId' => $data['disruptionTypeId'],
            'email'            => $data['email'],
            'name'             => $data['name'],
            'surnames'         => $data['surnames'],
            'flightNumber'     => $data['flightNumber'],
            'flightDate'       => $data['flightDate'],
            'isDisrupted'      => $data['isDisrupted'],
            'amountEstimated'  => $data['amountEstimated'],
            'claimId'          => $data['claimId'],
            'isRemarketing'    => $data['isRemarketing'],
            'finishedAt'       => $data['finishedAt']
        ]);
    }

    public function deleteAccessLogs($id)
    {
        $query = $this->db->prepare('DELETE FROM access_logs WHERE id = :id');
        $query->execute([ 'id' => $id ]);
    }



    /*********************
     *** AIRLINES INFO ***
     *********************/

    public function getAllAirlinesInfoTotals(): array
    {
        $query = $this->db->prepare('
            SELECT
                fk_airline as airlineId,
                total_delays as totalDelays,
                total_cancellations as totalCancellations,
                total_overbookings as totalOverbookings,
                total_luggages as totalLuggages
            FROM airlines_info
        ');
        $query->execute();

        return $query->fetchAll();
    }

    public function getAirlineInfoBioByAirlineId($airlineId): array
    {
        $query = $this->db->prepare('SELECT bio FROM airlines_info WHERE fk_airline = :airlineId');
        $query->execute([ 'airlineId' => $airlineId ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }



    /*********************
     ** DYNAMIC LANDINGS **
     *********************/

    private $dynamicLandingsFieldsToGet = '
        SELECT
            id,
            language_code as languageCode,
            generic,
            param1,
            param2,
            element,
            content
        FROM dynamic_landings
    ';

    public function getAllDynamicLandings(): array
    {
        $query = $this->db->prepare($this->dynamicLandingsFieldsToGet);
        $query->execute();

        return $query->fetchAll();
    }

    public function getDynamicLandingsById($id): array
    {
        $query = $this->db->prepare($this->dynamicLandingsFieldsToGet . ' WHERE id = :id');
        $query->execute([ 'id' => $id ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }



    /*********************
     ** LANDING AIRLINES **
     *********************/

    private $landingAirlinesFieldsToGet = '
        SELECT
            id,
            page_id as pageId,
            language_code as languageCode,
            airline_name as airlineName,
            fk_airline as airlineId,
            type,
            canonical_airline as canonicalAirline,
            canonical_url as canonicalUrl,
            special_canonical_url as specialCanonicalUrl,
            has_no_index as hasNoIndex,
            score
        FROM landing_airlines
    ';

    public function getAllLandingAirlines(): array
    {
        $query = $this->db->prepare($this->landingAirlinesFieldsToGet);
        $query->execute();

        return $query->fetchAll();
    }

    public function getLandingAirlinesById($id): array
    {
        $query = $this->db->prepare($this->landingAirlinesFieldsToGet . ' WHERE id = :id');
        $query->execute([ 'id' => $id ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function getLandingAirlinesByAirlineId($airlineId): array
    {
        $query = $this->db->prepare($this->landingAirlinesFieldsToGet . ' WHERE fk_airline = :airlineId');
        $query->execute([ 'airlineId' => $airlineId ]);

        return $query->fetchAll();
    }



    /*******************
     ** URL LANGUAGES **
     *******************/

    private $urlLanguagesFieldsToGet = '
        SELECT
            id,
            page_id as pageId,
            language_code as languageCode,
            url,
            has_no_index as hasNoIndex
        FROM url_languages
    ';

    public function getAllUrlLanguages(): array
    {
        $query = $this->db->prepare($this->urlLanguagesFieldsToGet);
        $query->execute();

        return $query->fetchAll();
    }

    public function getUrlLanguagesById($id): array
    {
        $query = $this->db->prepare($this->urlLanguagesFieldsToGet . ' WHERE id = :id');
        $query->execute([ 'id' => $id ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }
}