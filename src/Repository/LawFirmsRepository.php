<?php

namespace App\Repository;

use App\Entity\LawFirm;

class LawFirmsRepository extends BaseRepository
{
    /*********************
     ***** LAW FIRMS *****
     *********************/

    private $fieldsToGet = '
        SELECT
            lf.id,
            lf.name,
            lf.fk_country as countryId,
            c.name as countryName,
            lf.email,
            lf.phone,
            lf.fax,
            lf.address,
            lf.website,
            lf.created_at as createdAt,
            lf.updated_at as updatedAt
        FROM law_firms lf
        LEFT JOIN countries c ON c.id = lf.fk_country
    ';

    public function getAll(): array
    {
        $query = $this->db->prepare($this->fieldsToGet);
        $query->execute();

        return $query->fetchAll();
    }

    public function getById($id): array
    {
        $query = $this->db->prepare($this->fieldsToGet . ' WHERE lf.id = :id');
        $query->execute([ 'id' => $id ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function create(LawFirm $lawFirm): int
    {
        $query = $this->db->prepare('
            INSERT INTO law_firms (name, fk_country, email, phone, fax, address, website)
            VALUES (:name, :countryId, :email, :phone, :fax, :address, :website)
        ');
        $query->execute([
            'name'      => $lawFirm->name,
            'countryId' => $lawFirm->countryId,
            'email'     => $lawFirm->email,
            'phone'     => $lawFirm->phone,
            'fax'       => $lawFirm->fax,
            'address'   => $lawFirm->address,
            'website'   => $lawFirm->website
        ]);

        return $this->db->lastInsertId();
    }

    public function edit(LawFirm $lawFirm)
    {
        $query = $this->db->prepare('
            UPDATE law_firms
            SET
                name = :name,
                fk_country = :countryId,
                email = :email,
                phone = :phone,
                fax = :fax,
                address = :address,
                website = :website
            WHERE id = :id
        ');
        $query->execute([
            'id'        => $lawFirm->id,
            'name'      => $lawFirm->name,
            'countryId' => $lawFirm->countryId,
            'email'     => $lawFirm->email,
            'phone'     => $lawFirm->phone,
            'fax'       => $lawFirm->fax,
            'address'   => $lawFirm->address,
            'website'   => $lawFirm->website
        ]);
    }

    public function delete($id)
    {
        $query = $this->db->prepare('DELETE FROM law_firms WHERE id = :id');
        $query->execute([ 'id' => $id ]);
    }



    /*********************************
     * DISRUPTION TYPES BY LAW FIRM *
     *********************************/

    public function getDisrTypesByLawFirm($lawFirmId, $disruptionTypeId = null): array
    {
        $sql = '
            SELECT
                dtblw.fk_law_firm as lawFirmId,
                lf.name as lawFirmName,
                dtblw.fk_disruption_type as disruptionTypeId,
                dt.name as disruptionTypeName,
                dtblw.name
            FROM disruption_types_by_law_firm dtblw
            LEFT JOIN law_firms lf ON lf.id = dtblw.fk_law_firm
            LEFT JOIN disruption_types dt ON dt.id = dtblw.fk_disruption_type
            WHERE dtblw.fk_law_firm = ' . $lawFirmId;

        if ($disruptionTypeId) {
            $sql .= ' AND dtblw.fk_disruption_type = ' . $disruptionTypeId;
        }

        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function createDisrTypeByLawFirm($data)
    {
        $query = $this->db->prepare('
            INSERT INTO disruption_types_by_law_firm (fk_law_firm, fk_disruption_type, name)
            VALUES (:lawFirmId, :disruptionTypeId, :name)
        ');
        $query->execute([
            'lawFirmId'        => $data['lawFirmId'],
            'disruptionTypeId' => $data['disruptionTypeId'],
            'name'             => $data['name']
        ]);
    }

    public function editDisrTypeByLawFirm($data)
    {
        $query = $this->db->prepare('
            UPDATE disruption_types_by_law_firm
            SET name = :name
            WHERE
                fk_law_firm = :lawFirmId AND
                fk_disruption_type = :disruptionTypeId
        ');
        $query->execute([
            'lawFirmId'        => $data['lawFirmId'],
            'disruptionTypeId' => $data['disruptionTypeId'],
            'name'             => $data['name']
        ]);
    }

    public function deleteDisrTypeByLawFirm($lawFirmId, $disruptionTypeId)
    {
        $query = $this->db->prepare('
            DELETE FROM disruption_types_by_law_firm
            WHERE
                fk_law_firm = :lawFirmId AND
                fk_disruption_type = :disruptionTypeId
        ');
        $query->execute([
            'lawFirmId'        => $lawFirmId,
            'disruptionTypeId' => $disruptionTypeId
        ]);
    }

    public function disrTypeByLawFirmExists($lawFirmId, $disruptionTypeId): bool
    {
        $query = $this->db->prepare('
            SELECT COUNT(*) as counter
            FROM disruption_types_by_law_firm
            WHERE
                fk_law_firm = :lawFirmId AND
                fk_disruption_type = :disruptionTypeId
        ');
        $query->execute([
            'lawFirmId'        => $lawFirmId,
            'disruptionTypeId' => $disruptionTypeId
        ]);

        return (int)$query->fetch()['counter'] > 0;
    }

    /*********************
     * LAW FIRM ADMINISTRATORS *
     *********************/

    private $lawFirmAdministratorsFieldsToGet = '
        SELECT
            lfa.fk_law_firm as lawFirmId,
            lfa.fk_user as userId,
            u.username,
            u.name,
            u.surnames
        FROM law_firm_administrators lfa
        LEFT JOIN users u ON u.id = lfa.fk_user
    ';

    private $whereExistsArray = [];

    public function getAllLawFirmAdministrators(): array
    {
        $query = $this->db->prepare($this->lawFirmAdministratorsFieldsToGet);
        $query->execute();

        return $query->fetchAll();
    }

    public function getAllLawFirmAdministratorById($lawFirmId): array
    {
        $query = $this->db->prepare($this->lawFirmAdministratorsFieldsToGet . ' WHERE lfa.fk_law_firm = :lawFirmId');
        $query->execute(['lawFirmId' => $lawFirmId]);

        return $query->fetchAll();
    }

    public function createLawFirmAdministrator($data)
    {
        $query = $this->db->prepare('
            INSERT INTO law_firm_administrators (fk_law_firm, fk_user)
            VALUES (:lawFirmId, :userId)
        ');

        $query->execute([
            'lawFirmId' => $data['lawFirmId'],
            'userId'    => $data['userId']
        ]);
    }

    public function deleteLawFirmAdministrator($lawFirmId, $userId)
    {
        $query = $this->db->prepare('
            DELETE FROM law_firm_administrators WHERE fk_law_firm = :lawFirmId AND fk_user = :userId
        ');
        $query->execute([
            'lawFirmId' => $lawFirmId,
            'userId'    => $userId
        ]);
    }

    public function lawFirmAdministratorExists($lawFirmId =  null, $userId = null)
    {
        $where = $this->getWhereExistByData($lawFirmId, $userId);

        $query = $this->db->prepare('
            SELECT COUNT(fk_user) as counter
            FROM law_firm_administrators
            ' .$where);
        $query->execute();

        return (int)$query->fetch()['counter'] > 0;
    }

    public function getWhereExistByData($lawFirmId, $userId)
    {
        if ($lawFirmId !== null) {
            $this->whereExistsArray[] = 'fk_law_firm = ' . $lawFirmId;
        }

        if ($userId !== null) {
            $this->whereExistsArray[] = 'fk_user = ' . $userId;
        }

        $where = implode(' AND ', $this->whereExistsArray);

        return $where ? ' WHERE ' . $where : '';
    }

    public function validaCorrespondenceLawFirmIdToUserId($lawFirmId, $userId)
    {
        $query = $this->db->prepare('
            SELECT COUNT(id) as counter
            FROM users
            WHERE
                id = :userId AND
                fk_law_firm = :lawFirmId');

        $query->execute([
            'lawFirmId' => $lawFirmId,
            'userId'    => $userId
        ]);

        return (int)$query->fetch()['counter'] > 0;
    }
}