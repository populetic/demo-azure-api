<?php

namespace App\Repository;

class ExtrajudicialRepository extends BaseRepository
{
    /***********************
     * ADDITIONAL EXPENSES *
     ***********************/

    private $addExpFieldsToGet = '
        SELECT
            ae.id,
            ae.fk_extrajudicial_type_sending as extrajudicialTypeSendingId,
            ts.name as extrajudicialTypeSendingName,
            ts.fk_country as extrajudicialTypeSendingCountryId,
            ae.expenses,
            ae.comments,
            ae.sent_at as sentAt
        FROM extrajudicial_additional_expenses ae
        LEFT JOIN extrajudicial_type_sending ts ON ts.id = ae.fk_extrajudicial_type_sending
    ';

    public function getAllAdditionalExp(): array
    {
        $query = $this->db->prepare($this->addExpFieldsToGet);
        $query->execute();

        return $query->fetchAll();
    }

    public function getAdditionalExpById($id): array
    {
        $query = $this->db->prepare($this->addExpFieldsToGet . ' WHERE ae.id = :id');
        $query->execute([ 'id' => $id ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function createAdditionalExp($data): int
    {
        $query = $this->db->prepare('
            INSERT INTO extrajudicial_additional_expenses (fk_extrajudicial_type_sending, expenses, comments, sent_at)
            VALUES (:extrajudicialTypeSendingId, :expenses, :comments, :sentAt)
        ');
        $query->execute([
            'extrajudicialTypeSendingId' => $data['extrajudicialTypeSendingId'],
            'expenses'                   => $data['expenses'],
            'comments'                   => $data['comments'],
            'sentAt'                     => $data['sentAt']
        ]);

        return $this->db->lastInsertId();
    }

    public function editAdditionalExp($data)
    {
        $query = $this->db->prepare('
            UPDATE extrajudicial_additional_expenses 
            SET
                fk_extrajudicial_type_sending = :extrajudicialTypeSendingId,
                expenses = :expenses,
                comments = :comments,
                sent_at = :sentAt
            WHERE id = :id
        ');
        $query->execute([
            'id'                         => $data['id'],
            'extrajudicialTypeSendingId' => $data['extrajudicialTypeSendingId'],
            'expenses'                   => $data['expenses'],
            'comments'                   => $data['comments'],
            'sentAt'                     => $data['sentAt']
        ]);
    }

    public function deleteAdditionalExp($id)
    {
        $query = $this->db->prepare('DELETE FROM extrajudicial_additional_expenses WHERE id = :id');
        $query->execute([ 'id' => $id ]);
    }



    /**********************
     **** TYPE SENDING ****
     **********************/

    private $typeSendingFieldsToGet = '
        SELECT
            ts.id,
            ts.name,
            ts.fk_country as countryId,
            c.name as countryName            
        FROM extrajudicial_type_sending ts
        LEFT JOIN countries c ON c.id = ts.fk_country
    ';

    public function getAllTypeSendingByCountry($countryId): array
    {
        $query = $this->db->prepare($this->typeSendingFieldsToGet . ' WHERE ts.fk_country = :countryId');
        $query->execute([ 'countryId' => $countryId ]);

        return $query->fetchAll();
    }

    public function getTypeSendingById($id): array
    {
        $query = $this->db->prepare($this->typeSendingFieldsToGet . ' WHERE ts.id = :id');
        $query->execute([ 'id' => $id ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function createTypeSending($data): int
    {
        $query = $this->db->prepare('
            INSERT INTO extrajudicial_type_sending (name, fk_country) 
            VALUES (:name, :countryId)
        ');
        $query->execute([ 'name' => $data['name'], 'countryId' => $data['countryId'] ]);

        return $this->db->lastInsertId();
    }

    public function editTypeSending($data)
    {
        $query = $this->db->prepare('
            UPDATE extrajudicial_type_sending 
            SET
                name = :name,
                fk_country = :countryId
            WHERE id = :id
        ');
        $query->execute([
            'id'        => $data['id'],
            'name'      => $data['name'],
            'countryId' => $data['countryId']
        ]);
    }

    public function deleteTypeSending($id)
    {
        $query = $this->db->prepare('DELETE FROM extrajudicial_type_sending WHERE id = :id');
        $query->execute([ 'id' => $id ]);
    }

    public function typeSendingNameExists($name, $idToAvoid = null): bool
    {
        $query = $this->db->prepare('
            SELECT COUNT(id) as counter FROM extrajudicial_type_sending WHERE name = :name AND id != :idToAvoid
        ');
        $query->execute([
            'name'      => $name,
            'idToAvoid' => $idToAvoid === null ? '' : $idToAvoid
        ]);

        return (int)$query->fetch()['counter'] > 0;
    }



    /************************
     **** RELATED CLAIMS ****
     ************************/

    public function getAdditionalExpensesByRelatedClaim($claimId): array
    {
        $query = $this->db->prepare(
            $this->addExpFieldsToGet . '
            INNER JOIN extrajudicial_related_claims rc ON rc.fk_extrajudicial_additional_expenses = ae.id 
            WHERE rc.fk_claim = :claimId
        ');
        $query->execute([ 'claimId' => $claimId ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function createRelatedClaims($data)
    {
        $query = $this->db->prepare('
            INSERT INTO extrajudicial_related_claims (fk_extrajudicial_additional_expenses, fk_claim) 
            VALUES (:additionalExpensesId, :claimId)
        ');
        $query->execute([
            'additionalExpensesId' => $data['extrajudicialAdditionalExpensesId'],
            'claimId'              => $data['claimId']
        ]);
    }

    public function deleteRelatedClaims($claimId, $additionalExpensesId)
    {
        $query = $this->db->prepare('
            DELETE FROM extrajudicial_related_claims
            WHERE fk_claim = :claimId AND fk_extrajudicial_additional_expenses = :additionalExpensesId
        ');
        $query->execute([
            'claimId'              => $claimId,
            'additionalExpensesId' => $additionalExpensesId
        ]);
    }

    public function extrajudicialRelatedClaimExists($claimId, $additionalExpensesId): bool
    {
        $query = $this->db->prepare('
            SELECT COUNT(*) as counter 
            FROM extrajudicial_related_claims 
            WHERE fk_claim = :claimId AND fk_extrajudicial_additional_expenses = :additionalExpensesId
        ');
        $query->execute([
            'claimId'              => $claimId,
            'additionalExpensesId' => $additionalExpensesId
        ]);

        return (int)$query->fetch()['counter'] > 0;
    }
}