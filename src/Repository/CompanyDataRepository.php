<?php

namespace App\Repository;

use App\Entity\CompanyData;

class CompanyDataRepository extends BaseRepository
{
    private $fieldsToGet = '
        SELECT
            cd.fk_country as countryId,
            c.name as countryName,
            cd.fiscal_name as fiscalName,
            cd.business_name as businessName,
            cd.address,
            cd.phone,
            cd.business_email as businessEmail,
            cd.admin_email as adminEmail,
            cd.adwords_cookies_expire_days as adwordsCookiesExpireDays,
            cd.fee
        FROM company_data_by_country cd
        LEFT JOIN countries c ON c.id = cd.fk_country
    ';

    public function getAll(): array
    {
        $query = $this->db->prepare($this->fieldsToGet);
        $query->execute();

        return $query->fetchAll();
    }

    public function getByCountryId($countryId): array
    {
        $query = $this->db->prepare($this->fieldsToGet . ' WHERE cd.fk_country = :countryId');
        $query->execute([ 'countryId' => $countryId ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function create(CompanyData $companyData)
    {
        $query = $this->db->prepare('
            INSERT INTO company_data_by_country 
            (fk_country, fiscal_name, business_name, address, phone, business_email, admin_email, adwords_cookies_expire_days, fee) 
            VALUES 
            (:countryId, :fiscalName, :businessName, :address, :phone, :businessEmail, :adminEmail, :adwordsCookiesExpireDays, :fee)
        ');
        $query->execute([
            'countryId'                => $companyData->countryId,
            'fiscalName'               => $companyData->fiscalName,
            'businessName'             => $companyData->businessName,
            'address'                  => $companyData->address,
            'phone'                    => $companyData->phone,
            'businessEmail'            => $companyData->businessEmail,
            'adminEmail'               => $companyData->adminEmail,
            'adwordsCookiesExpireDays' => $companyData->adwordsCookiesExpireDays,
            'fee'                      => $companyData->fee
        ]);
    }

    public function edit(CompanyData $companyData)
    {
        $query = $this->db->prepare('
            UPDATE company_data_by_country
            SET
                fiscal_name = :fiscalName,
                business_name = :businessName,
                address = :address,
                phone = :phone,
                business_email = :businessEmail,
                admin_email = :adminEmail,
                adwords_cookies_expire_days = :adwordsCookiesExpireDays,
                fee = :fee
            WHERE fk_country = :countryId
        ');
        $query->execute([
            'countryId'                => $companyData->countryId,
            'fiscalName'               => $companyData->fiscalName,
            'businessName'             => $companyData->businessName,
            'address'                  => $companyData->address,
            'phone'                    => $companyData->phone,
            'businessEmail'            => $companyData->businessEmail,
            'adminEmail'               => $companyData->adminEmail,
            'adwordsCookiesExpireDays' => $companyData->adwordsCookiesExpireDays,
            'fee'                      => $companyData->fee
        ]);
    }

    public function delete($countryId)
    {
        $query = $this->db->prepare('DELETE FROM company_data_by_country WHERE fk_country = :countryId');
        $query->execute([ 'countryId' => $countryId ]);
    }

    public function companyDataExistsForCountryId($countryId): bool
    {
        $query = $this->db->prepare('
            SELECT COUNT(fk_country) as counter FROM company_data_by_country WHERE fk_country = :countryId
        ');
        $query->execute([ 'countryId' => $countryId ]);

        return (int)$query->fetch()['counter'] > 0;
    }
}