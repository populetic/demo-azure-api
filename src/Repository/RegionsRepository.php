<?php

namespace App\Repository;

use App\Entity\Region;

class RegionsRepository extends BaseRepository
{
    private $fieldsToGet = '
        SELECT
            id,
            name,
            code,
            shortname,
            metaregion,
            is_active as isActive
        FROM regions
    ';

    public function getAll(): array
    {
        $query = $this->db->prepare($this->fieldsToGet);
        $query->execute();

        return $query->fetchAll();
    }

    public function getById($id): array
    {
        $query = $this->db->prepare($this->fieldsToGet . ' WHERE id = :id');
        $query->execute([ 'id' => $id ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function create(Region $region): int
    {
        $query = $this->db->prepare('
            INSERT INTO regions (name, code, shortname, metaregion) VALUES (:name, :code, :shortname, :metaregion);
        ');
        $query->execute([
            'name'          => $region->name,
            'code'          => $region->code,
            'shortname'     => $region->shortname,
            'metaregion'    => $region->metaregion
        ]);

        return $this->db->lastInsertId();
    }

    public function edit(Region $region)
    {
        $query = $this->db->prepare('
            UPDATE regions
            SET
                name        = :name,
                code        = :code,
                shortname   = :shortname,
                metaregion  = :metaregion,
                is_active   = :isActive
            WHERE id = :id
        ');
        $query->execute([
            'id'            => $region->id,
            'name'          => $region->name,
            'code'          => $region->code,
            'shortname'     => $region->shortname,
            'metaregion'    => $region->metaregion,
            'isActive'      => $region->isActive
        ]);
    }

    public function delete($id)
    {
        $query = $this->db->prepare('DELETE FROM regions WHERE id = :id;');
        $query->execute([ 'id' => $id ]);
    }

    public function codeAlreadyExists($code, $idToAvoid = null): bool
    {
        $query = $this->db->prepare('SELECT COUNT(id) as counter FROM regions WHERE code = :code AND id != :idToAvoid');
        $query->execute([
            'code'      => $code,
            'idToAvoid' => $idToAvoid === null ? '' : $idToAvoid
        ]);

        return (int)$query->fetch()['counter'] > 0;
    }

    public function shortnameAlreadyExists($shortname, $idToAvoid = null): bool
    {
        $query = $this->db->prepare(
            'SELECT COUNT(id) as counter FROM regions WHERE shortname = :shortname AND id != :idToAvoid;'
        );
        $query->execute([
            'shortname' => $shortname,
            'idToAvoid' => $idToAvoid === null ? '' : $idToAvoid
        ]);

        return (int)$query->fetch()['counter'] > 0;
    }
}