<?php

namespace App\Repository;

use App\Entity\GmailApiDataAccess;

class GmailApiRepository extends BaseRepository
{
    /*************************
     * GMAIL API DATA ACCESS *
     *************************/

    private $fieldsToGet = '
        SELECT
            gada.id,
            gada.email,
            gada.access_token as accessToken,
            gada.refresh_token as refreshToken,
            gada.fk_client as clientId,
            c.name as clientName,
            c.surnames as clientSurnames,
            gada.last_scan_at as lastScanAt
        FROM gmail_api_data_access gada
        LEFT JOIN clients c ON c.id = gada.fk_client
    ';

    public function getAll(): array
    {
        $query = $this->db->prepare($this->fieldsToGet);
        $query->execute();

        return $query->fetchAll();
    }

    public function getById($id): array
    {
        $query = $this->db->prepare($this->fieldsToGet . ' WHERE gada.id = :id');
        $query->execute([ 'id' => $id ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function getByEmail($email): array
    {
        $query = $this->db->prepare($this->fieldsToGet . ' WHERE gada.email = :email');
        $query->execute([ 'email' => $email ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function create(GmailApiDataAccess $dataAccess): int
    {
        $query = $this->db->prepare('
            INSERT INTO gmail_api_data_access (email, access_token, refresh_token, fk_client, last_scan_at)
            VALUES (:email, :accessToken, :refreshToken, :clientId, :lastScanAt)
        ');
        $query->execute([
            'email'        => $dataAccess->email,
            'accessToken'  => $dataAccess->accessToken,
            'refreshToken' => $dataAccess->refreshToken,
            'clientId'     => $dataAccess->clientId,
            'lastScanAt'   => $dataAccess->lastScanAt
        ]);

        return $this->db->lastInsertId();
    }

    public function edit(GmailApiDataAccess $dataAccess)
    {
        $query = $this->db->prepare('
            UPDATE gmail_api_data_access
            SET
                email = :email,
                access_token = :accessToken,
                refresh_token = :refreshToken,
                fk_client = :clientId,
                last_scan_at = :lastScanAt
            WHERE id = :id
        ');
        $query->execute([
            'id'           => $dataAccess->id,
            'email'        => $dataAccess->email,
            'accessToken'  => $dataAccess->accessToken,
            'refreshToken' => $dataAccess->refreshToken,
            'clientId'     => $dataAccess->clientId,
            'lastScanAt'   => $dataAccess->lastScanAt
        ]);
    }

    public function delete($id)
    {
        $query = $this->db->prepare('DELETE FROM gmail_api_data_access WHERE id = :id');
        $query->execute([ 'id' => $id ]);
    }



    /*******************************
     * GMAIL API DISRUPTED FLIGHTS *
     *******************************/

    public function getGmailApiDisruptedFlightByIds($data): array
    {
        $query = $this->db->prepare('
            SELECT
                fk_gmail_api_data_access as dataAccessId,
                fk_flight as flightId,
                fk_claim as claimId
            FROM gmail_api_disrupted_flights
            WHERE
                fk_gmail_api_data_access = :dataAccessId AND
                fk_flight = :flightId
        ');
        $query->execute([
            'dataAccessId' => $data['dataAccessId'],
            'flightId'     => $data['flightId']
        ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function createGmailApiDisruptedFlight($data)
    {
        $query = $this->db->prepare('
            INSERT INTO gmail_api_disrupted_flights (fk_gmail_api_data_access, fk_flight, fk_claim)
            VALUES (:dataAccessId, :flightId, :claimId)
        ');
        $query->execute([
            'dataAccessId' => $data['dataAccessId'],
            'flightId'     => $data['flightId'],
            'claimId'      => $data['claimId']
        ]);
    }

    public function deleteGmailApiDisruptedFlight($data)
    {
        $query = $this->db->prepare('
            DELETE FROM gmail_api_disrupted_flights
            WHERE
                fk_gmail_api_data_access = :dataAccessId AND
                fk_flight = :flightId
        ');
        $query->execute([
            'dataAccessId' => $data['dataAccessId'],
            'flightId'     => $data['flightId']
        ]);
    }

    public function gmailApiDisruptedFlightExists($data): bool
    {
        $query = $this->db->prepare('
            SELECT COUNT(fk_gmail_api_data_access) as counter FROM gmail_api_disrupted_flights
            WHERE
                fk_gmail_api_data_access = :dataAccessId AND
                fk_flight = :flightId
        ');
        $query->execute([
            'dataAccessId' => $data['dataAccessId'],
            'flightId'     => $data['flightId']
        ]);

        return (int)$query->fetch()['counter'] > 0;
    }
}