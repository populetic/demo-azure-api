<?php

namespace App\Repository;

use App\Entity\Court;

class CourtsRepository extends BaseRepository
{
    /*********************
     ****** COURTS ******
     *********************/

    private $fieldsToGet = '
        SELECT
            c.id,
            c.fk_court_type as courtTypeId,
            ct.name as courtTypeName,
            c.address,
            c.postal_code as postalCode,
            c.fk_city as cityId,
            ci.name as cityName,
            c.fk_country as countryId,
            co.name as countryName,
            c.created_at as createdAt,
            c.updated_at as updatedAt
        FROM courts c
        LEFT JOIN court_types ct ON ct.id = c.fk_court_type
        LEFT JOIN cities ci ON ci.id = c.fk_city
        LEFT JOIN countries co ON co.id = c.fk_country
    ';

    public function getAll(): array
    {
        $query = $this->db->prepare($this->fieldsToGet);
        $query->execute();

        return $query->fetchAll();
    }

    public function getById($id): array
    {
        $query = $this->db->prepare($this->fieldsToGet . ' WHERE c.id = :id');
        $query->execute([ 'id' => $id ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function create(Court $court): int
    {
        $query = $this->db->prepare('
            INSERT INTO courts (fk_court_type, address, postal_code, fk_city, fk_country)
            VALUES (:courtTypeId, :address, :postalCode, :cityId, :countryId);
        ');
        $query->execute([
            'courtTypeId' => $court->courtTypeId,
            'address'     => $court->address,
            'postalCode'  => $court->postalCode,
            'cityId'      => $court->cityId,
            'countryId'   => $court->countryId
        ]);

        return $this->db->lastInsertId();
    }

    public function edit(Court $court)
    {
        $query = $this->db->prepare('
            UPDATE courts
            SET
                fk_court_type = :courtTypeId,
                address = :address,
                postal_code = :postalCode,
                fk_city = :cityId,
                fk_country = :countryId
            WHERE id = :id
        ');
        $query->execute([
            'id'          => $court->id,
            'courtTypeId' => $court->courtTypeId,
            'address'     => $court->address,
            'postalCode'  => $court->postalCode,
            'cityId'      => $court->cityId,
            'countryId'   => $court->countryId
        ]);
    }

    public function delete($id)
    {
        $query = $this->db->prepare('DELETE FROM courts WHERE id = :id;');
        $query->execute([ 'id' => $id ]);
    }

    public function courtTypeIdExistsForCountry($id, $countryId)
    {
        $query = $this->db->prepare('
            SELECT COUNT(id) as counter FROM court_types WHERE id = :id AND fk_country = :countryId
        ');
        $query->execute([ 'id' => $id, 'countryId' => $countryId ]);

        return (int)$query->fetch()['counter'] > 0;
    }

    public function cityIdExistsForCountry($id, $countryId)
    {
        $query = $this->db->prepare('
            SELECT COUNT(id) as counter FROM cities WHERE id = :id AND fk_country = :countryId and is_active = 1
        ');
        $query->execute([ 'id' => $id, 'countryId' => $countryId ]);

        return (int)$query->fetch()['counter'] > 0;
    }

    public function typeAndCityExists($courtTypeId, $cityId, $idToAvoid = null): bool
    {
        $query = $this->db->prepare('
            SELECT COUNT(id) as counter
            FROM courts
            WHERE
                fk_court_type = :courtTypeId AND
                fk_city = :cityId AND
                id != :idToAvoid;
        ');
        $query->execute([
            'courtTypeId' => $courtTypeId,
            'cityId'      => $cityId,
            'idToAvoid'   => $idToAvoid === null ? '' : $idToAvoid
        ]);

        return (int)$query->fetch()['counter'] > 0;
    }



    /*********************
     **** COURT TYPES ****
     *********************/

    private $courtTypesFieldsToGet = '
        SELECT
            ct.id,
            ct.name,
            ct.fk_country as countryId,
            co.name as countryName
        FROM court_types ct
        LEFT JOIN countries co ON co.id = ct.fk_country
    ';

    public function getAllCourtTypes(): array
    {
        $query = $this->db->prepare($this->courtTypesFieldsToGet);
        $query->execute();

        return $query->fetchAll();
    }

    public function getCourtTypeById($id): array
    {
        $query = $this->db->prepare($this->courtTypesFieldsToGet . ' WHERE ct.id = :id');
        $query->execute([ 'id' => $id ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function createCourtType($data): int
    {
        $query = $this->db->prepare('INSERT INTO court_types (name, fk_country) VALUES (:name, :countryId)');
        $query->execute([
            'name'      => $data['name'],
            'countryId' => $data['countryId']
        ]);

        return $this->db->lastInsertId();
    }

    public function editCourtType($data)
    {
        $query = $this->db->prepare('UPDATE court_types SET name = :name, fk_country = :countryId WHERE id = :id');
        $query->execute([
            'id'        => $data['id'],
            'name'      => $data['name'],
            'countryId' => $data['countryId']
        ]);
    }

    public function deleteCourtType($id)
    {
        $query = $this->db->prepare('DELETE FROM court_types WHERE id = :id');
        $query->execute([ 'id' => $id ]);
    }

    public function courtTypeNameExistsForCountry($name, $countryId, $idToAvoid = null): bool
    {
        $query = $this->db->prepare('
            SELECT COUNT(id) as counter
            FROM court_types
            WHERE
                name = :name AND
                fk_country = :countryId AND
                id != :idToAvoid;
        ');
        $query->execute([
            'name'      => $name,
            'countryId' => $countryId,
            'idToAvoid' => $idToAvoid === null ? '' : $idToAvoid
        ]);

        return (int)$query->fetch()['counter'] > 0;
    }



    /*********************
     *** COURTS NUMBER ***
     *********************/

    private $courtsNumberFieldsToGet = '
        SELECT
            cn.id,
            cn.fk_court as courtId,
            c.fk_court_type as courtTypeId,
            ct.name as courtTypeName,
            c.address,
            c.postal_code as postalCode,
            c.fk_city as cityId,
            ci.name as cityName,
            c.fk_country as countryId,
            co.name as countryName,
            cn.number,
            cn.fax,
            cn.phone,
            cn.created_at as createdAt,
            cn.updated_at as updatedAt
        FROM courts_number cn
        LEFT JOIN courts c ON c.id = cn.fk_court
        LEFT JOIN court_types ct ON ct.id = c.fk_court_type
        LEFT JOIN cities ci ON ci.id = c.fk_city
        LEFT JOIN countries co ON co.id = c.fk_country
    ';

    public function getAllCourtsNumber(): array
    {
        $query = $this->db->prepare($this->courtsNumberFieldsToGet);
        $query->execute();

        return $query->fetchAll();
    }

    public function getCourtsNumberByCourtId($courtId): array
    {
        $query = $this->db->prepare($this->courtsNumberFieldsToGet . ' WHERE cn.fk_court = :courtId');
        $query->execute([ 'courtId' => $courtId ]);

        return $query->fetchAll();
    }

    public function getCourtNumberById($id): array
    {
        $query = $this->db->prepare($this->courtsNumberFieldsToGet . ' WHERE cn.id = :id');
        $query->execute([ 'id' => $id ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function createCourtNumber($data): int
    {
        $query = $this->db->prepare('
            INSERT INTO courts_number (fk_court, number, fax, phone) VALUES (:courtId, :number, :fax, :phone)
        ');
        $query->execute([
            'courtId' => $data['courtId'],
            'number'  => $data['number'],
            'fax'     => $data['fax'],
            'phone'   => $data['phone']
        ]);

        return $this->db->lastInsertId();
    }

    public function editCourtNumber($data)
    {
        $query = $this->db->prepare('
            UPDATE courts_number
            SET
                fk_court = :courtId,
                number = :number,
                fax = :fax,
                phone = :phone
            WHERE id = :id
        ');
        $query->execute([
            'id'       => $data['id'],
            'courtId'  => $data['courtId'],
            'number'   => $data['number'],
            'fax'      => $data['fax'],
            'phone'    => $data['phone']
        ]);
    }

    public function deleteCourtNumber($id)
    {
        $query = $this->db->prepare('DELETE FROM courts_number WHERE id = :id');
        $query->execute([ 'id' => $id ]);
    }

    public function numberExistsForCourtId($courtId, $number, $idToAvoid = null): bool
    {
        $query = $this->db->prepare('
            SELECT COUNT(id) as counter
            FROM courts_number
            WHERE
                fk_court = :courtId AND
                number = :number AND
                id != :idToAvoid;
        ');
        $query->execute([
            'courtId'   => $courtId,
            'number'    => $number,
            'idToAvoid' => $idToAvoid === null ? '' : $idToAvoid
        ]);

        return (int)$query->fetch()['counter'] > 0;
    }
}