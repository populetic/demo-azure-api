<?php

namespace App\Repository;

use App\Entity\Documentation;

class DocumentationRepository extends BaseRepository
{
    /*********************
     *** DOCUMENTATION ***
     *********************/

    private $fieldsToGet = '
        SELECT
            d.id,
            d.fk_claim as claimId,
            d.fk_documentation_type as documentationTypeId,
            dt.name as documentationTypeName,
            d.is_valid as isValid,
            d.to_delete as toDelete,
            d.not_available as notAvailable,
            d.extension,
            d.created_at as createdAt,
            d.updated_at as updatedAt
        FROM documentation d
        LEFT JOIN documentation_types dt ON dt.id = d.fk_documentation_type
    ';

    public function getAll(): array
    {
        $query = $this->db->prepare($this->fieldsToGet);
        $query->execute();

        return $query->fetchAll();
    }

    public function getById($id): array
    {
        $query = $this->db->prepare($this->fieldsToGet . ' WHERE d.id = :id');
        $query->execute([ 'id' => $id ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function create(Documentation $documentation): int
    {
        $query = $this->db->prepare('
            INSERT INTO documentation (fk_claim, fk_documentation_type, is_valid, to_delete, not_available, extension) 
            VALUES (:claimId, :documentationTypeId, :isValid, :toDelete, :notAvailable, :extension)
        ');
        $query->execute([
            'claimId'             => $documentation->claimId,
            'documentationTypeId' => $documentation->documentationTypeId,
            'isValid'             => $documentation->isValid,
            'toDelete'            => $documentation->toDelete,
            'notAvailable'        => $documentation->notAvailable,
            'extension'           => $documentation->extension
        ]);

        return $this->db->lastInsertId();
    }

    public function edit(Documentation $documentation)
    {
        $query = $this->db->prepare('
            UPDATE documentation
            SET
                fk_claim = :claimId,
                fk_documentation_type = :documentationTypeId,
                is_valid = :isValid,
                to_delete = :toDelete,
                not_available = :notAvailable,
                extension = :extension
            WHERE id = :id
        ');
        $query->execute([
            'id'                  => $documentation->id,
            'claimId'             => $documentation->claimId,
            'documentationTypeId' => $documentation->documentationTypeId,
            'isValid'             => $documentation->isValid,
            'toDelete'            => $documentation->toDelete,
            'notAvailable'        => $documentation->notAvailable,
            'extension'           => $documentation->extension
        ]);
    }

    public function delete($id)
    {
        $query = $this->db->prepare('DELETE FROM documentation WHERE id = :id');
        $query->execute([ 'id' => $id ]);
    }



    /***********************
     * DOCUMENTATION TYPES *
     ***********************/

    private $docTypesFieldsToGet = 'SELECT id, name FROM documentation_types';

    public function getAllDocumentationTypes(): array
    {
        $query = $this->db->prepare($this->docTypesFieldsToGet);
        $query->execute();

        return $query->fetchAll();
    }

    public function getDocumentationTypeById($id): array
    {
        $query = $this->db->prepare($this->docTypesFieldsToGet . ' WHERE id = :id');
        $query->execute([ 'id' => $id ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function createDocumentationType($data): int
    {
        $query = $this->db->prepare('INSERT INTO documentation_types (name) VALUES (:name)');
        $query->execute([ 'name' => $data['name'] ]);

        return $this->db->lastInsertId();
    }

    public function editDocumentationType($data)
    {
        $query = $this->db->prepare('UPDATE documentation_types SET name = :name WHERE id = :id');
        $query->execute([ 'id' => $data['id'], 'name' => $data['name'] ]);
    }

    public function deleteDocumentationType($id)
    {
        $query = $this->db->prepare('DELETE FROM documentation_types WHERE id = :id');
        $query->execute([ 'id' => $id ]);
    }



    /*********************
     ** DOC GOOGLE VISIO **
     *********************/

    public function getDocGoogleVisioByDocumentationId($docId): array
    {
        $query = $this->db->prepare('
            SELECT
                fk_documentation as documentationId,
                texts,
                matches,
                texts_accurance as textsAccurance,
                matches_accurance as matchesAccurance,
                created_at as createdAt
            FROM documentation_data_from_google_visio
            WHERE fk_documentation = :docId
        ');
        $query->execute([ 'docId' => $docId ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function createDocGoogleVisio($documentationId, $data)
    {
        $query = $this->db->prepare('
            INSERT INTO documentation_data_from_google_visio
                (fk_documentation, texts, matches, texts_accurance, matches_accurance)
            VALUES
                (:documentationId, :texts, :matches, :textsAccurance, :matchesAccurance)
        ');
        $query->execute([
            'documentationId'  => $documentationId,
            'texts'            => $data['texts'],
            'matches'          => $data['matches'],
            'textsAccurance'   => $data['textsAccurance'],
            'matchesAccurance' => $data['matchesAccurance']
        ]);
    }

    public function deleteDocGoogleVisioByDocumentationId($docId)
    {
        $query = $this->db->prepare('DELETE FROM documentation_data_from_google_visio WHERE fk_documentation = :docId');
        $query->execute([ 'docId' => $docId ]);
    }

    public function docIdExistsForGoogleVisio($docId)
    {
        $query = $this->db->prepare('
            SELECT COUNT(fk_documentation) as counter FROM documentation_data_from_google_visio 
            WHERE fk_documentation = :docId
        ');
        $query->execute([ 'docId' => $docId ]);

        return (int)$query->fetch()['counter'] > 0;
    }



    /***********************
     * DOCU PDF TO CONVERT *
     ***********************/

    public function getDocuPdfToConvertByClaimId($claimId): array
    {
        $query = $this->db->prepare('
            SELECT fk_claim as claimId, folder FROM documentation_pdf_to_convert WHERE fk_claim = :claimId
        ');
        $query->execute([ 'claimId' => $claimId ]);

        return $query->fetchAll();
    }

    public function createDocuPdfToConvert($data)
    {
        $query = $this->db->prepare('
            INSERT INTO documentation_pdf_to_convert (fk_claim, folder) VALUES (:claimId, :folder)
        ');
        $query->execute([ 'claimId' => $data['claimId'], 'folder' => $data['folder'] ]);
    }

    public function deleteDocuPdfToConvert($data)
    {
        $query = $this->db->prepare('
            DELETE FROM documentation_pdf_to_convert WHERE fk_claim = :claimId AND folder = :folder
        ');
        $query->execute([ 'claimId' => $data['claimId'], 'folder' => $data['folder'] ]);
    }

    public function docuPdfToConvertExists($data): bool
    {
        $query = $this->db->prepare('
            SELECT COUNT(fk_claim) as counter 
            FROM documentation_pdf_to_convert 
            WHERE fk_claim = :claimId AND folder = :folder
        ');
        $query->execute([ 'claimId' => $data['claimId'], 'folder' => $data['folder'] ]);

        return (int)$query->fetch()['counter'] > 0;
    }
}