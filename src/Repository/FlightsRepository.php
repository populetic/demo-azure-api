<?php

namespace App\Repository;

use App\Entity\Flight;

class FlightsRepository extends BaseRepository
{
    /*********************
     ****** FLIGHTS ******
     *********************/

    private $fieldsToGet = '
        f.id,
        f.number,
        f.date,
        f.status,
        f.fk_airline as airlineId,
        al.name as airlineName,
        al.iata as airlineIata,
        CONCAT(al.name, " (", al.iata, ")") as airlineNameAndIata,
        f.fk_airport_departure as airportDepartureId,
        apd.name as airportDepartureName,
        apd.iata as airportDepartureIata,
        CONCAT(apd.name, " (", apd.iata, ")") as airportDepartureNameAndIata,
        apd.fk_country as airportDepartureCountryId,
        f.departure_scheduled_time as departureScheduledTime,
        f.departure_actual_time as departureActualTime,
        f.fk_airport_arrival as airportArrivalId,
        apa.name as airportArrivalName,
        apa.iata as airportArrivalIata,
        CONCAT(apa.name, " (", apa.iata, ")") as airportArrivalNameAndIata,
        apa.fk_country as airportArrivalCountryId,
        f.arrival_scheduled_time as arrivalScheduledTime,
        f.arrival_actual_time as arrivalActualTime,
        f.delay,
        f.is_disrupted as isDisrupted,
        f.fk_flight_data_origin as flightDataOriginId,
        fdo.name as flightDataOriginName,
        f.distance,
        f.created_at as createdAt
    ';

    public function getFlightsByPageAndItemsPerPage($page, $itemsPerPage): array
    {
        $offset = $this->getOffset($page, $itemsPerPage);

        $query = $this->db->prepare('
            SELECT ' . $this->fieldsToGet . ' 
            FROM flights f
            LEFT JOIN airlines al ON al.id = f.fk_airline
            LEFT JOIN airports apd ON apd.id = fk_airport_departure
            LEFT JOIN airports apa ON apa.id = fk_airport_arrival
            LEFT JOIN flight_data_origins fdo ON fdo.id = fk_flight_data_origin
            LIMIT ' . $offset . ',' . $itemsPerPage
        );
        $query->execute();

        return $query->fetchAll();
    }

    public function getById($id): array
    {
        $query = $this->db->prepare('
            SELECT ' . $this->fieldsToGet . ' 
            FROM flights f
            LEFT JOIN airlines al ON al.id = f.fk_airline
            LEFT JOIN airports apd ON apd.id = fk_airport_departure
            LEFT JOIN airports apa ON apa.id = fk_airport_arrival
            LEFT JOIN flight_data_origins fdo ON fdo.id = fk_flight_data_origin
            WHERE f.id = :id;
        ');
        $query->execute([ 'id' => $id ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function getFlightDataByClaimId($claimId): array
    {
        $query = $this->db->prepare('
            SELECT
                c.fk_flight as flightId,
                c.fk_disruption_type as disruptionTypeId,
                f.number as flightNumber,
                f.date as flightDate,
                f.fk_airline as airlineId,
                f.fk_airport_departure as airportDepartureId,
                f.fk_airport_arrival as airportArrivalId
            FROM claims c 
            LEFT JOIN flights f ON f.id = c.fk_flight
            WHERE c.id = :claimId
        ');
        $query->execute([ 'claimId' => $claimId ]);

        return $query->fetch();
    }

    public function getSameFlightsIdsByFlightData($flightData): array
    {
        $query = $this->db->prepare('
            SELECT id as flightId
            FROM flights
            WHERE
                number = :flightNumber AND
                date = :flightDate AND
                fk_airline = :airlineId AND
                fk_airport_departure = :airportDepartureId AND 
                fk_airport_arrival = :airportArrivalId
                
        ');
        $query->execute([
            'flightNumber'       => $flightData['flightNumber'],
            'flightDate'         => $flightData['flightDate'],
            'airlineId'          => $flightData['airlineId'],
            'airportDepartureId' => $flightData['airportDepartureId'],
            'airportArrivalId'   => $flightData['airportArrivalId']
        ]);

        return array_column($query->fetchAll(), 'flightId');
    }

    public function create(Flight $flight): int
    {
        $query = $this->db->prepare('
            INSERT INTO flights
            (number, date, status, fk_airline, fk_airport_departure, departure_scheduled_time,
            departure_actual_time, fk_airport_arrival, arrival_scheduled_time, arrival_actual_time, delay, 
            is_disrupted, fk_flight_data_origin, distance)
            VALUES
            (:number, :date, :status, :airlineId, :airportDepartureId, :departureScheduledTime, 
            :departureActualTime, :airportArrivalId, :arrivalScheduledTime, :arrivalActualTime, :delay, 
            :isDisrupted, :flightDataOriginId, :distance);
        ');
        $query->execute([
            'number'                 => $flight->number,
            'date'                   => $flight->date,
            'status'                 => $flight->status,
            'airlineId'              => $flight->airlineId,
            'airportDepartureId'     => $flight->airportDepartureId,
            'departureScheduledTime' => $flight->departureScheduledTime,
            'departureActualTime'    => $flight->departureActualTime,
            'airportArrivalId'       => $flight->airportArrivalId,
            'arrivalScheduledTime'   => $flight->arrivalScheduledTime,
            'arrivalActualTime'      => $flight->arrivalActualTime,
            'delay'                  => $flight->delay,
            'isDisrupted'            => $flight->isDisrupted,
            'flightDataOriginId'     => $flight->flightDataOriginId,
            'distance'               => $flight->distance
        ]);

        return $this->db->lastInsertId();
    }

    public function editClaimWithNewFlightId($claimId, $newFlightId)
    {
        $query = $this->db->prepare('UPDATE claims SET fk_flight = :flightId WHERE id = :claimId');
        $query->execute([ 'claimId' => $claimId, 'flightId' => $newFlightId ]);
    }

    public function edit(Flight $flight)
    {
        $query = $this->db->prepare('
            UPDATE flights
            SET
                number = :number,
                status = :status,
                fk_airline = :airlineId,
                fk_airport_departure = :airportDepartureId,
                departure_scheduled_time = :departureScheduledTime,
                departure_actual_time = :departureActualTime,
                fk_airport_arrival = :airportArrivalId,
                arrival_scheduled_time = :arrivalScheduledTime,
                arrival_actual_time = :arrivalActualTime,
                delay = :delay,
                is_disrupted = :isDisrupted,
                distance = :distance
            WHERE id = :id
        ');
        $query->execute([
            'id'                     => $flight->id,
            'number'                 => $flight->number,
            'status'                 => $flight->status,
            'airlineId'              => $flight->airlineId,
            'airportDepartureId'     => $flight->airportDepartureId,
            'departureScheduledTime' => $flight->departureScheduledTime,
            'departureActualTime'    => $flight->departureActualTime,
            'airportArrivalId'       => $flight->airportArrivalId,
            'arrivalScheduledTime'   => $flight->arrivalScheduledTime,
            'arrivalActualTime'      => $flight->arrivalActualTime,
            'delay'                  => $flight->delay,
            'isDisrupted'            => $flight->isDisrupted,
            'distance'               => $flight->distance
        ]);
    }

    public function delete($id)
    {
        $query = $this->db->prepare('DELETE FROM flights WHERE id = :id');
        $query->execute([ 'id' => $id ]);
    }



    /************************************
     ******** FLIGHT DATA ORIGINS ********
     ************************************/

    public function getAllDataOrigins(): array
    {
        $query = $this->db->prepare('SELECT id, name FROM flight_data_origins;');
        $query->execute();

        return $query->fetchAll();
    }

    public function getDataOriginById($id): array
    {
        $query = $this->db->prepare('SELECT id, name FROM flight_data_origins WHERE id = :id;');
        $query->execute([ 'id' => $id ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function createDataOrigin($data): int
    {
        $query = $this->db->prepare('INSERT INTO flight_data_origins (name) VALUES (:name);');
        $query->execute([ 'name' => $data['name'] ]);

        return $this->db->lastInsertId();
    }

    public function editDataOrigin($data)
    {
        $query = $this->db->prepare('UPDATE flight_data_origins SET name = :name WHERE id = :id');
        $query->execute([ 'id' => $data['id'], 'name' => $data['name'] ]);
    }

    public function deleteDataOrigin($id)
    {
        $query = $this->db->prepare('DELETE FROM flight_data_origins WHERE id = :id');
        $query->execute([ 'id' => $id ]);
    }



    /************************************
     ******** CONNECTION FLIGHTS ********
     ************************************/

    private $connFlightsFieldsToGet = '
        SELECT
            cf.id,
            cf.fk_airport_departure as airportDepartureId,
            apd.name as airportDepartureName,
            cf.fk_airport_arrival as airportArrivalId,
            apa.name as airportArrivalName,
            cf.arrival_scheduled_time as arrivalScheduledTime,
            cf.arrival_actual_time as arrivalActualTime,
            cf.is_disrupted as isDisrupted,
            cf.distance,
            cf.created_at as createdAt
        FROM connection_flights cf
        LEFT JOIN airports apd ON apd.id = cf.fk_airport_departure
        LEFT JOIN airports apa ON apa.id = cf.fk_airport_arrival
    ';

    public function getAllConnectionFlights(): array
    {
        $query = $this->db->prepare($this->connFlightsFieldsToGet);
        $query->execute();

        return $query->fetchAll();
    }

    public function getConnectionFlightById($id): array
    {
        $query = $this->db->prepare($this->connFlightsFieldsToGet . ' WHERE cf.id = :id');
        $query->execute([ 'id' => $id ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function createConnectionFlight($data): int
    {
        $query = $this->db->prepare('
            INSERT INTO connection_flights 
            (fk_airport_departure, fk_airport_arrival, arrival_scheduled_time, arrival_actual_time, is_disrupted, distance) 
            VALUES 
            (:airportDepartureId, :airportArrivalId, :arrivalScheduledTime, :arrivalActualTime, :isDisrupted, :distance);
        ');
        $query->execute([
            'airportDepartureId'   => $data['airportDepartureId'],
            'airportArrivalId'     => $data['airportArrivalId'],
            'arrivalScheduledTime' => $data['arrivalScheduledTime'],
            'arrivalActualTime'    => $data['arrivalActualTime'],
            'isDisrupted'          => $data['isDisrupted'],
            'distance'             => $data['distance']
        ]);

        return $this->db->lastInsertId();
    }

    public function editConnectionFlight($data)
    {
        $query = $this->db->prepare('
            UPDATE connection_flights 
            SET
                fk_airport_departure = :airportDepartureId,
                fk_airport_arrival = :airportArrivalId,
                arrival_scheduled_time = :arrivalScheduledTime,
                arrival_actual_time = :arrivalActualTime,
                is_disrupted = :isDisrupted,
                distance = :distance
            WHERE id = :id
        ');
        $query->execute([
            'id'                   => $data['id'],
            'airportDepartureId'   => $data['airportDepartureId'],
            'airportArrivalId'     => $data['airportArrivalId'],
            'arrivalScheduledTime' => $data['arrivalScheduledTime'],
            'arrivalActualTime'    => $data['arrivalActualTime'],
            'isDisrupted'          => $data['isDisrupted'],
            'distance'             => $data['distance']
        ]);
    }

    public function deleteConnectionFlight($id)
    {
        $query = $this->db->prepare('DELETE FROM connection_flights WHERE id = :id');
        $query->execute([ 'id' => $id ]);
    }



    /************************************
     ***** CLAIM CONNECTION FLIGHTS *****
     ************************************/

    public function getAllClaimConnectionFlights($claimId): array
    {
        $query = $this->db->prepare('
            SELECT 
                ccf.fk_claim as claimId, 
                ccf.fk_connection_flight as connectionFlightId,
                ccf.`order`,
                cf.id,
                cf.fk_airport_departure as airportDepartureId,
                apd.name as airportDepartureName,
                apd.iata as airportDepartureIata,
                CONCAT(apd.name, " (", apd.iata, ")") as airportDepartureNameAndIata,
                cf.fk_airport_arrival as airportArrivalId,
                apa.name as airportArrivalName,
                apa.iata as airportArrivalIata,
                CONCAT(apa.name, " (", apa.iata, ")") as airportArrivalNameAndIata,
                cf.arrival_scheduled_time as arrivalScheduledTime,
                cf.arrival_actual_time as arrivalActualTime,
                cf.is_disrupted as isDisrupted,
                cf.distance,
                cf.created_at as createdAt
            FROM claim_connection_flights ccf
            LEFT JOIN connection_flights cf ON cf.id = ccf.fk_connection_flight
            LEFT JOIN airports apd ON apd.id = cf.fk_airport_departure
            LEFT JOIN airports apa ON apa.id = cf.fk_airport_arrival
            WHERE ccf.fk_claim = :claimId
            ORDER BY ccf.`order`
        ');
        $query->execute([ 'claimId' => $claimId ]);

        return $query->fetchAll();
    }

    public function createClaimConnectionFlight($claimId, $data)
    {
        $query = $this->db->prepare('
            INSERT INTO claim_connection_flights (fk_claim, fk_connection_flight, `order`) 
            VALUES (:claimId, :connectionFlightId, :order);
        ');
        $query->execute([
            'claimId'            => $claimId,
            'connectionFlightId' => $data['connectionFlightId'],
            'order'              => $data['order']
        ]);
    }

    public function deleteClaimConnectionFlight($claimId, $connFlightId)
    {
        $query = $this->db->prepare('
            DELETE FROM claim_connection_flights 
            WHERE fk_claim = :claimId AND fk_connection_flight = :connFlightId
        ');
        $query->execute([ 'claimId' => $claimId, 'connFlightId' => $connFlightId ]);
    }

    public function deleteAllClaimConnectionFlights($claimId)
    {
        $query = $this->db->prepare('DELETE FROM claim_connection_flights WHERE fk_claim = :claimId');
        $query->execute([ 'claimId' => $claimId ]);
    }

    public function setHasConnectionFlightByClaimId($claimId, bool $hasIt)
    {
        $query = $this->db->prepare('UPDATE claims SET has_connection_flight = :hasIt WHERE id = :claimId');
        $query->execute([ 'claimId' => $claimId, 'hasIt' => $hasIt ]);
    }

    public function connectionFlightExistsByClaimId($claimId): bool
    {
        $query = $this->db->prepare(
            'SELECT COUNT(fk_claim) as counter FROM claim_connection_flights WHERE fk_claim = :claimId;'
        );
        $query->execute([ 'claimId' => $claimId ]);

        return (int)$query->fetch()['counter'] > 0;
    }

    public function connFlightIdExistsForClaimConnectionFlight($claimId, $connFlightId): bool
    {
        $query = $this->db->prepare('
            SELECT COUNT(fk_claim) as counter FROM claim_connection_flights 
            WHERE fk_claim = :claimId AND fk_connection_flight = :connFlightId;
        ');
        $query->execute([ 'claimId' => $claimId, 'connFlightId' => $connFlightId ]);

        return (int)$query->fetch()['counter'] > 0;
    }

    public function orderExistsForClaimConnectionFlight($claimId, $order): bool
    {
        $query = $this->db->prepare('
            SELECT COUNT(fk_claim) as counter FROM claim_connection_flights 
            WHERE fk_claim = :claimId AND `order` = :order;
        ');
        $query->execute([ 'claimId' => $claimId, 'order' => $order ]);

        return (int)$query->fetch()['counter'] > 0;
    }
}