<?php

namespace App\Repository;

use App\Entity\Bill;

class BillsRepository extends BaseRepository
{
    /*********************
     ******* BILLS *******
     *********************/

    private $fieldsToGet = '
        SELECT
            b.id,
            LPAD(b.number, 6, "0") as number,
            b.fk_claim as claimId,
            c.fk_disruption_type as disruptionTypeId,
            c.fk_flight as flightId,
            c.fk_client as clientId,
            b.fk_claim_billing_data as claimBillingDataId,
            b.fk_bill_type as billTypeId,
            bt.name as billTypeName,
            b.base,
            b.iva_total as ivaTotal,
            b.total,
            b.comments,
            b.parent_bill_id as parentBillId,
            b.fk_bill_preference as billPreferenceId,
            bp.iva as iva,
            b.expires_at as expiresAt,
            b.created_at as createdAt
        FROM bills b
        LEFT JOIN bill_types bt ON bt.id = b.fk_bill_type
        LEFT JOIN bill_preferences bp ON bp.id = b.fk_bill_preference
        LEFT JOIN claims c ON c.id = b.fk_claim
    ';

    public function getAll(): array
    {
        $query = $this->db->prepare($this->fieldsToGet);
        $query->execute();

        return $query->fetchAll();
    }

    public function getById($id): array
    {
        $query = $this->db->prepare($this->fieldsToGet . ' WHERE b.id = :id');
        $query->execute([ 'id' => $id ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function getByClaimId($claimId): array
    {
        $query = $this->db->prepare($this->fieldsToGet . ' WHERE b.fk_claim = :claimId');
        $query->execute([ 'claimId' => $claimId ]);

        return $query->fetchAll();
    }

    public function create(Bill $bill): int
    {
        $query = $this->db->prepare('
            INSERT INTO bills 
                (number, fk_claim, fk_claim_billing_data, fk_bill_type,
                 base, iva_total, total, comments, parent_bill_id, fk_bill_preference, expires_at) 
            VALUES 
                (:number, :claimId, (SELECT id FROM claims_billing_data WHERE fk_claim = :claimId), :billTypeId, 
                 :base, :ivaTotal, :total, :comments, :parentBillId, :billPreferenceId, :expiresAt)
        ');
        $query->execute([
            'number'             => ltrim($bill->number, '0'),
            'claimId'            => $bill->claimId,
            'claimBillingDataId' => $bill->claimBillingDataId,
            'billTypeId'         => $bill->billTypeId,
            'base'               => $bill->base,
            'ivaTotal'           => $bill->ivaTotal,
            'total'              => $bill->total,
            'comments'           => $bill->comments,
            'parentBillId'       => $bill->parentBillId,
            'billPreferenceId'   => $bill->billPreferenceId,
            'expiresAt'          => $bill->expiresAt
        ]);

        return $this->db->lastInsertId();
    }

    public function edit(Bill $bill)
    {
        $updateClaimBillingDataId = '';
        if ($bill->claimBillingDataId) {
            $updateClaimBillingDataId = ', fk_claim_billing_data = ' . $bill->claimBillingDataId . ' ';
        }

        $query = $this->db->prepare('
            UPDATE bills
            SET
                number = :number,
                fk_claim = :claimId,
                fk_bill_type = :billTypeId,
                base = :base,
                iva_total = :ivaTotal,
                total = :total,
                comments = :comments,
                parent_bill_id = :parentBillId,
                fk_bill_preference = :billPreferenceId,
                expires_at = :expiresAt ' .
                $updateClaimBillingDataId . '   
            WHERE id = :id
        ');
        $query->execute([
            'id'                 => $bill->id,
            'number'             => ltrim($bill->number, '0'),
            'claimId'            => $bill->claimId,
            'billTypeId'         => $bill->billTypeId,
            'base'               => $bill->base,
            'ivaTotal'           => $bill->ivaTotal,
            'total'              => $bill->total,
            'comments'           => $bill->comments,
            'parentBillId'       => $bill->parentBillId,
            'billPreferenceId'   => $bill->billPreferenceId,
            'expiresAt'          => $bill->expiresAt
        ]);
    }

    public function delete($id)
    {
        $query = $this->db->prepare('DELETE FROM bills WHERE id = :id');
        $query->execute([ 'id' => $id ]);
    }

    public function numberForBillTypeExists($number, $billTypeId, $idToAvoid): bool
    {
        $query = $this->db->prepare('
            SELECT COUNT(id) as counter 
            FROM bills 
            WHERE
                number = :number AND
                fk_bill_type = :billTypeId AND
                id != :idToAvoid
        ');
        $query->execute([
            'number'     => ltrim($number, '0'),
            'billTypeId' => $billTypeId,
            'idToAvoid'  => empty($idToAvoid) ? '' : $idToAvoid
        ]);

        return (int)$query->fetch()['counter'] > 0;
    }



    /*********************
     * CLAIM BILLING DATA *
     *********************/

    private $claimsBillingDataFieldsToGet = '
        SELECT
            id,
            fk_claim as claimId,
            account_holder as accountHolder,
            account_number as accountNumber,
            swift,
            is_valid as isValid,
            created_at as createdAt,
            updated_at as updatedAt
        FROM claims_billing_data
    ';

    public function getAllClaimsBillingData(): array
    {
        $query = $this->db->prepare($this->claimsBillingDataFieldsToGet);
        $query->execute();

        return $query->fetchAll();
    }

    public function getClaimBillingDataByClaimId($claimId): array
    {
        $query = $this->db->prepare($this->claimsBillingDataFieldsToGet . ' WHERE fk_claim = :claimId');
        $query->execute([ 'claimId' => $claimId ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function getClaimBillingDataById($id): array
    {
        $query = $this->db->prepare($this->claimsBillingDataFieldsToGet . ' WHERE id = :id');
        $query->execute([ 'id' => $id ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function createClaimBillingData($data): int
    {
        $query = $this->db->prepare('
            INSERT INTO claims_billing_data (fk_claim, account_holder, account_number, swift, is_valid) 
            VALUES (:claimId, :accountHolder, :accountNumber, :swift, :isValid)
        ');
        $query->execute([
            'claimId'       => $data['claimId'],
            'accountHolder' => $data['accountHolder'],
            'accountNumber' => $data['accountNumber'],
            'swift'         => $data['swift'],
            'isValid'       => array_key_exists('isValid', $data) ? $data['isValid'] : 1
        ]);

        return $this->db->lastInsertId();
    }

    public function editClaimBillingData($data)
    {
        $query = $this->db->prepare('
            UPDATE claims_billing_data
            SET
                fk_claim = :claimId,
                account_holder = :accountHolder,
                account_number = :accountNumber,
                swift = :swift,
                is_valid = :isValid
            WHERE id = :id
        ');
        $query->execute([
            'id'            => $data['id'],
            'claimId'       => $data['claimId'],
            'accountHolder' => $data['accountHolder'],
            'accountNumber' => $data['accountNumber'],
            'swift'         => $data['swift'],
            'isValid'       => array_key_exists('isValid', $data) ? $data['isValid'] : 1
        ]);
    }

    public function editClaimBillingDataIsValidByClaim($claimId, $isValid)
    {
        $query = $this->db->prepare('UPDATE claims_billing_data SET is_valid = :isValid WHERE fk_claim = :claimId');
        $query->execute([ 'claimId' => $claimId, 'isValid' => $isValid ]);
    }

    public function deleteClaimBillingData($id)
    {
        $query = $this->db->prepare('DELETE FROM claims_billing_data WHERE id = :id');
        $query->execute([ 'id' => $id ]);
    }



    /*********************
     ** BILL PREFERENCES **
     *********************/

    private $billPreferencesFieldsToGet = '
        SELECT
            bp.id,
            bp.fk_country as countryId,
            c.name as countryName,
            bp.iva,
            bp.expiration_days_client as expirationDaysClient,
            bp.expiration_days_populetic as expirationDaysPopuletic,
            bp.expiration_days_cancellation as expirationDaysCancellation,
            bp.amount_cancellation as amountCancellation,
            bp.is_active as isActive,
            bp.created_at as createdAt
        FROM bill_preferences bp
        LEFT JOIN countries c ON c.id = bp.fk_country
    ';

    public function getAllBillPreferences(): array
    {
        $query = $this->db->prepare($this->billPreferencesFieldsToGet);
        $query->execute();

        return $query->fetchAll();
    }

    public function getBillPreferenceById($id): array
    {
        $query = $this->db->prepare($this->billPreferencesFieldsToGet . ' WHERE bp.id = :id');
        $query->execute([ 'id' => $id ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function createBillPreference($data): int
    {
        $query = $this->db->prepare('
            INSERT INTO bill_preferences
                (fk_country, iva, expiration_days_client, expiration_days_populetic, 
                 expiration_days_cancellation, amount_cancellation) 
            VALUES
                (:countryId, :iva, :expirationDaysClient, :expirationDaysPopuletic, 
                 :expirationDaysCancellation, :amountCancellation)
        ');
        $query->execute([
            'countryId'                  => $data['countryId'],
            'iva'                        => $data['iva'],
            'expirationDaysClient'       => $data['expirationDaysClient'],
            'expirationDaysPopuletic'    => $data['expirationDaysPopuletic'],
            'expirationDaysCancellation' => $data['expirationDaysCancellation'],
            'amountCancellation'         => $data['amountCancellation']
        ]);

        return $this->db->lastInsertId();
    }

    public function editBillPreference($data)
    {
        $query = $this->db->prepare('
            UPDATE bill_preferences
            SET
                fk_country = :countryId,
                iva = :iva,
                expiration_days_client = :expirationDaysClient,
                expiration_days_populetic = :expirationDaysPopuletic,
                expiration_days_cancellation = :expirationDaysCancellation,
                amount_cancellation = :amountCancellation,
                is_active = :isActive
            WHERE id = :id
        ');
        $query->execute([
            'id'                         => $data['id'],
            'countryId'                  => $data['countryId'],
            'iva'                        => $data['iva'],
            'expirationDaysClient'       => $data['expirationDaysClient'],
            'expirationDaysPopuletic'    => $data['expirationDaysPopuletic'],
            'expirationDaysCancellation' => $data['expirationDaysCancellation'],
            'amountCancellation'         => $data['amountCancellation'],
            'isActive'                   => array_key_exists('isActive', $data) ? $data['isActive'] : 1
        ]);
    }

    public function deleteBillPreference($id)
    {
        $query = $this->db->prepare('DELETE FROM bill_preferences WHERE id = :id');
        $query->execute([ 'id' => $id ]);
    }



    /*********************
     ***** BILL TYPES *****
     *********************/

    private $billTypesFieldsToGet = 'SELECT id, name, series FROM bill_types';

    public function getAllBillTypes(): array
    {
        $query = $this->db->prepare($this->billTypesFieldsToGet);
        $query->execute();

        return $query->fetchAll();
    }

    public function getBillTypeById($id): array
    {
        $query = $this->db->prepare($this->billTypesFieldsToGet . ' WHERE id = :id');
        $query->execute([ 'id' => $id ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function createBillType($data): int
    {
        $query = $this->db->prepare('INSERT INTO bill_types (name, series) VALUES (:name, :series)');
        $query->execute([ 'name' => $data['name'], 'series' => $data['series'] ]);

        return $this->db->lastInsertId();
    }

    public function editBillType($data)
    {
        $query = $this->db->prepare('UPDATE bill_types SET name = :name, series = :series WHERE id = :id');
        $query->execute([ 'id' => $data['id'], 'name' => $data['name'], 'series' => $data['series'] ]);
    }

    public function deleteBillType($id)
    {
        $query = $this->db->prepare('DELETE FROM bill_types WHERE id = :id');
        $query->execute([ 'id' => $id ]);
    }

    public function getBillNextNumberBySeries($series): array
    {
        $query = $this->db->prepare('
            SELECT
                LPAD(IFNULL(MAX(b.number), 0) + 1 , 6, "0") as nextNumber,
                bt.id as billTypeId
            FROM bill_types bt
            LEFT JOIN bills b ON bt.id = b.fk_bill_type
            WHERE bt.series = :series
        ');
        $query->execute([ 'series' => $series ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function seriesExists($series, $idToAvoid = null): bool
    {
        $query = $this->db->prepare(
            'SELECT COUNT(id) as counter FROM bill_types WHERE series = :series AND id != :idToAvoid'
        );
        $query->execute([
            'series'    => $series,
            'idToAvoid' => $idToAvoid === null ? '' : $idToAvoid
        ]);

        return (int)$query->fetch()['counter'] > 0;
    }



    /***********************
     * PAYMENTS EMAIL SENT *
     ***********************/

    public function getPaymentEmailSentByClaimId($claimId): array
    {
        $query = $this->db->prepare('
            SELECT 
                fk_claim as claimId, 
                number_of_times_sent as numberOfTimesSent, 
                created_at as createdAt
            FROM payments_email_sent_by_claim
            WHERE fk_claim = :claimId
        ');
        $query->execute([ 'claimId' => $claimId ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function addPaymentsTimesSent($claimId)
    {
        if ($this->claimIdHasEmailSent($claimId)) {
            $query = $this->db->prepare('
                UPDATE payments_email_sent_by_claim SET number_of_times_sent = number_of_times_sent + 1
                WHERE fk_claim = :claimId
            ');
        } else {
            $query = $this->db->prepare('
                INSERT INTO payments_email_sent_by_claim (fk_claim) VALUES (:claimId)
            ');
        }
        $query->execute([ 'claimId' => $claimId ]);
    }

    public function deletePaymentEmailSent($claimId)
    {
        $query = $this->db->prepare('DELETE FROM payments_email_sent_by_claim WHERE fk_claim = :claimId');
        $query->execute([ 'claimId' => $claimId ]);
    }

    public function claimIdHasEmailSent($claimId): bool
    {
        $query = $this->db->prepare(
            'SELECT COUNT(fk_claim) as counter FROM payments_email_sent_by_claim WHERE fk_claim = :claimId'
        );
        $query->execute([ 'claimId' => $claimId ]);
        return (int)$query->fetch()['counter'] > 0;
    }
}