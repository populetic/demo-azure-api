<?php

namespace App\Repository;

use App\Model\ConstantsModel;

class PaymentsRepository extends BaseRepository
{
    public function getPaymentsByClaimStatus($claimStatusId): array
    {
        $query = $this->db->prepare('
            SELECT
                cbd.fk_claim AS claimId,
                group_concat(cbd.fk_claim SEPARATOR ", ") as listClaims,
                cbd.account_holder AS accountHolder,
                cbd.account_number AS accountNumber,
                cbd.swift,
                cli.email,
                cbd.is_valid as isValid,
                SUM(c.amount_received) AS amountReceived
            FROM claims_billing_data cbd
            INNER JOIN claims c ON cbd.fk_claim = c.id
            INNER JOIN clients cli ON c.fk_client = cli.id
            WHERE c.fk_claim_status = :claimStatusId
            GROUP BY cbd.account_number, cbd.account_holder, cbd.swift
        ');
        $query->execute([ 'claimStatusId' => $claimStatusId ]);

        return $query->fetchAll();
    }

    public function getPaymentsByClaimStatusAndDate($claimStatusId, $lastSentAt): array
    {
        $query = $this->db->prepare('
            SELECT
                cbd.fk_claim AS claimId,
                group_concat(cbd.fk_claim SEPARATOR ", ") as listClaims,
                cbd.account_holder AS accountHolder,
                cbd.account_number AS accountNumber,
                cbd.swift,
                cli.email,
                cbd.is_valid as isValid,
                SUM(c.amount_received) AS amountReceived
            FROM claims_billing_data cbd
            INNER JOIN claims c ON cbd.fk_claim = c.id
            INNER JOIN clients cli ON c.fk_client = cli.id
            INNER JOIN payments_email_sent_by_claim pesbc ON cbd.fk_claim = pesbc.fk_claim AND
                                                             pesbc.last_sent_at LIKE "' . $lastSentAt . ' %"
            WHERE c.fk_claim_status = :claimStatusId AND cbd.is_valid = 1
            GROUP BY cbd.account_number, cbd.account_holder
        ');
        $query->execute([ 'claimStatusId' => $claimStatusId ]);

        return $query->fetchAll();
    }

    public function getDatesToSendEmailPayment(): array
    {
        $query = $this->db->prepare('
            SELECT
                DATE_FORMAT(pesbc.last_sent_at, "%Y-%m-%d") as createdAt,
                DATE_ADD(pesbc.last_sent_at, INTERVAL 7 DAY) as dateEnd
            FROM payments_email_sent_by_claim pesbc
            INNER JOIN claims_billing_data cbd ON pesbc.fk_claim = cbd.fk_claim AND cbd.is_valid = 1
            INNER JOIN claims c ON pesbc.fk_claim = c.id AND c.fk_claim_status = :readyToPay
            GROUP BY DATE_FORMAT(pesbc.last_sent_at, "%%Y-%%m-%%d")
        ');
        $query->execute([ 'readyToPay' => ConstantsModel::CLAIM_STATUS_READY_TO_PAY ]);

        return $query->fetchAll();
    }

    public function getClaimsGroupedByAccountData($claimId): array
    {
        $query = $this->db->prepare('
            SELECT cbd.fk_claim as claimId
            FROM claims_billing_data cbd
            WHERE
                cbd.account_holder = (
                    SELECT cbd2.account_holder FROM claims_billing_data cbd2 WHERE cbd2.fk_claim = :claimId
                ) AND
                cbd.account_number = (
                    SELECT cbd3.account_number FROM claims_billing_data cbd3 WHERE cbd3.fk_claim = :claimId
                )
        ');
        $query->execute([ 'claimId' => $claimId ]);

        return $query->fetchAll();
    }

    public function getClientsDataWithCompletedPayment(): array
    {
        $query = $this->db->prepare('
            SELECT
                c.ref,
                cli.name,
                cli.email,
                l.code as languageCode
            FROM bills b
            INNER JOIN claims_billing_data cbd ON cbd.id = b.fk_claim_billing_data AND
                timestampdiff(
                    MONTH,
                    DATE_FORMAT(cbd.created_at, "%Y-%m-%d"),
                    DATE_FORMAT(now(), "%Y-%m-%d")
                ) <= 1 AND 
                cbd.is_valid = 1
            INNER JOIN claims c ON c.id = cbd.fk_claim
            INNER JOIN clients cli ON cli.id = c.fk_client
            INNER JOIN translation_system.languages l ON l.id = cli.fk_language
            WHERE
                timestampdiff(
                    DAY,
                    DATE_FORMAT(b.created_at, "%Y-%m-%d"),
                    DATE_FORMAT(now(), "%Y-%m-%d")
                ) = 7 AND
                c.fk_claim_status = :completed
        ');
        $query->execute([ 'completed' => ConstantsModel::CLAIM_STATUS_COMPLETED ]);

        return $query->fetchAll();
    }

    public function getClaimsLogsStatusToRevert(): array
    {
        $query = $this->db->prepare('
            SELECT
                lcs.fk_claim_status AS claimOldStatus,
                lcs.fk_claim AS claimId
            FROM logs_claim_status lcs
            INNER JOIN claims c ON c.id = lcs.fk_claim
            WHERE
                c.fk_claim_status = :requestPaymentData AND
                lcs.fk_claim_status IN (:populeticCollected, :vipPayment) AND
                lcs.created_at < NOW() - INTERVAL 8 DAY
            ORDER BY lcs.id
        ');
        $query->execute([
            'requestPaymentData' => ConstantsModel::CLAIM_STATUS_REQUEST_PAYMENT_DATA,
            'populeticCollected' => ConstantsModel::CLAIM_STATUS_POPULETIC_COLLECTED,
            'vipPayment'         => ConstantsModel::CLAIM_STATUS_VIP_PAYMENT
        ]);

        return $query->fetchAll();
    }
}