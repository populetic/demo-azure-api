<?php

namespace App\Repository;

use App\Entity\CountryProvince;

class CountryProvincesRepository extends BaseRepository
{
    private $fieldsToGet = '
        SELECT
            cp.id,
            cp.fk_country as countryId,
            c.name as countryName,
            cp.name,
            cp.is_active as isActive
        FROM country_provinces cp
        LEFT JOIN countries c ON c.id = cp.fk_country
    ';

    public function getAll(): array
    {
        $query = $this->db->prepare($this->fieldsToGet);
        $query->execute();

        return $query->fetchAll();
    }

    public function getById($id): array
    {
        $query = $this->db->prepare($this->fieldsToGet . ' WHERE cp.id = :id');
        $query->execute([ 'id' => $id ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function create(CountryProvince $province): int
    {
        $query = $this->db->prepare('INSERT INTO country_provinces (fk_country, name) VALUES (:countryId, :name)');
        $query->execute([ 'countryId' => $province->countryId, 'name' => $province->name ]);

        return $this->db->lastInsertId();
    }

    public function edit(CountryProvince $province)
    {
        $query = $this->db->prepare('
            UPDATE country_provinces
            SET
                fk_country  = :countryId,
                name        = :name,
                is_active   = :isActive
            WHERE id = :id
        ');
        $query->execute([
            'id'        => $province->id,
            'countryId' => $province->countryId,
            'name'      => $province->name,
            'isActive'  => $province->isActive
        ]);
    }

    public function delete($id)
    {
        $query = $this->db->prepare('DELETE FROM country_provinces WHERE id = :id;');
        $query->execute([ 'id' => $id ]);
    }
}