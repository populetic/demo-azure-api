<?php

namespace App\Repository;

use App\Entity\Demand;

class DemandsRepository extends BaseRepository
{
    /*********************
     ****** DEMANDS ******
     *********************/

     private $whereArray = [];

    public function getAll($filters): array
    {
        $where = $this->getWhereByFilters($filters);

        $query = $this->db->prepare('
            SELECT
                d.id,
                d.procedure_number as procedureNumber,
                d.judgement_number as judgementNumber,
                d.fk_court_number as courtNumberId,
                cn.number as courtNumber,
                c.fk_court_type as courtTypeId,
                c.fk_city as courtCityId,
                ci.name as courtCityName,
                d.amount_requested as amountRequested,
                d.amount_received as amountReceived,
                d.amount_interests as amountInterests,
                d.comments,
                d.is_no_show as isNoShow,
                d.is_overseas as isOverseas,
                d.has_flightstats as hasFlightstats,
                d.presented_at as presentedAt,
                d.admited_at as admitedAt,
                d.judged_at as judgedAt,
                d.cancellated_at as cancellatedAt,
                d.created_at as createdAt,
                d.updated_at as updatedAt,
                cl.fk_law_firm as lawFirmId,
                lf.name as lawFirmName
            FROM demands d
            LEFT JOIN demand_related_claims drc ON drc.fk_demand = d.id
            LEFT JOIN claims cl ON cl.id = drc.fk_claim
            LEFT JOIN courts_number cn ON cn.id = d.fk_court_number
            LEFT JOIN courts c ON c.id = cn.fk_court
            LEFT JOIN cities ci ON ci.id = c.fk_city
            LEFT JOIN law_firms lf ON lf.id = cl.fk_law_firm
            ' . $where . '
            GROUP BY d.id
        ');
        $query->execute();

        return $query->fetchAll();
    }

    public function getById($id): array
    {
        $query = $this->db->prepare('
            SELECT
                d.id,
                d.procedure_number as procedureNumber,
                d.judgement_number as judgementNumber,
                d.fk_court_number as courtNumberId,
                cn.number as courtNumber,
                c.fk_court_type as courtTypeId,
                c.fk_city as courtCityId,
                ci.name as courtCityName,
                d.amount_requested as amountRequested,
                d.amount_received as amountReceived,
                d.amount_interests as amountInterests,
                d.comments,
                d.is_no_show as isNoShow,
                d.is_overseas as isOverseas,
                d.has_flightstats as hasFlightstats,
                d.presented_at as presentedAt,
                d.admited_at as admitedAt,
                d.judged_at as judgedAt,
                d.cancellated_at as cancellatedAt,
                d.created_at as createdAt,
                d.updated_at as updatedAt,
                cl.fk_disruption_type as disruptionTypeId,
                cl.fk_lawsuite_type as lawsuiteTypeId,
                f.fk_airline as airlineId,
                al.name as airlineName
            FROM demands d
            LEFT JOIN demand_related_claims drc ON drc.fk_demand = d.id
            LEFT JOIN claims cl ON cl.id = drc.fk_claim
            LEFT JOIN flights f ON f.id = cl.fk_flight
            LEFT JOIN airlines al ON al.id = f.fk_airline
            LEFT JOIN courts_number cn ON cn.id = d.fk_court_number
            LEFT JOIN courts c ON c.id = cn.fk_court
            LEFT JOIN cities ci ON ci.id = c.fk_city
            WHERE d.id = :id
        ');
        $query->execute([ 'id' => $id ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function create(Demand $demand): int
    {
        $query = $this->db->prepare('
            INSERT INTO demands
            (procedure_number, judgement_number, fk_court_number, amount_requested, amount_received, amount_interests,
             comments, is_no_show, is_overseas, has_flightstats, presented_at, admited_at, judged_at, cancellated_at)
            VALUES
            (:procedureNumber, :judgementNumber, :courtNumberId, :amountRequested, :amountReceived, :amountInterests,
             :comments, :isNoShow, :isOverseas, :hasFlightstats, :presentedAt, :admitedAt, :judgedAt, :cancellatedAt);
        ');
        $query->execute([
            'procedureNumber' => $demand->procedureNumber,
            'judgementNumber' => $demand->judgementNumber,
            'courtNumberId'   => $demand->courtNumberId,
            'amountRequested' => $demand->amountRequested,
            'amountReceived'  => $demand->amountReceived,
            'amountInterests' => $demand->amountInterests,
            'comments'        => $demand->comments,
            'isNoShow'        => $demand->isNoShow,
            'isOverseas'      => $demand->isOverseas,
            'hasFlightstats'  => $demand->hasFlightstats,
            'presentedAt'     => $demand->presentedAt,
            'admitedAt'       => $demand->admitedAt,
            'judgedAt'        => $demand->judgedAt,
            'cancellatedAt'   => $demand->cancellatedAt
        ]);

        return $this->db->lastInsertId();
    }

    public function edit(Demand $demand)
    {
        $query = $this->db->prepare('
            UPDATE demands
            SET
                procedure_number = :procedureNumber,
                judgement_number = :judgementNumber,
                fk_court_number = :courtNumberId,
                amount_requested = :amountRequested,
                amount_received = :amountReceived,
                amount_interests = :amountInterests,
                comments = :comments,
                is_no_show = :isNoShow,
                is_overseas = :isOverseas,
                has_flightstats = :hasFlightstats,
                presented_at = :presentedAt,
                admited_at = :admitedAt,
                judged_at = :judgedAt,
                cancellated_at = :cancellatedAt
            WHERE id = :id
        ');
        $query->execute([
            'id'              => $demand->id,
            'procedureNumber' => $demand->procedureNumber,
            'judgementNumber' => $demand->judgementNumber,
            'courtNumberId'   => $demand->courtNumberId,
            'amountRequested' => $demand->amountRequested,
            'amountReceived'  => $demand->amountReceived,
            'amountInterests' => $demand->amountInterests,
            'comments'        => $demand->comments,
            'isNoShow'        => $demand->isNoShow,
            'isOverseas'      => $demand->isOverseas,
            'hasFlightstats'  => $demand->hasFlightstats,
            'presentedAt'     => $demand->presentedAt,
            'admitedAt'       => $demand->admitedAt,
            'judgedAt'        => $demand->judgedAt,
            'cancellatedAt'   => $demand->cancellatedAt
        ]);
    }

    public function delete($id)
    {
        $query = $this->db->prepare('DELETE FROM demands WHERE id = :id');
        $query->execute([ 'id' => $id ]);
    }

    public function getWhereByFilters($filters)
    {
        if ($filters['procedureNumber'] !== null) {
            $this->whereArray[] = $filters['procedureNumber'] ?
            '(d.procedure_number IS NOT NULL AND TRIM(d.procedure_number) NOT LIKE "")' :
            '(d.procedure_number IS NULL OR TRIM(d.procedure_number) LIKE "")';
        }

        if ($filters['cancellated'] !== null) {
            $this->whereArray[] = $filters['cancellated'] ?
            '(d.cancellated_at IS NOT NULL AND TRIM(d.cancellated_at) NOT LIKE "")' :
            '(d.cancellated_at IS NULL OR TRIM(d.cancellated_at) LIKE "")';
        }

        if ($filters['lawFirmId'] !== null && $filters['lawFirmId'] != 0) {
            $this->whereArray[] = 'cl.fk_law_firm = ' . $filters['lawFirmId'];
        }

        $where = implode(' AND ', $this->whereArray);

        return $where ? ' WHERE ' . $where : '';
    }


    /*********************
     * DEMANDS EXECUTIVE *
     *********************/

    private $demandsExecutiveFieldsToGet = '
        SELECT
            id,
            fk_demand as demandId,
            procedure_number as procedureNumber,
            presented_at as presentedAt,
            amount_requested as amountRequested,
            amount_received as amountReceived,
            amount_interests as amountInterests,
            comments,
            created_at as createdAt,
            updated_at as updatedAt
        FROM demands_executive
    ';

    public function getAllDemandsExecutive(): array
    {
        $query = $this->db->prepare($this->demandsExecutiveFieldsToGet);
        $query->execute();

        return $query->fetchAll();
    }

    public function getDemandExecutiveById($id): array
    {
        $query = $this->db->prepare($this->demandsExecutiveFieldsToGet . ' WHERE id = :id');
        $query->execute([ 'id' => $id ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function createDemandExecutive($data): int
    {
        $query = $this->db->prepare('
            INSERT INTO demands_executive
            (fk_demand, procedure_number, presented_at, amount_requested, amount_received, amount_interests, comments)
            VALUES
            (:demandId, :procedureNumber, :presentedAt, :amountRequested, :amountReceived, :amountInterests, :comments);
        ');
        $query->execute([
            'demandId'        => $data['demandId'],
            'procedureNumber' => $data['procedureNumber'],
            'presentedAt'     => $data['presentedAt'],
            'amountRequested' => $data['amountRequested'],
            'amountReceived'  => $data['amountReceived'],
            'amountInterests' => $data['amountInterests'],
            'comments'        => $data['comments']
        ]);

        return $this->db->lastInsertId();
    }

    public function editDemandExecutive($data)
    {
        $query = $this->db->prepare('
            UPDATE demands_executive
            SET
                fk_demand = :demandId,
                procedure_number = :procedureNumber,
                presented_at = :presentedAt,
                amount_requested = :amountRequested,
                amount_received = :amountReceived,
                amount_interests = :amountInterests,
                comments = :comments
            WHERE id = :id
        ');
        $query->execute([
            'id'              => $data['id'],
            'demandId'        => $data['demandId'],
            'procedureNumber' => $data['procedureNumber'],
            'presentedAt'     => $data['presentedAt'],
            'amountRequested' => $data['amountRequested'],
            'amountReceived'  => $data['amountReceived'],
            'amountInterests' => $data['amountInterests'],
            'comments'        => $data['comments']
        ]);
    }

    public function deleteDemandExecutive($id)
    {
        $query = $this->db->prepare('DELETE FROM demands_executive WHERE id = :id');
        $query->execute([ 'id' => $id ]);
    }



    /*********************
     ** DEMAND EXPENSES **
     *********************/

    private $demandExpensesFieldsToGet = '
        SELECT
            de.id,
            de.fk_demand as demandId,
            de.amount,
            de.description,
            de.fk_user as userId,
            u.username,
            de.created_at as createdAt,
            de.updated_at as updatedAt
        FROM demand_expenses de
        LEFT JOIN users u ON u.id = de.fk_user
    ';

    public function getAllDemandExpenses(): array
    {
        $query = $this->db->prepare($this->demandExpensesFieldsToGet);
        $query->execute();

        return $query->fetchAll();
    }

    public function getDemandExpenseById($id): array
    {
        $query = $this->db->prepare($this->demandExpensesFieldsToGet . ' WHERE de.id = :id');
        $query->execute([ 'id' => $id ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function createDemandExpense($data): int
    {
        $query = $this->db->prepare('
            INSERT INTO demand_expenses (fk_demand, amount, description, fk_user)
            VALUES (:demandId, :amount, :description, :userId);
        ');
        $query->execute([
            'demandId'    => $data['demandId'],
            'amount'      => $data['amount'],
            'description' => $data['description'],
            'userId'      => $data['userId']
        ]);

        return $this->db->lastInsertId();
    }

    public function editDemandExpense($data)
    {
        $query = $this->db->prepare('
            UPDATE demand_expenses
            SET
                fk_demand = :demandId,
                amount = :amount,
                description = :description,
                fk_user = :userId
            WHERE id = :id
        ');
        $query->execute([
            'id'          => $data['id'],
            'demandId'    => $data['demandId'],
            'amount'      => $data['amount'],
            'description' => $data['description'],
            'userId'      => $data['userId']
        ]);
    }

    public function deleteDemandExpense($id)
    {
        $query = $this->db->prepare('DELETE FROM demand_expenses WHERE id = :id');
        $query->execute([ 'id' => $id ]);
    }



    /*********************
     **** DEMAND VIEW ****
     *********************/

    public function getDemandViewByDemandId($demandId): array
    {
        $query = $this->db->prepare('
            SELECT
                fk_demand as demandId,
                location,
                view_date as viewDate,
                created_at as createdAt
            FROM demand_view WHERE fk_demand = :demandId
        ');
        $query->execute([ 'demandId' => $demandId ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function createDemandView($data)
    {
        $query = $this->db->prepare('
            INSERT INTO demand_view (fk_demand, location, view_date) VALUES (:demandId, :location, :viewDate);
        ');
        $query->execute([
            'demandId' => $data['demandId'],
            'location' => $data['location'],
            'viewDate' => $data['viewDate']
        ]);
    }

    public function deleteDemandView($demandId)
    {
        $query = $this->db->prepare('DELETE FROM demand_view WHERE fk_demand = :demandId');
        $query->execute([ 'demandId' => $demandId ]);
    }

    public function demandViewExists($demandId): bool
    {
        $query = $this->db->prepare('SELECT COUNT(fk_demand) as counter FROM demand_view WHERE fk_demand = :demandId');
        $query->execute([ 'demandId' => $demandId ]);

        return (int)$query->fetch()['counter'] > 0;
    }



    /*********************
     *** RELATED CLAIMS ***
     *********************/

    public function getRelatedClaimsByDemandId($demandId): array
    {
        $query = $this->db->prepare('
            SELECT fk_demand as demandId, fk_claim as claimId FROM demand_related_claims WHERE fk_demand = :demandId
        ');
        $query->execute([ 'demandId' => $demandId ]);

        return $query->fetchAll();
    }

    public function createDemandRelatedClaim($demandId, $claimId)
    {
        $query = $this->db->prepare('
            INSERT INTO demand_related_claims (fk_demand, fk_claim) VALUES (:demandId, :claimId);
        ');
        $query->execute([ 'demandId' => $demandId, 'claimId'  => $claimId ]);
    }

    public function deleteDemandRelatedClaim($demandId, $claimId)
    {
        $query = $this->db->prepare('
            DELETE FROM demand_related_claims WHERE fk_demand = :demandId AND fk_claim = :claimId
        ');
        $query->execute([ 'demandId' => $demandId, 'claimId' => $claimId ]);
    }

    public function demandRelatedClaimExists($demandId, $claimId): bool
    {
        $query = $this->db->prepare('
            SELECT COUNT(fk_demand) as counter FROM demand_related_claims WHERE fk_demand = :demandId AND fk_claim = :claimId;
        ');
        $query->execute([ 'demandId' => $demandId, 'claimId' => $claimId ]);

        return (int)$query->fetch()['counter'] > 0;
    }



    /************************
     * DEMAND NOTIFICATIONS *
     ************************/

    public function getDemandNotificationsByDemandId($demandId): array
    {
        $query = $this->db->prepare('
            SELECT
                dn.fk_demand as demandId,
                dn.fk_judicial_notification_type as judicialNotificationTypeId,
                jnt.name as judicialNotificationTypeName,
                jnt.fk_country as judicialNotificationTypeCountryId,
                c.name as judicialNotificationTypeCountryName,
                dn.days_to_notify as daysToNotify,
                dn.created_at as createdAt
            FROM demand_notifications dn
            LEFT JOIN judicial_notification_types_by_country jnt ON jnt.id = dn.fk_judicial_notification_type
            LEFT JOIN countries c ON c.id = jnt.fk_country
            WHERE dn.fk_demand = :demandId
        ');
        $query->execute([ 'demandId' => $demandId ]);

        return $query->fetchAll();
    }

    public function createDemandNotification($data)
    {
        $query = $this->db->prepare('
            INSERT INTO demand_notifications (fk_demand, fk_judicial_notification_type, days_to_notify)
            VALUES (:demandId, :notificationTypeId, :daysToNotify);
        ');
        $query->execute([
            'demandId'           => $data['demandId'],
            'notificationTypeId' => $data['notificationTypeId'],
            'daysToNotify'       => $data['daysToNotify']
        ]);
    }

    public function deleteDemandNotification($data)
    {
        $query = $this->db->prepare('
            DELETE FROM demand_notifications
            WHERE
                fk_demand = :demandId AND
                fk_judicial_notification_type = :notificationTypeId AND
                created_at = :createdAt
        ');
        $query->execute([
            'demandId'           => $data['demandId'],
            'notificationTypeId' => $data['judicialNotificationTypeId'],
            'createdAt'          => $data['createdAt']
        ]);
    }

    public function demandNotificationExists($data): bool
    {
        $query = $this->db->prepare('
            SELECT COUNT(fk_demand) as counter FROM demand_notifications
            WHERE
                fk_demand = :demandId AND
                fk_judicial_notification_type = :notificationTypeId AND
                created_at = :createdAt
        ');
        $query->execute([
            'demandId'           => $data['demandId'],
            'notificationTypeId' => $data['judicialNotificationTypeId'],
            'createdAt'          => $data['createdAt']
        ]);

        return (int)$query->fetch()['counter'] > 0;
    }



    /*********************
     * NOTIFICATION TYPES *
     *********************/

    private $notificationTypesFieldsToGet = '
        SELECT
            jnt.id,
            jnt.name,
            jnt.fk_country as countryId,
            c.name as countryName
        FROM judicial_notification_types_by_country jnt
        LEFT JOIN countries c ON c.id = jnt.fk_country
    ';

    public function getAllNotificationTypesByCountry($countryId): array
    {
        if ($countryId) {
            $sql = $this->notificationTypesFieldsToGet . ' WHERE jnt.fk_country = ' . $countryId;
        } else {
            $sql = $this->notificationTypesFieldsToGet . ' ORDER BY jnt.fk_country, jnt.id';
        }
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function getNotificationTypeById($id): array
    {
        $query = $this->db->prepare($this->notificationTypesFieldsToGet . ' WHERE jnt.id = :id');
        $query->execute([ 'id' => $id ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function createNotificationType($data): int
    {
        $query = $this->db->prepare('
            INSERT INTO judicial_notification_types_by_country (name, fk_country) VALUES (:name, :countryId)
        ');
        $query->execute([ 'name' => $data['name'], 'countryId' => $data['countryId'] ]);

        return $this->db->lastInsertId();
    }

    public function editNotificationType($data)
    {
        $query = $this->db->prepare('
            UPDATE judicial_notification_types_by_country SET name = :name, fk_country = :countryId WHERE id = :id
        ');
        $query->execute([
            'id'        => $data['id'],
            'name'      => $data['name'],
            'countryId' => $data['countryId']
        ]);
    }

    public function deleteNotificationType($id)
    {
        $query = $this->db->prepare('DELETE FROM judicial_notification_types_by_country WHERE id = :id');
        $query->execute([ 'id' => $id ]);
    }

    public function notificationTypeNameExistsForCountry($name, $countryId, $idToAvoid = null): bool
    {
        $query = $this->db->prepare('
            SELECT COUNT(id) as counter
            FROM judicial_notification_types_by_country
            WHERE
                name = :name AND
                fk_country = :countryId AND
                id != :idToAvoid
        ');
        $query->execute([
            'name'      => $name,
            'countryId' => $countryId,
            'idToAvoid' => $idToAvoid === null ? '' : $idToAvoid
        ]);

        return (int)$query->fetch()['counter'] > 0;
    }
}