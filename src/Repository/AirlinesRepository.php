<?php

namespace App\Repository;

use App\Entity\Airline;

class AirlinesRepository extends BaseRepository
{
    /*********************
     ****** AIRLINES ******
     *********************/

    private $fieldsToGet = '
        SELECT
            id,
            name,
            iata,
            CONCAT(name, " (", iata, ")") as nameAndIata,
            icao,
            flightstats_iata as flightstatsIata,
            is_bankruptcy as isBankruptcy,
            has_community_license as hasCommunityLicense,
            is_active as isActive,
            created_at as createdAt,
            updated_at as updatedAt
        FROM airlines
    ';

    public function getAll(): array
    {
        $query = $this->db->prepare($this->fieldsToGet);
        $query->execute();

        return $query->fetchAll();
    }

    public function getById($id): array
    {
        $query = $this->db->prepare($this->fieldsToGet . ' WHERE id = :id;');
        $query->execute([ 'id' => $id ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function create(Airline $airline): int
    {
        $query = $this->db->prepare('
            INSERT INTO airlines (name, iata, icao, flightstats_iata, is_bankruptcy, has_community_license, is_active)
            VALUES (:name, :iata, :icao, :flightstatsIata, :isBankruptcy, :hasCommunityLicense, :isActive);
        ');
        $query->execute([
            'name'                => $airline->name,
            'iata'                => $airline->iata,
            'icao'                => $airline->icao,
            'flightstatsIata'     => $airline->flightstatsIata,
            'isBankruptcy'        => $airline->isBankruptcy,
            'hasCommunityLicense' => $airline->hasCommunityLicense,
            'isActive'            => $airline->isActive ? $airline->isActive : true
        ]);

        return $this->db->lastInsertId();
    }

    public function edit(Airline $airline)
    {
        $query = $this->db->prepare('
            UPDATE airlines
            SET
                name = :name,
                iata = :iata,
                icao = :icao,
                flightstats_iata = :flightstatsIata,
                is_bankruptcy = :isBankruptcy,
                has_community_license = :hasCommunityLicense,
                is_active = :isActive
            WHERE id = :id
        ');
        $query->execute([
            'id'                  => $airline->id,
            'name'                => $airline->name,
            'iata'                => $airline->iata,
            'icao'                => $airline->icao,
            'flightstatsIata'     => $airline->flightstatsIata,
            'isBankruptcy'        => $airline->isBankruptcy,
            'hasCommunityLicense' => $airline->hasCommunityLicense,
            'isActive'            => $airline->isActive
        ]);
    }

    public function delete($id)
    {
        $query = $this->db->prepare('DELETE FROM airlines WHERE id = :id;');
        $query->execute([ 'id' => $id ]);
    }



    /************************************
     ** AIRLINES LEGAL DATA BY COUNTRY **
     ************************************/

    private $legalDataFieldsToGet = '
        aldbc.fk_country as countryId,
        c.name as countryName,
        aldbc.fk_language as languageId,
        l.name as languageName,
        aldbc.email,
        aldbc.fax,
        aldbc.branch_address as branchAddress,
        aldbc.headquarters_address as headquartersAddress,
        aldbc.is_judicial_enabled as isJudicialEnabled,
        aldbc.has_non_interest_agreement as hasNonInterestAgreement,
        aldbc.created_at as createdAt,
        aldbc.updated_at as updatedAt
    ';

    public function getAllCountryLegalDataByAirlineId($airlineId): array
    {
        $query = $this->db->prepare('
            SELECT ' . $this->legalDataFieldsToGet . '
            FROM airlines_legal_data_by_country as aldbc
            LEFT JOIN countries c ON c.id = aldbc.fk_country
            LEFT JOIN translation_system.languages l ON l.id = aldbc.fk_language
            WHERE aldbc.fk_airline = :airlineId;
        ');
        $query->execute([ 'airlineId' => $airlineId ]);

        return $query->fetchAll();
    }

    public function getLegalDataByAirlineAndCountry($airlineId, $countryId): array
    {
        $query = $this->db->prepare('
            SELECT ' . $this->legalDataFieldsToGet . '
            FROM airlines_legal_data_by_country as aldbc
            LEFT JOIN airlines a ON a.id = aldbc.fk_airline
            LEFT JOIN countries c ON c.id = aldbc.fk_country
            LEFT JOIN translation_system.languages l ON l.id = aldbc.fk_language
            WHERE aldbc.fk_airline = :airlineId AND aldbc.fk_country = :countryId;
        ');
        $query->execute([ 'airlineId' => $airlineId, 'countryId' => $countryId ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function createLegalData($data)
    {
        $query = $this->db->prepare('
            INSERT INTO airlines_legal_data_by_country
                (fk_airline, fk_country, fk_language, email, fax, branch_address,
                 headquarters_address, is_judicial_enabled, has_non_interest_agreement)
            VALUES
                (:airlineId, :countryId, :languageId, :email, :fax, :branchAddress, 
                 :headquartersAddress, :isJudicialEnabled, :hasNonInterestAgreement);
        ');
        $query->execute([
            'airlineId'               => $data['airlineId'],
            'countryId'               => $data['countryId'],
            'languageId'              => $data['languageId'],
            'email'                   => $data['email'],
            'fax'                     => $data['fax'],
            'branchAddress'           => $data['branchAddress'],
            'headquartersAddress'     => $data['headquartersAddress'],
            'isJudicialEnabled'       => $data['isJudicialEnabled'],
            'hasNonInterestAgreement' => $data['hasNonInterestAgreement']
        ]);
    }

    public function editLegalData($data)
    {
        $query = $this->db->prepare('
            UPDATE airlines_legal_data_by_country
            SET
                fk_language = :languageId,
                email = :email,
                fax = :fax,
                branch_address = :branchAddress,
                headquarters_address = :headquartersAddress,
                is_judicial_enabled = :isJudicialEnabled,
                has_non_interest_agreement = :hasNonInterestAgreement
            WHERE fk_airline = :airlineId AND fk_country = :countryId;
        ');
        $query->execute([
            'airlineId'               => $data['airlineId'],
            'countryId'               => $data['countryId'],
            'languageId'              => $data['languageId'],
            'email'                   => $data['email'],
            'fax'                     => $data['fax'],
            'branchAddress'           => $data['branchAddress'],
            'headquartersAddress'     => $data['headquartersAddress'],
            'isJudicialEnabled'       => $data['isJudicialEnabled'],
            'hasNonInterestAgreement' => $data['hasNonInterestAgreement']
        ]);
    }

    public function deleteLegalData($airlineId, $countryId)
    {
        $query = $this->db->prepare('
            DELETE FROM airlines_legal_data_by_country WHERE fk_airline = :airlineId AND fk_country = :countryId;
        ');
        $query->execute([ 'airlineId' => $airlineId, 'countryId' => $countryId ]);
    }

    public function legalDataExistsForAirlineAndCountry($airlineId, $countryId)
    {
        $query = $this->db->prepare('
            SELECT COUNT(fk_airline) as counter FROM airlines_legal_data_by_country 
            WHERE fk_airline = :airlineId AND fk_country = :countryId;
        ');
        $query->execute([ 'airlineId' => $airlineId, 'countryId' => $countryId ]);

        return (int)$query->fetch()['counter'] > 0;
    }

    public function hasAirlineOfficeInEurope($airlineId): bool
    {
        $query = $this->db->prepare('
            SELECT COUNT(aldbc.fk_airline) as counter
            FROM airlines_legal_data_by_country aldbc
            LEFT JOIN countries c ON c.id = aldbc.fk_country
            WHERE
                aldbc.fk_airline = :airlineId AND
                c.is_ue = 1
        ');
        $query->execute([ 'airlineId' => $airlineId ]);

        return (int)$query->fetch()['counter'] > 0;
    }

    public function airlineHasAddressInCountryByAddressType($airlineId, $countryId, $addressType): bool
    {
        $query = $this->db->prepare('
            SELECT COUNT(*) as counter
            FROM airlines_legal_data_by_country
            WHERE
                fk_airline = :airlineId AND
                fk_country = :countryId AND
                ' . $addressType . ' IS NOT NULL
        ');
        $query->execute([ 'airlineId' => $airlineId, 'countryId' => $countryId ]);

        return (int)$query->fetch()['counter'] > 0;
    }



    /************************************
     * AIRLINES REQUIRED DOC BY COUNTRY *
     ************************************/

    public function getAllCountryRequiredDocByAirlineId($airlineId): array
    {
        $query = $this->db->prepare('
            SELECT 
                ardbc.fk_country as countryId,
                c.name as countryName,
                ardbc.fk_documentation_type as documentationTypeId,
                dt.name as documentationTypeName                
            FROM airlines_required_documentation_by_country as ardbc
            LEFT JOIN countries c ON c.id = ardbc.fk_country
            LEFT JOIN documentation_types dt ON dt.id = ardbc.fk_documentation_type
            WHERE ardbc.fk_airline = :airlineId;
        ');
        $query->execute([ 'airlineId' => $airlineId ]);

        return $query->fetchAll();
    }

    public function createAirlineRequiredDoc($data)
    {
        $query = $this->db->prepare('
            INSERT INTO airlines_required_documentation_by_country (fk_airline, fk_country, fk_documentation_type)
            VALUES (:airlineId, :countryId, :documentationTypeId);
        ');
        $query->execute([
            'airlineId'               => $data['airlineId'],
            'countryId'               => $data['countryId'],
            'documentationTypeId'     => $data['documentationTypeId']
        ]);
    }

    public function deleteAirlineRequiredDoc($data)
    {
        $query = $this->db->prepare('
            DELETE FROM airlines_required_documentation_by_country 
            WHERE fk_airline = :airlineId AND fk_country = :countryId AND fk_documentation_type = :documentationTypeId;
        ');
        $query->execute([
            'airlineId'               => $data['airlineId'],
            'countryId'               => $data['countryId'],
            'documentationTypeId'     => $data['documentationTypeId']
        ]);
    }

    public function airlineRequiredDocExists($data)
    {
        $query = $this->db->prepare('
            SELECT COUNT(fk_airline) as counter FROM airlines_required_documentation_by_country 
            WHERE fk_airline = :airlineId AND fk_country = :countryId AND fk_documentation_type = :documentationTypeId;
        ');
        $query->execute([
            'airlineId'               => $data['airlineId'],
            'countryId'               => $data['countryId'],
            'documentationTypeId'     => $data['documentationTypeId']
        ]);

        return (int)$query->fetch()['counter'] > 0;
    }
}