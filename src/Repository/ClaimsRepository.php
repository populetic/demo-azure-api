<?php

namespace App\Repository;

use App\Entity\Claim;
use App\Model\ConstantsModel;

class ClaimsRepository extends BaseRepository
{
    /********************
     ****** CLAIMS ******
     ********************/

    private $whereArray = [];

    public function getAll($filters): array
    {
        $where = $this->getWhereByFilters($filters);

        $sql = '
            SELECT
                c.id,
                c.ref,
                c.fk_flight as flightId,
                f.number as flightNumber,
                c.finished_at as finishedAt,
                DATE_FORMAT(c.finished_at, "%d/%m/%Y") as formattedFinishedAt,
                c.amount_estimated as amountEstimated,
                c.amount_received as amountReceived,
                c.reservation_number as reservationNumber,
                cli.name as clientName,
                cli.surnames as clientSurname,
                CONCAT(cli.name, " ", cli.surnames) AS clientCompleteName,
                lang.id as languageId,
                lang.code as languageCode,
                c.fk_claim_status as claimStatusId,
                cs.name as claimStatusName,
                c.fk_claim_documentation_status as claimDocumentationStatusId,
                cds.name as claimDocumentationStatusName,
                al.id as airlineId,
                al.name as airlineName,
                f.date as flightDate,
                DATE_FORMAT(f.date, "%d/%m/%Y") as formattedFlightDate,
                c.fk_disruption_type as disruptionTypeId,
                dt.name as disruptionTypeName,
                p.id as partnerId,
                p.business_name as partnerBusinessName,
                us.id as userId,
                us.username,
                c.fk_law_firm as lawFirmId
            FROM claims c
            LEFT JOIN clients cli ON cli.id = c.fk_client
            LEFT JOIN translation_system.languages lang ON lang.id = cli.fk_language
            LEFT JOIN users us ON us.id = c.fk_user
            LEFT JOIN claim_status cs ON cs.id = c.fk_claim_status
            LEFT JOIN claim_documentation_status cds ON cds.id = c.fk_claim_documentation_status
            LEFT JOIN disruption_types dt ON dt.id = c.fk_disruption_type
            LEFT JOIN flights f ON f.id = c.fk_flight
            LEFT JOIN airlines al ON al.id = f.fk_airline
            LEFT JOIN partners_claims pc ON pc.fk_claim = c.id
            LEFT JOIN partners p ON p.id = pc.fk_partner
            ' . $where . '
            ORDER BY c.id DESC';

        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function getAllByEmailAndStatus($email, $claimStatusId): array
    {
        $query = $this->db->prepare('
            SELECT
                c.id,
                c.ref,
                f.number as flightNumber,
                c.amount_received as amountReceived,
                c.fk_client as clientId,
                CONCAT(cli.name, " ", cli.surnames) as clientCompleteName,
                cli.id_card as idCard,
                cli.fk_documentation_type as documentationTypeId,
                cli.email,
                lang.id as languageId,
                lang.code as languageCode,
                IF(cli.fk_legal_guardian is NULL, 0, 1) as isMinor
            FROM claims c
            LEFT JOIN clients cli ON cli.id = c.fk_client
            LEFT JOIN translation_system.languages lang ON lang.id = cli.fk_language
            LEFT JOIN flights f ON f.id = c.fk_flight
            WHERE
                cli.email = :email AND
                c.fk_claim_status = :claimStatusId AND
                c.ref IS NOT NULL
            ORDER BY c.id DESC
        ');
        $query->execute([ 'email' => $email, 'claimStatusId' => $claimStatusId ]);

        return $query->fetchAll();
    }

    public function getById($id, $extraData = false): array
    {
        if ($extraData) {
            $sql = 'SELECT ' .
                        $this->claimEntityFields . ',' .
                        $this->claimExtraFields . ',' .
                        $this->claimAdditionalInformationFields . '
                    FROM claims c ' .
                    $this->claimExtraJoins . ' ' .
                    $this->claimAdditionalInformationJoin;
        } else {
            $sql = 'SELECT ' . $this->claimEntityFields . ' FROM claims c';
        }

        $query = $this->db->prepare($sql . ' WHERE c.id = :id');
        $query->execute([ 'id' => $id ]);

        $data = $query->fetch();

        if ($data && !$extraData) {
            $data['additionalInformation'] = $this->getClaimAdditionalInformationByClaimId($id);
        }

        return $data ? $data : [];
    }

    public function getByRef($ref): array
    {
        $query = $this->db->prepare(
            'SELECT ' .
                $this->claimEntityFields . ',' .
                $this->claimExtraFields . ',' .
                $this->claimAdditionalInformationFields . '
            FROM claims c ' .
            $this->claimExtraJoins . ' ' .
            $this->claimAdditionalInformationJoin . '
            WHERE c.ref = :ref
        ');
        $query->execute([ 'ref' => $ref ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function getClaimAdditionalInformationByClaimId($claimId): array
    {
        $query = $this->db->prepare('
            SELECT
                fk_claim as claimId,
                amount_initial_compensation as amountInitialCompensation,
                fk_airline_alternative as airlineAlternativeId,
                fk_luggage_issue_moment as luggageIssueMomentId,
                luggage_damage_description as luggageDamageDescription,
                luggage_received_at as luggageReceivedAt
            FROM claim_additional_information
            WHERE fk_claim = :claimId
        ');
        $query->execute([ 'claimId' => $claimId ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function getAllCompanionsByMainClaimId($id): array
    {
        $query = $this->db->prepare('
            SELECT
                c.id as companionClaimId,
                cli.name as companionName,
                cli.surnames as companionSurnames,
                cli.email as companionEmail,
                c.fk_claim_status as claimStatusId,
                c.fk_claim_documentation_status as claimDocumentationStatusId,
                c.amount_estimated as amountEstimated,
                c.amount_tickets as amountTickets,
                IF(c.finished_at is NULL, 0, 1) as isFinished
            FROM claims c
            LEFT JOIN clients cli ON cli.id = c.fk_client
            WHERE
                c.main_claim_id = :id AND
                (c.fk_law_firm IS NULL OR
                c.fk_law_firm = (SELECT c2.fk_law_firm FROM claims as c2 WHERE c2.id = :id))
        ');
        $query->execute([ 'id' => $id ]);

        return $query->fetchAll();
    }

    public function getAllPossibleSameFlightTravelersByClaimId($id, $flightData, $sameFlightsIds): array
    {
        $query = $this->db->prepare('
            SELECT
                c.id as claimId,
                cli.name as name,
                cli.surnames as surnames,
                cli.email as email,
                cli.id_card as idCard,
                c.fk_claim_status as claimStatusId,
                c.fk_claim_documentation_status as claimDocumentationStatusId,
                c.amount_estimated as amountEstimated,
                c.amount_tickets as amountTickets,
                c.finished_at as finishedAt
            FROM claims c
            LEFT JOIN clients cli ON cli.id = c.fk_client
            LEFT JOIN flights f ON f.id = c.fk_flight
            WHERE
                c.id != :id AND
                (c.main_claim_id IS NULL OR c.main_claim_id != :id) AND
                c.fk_disruption_type = :disruptionTypeId AND
                c.fk_flight IN (:sameFlightIds) AND
                (c.fk_law_firm IS NULL OR
                c.fk_law_firm = (SELECT c2.fk_law_firm FROM claims as c2 WHERE c2.id = :id))
        ');

        $query->execute([
            'id'               => $id,
            'disruptionTypeId' => $flightData['disruptionTypeId'],
            'sameFlightIds'    => implode(',', $sameFlightsIds)
        ]);

        return $query->fetchAll();
    }

    public function getAllDemandsByClaimId($id): array
    {
        $query = $this->db->prepare('
            SELECT
                d.id as demandId,
                d.presented_at as presentedAt,
                ct.id as courtTypeId,
                ct.name as courtTypeName,
                ct.fk_country as courtTypeCountryId,
                cn.number as courtNumber,
                c.fk_city as courtCityId,
                ci.name as courtCityName,
                d.amount_requested as amountRequested,
                d.cancellated_at as cancellatedAt
            FROM demands d
            LEFT JOIN demand_related_claims drc ON drc.fk_demand = d.id
            LEFT JOIN courts_number cn ON cn.id = d.fk_court_number
            LEFT JOIN courts c ON c.id = cn.fk_court
            LEFT JOIN court_types ct ON ct.id = c.fk_court_type
            LEFT JOIN cities ci ON ci.id = c.fk_city
            WHERE drc.fk_claim = :id
        ');

        $query->execute([ 'id' => $id ]);

        return $query->fetchAll();
    }

    public function getAllBillsByClaimId($id): array
    {
        $query = $this->db->prepare('
            SELECT
                bt.series,
                b.number,
                b.created_at as createdAt,
                b.total
            FROM bills b
            LEFT JOIN claims c ON c.id = b.fk_claim
            LEFT JOIN bill_types bt ON bt.id = b.fk_bill_type
            WHERE b.fk_claim = :id
        ');

        $query->execute([ 'id' => $id ]);

        return $query->fetchAll();
    }

    public function create(Claim $claim): int
    {
        $query = $this->db->prepare('
            INSERT INTO claims
            (ref, fk_client, fk_claim_status, fk_claim_documentation_status, fk_flight, is_onboarding_flight,
             reservation_number, fk_disruption_type, amount_estimated, amount_tickets, amount_received, fee,
             has_connection_flight, fk_delay_option, fk_airline_reason, description, main_claim_id,
             is_remarketing_enabled, is_first_remarketing_sent, is_claimable, is_extrajudicial_airline_negative,
             fk_resolution_way, fk_law_firm, fk_user, finished_at, extrajudicial_sent_at)
            VALUES
            (:ref, :clientId, :claimStatusId, :claimDocumentationStatusId, :flightId, :isOnboardingFlight,
             :reservationNumber, :disruptionTypeId, :amountEstimated, :amountTickets, :amountReceived, :fee,
             :hasConnectionFlight, :delayOptionId, :airlineReasonId, :description, :mainClaimId,
             :isRemarketingEnabled, :isFirstRemarketingSent, :isClaimable, :isExtrajudicialAirlineNegative,
             :resolutionWayId, :lawFirmId, :userId, :finishedAt, :extrajudicialSentAt)
        ');
        $query->execute([
            'ref'                            => $claim->ref,
            'clientId'                       => $claim->clientId,
            'claimStatusId'                  => $claim->claimStatusId,
            'claimDocumentationStatusId'     => $claim->claimDocumentationStatusId,
            'flightId'                       => $claim->flightId,
            'isOnboardingFlight'             => $claim->isOnboardingFlight,
            'reservationNumber'              => $claim->reservationNumber,
            'disruptionTypeId'               => $claim->disruptionTypeId,
            'amountEstimated'                => $claim->amountEstimated,
            'amountTickets'                  => $claim->amountTickets,
            'amountReceived'                 => $claim->amountReceived,
            'fee'                            => $claim->fee,
            'hasConnectionFlight'            => $claim->hasConnectionFlight,
            'delayOptionId'                  => $claim->delayOptionId,
            'airlineReasonId'                => $claim->airlineReasonId,
            'description'                    => $claim->description,
            'mainClaimId'                    => $claim->mainClaimId,
            'isRemarketingEnabled'           => $claim->isRemarketingEnabled,
            'isFirstRemarketingSent'         => $claim->isFirstRemarketingSent,
            'isClaimable'                    => $claim->isClaimable,
            'isExtrajudicialAirlineNegative' => $claim->isExtrajudicialAirlineNegative,
            'resolutionWayId'                => $claim->resolutionWayId,
            'lawFirmId'                      => $claim->lawFirmId,
            'userId'                         => $claim->userId,
            'finishedAt'                     => $claim->finishedAt,
            'extrajudicialSentAt'            => $claim->extrajudicialSentAt
        ]);

        return $this->db->lastInsertId();
    }

    public function edit(Claim $claim)
    {
        $query = $this->db->prepare('
            UPDATE claims
            SET
                ref = :ref,
                fk_client = :clientId,
                fk_claim_status = :claimStatusId,
                fk_claim_documentation_status = :claimDocumentationStatusId,
                fk_flight = :flightId,
                is_onboarding_flight = :isOnboardingFlight,
                reservation_number = :reservationNumber,
                fk_disruption_type = :disruptionTypeId,
                amount_estimated = :amountEstimated,
                amount_tickets = :amountTickets,
                amount_received = :amountReceived,
                fee = :fee,
                has_connection_flight = :hasConnectionFlight,
                fk_delay_option = :delayOptionId,
                fk_airline_reason = :airlineReasonId,
                fk_lawsuite_type = :lawsuiteTypeId,
                description = :description,
                main_claim_id = :mainClaimId,
                is_remarketing_enabled = :isRemarketingEnabled,
                is_first_remarketing_sent = :isFirstRemarketingSent,
                is_claimable = :isClaimable,
                is_extrajudicial_airline_negative = :isExtrajudicialAirlineNegative,
                fk_resolution_way = :resolutionWayId,
                fk_law_firm = :lawFirmId,
                fk_user = :userId,
                finished_at = :finishedAt,
                extrajudicial_sent_at = :extrajudicialSentAt
            WHERE id = :id
        ');
        $query->execute([
            'id'                             => $claim->id,
            'ref'                            => $claim->ref,
            'clientId'                       => $claim->clientId,
            'claimStatusId'                  => $claim->claimStatusId,
            'claimDocumentationStatusId'     => $claim->claimDocumentationStatusId,
            'flightId'                       => $claim->flightId,
            'isOnboardingFlight'             => $claim->isOnboardingFlight,
            'reservationNumber'              => $claim->reservationNumber,
            'disruptionTypeId'               => $claim->disruptionTypeId,
            'amountEstimated'                => $claim->amountEstimated,
            'amountTickets'                  => $claim->amountTickets,
            'amountReceived'                 => $claim->amountReceived,
            'fee'                            => $claim->fee,
            'hasConnectionFlight'            => $claim->hasConnectionFlight,
            'delayOptionId'                  => $claim->delayOptionId,
            'airlineReasonId'                => $claim->airlineReasonId,
            'lawsuiteTypeId'                 => $claim->lawsuiteTypeId,
            'description'                    => $claim->description,
            'mainClaimId'                    => $claim->mainClaimId,
            'isRemarketingEnabled'           => $claim->isRemarketingEnabled,
            'isFirstRemarketingSent'         => $claim->isFirstRemarketingSent,
            'isClaimable'                    => $claim->isClaimable,
            'isExtrajudicialAirlineNegative' => $claim->isExtrajudicialAirlineNegative,
            'resolutionWayId'                => $claim->resolutionWayId,
            'lawFirmId'                      => $claim->lawFirmId,
            'userId'                         => $claim->userId,
            'finishedAt'                     => $claim->finishedAt,
            'extrajudicialSentAt'            => $claim->extrajudicialSentAt
        ]);

        if ($claim->additionalInformation) {
            $this->deleteAdditionalInformation($claim->id);
            $this->createAdditionalInformation($claim);
        }
    }

    public function delete($id)
    {
        $this->deleteAdditionalInformation($id);
        $this->deleteAllClaimCompetencesAndPlacements($id);
        $query = $this->db->prepare('DELETE FROM claims WHERE id = :id');
        $query->execute([ 'id' => $id ]);
    }

    private function createAdditionalInformation(Claim $claim)
    {
        $query = $this->db->prepare('
            INSERT INTO claim_additional_information
            (fk_claim, amount_initial_compensation, fk_airline_alternative,
             fk_luggage_issue_moment, luggage_damage_description, luggage_received_at)
            VALUES
            (:claimId, :amountInitialCompensation, :airlineAlternativeId,
             :luggageIssueMomentId, :luggageDamageDescription, :luggageReceivedAt)
        ');
        $query->execute([
            'claimId'                   => $claim->id,
            'amountInitialCompensation' => $claim->additionalInformation['amountInitialCompensation'],
            'airlineAlternativeId'      => $claim->additionalInformation['airlineAlternativeId'],
            'luggageIssueMomentId'      => $claim->additionalInformation['luggageIssueMomentId'],
            'luggageDamageDescription'  => $claim->additionalInformation['luggageDamageDescription'],
            'luggageReceivedAt'         => $claim->additionalInformation['luggageReceivedAt'],
        ]);
    }

    private function deleteAdditionalInformation($claimId)
    {
        $query = $this->db->prepare('DELETE FROM claim_additional_information WHERE fk_claim = :claimId');
        $query->execute([ 'claimId' => $claimId ]);
    }

    public function getAllRelatedClaimIds($claimId)
    {
        $query = $this->db->prepare('SELECT id FROM claims WHERE id = :claimId OR main_claim_id = :claimId;');
        $query->execute([ 'claimId' => $claimId ]);

        return array_column($query->fetchAll(), 'id');
    }

    public function getRefByClaimId($claimId)
    {
        $query = $this->db->prepare('SELECT ref FROM claims WHERE id = :id');
        $query->execute([ 'id' => $claimId ]);

        return $query->fetch()['ref'];
    }

    public function getClaimFlightDataToCheckIfDuplicated($claimId): array
    {
        $query = $this->db->prepare('
            SELECT
                c.fk_flight as flightId,
                c.fk_disruption_type as disruptionTypeId,
                fli.number as flightNumber,
                fli.date as flightDate
            FROM claims c
            LEFT JOIN flights fli ON fli.id = c.fk_flight
            WHERE c.id = :claimId
        ');
        $query->execute([ 'claimId' => $claimId ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function getIfPossibleDuplicatedClaim($claimId, $clientName, $clientSurnames, $clientEmail, $data): array
    {
        $query = $this->db->prepare('
            SELECT c.id
            FROM claims c
            LEFT JOIN flights as fli ON fli.id = c.fk_flight
            LEFT JOIN clients as cli ON cli.id = c.fk_client
            WHERE
                c.id != :claimId AND
                c.is_claimable = 1 AND
                c.ref IS NOT NULL AND
                c.fk_disruption_type = :disruptionTypeId AND
                c.finished_at IS NOT NULL AND
                (c.fk_flight = :flightId OR (fli.number = :flightNumber AND fli.date = :flightDate)) AND
                cli.email = :clientEmail AND
                cli.name = :clientName AND
                cli.surnames = :clientSurnames
            ORDER BY id
        ');
        $query->execute([
            'claimId'          => $claimId,
            'disruptionTypeId' => $data['disruptionTypeId'],
            'flightId'         => $data['flightId'],
            'flightNumber'     => $data['flightNumber'],
            'flightDate'       => $data['flightDate'],
            'clientEmail'      => $clientEmail,
            'clientName'       => $clientName,
            'clientSurnames'   => $clientSurnames
        ]);

        return $query->fetchAll();
    }

    public function countClaimsByClaimStatusId($claimStatusId): int
    {
        $query = $this->db->prepare('SELECT COUNT(id) as counter FROM claims WHERE fk_claim_status = :claimStatusId');
        $query->execute([ 'claimStatusId' => $claimStatusId ]);

        return (int)$query->fetch()['counter'];
    }

    public function getOldestDateWithStatusPopuleticCollected($maxDate = null): array
    {
        $andWhere = $maxDate ? ('AND lcs.created_at > "' . $maxDate . '"') : '';

        $query = $this->db->prepare('
            SELECT lcs.created_at as date
            FROM claims c
            INNER JOIN logs_claim_status lcs ON lcs.fk_claim = c.id
            WHERE c.fk_claim_status = :popuCollected ' . $andWhere . '
            ORDER BY lcs.created_at
            LIMIT 1
        ');
        $query->execute([ 'popuCollected' => ConstantsModel::CLAIM_STATUS_POPULETIC_COLLECTED ]);

        return $query->fetch();
    }

    public function changeClaimStatus($claimId, $claimStatusId)
    {
        $query = $this->db->prepare('UPDATE claims SET fk_claim_status = :claimStatusId WHERE id = :claimId');
        $query->execute([ 'claimId' => $claimId, 'claimStatusId' => $claimStatusId ]);
    }

    public function changeClaimLawFirm($claimId, $lawFirmId)
    {
        $query = $this->db->prepare('UPDATE claims SET fk_law_firm = :lawFirmId WHERE id = :claimId AND fk_law_firm IS NULL');
        $query->execute([ 'claimId' => $claimId, 'lawFirmId' => $lawFirmId ]);
    }

    public function createCompanionClaim($mainClaim, $clientId): int
    {
        $query = $this->db->prepare('
            INSERT INTO claims
            (ref, fk_client, fk_claim_status, fk_claim_documentation_status, fk_flight, is_onboarding_flight,
             fk_disruption_type, amount_estimated, fee, has_connection_flight, fk_delay_option, fk_airline_reason,
             fk_lawsuite_type, main_claim_id, is_claimable, fk_law_firm)
            VALUES
            (:ref, :clientId, :claimStatusId, :claimDocumentationStatusId, :flightId, :isOnboardingFlight,
             :disruptionTypeId, :amountEstimated, :fee, :hasConnectionFlight, :delayOptionId, :airlineReasonId,
             :lawsuiteTypeId, :mainClaimId, :isClaimable, :lawFirmId)
        ');
        $query->execute([
            'ref'                            => $mainClaim->ref,
            'clientId'                       => $clientId,
            'claimStatusId'                  => ConstantsModel::CLAIM_STATUS_PENDING_TO_ASSIGN,
            'claimDocumentationStatusId'     => ConstantsModel::CLAIM_DOC_STATUS_REVIEW_PENDING,
            'flightId'                       => $mainClaim->flightId,
            'isOnboardingFlight'             => $mainClaim->isOnboardingFlight,
            'disruptionTypeId'               => $mainClaim->disruptionTypeId,
            'amountEstimated'                => $mainClaim->amountEstimated,
            'fee'                            => $mainClaim->fee,
            'hasConnectionFlight'            => $mainClaim->hasConnectionFlight,
            'delayOptionId'                  => $mainClaim->delayOptionId,
            'airlineReasonId'                => $mainClaim->airlineReasonId,
            'lawsuiteTypeId'                 => $mainClaim->lawsuiteTypeId,
            'mainClaimId'                    => $mainClaim->id,
            'isClaimable'                    => $mainClaim->isClaimable,
            'lawFirmId'                      => $mainClaim->lawFirmId
        ]);

        return $this->db->lastInsertId();
    }

    public function duplicateClaimAdditionalInfoForCompanion($mainClaimId, $companionClaimId)
    {
        $claimAddInfo = $this->getClaimAdditionalInformationByClaimId($mainClaimId);

        if ($claimAddInfo) {
            $query = $this->db->prepare('
                INSERT INTO claim_additional_information
                (fk_claim, amount_initial_compensation, fk_airline_alternative,
                 fk_luggage_issue_moment, luggage_damage_description, luggage_received_at)
                VALUES
                (:claimId, :amountInitialCompensation, :airlineAlternativeId,
                 :luggageIssueMomentId, :luggageDamageDescription, :luggageReceivedAt)
            ');
            $query->execute([
                'claimId'                   => $companionClaimId,
                'amountInitialCompensation' => $claimAddInfo['amountInitialCompensation'],
                'airlineAlternativeId'      => $claimAddInfo['airlineAlternativeId'],
                'luggageIssueMomentId'      => $claimAddInfo['luggageIssueMomentId'],
                'luggageDamageDescription'  => $claimAddInfo['luggageDamageDescription'],
                'luggageReceivedAt'         => $claimAddInfo['luggageReceivedAt']
            ]);
        }
    }

    public function setCompanionForClaim($mainClaimId, $companionClaimId)
    {
        $query = $this->db->prepare('UPDATE claims SET main_claim_id = :mainClaimId WHERE id = :companionClaimId');
        $query->execute([ 'mainClaimId' => $mainClaimId, 'companionClaimId' => $companionClaimId ]);
    }

    public function claimHasInvalidDocumentation($claimId)
    {
        $query = $this->db->prepare('
            SELECT COUNT(id) as counter
            FROM documentation
            WHERE
                fk_claim = :claimId AND
                is_valid = 0 AND
                to_delete = 0 AND
                not_available = 0
        ');
        $query->execute([ 'claimId' => $claimId ]);

        return (int)$query->fetch()['counter'] > 0;
    }

    public function detachClaimLawFirm($claimId)
    {
        $query = $this->db->prepare('UPDATE claims SET fk_law_firm = NULL WHERE id = :claimId');
        $query->execute([ 'claimId' => $claimId ]);
    }

    public function assignClaimToUserId($claimId, $userId)
    {
        $query = $this->db->prepare('UPDATE claims SET fk_user = :userId WHERE id = :claimId');
        $query->execute([ 'claimId' => $claimId, 'userId' => $userId ]);
    }

    private $claimEntityFields = '
        c.id,
        c.ref,
        c.fk_client as clientId,
        c.fk_claim_status as claimStatusId,
        c.fk_claim_documentation_status as claimDocumentationStatusId,
        c.fk_flight as flightId,
        c.is_onboarding_flight as isOnboardingFlight,
        c.reservation_number as reservationNumber,
        c.fk_disruption_type as disruptionTypeId,
        c.amount_estimated as amountEstimated,
        c.amount_tickets as amountTickets,
        c.amount_received as amountReceived,
        c.fee,
        c.has_connection_flight as hasConnectionFlight,
        c.fk_delay_option as delayOptionId,
        c.fk_airline_reason as airlineReasonId,
        c.fk_lawsuite_type as lawsuiteTypeId,
        c.description,
        c.main_claim_id as mainClaimId,
        c.is_remarketing_enabled as isRemarketingEnabled,
        c.is_first_remarketing_sent as isFirstRemarketingSent,
        c.is_claimable as isClaimable,
        c.is_extrajudicial_airline_negative as isExtrajudicialAirlineNegative,
        c.fk_resolution_way as resolutionWayId,
        c.fk_law_firm as lawFirmId,
        c.fk_user as userId,
        c.finished_at as finishedAt,
        c.extrajudicial_sent_at as extrajudicialSentAt,
        c.created_at as createdAt,
        c.updated_at as updatedAt
    ';

    private $claimExtraFields = '
        cli.name as clientName,
        cli.surnames as clientSurname,
        cli.fk_country as clientCountryId,
        CONCAT(cli.name, " ", cli.surnames) AS clientCompleteName,
        cli.email as clientEmail,
        lang.id as languageId,
        lang.code as languageCode,
        cs.name as claimStatusName,
        cds.name as claimDocumentationStatusName,
        al.id as airlineId,
        al.name as airlineName,
        al.iata as airlineIata,
        f.date as flightDate,
        dt.name as disruptionTypeName,
        p.business_name as partnerBusinessName,
        drc.fk_demand as demandId,
        cc.fk_country as claimCompetenceCountryId,
        cp.fk_country as claimPlacementCountryId,
        us.id as userId,
        us.username,
        CONCAT(us.name, " ", us.surnames) as userCompleteName,
        rw.name as resolutionWayName
    ';

    private $claimAdditionalInformationFields = '
        cai.amount_initial_compensation as amountInitialCompensation,
        cai.fk_airline_alternative as airlineAlternativeId,
        alal.name as airlineAlternativeName,
        cai.fk_luggage_issue_moment as luggageIssueMomentId,
        lim.name as luggageIssueMomentName,
        cai.luggage_damage_description as luggageDamageDescription,
        cai.luggage_received_at as luggageReceivedAt
    ';

    private $claimExtraJoins = '
        LEFT JOIN clients cli ON cli.id = c.fk_client
        LEFT JOIN translation_system.languages lang ON lang.id = cli.fk_language
        LEFT JOIN users us ON us.id = c.fk_user
        LEFT JOIN claim_status cs ON cs.id = c.fk_claim_status
        LEFT JOIN claim_documentation_status cds ON cds.id = c.fk_claim_documentation_status
        LEFT JOIN disruption_types dt ON dt.id = c.fk_disruption_type
        LEFT JOIN flights f ON f.id = c.fk_flight
        LEFT JOIN airlines al ON al.id = f.fk_airline
        LEFT JOIN partners_claims pc ON pc.fk_claim = c.id
        LEFT JOIN partners p ON p.id = pc.fk_partner
        LEFT JOIN demand_related_claims drc ON drc.fk_claim = c.id
        LEFT JOIN claims_competence cc ON cc.fk_claim = c.id AND cc.is_selected = 1
        LEFT JOIN claims_placement cp ON cp.fk_claim = c.id AND cp.is_selected = 1
        LEFT JOIN resolution_ways rw on rw.id = c.fk_resolution_way
    ';

    private $claimAdditionalInformationJoin = '
        LEFT JOIN claim_additional_information cai ON cai.fk_claim = c.id
        LEFT JOIN airline_alternatives alal ON alal.id = cai.fk_airline_alternative
        LEFT JOIN luggage_issue_moment lim ON lim.id = cai.fk_luggage_issue_moment
    ';

    private function getWhereByFilters($filters): string
    {
        $user = $this->getUserData($filters['userId']);

        if ($user['departmentId'] == ConstantsModel::DEPARTMENT_ID_LEGAL) {
            $this->whereArray[] = 'c.fk_claim_status = ' . ConstantsModel::CLAIM_STATUS_PENDING_LEGAL_DEPT;
            $this->setFilterByLawFirmIfIsSet($filters['lawFirmId']);

            if ($user['roleId'] == ConstantsModel::ROLE_ID_JUNIOR_LAWYER) {
                $this->whereArray[] = '
                    c.fk_disruption_type IN (' .
                        ConstantsModel::DISR_TYPE_ID_DELAY . ',' . ConstantsModel::DISR_TYPE_ID_CANCELLATION . '
                    ) AND
                    (SELECT count(*) FROM claim_connection_flights WHERE fk_claim = c.id) = 0';
            }

        } elseif ($user['departmentId'] == ConstantsModel::DEPARTMENT_ID_CUSTOMER_SERVICE) {
            $this->setFilterByLawFirmIfIsSet($filters['lawFirmId']);
            $this->setFilterByClaimStatusIdsIfIsSet($filters['claimStatusIds']);
            if ($user['isAdmin'] == 0) $this->setFilterByUser($filters['userId']);

        } else {
            // OTHER DEPTS
            $this->setFilterByLawFirmIfIsSet($filters['lawFirmId']);
            $this->setFilterByClaimStatusIdsIfIsSet($filters['claimStatusIds']);
            $this->setFilterByUser($filters['userId']);
        }

        $this->setCommonFiltersIfTheyAreSet($filters);

        $where = implode(' AND ', $this->whereArray);

        return $where ? ' WHERE ' . $where : '';
    }

    private function getUserData($userId): array
    {
        $query = $this->db->prepare('
                SELECT
                    u.fk_law_firm as lawFirmId,
                    lf.fk_country as lawFirmCountryId,
                    r.id as roleId,
                    r.fk_department as departmentId,
                    r.is_admin as isAdmin
                FROM users u
                LEFT JOIN law_firms lf ON lf.id = u.fk_law_firm
                LEFT JOIN statia.roles r ON r.id = u.fk_role
                WHERE u.id = :userId
            ');
        $query->execute([ 'userId' => $userId ]);

        return $query->fetch();
    }

    private function setFilterByUser($userId)
    {
        $this->whereArray[] = '(c.fk_user IS NULL OR c.fk_user = ' . $userId . ')';
    }

    private function setFilterByLawFirmIfIsSet($lawFirmId)
    {
        if ($lawFirmId !== null) {
            $this->whereArray[] = $lawFirmId ? 'c.fk_law_firm = ' . $lawFirmId : 'c.fk_law_firm IS NULL';
        }
    }

    private function setFilterByClaimStatusIdsIfIsSet($claimStatusIds)
    {
        if ($claimStatusIds !== null) {
            $this->whereArray[] = 'c.fk_claim_status IN (' . $claimStatusIds . ')';
        }
    }

    private function setCommonFiltersIfTheyAreSet($filters)
    {
        if ($filters['finished'] !== null) {
            $this->whereArray[] = $filters['finished'] ? 'c.finished_at IS NOT NULL' : 'c.finished_at IS NULL';
        }

        if ($filters['closed'] !== null) {
            $this->whereArray[] = 'cs.closed = ' . $filters['closed'];
        }

        if ($filters['languageId'] !== null) {
            $this->whereArray[] = 'cli.fk_language = ' . $filters['languageId'];
        }
    }



    /**********************
     **** CLAIM STATUS ****
     **********************/

    private $claimStatusFieldsToGet = 'SELECT id, name, closed FROM claim_status';

    public function getAllClaimStatus(): array
    {
        $query = $this->db->prepare($this->claimStatusFieldsToGet);
        $query->execute();

        return $query->fetchAll();
    }

    public function getAllClaimStatusByRoleAndCountry($roleId, $countryId): array
    {
        $query = $this->db->prepare('
            SELECT
                cs.id,
                cs.name,
                cs.closed,
                csbr.is_readable as isReadable,
                csbr.is_editable as isEditable,
                csc.order
            FROM claim_status cs
            INNER JOIN claim_status_countries csc ON csc.fk_claim_status = cs.id
            INNER JOIN claim_status_by_role csbr ON csbr.fk_claim_status_country = csc.id
            WHERE
                csbr.fk_role = :roleId AND
                csc.fk_country = :countryId
            ORDER BY csc.`order`;
        ');
        $query->execute([ 'roleId' => $roleId, 'countryId' => $countryId ]);

        return $query->fetchAll();
    }

    public function getClaimStatusById($id): array
    {
        $query = $this->db->prepare($this->claimStatusFieldsToGet . ' WHERE id = :id');
        $query->execute([ 'id' => $id ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function createClaimStatus($data): int
    {
        $query = $this->db->prepare('INSERT INTO claim_status (name, closed) VALUES (:name, :closed)');
        $query->execute([ 'name' => $data['name'], 'closed' => $data['closed'] ]);

        return $this->db->lastInsertId();
    }

    public function editClaimStatus($data)
    {
        $query = $this->db->prepare('UPDATE claim_status SET name = :name, closed = :closed WHERE id = :id');
        $query->execute([ 'id' => $data['id'], 'name' => $data['name'], 'closed' => $data['closed'] ]);
    }

    public function deleteClaimStatus($id)
    {
        $query = $this->db->prepare('DELETE FROM claim_status WHERE id = :id');
        $query->execute([ 'id' => $id ]);
    }



    /**************************
     * CLAIM STATUS COUNTRIES *
     **************************/

    private $csCountriesFieldsToGet = '
        SELECT
            csc.id,
            csc.fk_country as countryId,
            c.name as countryName,
            csc.fk_claim_status as claimStatusId,
            cs.name as claimStatusName,
            csc.`order`
        FROM claim_status_countries csc
        LEFT JOIN claim_status cs ON cs.id = csc.fk_claim_status
        LEFT JOIN countries c ON c.id = csc.fk_country
    ';

    public function getAllClaimStatusCountries(): array
    {
        $query = $this->db->prepare(
            $this->csCountriesFieldsToGet . '
            ORDER BY csc.fk_country, csc.order, csc.fk_claim_status
        ');
        $query->execute();

        return $query->fetchAll();
    }

    public function getClaimStatusCountryById($id): array
    {
        $query = $this->db->prepare($this->csCountriesFieldsToGet . ' WHERE csc.id = :id');
        $query->execute([ 'id' => $id ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function createClaimStatusCountry($data): int
    {
        $query = $this->db->prepare('
            INSERT INTO claim_status_countries (fk_country, fk_claim_status, `order`)
            VALUES (:countryId, :claimStatusId, :order)
        ');
        $query->execute([
            'countryId'     => $data['countryId'],
            'claimStatusId' => $data['claimStatusId'],
            'order'         => $data['order']
        ]);

        return $this->db->lastInsertId();
    }

    public function editClaimStatusCountry($data)
    {
        $query = $this->db->prepare('
            UPDATE claim_status_countries
            SET
                fk_country = :countryId,
                fk_claim_status = :claimStatusId,
                `order` = :order
            WHERE id = :id
        ');
        $query->execute([
            'id'            => $data['id'],
            'countryId'     => $data['countryId'],
            'claimStatusId' => $data['claimStatusId'],
            'order'         => $data['order']
        ]);
    }

    public function deleteClaimStatusCountry($id)
    {
        $query = $this->db->prepare('DELETE FROM claim_status_countries WHERE id = :id');
        $query->execute([ 'id' => $id ]);
    }

    public function claimStatusExistsForCountry($countryId, $claimStatusId, $idToAvoid): bool
    {
        $query = $this->db->prepare('
            SELECT COUNT(id) as counter
            FROM claim_status_countries
            WHERE
                fk_country = :countryId AND
                fk_claim_status = :claimStatusId AND
                id != :idToAvoid
        ');
        $query->execute([
            'countryId'     => $countryId,
            'claimStatusId' => $claimStatusId,
            'idToAvoid'     => $idToAvoid === null ? '' : $idToAvoid
        ]);

        return (int)$query->fetch()['counter'] > 0;
    }



    /**************************
     ** CLAIM STATUS BY ROLE **
     **************************/

    private $csByRoleFieldsToGet = '
        SELECT
            csbr.fk_claim_status_country as claimStatusCountryId,
            csbr.fk_role as roleId,
            r.name as roleName,
            csc.fk_country as countryId,
            csc.fk_claim_status as claimStatusId,
            cs.name as claimStatusName,
            csc.order,
            csbr.is_readable as isReadable,
            csbr.is_editable as isEditable
        FROM claim_status_by_role csbr
        LEFT JOIN claim_status_countries csc ON csc.id = csbr.fk_claim_status_country
        LEFT JOIN claim_status cs ON cs.id = csc.fk_claim_status
        LEFT JOIN statia.roles r ON r.id = csbr.fk_role
    ';

    public function getAllClaimStatusByRole(): array
    {
        $query = $this->db->prepare(
            $this->csByRoleFieldsToGet . '
            ORDER BY csbr.fk_claim_status_country, csbr.fk_role
        ');
        $query->execute();

        return $query->fetchAll();
    }

    public function getClaimStatusByRole($claimStatusCountryId, $roleId): array
    {
        $query = $this->db->prepare(
            $this->csByRoleFieldsToGet . '
            WHERE
                fk_claim_status_country = :claimStatusCountryId AND
                fk_role = :roleId
        ');
        $query->execute([
            'claimStatusCountryId' => $claimStatusCountryId,
            'roleId'               => $roleId
        ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function createClaimStatusByRole($data)
    {
        $query = $this->db->prepare('
            INSERT INTO claim_status_by_role (fk_claim_status_country, fk_role, is_readable, is_editable)
            VALUES (:claimStatusCountryId, :roleId, :isReadable, :isEditable)
        ');
        $query->execute([
            'claimStatusCountryId' => $data['claimStatusCountryId'],
            'roleId'               => $data['roleId'],
            'isReadable'           => $data['isReadable'],
            'isEditable'           => $data['isEditable']
        ]);
    }

    public function editClaimStatusByRole($data)
    {
        $query = $this->db->prepare('
            UPDATE claim_status_by_role
            SET
                is_readable = :isReadable,
                is_editable = :isEditable
            WHERE
                fk_claim_status_country = :claimStatusCountryId AND
                fk_role = :roleId
        ');
        $query->execute([
            'claimStatusCountryId' => $data['claimStatusCountryId'],
            'roleId'               => $data['roleId'],
            'isReadable'           => $data['isReadable'],
            'isEditable'           => $data['isEditable']
        ]);
    }

    public function deleteClaimStatusByRole($claimStatusCountryId, $roleId)
    {
        $query = $this->db->prepare('
            DELETE FROM claim_status_by_role
            WHERE
                fk_claim_status_country = :claimStatusCountryId AND
                fk_role = :roleId
        ');
        $query->execute([
            'claimStatusCountryId' => $claimStatusCountryId,
            'roleId'               => $roleId
        ]);
    }

    public function claimStatusByRoleExists($claimStatusCountryId, $roleId): bool
    {
        $query = $this->db->prepare('
            SELECT COUNT(*) as counter
            FROM claim_status_by_role
            WHERE
                fk_claim_status_country = :claimStatusCountryId AND
                fk_role = :roleId
        ');
        $query->execute([
            'claimStatusCountryId' => $claimStatusCountryId,
            'roleId'               => $roleId
        ]);

        return (int)$query->fetch()['counter'] > 0;
    }



    /*******************************
     * CLAIMS COMPETENCE/PLACEMENT *
     *******************************/

    private $competenceOrPlacementFieldsToGet = '
        SELECT
            cl.fk_claim as claimId,
            cl.fk_country as countryId,
            c.name as countryName,
            cl.is_selected as isSelected,
            cl.is_selectable as isSelectable
    ';

    public function getAllClaimsCompetenceOrPlacement($claimId, $type): array
    {
        $query = $this->db->prepare(
            $this->competenceOrPlacementFieldsToGet . '
            FROM claims_' . $type . ' cl
            LEFT JOIN countries c ON c.id = cl.fk_country
            WHERE cl.fk_claim = :claimId
            ORDER BY cl.fk_country
        ');
        $query->execute([ 'claimId' => $claimId ]);

        return $query->fetchAll();
    }

    public function getClaimCompetenceOrPlacement($claimId, $countryId, $type): array
    {
        $query = $this->db->prepare(
            $this->competenceOrPlacementFieldsToGet . '
            FROM claims_' . $type . ' cl
            LEFT JOIN countries c ON c.id = cl.fk_country
            WHERE
                cl.fk_claim = :claimId AND
                cl.fk_country = :countryId
        ');
        $query->execute([ 'claimId' => $claimId, 'countryId' => $countryId ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function createClaimCompetenceOrPlacement($data, $type)
    {
        if (array_key_exists('isSelected', $data) && array_key_exists('isSelectable', $data)) {
            $sql = 'INSERT INTO claims_' . $type . ' (fk_claim, fk_country, is_selected, is_selectable)
                    VALUES (:claimId, :countryId, ' . $data['isSelected'] . ', ' . $data['isSelectable'] . ')';
        } elseif (!array_key_exists('isSelected', $data) && array_key_exists('isSelectable', $data)) {
            $sql = 'INSERT INTO claims_' . $type . ' (fk_claim, fk_country, is_selectable)
                    VALUES (:claimId, :countryId, ' . $data['isSelectable'] . ')';
        } elseif (array_key_exists('isSelected', $data) && !array_key_exists('isSelectable', $data)) {
            $sql = 'INSERT INTO claims_' . $type . ' (fk_claim, fk_country, is_selected)
                    VALUES (:claimId, :countryId, ' . $data['isSelected'] . ')';
        } else {
            $sql = 'INSERT INTO claims_' . $type . ' (fk_claim, fk_country) VALUES (:claimId, :countryId)';
        }

        $query = $this->db->prepare($sql);
        $query->execute([ 'claimId' => $data['claimId'], 'countryId' => $data['countryId'] ]);
    }

    public function editClaimCompetenceOrPlacement($data, $type)
    {
        if (array_key_exists('isSelected', $data) && array_key_exists('isSelectable', $data)) {
            $sql = 'UPDATE claims_' . $type . '
                    SET is_selected = ' . $data['isSelected'] . ', is_selectable = ' . $data['isSelectable'] . '
                    WHERE fk_claim = :claimId AND fk_country = :countryId';
        } elseif (!array_key_exists('isSelected', $data) && array_key_exists('isSelectable', $data)) {
            $sql = 'UPDATE claims_' . $type . '
                    SET is_selectable = ' . $data['isSelectable'] . '
                    WHERE fk_claim = :claimId AND fk_country = :countryId';
        } elseif (array_key_exists('isSelected', $data) && !array_key_exists('isSelectable', $data)) {
            $sql = 'UPDATE claims_' . $type . '
                    SET is_selected = ' . $data['isSelected'] . '
                    WHERE fk_claim = :claimId AND fk_country = :countryId';
        } else {
            $sql = '';
        }

        if ($sql) {
            $query = $this->db->prepare($sql);
            $query->execute([ 'claimId' => $data['claimId'], 'countryId' => $data['countryId'] ]);
        }
    }

    public function deleteAllClaimCompetencesAndPlacements($claimId)
    {
        $query = $this->db->prepare('DELETE FROM claims_competence WHERE fk_claim = :claimId');
        $query->execute([ 'claimId' => $claimId ]);

        $query = $this->db->prepare('DELETE FROM claims_placement WHERE fk_claim = :claimId');
        $query->execute([ 'claimId' => $claimId ]);
    }

    public function deleteClaimCompetenceOrPlacement($claimId, $countryId, $type)
    {
        $query = $this->db->prepare('
            DELETE FROM claims_' . $type . '
            WHERE
                fk_claim = :claimId AND
                fk_country = :countryId
        ');
        $query->execute([ 'claimId' => $claimId, 'countryId' => $countryId ]);
    }

    public function claimCompetenceOrPlacementExists($claimId, $countryId, $type): bool
    {
        $query = $this->db->prepare('
            SELECT COUNT(*) as counter
            FROM claims_' . $type . '
            WHERE
                fk_claim = :claimId AND
                fk_country = :countryId
        ');
        $query->execute([ 'claimId' => $claimId, 'countryId' => $countryId ]);

        return (int)$query->fetch()['counter'] > 0;
    }



    /**********************
     ** CLAIM DOC STATUS **
     **********************/

    private $claimDocStatusFieldsToGet = 'SELECT id, name FROM claim_documentation_status';

    public function getAllClaimDocStatus(): array
    {
        $query = $this->db->prepare($this->claimDocStatusFieldsToGet);
        $query->execute();

        return $query->fetchAll();
    }

    public function getClaimDocStatusById($id): array
    {
        $query = $this->db->prepare($this->claimDocStatusFieldsToGet . ' WHERE id = :id');
        $query->execute([ 'id' => $id ]);

        $data = $query->fetch();

        return $data ? $data : [];
    }

    public function createClaimDocStatus($data): int
    {
        $query = $this->db->prepare('INSERT INTO claim_documentation_status (name) VALUES (:name)');
        $query->execute([ 'name' => $data['name'] ]);

        return $this->db->lastInsertId();
    }

    public function editClaimDocStatus($data)
    {
        $query = $this->db->prepare('UPDATE claim_documentation_status SET name = :name WHERE id = :id');
        $query->execute([ 'id' => $data['id'], 'name' => $data['name'] ]);
    }

    public function deleteClaimDocStatus($id)
    {
        $query = $this->db->prepare('DELETE FROM claim_documentation_status WHERE id = :id');
        $query->execute([ 'id' => $id ]);
    }



    /****************************
     * CLAIM DOC STATUS BY ROLE *
     ****************************/

    public function getAllClaimDocStatusByRole($roleId): array
    {
        $query = $this->db->prepare('
            SELECT
                cdsbr.fk_claim_documentation_status as claimDocumentationStatusId,
                cdsbr.fk_role as roleId,
                r.name as roleName
            FROM claim_documentation_status_by_role cdsbr
            LEFT JOIN statia.roles r ON r.id = cdsbr.fk_role WHERE fk_role = :roleId
            ORDER BY fk_claim_documentation_status
        ');
        $query->execute([ 'roleId' => $roleId ]);

        return $query->fetchAll();
    }

    public function createClaimDocStatusByRole($data)
    {
        $query = $this->db->prepare('
            INSERT INTO claim_documentation_status_by_role (fk_claim_documentation_status, fk_role)
            VALUES (:claimDocStatusId, :roleId)
        ');
        $query->execute([
            'claimDocStatusId' => $data['claimDocumentationStatusId'],
            'roleId'           => $data['roleId']
        ]);
    }

    public function deleteClaimDocStatusByRole($claimDocStatusId, $roleId)
    {
        $query = $this->db->prepare('
            DELETE FROM claim_documentation_status_by_role
            WHERE
                fk_claim_documentation_status = :claimDocStatusId AND
                fk_role = :roleId
        ');
        $query->execute([
            'claimDocStatusId' => $claimDocStatusId,
            'roleId'           => $roleId
        ]);
    }

    public function claimDocStatusByRoleExists($claimDocStatusId, $roleId): bool
    {
        $query = $this->db->prepare('
            SELECT COUNT(*) as counter
            FROM claim_documentation_status_by_role
            WHERE
                fk_claim_documentation_status = :claimDocStatusId AND
                fk_role = :roleId
        ');
        $query->execute([
            'claimDocStatusId' => $claimDocStatusId,
            'roleId'           => $roleId
        ]);

        return (int)$query->fetch()['counter'] > 0;
    }
}