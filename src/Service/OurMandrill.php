<?php

namespace App\Service;

use Mandrill;

class OurMandrill
{
    const PROD_APP_ENV = 'prod';
    const PROD_KEY     = 'zfjHU6ZX9MK4EVZwnw2RqA';
    const TEST_KEY     = 'vrjSix_nhICY9pQa2gnYtQ';

    const POOL  = 'Main Pool';
    const ASYNC = false;

    const POPULETIC_FROM_NAME = 'Populetic';
    const POPULETIC_DOMAIN    = 'populetic.com';

    private $mandrill;

    /**
     * (**)IMPORTANTE: Cuando se use OurMandrill, recordar try-catchear el
     * Mandrill_Error $e->getMessage() en el Controller
     * @throws \Mandrill_Error
     */
    public function __construct()
    {
        $this->mandrill = new Mandrill($this->getApiKey());
    }

    /**
     * SEND MANDRILL TEMPLATE (EMAIL MANDRILL)
     * @param $data array con la siguientes keys:
     * - subject -> el subject del correo
     * - languageCode -> p.e.: 'es', 'en', 'fr', etc
     * - emailTo -> el correo del receptor
     * - nameTo -> el nombre del receptor
     * - attachments -> (optional) array de archivos a enviar, con los siguientes datos:
     *         0 => [
     *             'type' => <mimeType>,
     *             'name' => <document name>.<extension>,
     *             'content' => file_get_contents(base64_encode(<archivo>))
     *         ],
     * - templateVars -> array con las variables que se usarán en el template p.e.:
     *         [ 'CLIENT_ID' => 1234, 'AIRLINE_NAME' => 'Vueling' ]
     * - templateName -> el nombre del template de Mailchimp que se usará
     *
     * @param $sendAt
     */
    public function sendTemplate($data, $sendAt = ''): array
    {
        $sendAt  = $this->getYesterdayIfSendAtIsEmpty($sendAt);
        $message = $this->constructTemplateMessageArray($data);

        return $this->mandrill->messages->sendTemplate($data['templateName'], [], $message, self::ASYNC, self::POOL, $sendAt);
    }

    /**
     * SEND MANDRILL TEXT (EMAIL CUSTOM)
     * @param $data array con la siguientes keys:
     * - subject -> el subject del correo
     * - languageCode -> p.e.: 'es', 'en', 'fr', etc
     * - emailTo -> el correo del receptor
     * - nameTo -> el nombre del receptor
     * - attachments -> (optional) array de archivos a enviar, con los siguientes datos:
     *         0 => [
     *             'type' => <mimeType>,
     *             'name' => <document name>.<extension>,
     *             'content' => file_get_contents(base64_encode(<archivo>))
     *         ],
     * - html -> el html del correo a enviar
     *
     * @param $sendAt
     */
    public function sendText($data, $sendAt = ''): array
    {
        $sendAt  = $this->getYesterdayIfSendAtIsEmpty($sendAt);
        $message = $this->constructTextMessageArray($data);

        return $this->mandrill->messages->send($message, self::ASYNC, self::POOL, $sendAt);
    }

    /**
     * @param $mandrillId string -> El id del email en la API (campo mandrill_id en email_mandrill_YYYY)
     * @param $sendAt
     */
    public function rescheduleTemplateEmail($mandrillId, $sendAt)
    {
        return $this->mandrill->messages->reschedule($mandrillId, $sendAt);
    }

    private function constructTemplateMessageArray($data): array
    {
        $msgArray = $this->getCommonData($data);
        $msgArray['global_merge_vars'] = $this->getMergeVarsFormat($data['templateVars']);

        return $msgArray;
    }

    private function getMergeVarsFormat($templateVarsArray): array
    {
        $mergeVars = [];
        foreach ($templateVarsArray as $key => $value) {
            $mergeVars[] = [
                'name'    => $key,
                'content' => $value
            ];
        }

        return $mergeVars;
    }

    private function constructTextMessageArray($data): array
    {
        $msgArray = $this->getCommonData($data);
        $msgArray['merge'] = false;
        $msgArray['html']  = $data['html'];

        return $msgArray;
    }

    private function getCommonData($data): array
    {
        $emailFrom = 'info.' . $data['languageCode'] . '@' . self::POPULETIC_DOMAIN;
        return [
            'subject'                   => $data['subject'],
            'from_email'                => $emailFrom,
            'from_name'                 => self::POPULETIC_FROM_NAME,
            'to'                        => [
                [
                    'email' => $data['emailTo'],
                    'name'  => $data['nameTo'],
                    'type'  => 'to'
                ]
            ],
            'track_opens'               => true,
            'track_clicks'              => true,
            'merge_language'            => 'mailchimp',
            'google_analytics_domains'  => [ self::POPULETIC_DOMAIN ],
            'google_analytics_campaign' => 'info@' . self::POPULETIC_DOMAIN,
            'metadata'                  => [ 'website' => 'www.' . self::POPULETIC_DOMAIN ],
            'headers'                   => [ 'Reply-To' => $emailFrom ],
            'attachments'               => array_key_exists('attachments', $data) ? $data['attachments'] : []
        ];
    }

    /** @param $mandrillId string -> El id del email en la API (campo mandrill_id en email_mandrill_YYYY) */
    public function getTemplateTrackingInfo($mandrillId)
    {
        return $this->mandrill->messages->info($mandrillId);
    }

    private function getApiKey()
    {
        return $_SERVER['APP_ENV'] === self::PROD_APP_ENV ? self::PROD_KEY : self::TEST_KEY;
    }

    /** Si no nos dicen cuando enviarlo, que se envíe inmediatamente, para lo cual ponemos una fecha del pasado */
    private function getYesterdayIfSendAtIsEmpty($sendAt): string
    {
        if ($sendAt === '') {
            $sendAt = date('Y-m-d H:i:s', strtotime('-1 days'));
        }

        return $sendAt;
    }
}
