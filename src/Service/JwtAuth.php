<?php

namespace App\Service;

use App\Entity\User;
use App\Service\Exceptions\ValidationException;
use Firebase\JWT\JWT;

class JwtAuth
{
    const AUTH_ERROR  = 'authenticationError';
    const ALGORITHM   = 'HS256';
    const DAYS_LOGGED = 30;

    public function getToken(User $user): string
    {
        $token = [
            'id'        => $user->id,
            'name'      => $user->name,
            'surnames'  => $user->surnames,
            'username'  => $user->username,
            'iat'       => time(),
            'exp'       => time() + (self::DAYS_LOGGED * 24 * 60 * 60)
        ];

        return JWT::encode($token, $_SERVER['APP_SECRET'], self::ALGORITHM);
    }

    public function checkToken($jwt): array
    {
        $decodedToken = [];

        try {
            $decodedToken = JWT::decode($jwt, $_SERVER['APP_SECRET'], [self::ALGORITHM]);
            $auth = !empty($decodedToken) && is_object($decodedToken) && isset($decodedToken->id);
        } catch (\UnexpectedValueException | \DomainException $e) {
            $auth = false;
        }

        if (!$auth) {
            throw new ValidationException(self::AUTH_ERROR);
        }

        return (array)$decodedToken;
    }
}
