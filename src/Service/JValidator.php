<?php

namespace App\Service;

use App\Service\Exceptions\ValidationException;
use DateTime;

class JValidator
{
    const E_EMPTY                   = 'Empty';
    const E_NONUMBER                = 'NoNumber';
    const E_NODECIMAL               = 'NoDecimal';
    const E_NOARRAY                 = 'NoArray';
    const E_MINVALUE                = 'MinValue';
    const E_MAXVALUE                = 'MaxValue';
    const E_NOINT                   = 'NoInt';
    const E_MAXDECIMALS             = 'MaxDecimals';
    const E_MINLENGTH               = 'MinLength';
    const E_MAXLENGTH               = 'MaxLength';
    const E_NOSTRING                = 'NoString';
    const E_INCORRECT_CHARACTERS    = 'IncorrectChars';
    const E_NOSPACES_INPASSWORD     = 'NoSpaces';
    const E_TOOWEAK                 = 'TooWeak';
    const E_NORMAL                  = 'Normal';
    const E_WEAK                    = 'Weak';
    const E_NOBOOLEAN               = 'NoBoolean';
    const E_NOT_EQUALS              = 'NotEquals';
    const E_INVALID_FORMAT          = 'InvalidFormat';
    const E_ALREADY_EXISTS          = 'AlreadyExists';
    const E_DOESNT_EXISTS           = 'DoesntExists';
    const E_DISABLED                = 'Disabled';
    const E_INVALID_COUNTRY_PREFIX  = 'InvalidCountryPrefix';
    const E_PARAM_NOT_FOUND         = 'ParamNotFound';
    const E_MORE_THAN_ONE_DEFAULT   = 'MoreThanOneDefaultSelected';
    const E_NO_DEFAULT_SELECTED     = 'NoDefaultSelected';

    const TIMESTAMP_DATE = 'Y-m-d H:i:s';

    private $errors = array();

    /** Método para saber si hay errores en la validación */
    public function hasErrors(): bool
    {
        return $this->getNumErrors() > 0;
    }

    /** Método para saber si un identificador concreto ($key) tiene algún error de validación */
    public function hasError($key): bool
    {
        return $this->getError($key) != '';
    }

    public function setError($key, $errorCode, $value = null)
    {
        $value = ($value === null) ? "" : (":'" . $value . "'");
        $this->errors[$key][] = $errorCode . $value;
    }

    /**
     * Método para obtener todos los errores de validación que se hayan producido
     * @return array 'identificador'=>'mensaje'
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * Método para obtener el error de validación del identificador $key
     * @param string $key Identificador del cual queremos obtener el error de validación
     * @return string|bool Una cadena de texto con el error correspondiente o false
     */
    public function getError($key)
    {
        return (isset($this->errors[$key])) ? $this->errors[$key] : false;
    }

    public function getNumErrors(): int
    {
        return count($this->errors);
    }

    public function validaInt($var, $required = false, $key = 'int', $min = null, $max = null): bool
    {
        if ($required && ($var === null || $var === '')) {
            $this->setError($key, self::E_EMPTY);
        } elseif ($var !== null && (!empty($var) || $var == 0)) {
            if (is_numeric($var) === false) {
                $this->setError($key, self::E_NONUMBER, $var);
            } elseif ((int) $var != $var) {
                $this->setError($key, self::E_NOINT, $var);
            } elseif ($min !== null && bccomp($var, $min) === -1) {
                $this->setError($key, self::E_MINVALUE . $min, $var);
            } elseif ($max !== null && bccomp($var, $max) === 1) {
                $this->setError($key, self::E_MAXVALUE . $max, $var);
            }
        }

        return !$this->hasError($key);
    }

    /**
     * @param $var
     * @param bool $required
     * @param string $key
     * @param null $min
     * @param null $max
     * @param null $decimals Número de decimales que tiene que tener como máximo. (opcional)
     */
    public function validaFloat($var, $required = false, $key = 'float', $min = null, $max = null, $decimals = null): bool
    {
        if ($required && ($var === null || $var === '')) {
            $this->setError($key, self::E_EMPTY);
        } elseif ($var !== null && (!empty($var) || $var == 0)) {
            if (is_numeric($var) === false) {
                $this->setError($key, self::E_NONUMBER, $var);
            } elseif (! preg_match("/^-?[0-9]+(\.[0-9]+)?$/", $var)) {
                $this->setError($key, self::E_NODECIMAL, $var);
            } elseif ($min !== null && floatval($var) < floatval($min)) {
                $this->setError($key, self::E_MINVALUE . $min, $var);
            } elseif ($max !== null && floatval($var) > floatval($max)) {
                $this->setError($key, self::E_MAXVALUE . $max, $var);
            } elseif ($decimals !== null && strlen(substr(strrchr($var, '.'), 1)) > $decimals) {
                $this->setError($key, self::E_MAXDECIMALS . $decimals, $var);
            }
        }

        return !$this->hasError($key);
    }

    public function validaString($var, $required = false, $key = 'string', $minLength = null, $maxLength = null): bool
    {
        if ($required && empty($var)) {
            $this->setError($key, self::E_EMPTY);
        } elseif (!empty($var)) {
            if (is_string($var) === false) {
                $this->setError($key, self::E_NOSTRING, $var);
            } elseif ($var && strip_tags($var) != $var) {
                $this->setError($key, self::E_INCORRECT_CHARACTERS, $var);
            } elseif ($minLength !== null && mb_strlen($var, 'utf8') < $minLength) {
                $this->setError($key, self::E_MINLENGTH . $minLength, $var);
            } elseif ($maxLength !== null && mb_strlen($var, 'utf8') > $maxLength) {
                $this->setError($key, self::E_MAXLENGTH . $maxLength, $var);
            }
        }

        return !$this->hasError($key);
    }

    public function validaArray($var, $required = false, $key = 'array'): bool
    {
        if (($required && empty($var)) || !is_array($var)) {
            $this->setError($key, self::E_EMPTY);
        }

        return !$this->hasError($key);
    }

    /**
     * Valida una variable si es un Password o no (si es fuerte, normal, debil o muy debil)
     * @param $var
     * @param bool $required
     * @param string $key
     * @param int $strong Valor de fortaleza que tiene que tener (0:solo longitud, 1:muy debil, 2:debil, 3:normal, 4:fuerte) (opcional)
     * @param null $minLength
     * @param null $maxLength
     * @param array $diffFields con los campos que deben ser diferentes del password (p.ej:nombre de usuario no tendría que estar en el password)
     * @return bool
     */
    public function validaPassword(
        $var,
        $required = false,
        $key = 'password',
        $strong = 3,
        $minLength = null,
        $maxLength = null,
        $diffFields = []
    ): bool {
        if ($required && empty($var)) {
            $this->setError($key, self::E_EMPTY);
        } elseif (!empty($var) && $this->validaString($var, $required, $key, $minLength, $maxLength)) {
            if (strpos($var, ' ') !== false) {
                $this->setError($key, self::E_NOSPACES_INPASSWORD);
            } elseif ($strong > 0) {
                $LOWER = "/[a-z]/";
                $UPPER = "/[A-Z]/";
                $DIGIT = "/[0-9]/";
                $DIGITS = "/[0-9].*[0-9]/";
                $SPECIAL = "/[^a-zA-Z0-9]/";

                foreach ($diffFields as $campoDiferente) {
                    if ($campoDiferente != '') {
                        if (stripos($var, $campoDiferente) !== false) {
                            $this->setError($key, self::E_TOOWEAK);
                        }
                    }
                }

                // Si no hay ningún error todavía, comprobamos la fortaleza del password
                if (! $this->hasError($key)) {
                    if ($strong > 1) {
                        $lower = preg_match($LOWER, $var);
                        $upper = preg_match($UPPER, $var);
                        $digit = preg_match($DIGIT, $var);
                        $digits = preg_match($DIGITS, $var);
                        $special = preg_match($SPECIAL, $var);
                        if ((($lower) && ($upper) && ($digit)) ||
                            (($lower) && ($digits)) ||
                            (($upper) && ($digits)) ||
                            ((($lower) || ($upper)) && ($digit) && ($special))
                        ) {
                            // Password strength strong->valid
                        } elseif ((($lower) && ($upper) && ($digit)) ||
                                 (($lower) && ($digit)) ||
                                 (($upper) && ($digit > 0))
                        ) {
                            if ($strong > 3) {
                                $this->setError($key, self::E_NORMAL);
                            }
                        } else {
                            $this->setError($key, self::E_WEAK);
                        }
                    }
                }
            }
        }

        return !$this->hasError($key);
    }

    /** Valida que dos campos sean iguales */
    public function assertEqual($field1, $field2, $required = false, $key = 'equal'): bool
    {
        if ($required && (empty($field1) || empty($field2))) {
            $this->setError($key, self::E_EMPTY);
        } elseif ($field1 != $field2) {
            $this->setError($key, self::E_NOT_EQUALS);
        }

        return !$this->hasError($key);
    }

    /** Valida que una fecha esté bien formateada y se validará según el formato de la variable $format. */
    public function validaDate($var, $required = false, $key = 'date', $format = self::TIMESTAMP_DATE, $min = null, $max = null): bool
    {
        if ($required && empty($var)) {
            $this->setError($key, self::E_EMPTY);
        } elseif (!empty($var)) {
            $dateTime = DateTime::createFromFormat($format, $var);
            $errors   = DateTime::getLastErrors();
            if ($dateTime === false || !empty($errors['warning_count'])) {
                $this->setError($key, self::E_INVALID_FORMAT, $var);
            } elseif ($min && strtotime($var) < strtotime($min)) {
                $this->setError($key, self::E_MINVALUE . $min, $var);
            } elseif ($max && strtotime($var) > strtotime($max)) {
                $this->setError($key, self::E_MAXVALUE . $max, $var);
            }
        }

        return !$this->hasError($key);
    }

    /** Valida que una hora esté bien formateada */
    public function validaHour($var, $required = false, $key = 'hour'): bool
    {
        if ($required && empty($var)) {
            $this->setError($key, self::E_EMPTY);
        } elseif (!empty($var)) {
            $value = explode(':', $var);

            if (isset($value[0]) === true && strlen($value[0]) == 1) {
                $value[0] = '0'.$value[0];
            }

            if (isset($value[2]) === false) {
                $value[2] = '00';
            }

            if (isset($value[1]) === false) {
                $this->setError($key, self::E_INVALID_FORMAT, $var);
            } elseif (
                (is_numeric($value[0]) === false) ||
                (is_numeric($value[1]) === false) ||
                (is_numeric($value[2]) === false)
            ) {
                $this->setError($key, self::E_INVALID_FORMAT, $var);
            }

            $hour = $value[0] + 0;
            $min = (isset($value[1])) ? $value[1] + 0 : 0;
            $sec = (isset($value[2])) ? $value[2] + 0 : 0;

            if (($hour > 24) || ($hour < 0) || ($min > 59) || ($min < 0) || ($sec > 59) || ($sec < 0)) {
                $this->setError($key, self::E_INVALID_FORMAT, $var);
            }
        }

        return !$this->hasError($key);
    }

    public function validaBoolean($var, $required = false, $key = 'boolean'): bool
    {
        $var = strtoupper($var);

        if ($required && (empty($var) && $var != 0)) {
            $this->setError($key, self::E_EMPTY);
        } elseif ($var !== null && (!empty($var) || $var == 0)) {
            if (filter_var($var, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) === null) {
                $this->setError($key, self::E_NOBOOLEAN, $var);
            }
        }

        return !$this->hasError($key);
    }

    public function validaEmail($var, $required = false, $key = 'email'): bool
    {
        if ($required && empty($var)) {
            $this->setError($key, self::E_EMPTY);
        } elseif (!empty($var)) {
            if (filter_var($var, FILTER_VALIDATE_EMAIL) === false) {
                $this->setError($key, self::E_INVALID_FORMAT, $var);
            }
        }

        return !$this->hasError($key);
    }

    public function validaDNI($var, $required = false, $key = 'dni'): bool
    {
        if ($required && empty($var)) {
            $this->setError($key, self::E_EMPTY);
        } elseif (!empty($var)) {
            if ($this->checkDNINIE($var) === false) {
                $this->setError($key, self::E_INVALID_FORMAT, $var);
            }
        }

        return !$this->hasError($key);
    }

    private function checkDNINIE($dniNie): bool
    {
        $dniNie = strtoupper($dniNie);

        if (strlen($dniNie) != 9) {
            return false;
        }

        $allowedLetters = array(
            0 => 'T',
            1 => 'R',
            2 => 'W',
            3 => 'A',
            4 => 'G',
            5 => 'M',
            6 => 'Y',
            7 => 'F',
            8 => 'P',
            9 => 'D',
            10 => 'X',
            11 => 'B',
            12 => 'N',
            13 => 'J',
            14 => 'Z',
            15 => 'S',
            16 => 'Q',
            17 => 'V',
            18 => 'H',
            19 => 'L',
            20 => 'C',
            21 => 'K',
            22 => 'E'
        );

        if (preg_match('/^[0-9]{8}[A-Z]$/i', $dniNie)) {
            // DNI Comprobar letra
            if (strtoupper($dniNie[strlen($dniNie) - 1]) !=
                $allowedLetters[((int) substr($dniNie, 0, strlen($dniNie) - 1)) % 23]) {
                return false;
            }

            return true;
        } elseif (preg_match('/^[XYZ][0-9]{7}[A-Z]$/i', $dniNie)) {
            // NIE
            $firstLetterNumber = '';
            $firstLetter = substr($dniNie, 0, 1);
            switch ($firstLetter) {
                case 'X':
                    $firstLetterNumber = '0';
                    break;
                case 'Y':
                    $firstLetterNumber = '1';
                    break;
                case 'Z':
                    $firstLetterNumber = '2';
                    break;
            }
            $number = $firstLetterNumber . substr($dniNie, 1, strlen($dniNie) - 2);
            // Comprobar letra
            if (strtoupper($dniNie[strlen($dniNie) - 1]) != $allowedLetters[((int) $number) % 23]) {
                return false;
            }

            return true;
        }

        return false;
    }

    public function validaURL($var, $required = false, $key = 'url'): bool
    {
        if ($required && empty($var)) {
            $this->setError($key, self::E_EMPTY);
        } elseif (!empty($var)) {
            if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i", $var)) {
                $this->setError($key, self::E_INVALID_FORMAT, $var);
            }
        }

        return !$this->hasError($key);
    }

    public function validaIP($var, $required = false, $key = 'ip'): bool
    {
        if ($required && empty($var)) {
            $this->setError($key, self::E_EMPTY);
        } elseif (!empty($var)) {
            if (filter_var($var, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4) === false) {
                $this->setError($key, self::E_INVALID_FORMAT, $var);
            }
        }

        return !$this->hasError($key);
    }

    /**
     * @param $var
     * @param bool $required
     * @param string $key
     * @return bool
     */
    public function validaPhone($var, $required = false, $key = 'phone'): bool
    {
        if ($required && empty($var)) {
            $this->setError($key, self::E_EMPTY);
        } elseif (!empty($var)) {
            $pattern = '/(^\+(9[976]\d|8[987530]\d|6[987]\d|5[90]\d|42\d|3[875]\d|2[98654321]\d
                        |9[8543210]|8[6421]|6[6543210]|5[87654321]|4[987654310]|3[963210]|2[70]
                        |7|1)\d{1,14}$)|(^(\+34)?[976]\d{8}$)/';
            if (! preg_match($pattern, $var)) {
                $this->setError($key, self::E_INVALID_FORMAT, $var);
            }
        }

        return !$this->hasError($key);
    }

    public function validaIBAN($iban, $required = false, $key = 'iban'): bool
    {
        if ($required && empty($iban)) {
            $this->setError($key, self::E_EMPTY);
        } elseif (!empty($iban)) {
            $iban      = strtolower(str_replace(' ', '', $iban));
            $countries = array('al'=>28,'ad'=>24,'at'=>20,'az'=>28,'bh'=>22,'be'=>16,'ba'=>20,'br'=>29,'bg'=>22,
                               'cr'=>21,'hr'=>21,'cy'=>28,'cz'=>24,'dk'=>18,'do'=>28,'ee'=>20,'fo'=>18,'fi'=>18,
                               'fr'=>27,'ge'=>22,'de'=>22,'gi'=>23,'gr'=>27,'gl'=>18,'gt'=>28,'hu'=>28,'is'=>26,
                               'ie'=>22,'il'=>23,'it'=>27,'jo'=>30,'kz'=>20,'kw'=>30,'lv'=>21,'lb'=>28,'li'=>21,
                               'lt'=>20,'lu'=>20,'mk'=>19,'mt'=>31,'mr'=>27,'mu'=>30,'mc'=>27,'md'=>24,'me'=>22,
                               'nl'=>18,'no'=>15,'pk'=>24,'ps'=>29,'pl'=>28,'pt'=>25,'qa'=>29,'ro'=>24,'sm'=>27,
                               'sa'=>24,'rs'=>22,'sk'=>24,'si'=>19,'es'=>24,'se'=>24,'ch'=>21,'tn'=>24,'tr'=>26,
                               'ae'=>23,'gb'=>22,'vg'=>24);
            $char = array('a'=>10,'b'=>11,'c'=>12,'d'=>13,'e'=>14,'f'=>15,'g'=>16,'h'=>17,'i'=>18,'j'=>19,'k'=>20,
                          'l'=>21,'m'=>22,'n'=>23,'o'=>24,'p'=>25,'q'=>26,'r'=>27,'s'=>28,'t'=>29,'u'=>30,'v'=>31,
                          'w'=>32,'x'=>33,'y'=>34,'z'=>35);
            if (array_key_exists(substr($iban, 0, 2), $countries)) {
                if (strlen($iban) == $countries[substr($iban, 0, 2)]) {
                    $movedChar = substr($iban, 4) . substr($iban, 0, 4);
                    $movedCharArray = str_split($movedChar);
                    $newString = '';

                    foreach ($movedCharArray as $index => $value) {
                        if (!is_numeric($movedCharArray[$index])) {
                            $movedCharArray[$index] = $char[$movedCharArray[$index]];
                        }
                        $newString .= $movedCharArray[$index];
                    }

                    if (bcmod($newString, '97') != 1) {
                        $this->setError($key, self::E_INVALID_FORMAT, $iban);
                    }
                } else {
                    $this->setError($key, self::E_INVALID_FORMAT, $iban);
                }
            } else {
                $this->setError($key, self::E_INVALID_COUNTRY_PREFIX, $iban);
            }
        }

        return !$this->hasError($key);
    }

    /**
     * This function takes 2 arguments, an IP address and a "range" in several different formats.
     * Network ranges can be specified as:
     * 1. Wildcard format: 1.2.3.*
     * 2. CIDR format: 1.2.3/24 OR 1.2.3.4/255.255.255.0
     * 3. Start-End IP format: 1.2.3.0-1.2.3.255
     * The function will return true if the supplied IP is within the range.
     * Note little validation is done on the range inputs - it expects you to
     * use one of the above 3 formats.
     * @param $ip
     * @param $range
     */
    public function ipInRange($ip, $range): bool
    {
        if (strpos($range, '/') !== false) {
            // $range is in IP/NETMASK format
            list ($range, $netmask) = explode('/', $range, 2);
            if (strpos($netmask, '.') !== false) {
                // $netmask is a 255.255.0.0 format
                $netmask = str_replace('*', '0', $netmask);
                $netmaskDec = ip2long($netmask);
                return ((ip2long($ip) & $netmaskDec) == (ip2long($range) & $netmaskDec));
            } else {
                // $netmask is a CIDR size block
                // fix the range argument
                $x = explode('.', $range);
                while (count($x) < 4) {
                    $x[] = '0';
                }

                list ($a, $b, $c, $d) = $x;
                $range = sprintf(
                    "%u.%u.%u.%u",
                    empty($a) ? '0' : $a,
                    empty($b) ? '0' : $b,
                    empty($c) ? '0' : $c,
                    empty($d) ? '0' : $d
                );
                $rangeDec = ip2long($range);
                $ipDec    = ip2long($ip);

                $wildcardDec = pow(2, (32 - $netmask)) - 1;
                $netmaskDec = ~ $wildcardDec;

                return (($ipDec & $netmaskDec) == ($rangeDec & $netmaskDec));
            }
        } else {
            // range might be 255.255.*.* or 1.2.3.0-1.2.3.255
            if (strpos($range, '*') !== false) {
                // a.b.*.* format
                // Just convert to A-B format by setting * to 0 for A and 255 for B
                $lower = str_replace('*', '0', $range);
                $upper = str_replace('*', '255', $range);
                $range = "$lower-$upper";
            }

            if (strpos($range, '-') !== false) {
                // A-B format
                list ($lower, $upper) = explode('-', $range, 2);
                $lowerDec = (float) sprintf("%u", ip2long($lower));
                $upperDec = (float) sprintf("%u", ip2long($upper));
                $ipDec    = (float) sprintf("%u", ip2long($ip));
                return (($ipDec >= $lowerDec) && ($ipDec <= $upperDec));
            }

            return false;
        }
    }

    /**
     * Función que lanza el ValidationException con el array de errores
     * @throws ValidationException
     */
    public function checkErrors()
    {
        if ($this->hasErrors()) {
            throw new ValidationException($this->getHtmlErrors());
        }
    }

    /** Monta un array de strings con los códigos de error encontrados */
    public function getHtmlErrors(): array
    {
        $errorList = [];
        foreach ($this->errors as $errorKey => $errors) {
            foreach ($errors as $msg) {
                $errorList[] =  $errorKey . $msg;
            }
        }

        return $errorList;
    }
}
