<?php

namespace App\Controller\Statia;

use App\Controller\BaseController;
use App\Model\Statia\OptionsModel;
use App\Service\Exceptions\ValidationException;
use Doctrine\DBAL\DBALException;
use ErrorException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use TypeError;

class OptionsController extends BaseController
{
    /*********************
     ****** OPTIONS ******
     *********************/

    /** @Route("/statia/options", methods={"GET", "OPTIONS"}) */
    public function getAllOptions(Request $request): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new OptionsModel($this->statiaConn());
            $response = $model->getAll();

            $page = $this->getPageFromURL($request);
            if ($page) {
                $response = $model->paginateResult($response, $page, $this->getItemsPerPageFromURL($request));
            }

            $this->setResponseOk(__FUNCTION__, $response);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/statia/options/{id}", methods={"GET", "OPTIONS"}) */
    public function getOptionById($id): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new OptionsModel($this->statiaConn());
            $model->validaId($id);

            $this->setResponseOk(__FUNCTION__, $model->getById($id));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/statia/options/create", methods={"POST", "OPTIONS"}) */
    public function createOption(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $newData = $this->getJsonParams($request);

            $model = new OptionsModel($this->statiaConn());
            $model->validaCreation($newData);
            $newData = $model->create($newData);

            $this->saveChangeLog(__FUNCTION__, $newData['id'], $newData);
            $this->setResponseOk(__FUNCTION__, $newData);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/statia/options/edit/{id}", methods={"PUT", "OPTIONS"}) */
    public function editOption(Request $request, $id): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);
            $data['id'] = $id;

            $model = new OptionsModel($this->statiaConn());
            $model->validaEdition($data);
            $oldData = $model->getById($id);
            $model->edit($data);

            $this->saveChangeLog(__FUNCTION__, $id, $data, $oldData);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/statia/options/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteOptions(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $ids = $this->getJsonParams($request);

            $model = new OptionsModel($this->statiaConn());
            $model->validaIdsToDelete($ids);

            $this->statiaConn()->beginTransaction();
            foreach ($ids as $id) {
                $optionToDelete = $model->getById($id);
                $model->delete($id);
                $this->saveChangeLog(__FUNCTION__, $id, $optionToDelete);
            }
            $this->statiaConn()->commit();

            $this->setResponseOk(__FUNCTION__);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->statiaConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }



    /*********************
     * MENU ROLE OPTIONS *
     *********************/

    /** @Route("/statia/menus-role-options", methods={"POST", "OPTIONS"}) */
    public function getMenuRoleOptions(Request $request): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $params = $this->getJsonParams($request);
            $menuId = $this->getDataIfKeyExistsOrNull($params, 'menuId');
            $roleId = $this->getDataIfKeyExistsOrNull($params, 'roleId');

            $model = new OptionsModel($this->statiaConn());
            $model->validaMenuAndRoleToGet($menuId, $roleId);
            $options = $model->getByMenuAndRole($menuId, $roleId);

            $this->setResponseOk(__FUNCTION__, $options);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/statia/menus-role-options/edit", methods={"POST", "OPTIONS"}) */
    public function editRoleMenusOptions(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $messagesToLog = [];
            $params = $this->getJsonParams($request);

            $model = new OptionsModel($this->statiaConn());
            $model->validaEditRoleMenusOptions($params);

            $this->statiaConn()->beginTransaction();

            $model->deleteAllMenusRolesOptionsByRoleId($params['roleId']);
            $model->deleteAllMenusRolesByRoleId($params['roleId']);
            $messagesToLog[] = 'Deleted all menus_roles and all menus_roles_options for roleId: ' . $params['roleId'];

            foreach ($params['menusOptions'] as $menuOptions) {
                $quickaccess = $this->getDataIfKeyExistsOrZero($menuOptions, 'quickaccess');
                $model->createMenuRole($menuOptions['menuId'], $params['roleId'], $quickaccess);
                $messagesToLog[] = 'Created menu role: menuId ' . $menuOptions['menuId'] . ', roleId ' . $params['roleId'] .
                    ', quickaccess ' . $quickaccess;

                foreach ($menuOptions['optionIds'] as $optionId) {
                    $model->createMenuRoleOption($menuOptions['menuId'], $params['roleId'], $optionId);
                    $messagesToLog[] = 'Created menu role option: menuId ' . $menuOptions['menuId'] .
                        ', roleId ' . $params['roleId'] . ', optionId ' . $optionId;
                }
            }

            foreach ($messagesToLog as $msg) {
                $this->saveChangeLog(__FUNCTION__, [], $msg);
            }

            $this->statiaConn()->commit();

            $this->setResponseOk(__FUNCTION__, $params);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->statiaConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }
}
