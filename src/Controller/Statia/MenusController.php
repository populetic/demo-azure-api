<?php

namespace App\Controller\Statia;

use App\Controller\BaseController;
use App\Model\Statia\MenusModel;
use App\Model\Statia\RolesModel;
use App\Service\Exceptions\ValidationException;
use Doctrine\DBAL\DBALException;
use ErrorException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use TypeError;

class MenusController extends BaseController
{
    /*********************
     ******* MENUS *******
     *********************/

    /** @Route("/statia/menus", methods={"GET", "OPTIONS"}) */
    public function getAllMenus(Request $request): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new MenusModel($this->statiaConn());
            $menus = $model->getAll();

            $main = $request->query->getInt('main', null);
            if ($main === 1) {
                $menus = $model->filterMainMenus($menus);
            }

            $page = $this->getPageFromURL($request);
            if ($page) {
                $response = $model->paginateResult($menus, $page, $this->getItemsPerPageFromURL($request));
            } else {
                $response = $menus;
            }

            $this->setResponseOk(__FUNCTION__, $response);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/statia/menus/{id}", methods={"GET", "OPTIONS"}) */
    public function getMenuById($id): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new MenusModel($this->statiaConn());
            $model->validaId($id);

            $this->setResponseOk(__FUNCTION__, $model->getById($id));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/statia/menus/by-role/{roleId}", methods={"GET", "OPTIONS"}) */
    public function getMenusByRole($roleId): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $db = $this->statiaConn();
            (new RolesModel($db))->validaId($roleId);
            $menus = (new MenusModel($db))->getByRole($roleId);

            $this->setResponseOk(__FUNCTION__, $menus);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/statia/menus/create", methods={"POST", "OPTIONS"}) */
    public function createMenu(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $newData = $this->getJsonParams($request);

            $model = new MenusModel($this->statiaConn());
            $model->validaCreation($newData);
            $newData = $model->create($newData);

            $this->saveChangeLog(__FUNCTION__, $newData['id'], $newData);
            $this->setResponseOk(__FUNCTION__, $newData);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/statia/menus/edit/{id}", methods={"PUT", "OPTIONS"}) */
    public function editMenu(Request $request, $id): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);
            $data['id'] = $id;

            $model = new MenusModel($this->statiaConn());
            $model->validaEdition($data);
            $oldData = $model->getById($id);
            $model->edit($data);

            $this->saveChangeLog(__FUNCTION__, $id, $data, $oldData);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/statia/menus/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteMenus(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $ids = $this->getJsonParams($request);

            $model = new MenusModel($this->statiaConn());
            $model->validaIdsToDelete($ids);

            $this->statiaConn()->beginTransaction();
            foreach ($ids as $id) {
                $menuToDelete = $model->getById($id);
                $model->delete($id);
                $this->saveChangeLog(__FUNCTION__, $id, $menuToDelete);
            }
            $this->statiaConn()->commit();

            $this->setResponseOk(__FUNCTION__);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->statiaConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }



    /********************
     *** MENU OPTIONS ***
     ********************/

    /** @Route("/statia/menus-options/edit", methods={"POST", "OPTIONS"}) */
    public function editMenuOptions(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $menuOptions = $this->getJsonParams($request);
            $menuId      = $this->getDataIfKeyExistsOrNull($menuOptions, 'menuId');
            $optionIds   = $this->getDataIfKeyExistsOrEmptyArray($menuOptions, 'optionIds');

            $model  = new MenusModel($this->statiaConn());
            $model->validaId($menuId, true);
            $model->validaMenuOptions($optionIds);

            $this->statiaConn()->beginTransaction();
            $model->deleteAllMenuOptions($menuId);
            $model->createMenuOptions($menuId, $optionIds);

            $this->saveChangeLog(__FUNCTION__, $menuId, 'Current options: ' . implode(', ', $optionIds));
            $this->statiaConn()->commit();

            $this->setResponseOk(__FUNCTION__, $menuOptions);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->statiaConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }



    /*********************
     ** MENU PROPERTIES **
     *********************/

    /** @Route("/statia/menus-properties/create", methods={"POST", "OPTIONS"}) */
    public function createMenuProperty(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $newMenuProperty = $this->getJsonParams($request);

            $model = new MenusModel($this->statiaConn());
            $model->validaMenuPropertyToCreate($newMenuProperty);
            $model->createMenuProperty($newMenuProperty);

            $this->saveChangeLog(__FUNCTION__, [], $newMenuProperty);
            $this->setResponseOk(__FUNCTION__, $newMenuProperty);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/statia/menus-properties/edit", methods={"PUT", "OPTIONS"}) */
    public function editMenuProperty(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $menuProperty = $this->getJsonParams($request);

            $model = new MenusModel($this->statiaConn());
            $model->validaMenuPropertyToEdit($menuProperty);
            $oldMenuProperty = $model->getMenuPropertyByFieldName($menuProperty['field']);
            $menuProperty    = $model->editMenuProperty($menuProperty);

            $this->saveChangeLog(__FUNCTION__, [], $menuProperty, $oldMenuProperty);
            $this->setResponseOk(__FUNCTION__, $menuProperty);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/statia/menus-properties/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteMenuProperty(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $menuProperty = $this->getJsonParams($request);

            $model = new MenusModel($this->statiaConn());
            $model->validaMenuPropertyToDelete($menuProperty);
            $model->deleteMenuProperty($menuProperty);

            $this->saveChangeLog(__FUNCTION__, [], $menuProperty);
            $this->setResponseOk(__FUNCTION__);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }
}
