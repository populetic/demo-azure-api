<?php

namespace App\Controller\Statia;

use App\Controller\BaseController;
use App\Model\Statia\DepartmentsModel;
use App\Service\Exceptions\ValidationException;
use Doctrine\DBAL\DBALException;
use ErrorException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use TypeError;

class DepartmentsController extends BaseController
{
    /** @Route("/statia/departments", methods={"GET", "OPTIONS"}) */
    public function getAllDepartments(Request $request): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new DepartmentsModel($this->statiaConn());
            $response = $model->getAll();

            $page = $this->getPageFromURL($request);
            if ($page) {
                $response = $model->paginateResult($response, $page, $this->getItemsPerPageFromURL($request));
            }

            $this->setResponseOk(__FUNCTION__, $response);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/statia/departments/{id}", methods={"GET", "OPTIONS"}) */
    public function getDepartmentById($id): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new DepartmentsModel($this->statiaConn());
            $model->validaId($id);

            $this->setResponseOk(__FUNCTION__, $model->getById($id));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/statia/departments/create", methods={"POST", "OPTIONS"}) */
    public function createDepartment(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $newData = $this->getJsonParams($request);

            $model = new DepartmentsModel($this->statiaConn());
            $model->validaCreation($newData);
            $newData = $model->create($newData);

            $this->saveChangeLog(__FUNCTION__, $newData['id'], $newData);
            $this->setResponseOk(__FUNCTION__, $newData);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/statia/departments/edit/{id}", methods={"PUT", "OPTIONS"}) */
    public function editDepartment(Request $request, $id): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);
            $data['id'] = $id;

            $model = new DepartmentsModel($this->statiaConn());
            $model->validaEdition($data);
            $oldData = $model->getById($id);
            $model->edit($data);

            $this->saveChangeLog(__FUNCTION__, $id, $data, $oldData);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/statia/departments/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteDepartments(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $ids = $this->getJsonParams($request);

            $model = new DepartmentsModel($this->statiaConn());
            $model->validaIdsToDelete($ids);

            $this->deleteSeveralIdsTransaction($this->statiaConn(), $model, $ids, __FUNCTION__);

            $this->setResponseOk(__FUNCTION__, $this->deleteErrors);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->statiaConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }
}
