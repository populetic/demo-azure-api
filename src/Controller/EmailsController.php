<?php

namespace App\Controller;

use App\Entity\EmailCustom;
use App\Entity\EmailMandrill;
use App\Model\EmailsModel;
use App\Service\Exceptions\ValidationException;
use App\Service\OurMandrill;
use Mandrill_Error;
use Doctrine\DBAL\DBALException;
use ErrorException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use TypeError;

class EmailsController extends BaseController
{
    /*********************
     ****** EMAILS *******
     *********************/

    /** @Route("/emails/{type}", methods={"GET", "OPTIONS"}) */
    public function getAllEmailsByType(Request $request, $type): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new EmailsModel($this->popuConn());
            $model->validaType($type, true);
            $response = $model->getAllByType($type);

            $page = $this->getPageFromURL($request);
            if ($page) {
                $response = $model->paginateResult($response, $page, $this->getItemsPerPageFromURL($request));
            }

            $this->setResponseOk(__FUNCTION__, $response);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/emails/by-claim-id/{claimId}", methods={"GET", "OPTIONS"}) */
    public function getAllEmailsByClaimId($claimId): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new EmailsModel($this->popuConn());
            $model->validaClaimId($claimId, true);

            $this->setResponseOk(__FUNCTION__, $model->getAllByClaimId($claimId));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/emails/{type}/{id}", methods={"GET", "OPTIONS"}) */
    public function getEmailByTypeAndId($type, $id): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new EmailsModel($this->popuConn());
            $model->validaType($type, true);
            $model->validaId($id);

            if ($type === $model::MANDRILL) {
                $response = $model->getMandrillById($id);
            } else {
                $response = $model->getCustomById($id);
            }

            $this->setResponseOk(__FUNCTION__, $response);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/emails/mandrill/send-and-insert", methods={"POST", "OPTIONS"}) */
    public function sendAndInsertMandrillEmails(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $emails = $this->getJsonParams($request);

            $model = new EmailsModel($this->popuConn());
            $model->validaMandrillEmailsToSend($emails);

            $this->popuConn()->beginTransaction();

            foreach ($emails as $emailData) {
                $template  = $model->getMandrillTemplateByName($emailData['templateName']);
                $emailData = $model->parseDataToSendMandrillEmail($emailData, $template['subject']);
                $sendAt    = !empty($emailData['sendAt']) ? $emailData['sendAt'] : '';

                $mandrillResponse = (new OurMandrill())->sendTemplate($emailData, $sendAt);

                $emailMandrill = $model->setEmailMandrillEntityWithData($emailData, $template, $mandrillResponse);

                $newEmail          = $model->insertMandrill($emailMandrill);
                $newEmail->docsIds = $emailMandrill->docsIds; //TODO de momento quedamos que no se añaden a db los adjuntos del envío

                $this->saveChangeLog(__FUNCTION__, $newEmail->id, $newEmail);
            }

            $this->popuConn()->commit();

            $this->setResponseOk(__FUNCTION__);

        } catch (DBALException | ErrorException | TypeError | Mandrill_Error $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
            $this->popuConn()->rollBack();
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/emails/custom/send-and-insert", methods={"POST", "OPTIONS"}) */
    public function sendAndInsertCustomEmails(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $emails = $this->getJsonParams($request);

            $model = new EmailsModel($this->popuConn());
            $model->validaCustomEmailsToSend($emails);

            $this->popuConn()->beginTransaction();

            foreach ($emails as $emailData) {
                $emailData = $model->parseDataToSendCustomEmail($this->translationsConn(), $emailData);
                $sendAt    = !empty($emailData['sendAt']) ? $emailData['sendAt'] : '';

                $mandrillResponse = (new OurMandrill())->sendText($emailData, $sendAt);

                $emailCustom = $model->setEmailCustomEntityWithData($emailData, $mandrillResponse);

                $newEmail          = $model->insertCustom($emailCustom);
                $newEmail->docsIds = $emailCustom->docsIds; //TODO de momento quedamos que no se añaden a db los adjuntos del envío

                $this->saveChangeLog(__FUNCTION__, $newEmail->id, $newEmail);
            }

            $this->popuConn()->commit();

            $this->setResponseOk(__FUNCTION__);

        } catch (DBALException | ErrorException | TypeError | Mandrill_Error $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
            $this->popuConn()->rollBack();
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/emails/mandrill/insert", methods={"POST", "OPTIONS"}) */
    public function insertMandrillEmail(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $email = new EmailMandrill($this->getJsonParams($request));
            $model = new EmailsModel($this->popuConn());

            $model->validaMandrill($email);
            $newEmail = $model->insertMandrill($email);
            $newEmail->docsIds = $email->docsIds;

            $this->saveChangeLog(__FUNCTION__, $newEmail->id, $newEmail);
            $this->setResponseOk(__FUNCTION__, $newEmail);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/emails/custom/insert", methods={"POST", "OPTIONS"}) */
    public function insertCustomEmail(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $email = new EmailCustom($this->getJsonParams($request));
            $model = new EmailsModel($this->popuConn());

            $model->validaCustom($email);
            $newEmail = $model->insertCustom($email);
            $newEmail->docsIds = $email->docsIds;

            $this->saveChangeLog(__FUNCTION__, $newEmail->id, $newEmail);
            $this->setResponseOk(__FUNCTION__, $newEmail);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/emails/mandrill/update-tracking-info/{mandrillId}", methods={"PUT", "OPTIONS"}) */
    public function updateMandrillTrackingInfoByMandrillId($mandrillId): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new EmailsModel($this->popuConn());
            $model->validaMandrillId($mandrillId);

            $ourMandrill = new OurMandrill();
            $data = $ourMandrill->getTemplateTrackingInfo($mandrillId);

            $model->updateMandrillTrackingInfoByMandrillId($data);
            $email = (array)$model->getEmailMandrillByMandrillId($mandrillId);
            unset($email['docsIds']);

            $this->setResponseOk(__FUNCTION__, $email);

        } catch (Mandrill_Error | DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }



    /****************************
     * EMAIL MANDRILL TEMPLATES *
     ****************************/

    /** @Route("/email-mandrill-templates", methods={"GET", "OPTIONS"}) */
    public function getMandrillTemplates(Request $request): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new EmailsModel($this->popuConn());
            $name = $request->query->get('name', null);

            if ($name) {
                $model->validaMandrillTemplateNameAndCheckThatExists($name, true);
                $response = $model->getMandrillTemplateByName($name);
            } else {
                $response = $model->getAllMandrillTemplates();

                $page = $this->getPageFromURL($request);
                if ($page) {
                    $response = $model->paginateResult($response, $page, $this->getItemsPerPageFromURL($request));
                }
            }

            $this->setResponseOk(__FUNCTION__, $response);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/email-mandrill-templates/{id}", methods={"GET", "OPTIONS"}) */
    public function getMandrillTemplateById($id): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new EmailsModel($this->popuConn());

            $model->validaEmailMandrillTemplateId($id, true);
            $this->setResponseOk(__FUNCTION__, $model->getMandrillTemplateById($id));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/email-mandrill-templates/create", methods={"POST", "OPTIONS"}) */
    public function createMandrillTemplate(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);

            $model = new EmailsModel($this->popuConn());
            $model->validaMandrillTemplate($data);
            $id   = $model->createMandrillTemplate($data);
            $data = $model->getMandrillTemplateById($id);

            $this->saveChangeLog(__FUNCTION__, $id, $data);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/email-mandrill-templates/edit/{id}", methods={"PUT", "OPTIONS"}) */
    public function editMandrillTemplate(Request $request, $id): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);
            $data['id'] = $id;

            $model = new EmailsModel($this->popuConn());
            $model->validaEmailMandrillTemplateId($id, true);
            $model->validaMandrillTemplate($data);
            $oldData = $model->getMandrillTemplateById($id);
            $model->editMandrillTemplate($data);
            $data = $model->getMandrillTemplateById($id);

            $this->saveChangeLog(__FUNCTION__, $id, $data, $oldData);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/email-mandrill-templates/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteMandrillTemplate(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $ids = $this->getJsonParams($request);

            $model = new EmailsModel($this->popuConn());
            $model->validaMandrillTemplateIdsToDelete($ids);

            $this->popuConn()->beginTransaction();
            foreach ($ids as $id) {
                $dataToDelete = $model->getMandrillTemplateById($id);
                try {
                    $model->deleteMandrillTemplate($id);
                } catch (DBALException $e) {
                    $this->deleteErrors[] = $this->parseErrorIfIsFkRestrictException($e->getMessage(), $id);
                    continue;
                }
                $this->saveChangeLog(__FUNCTION__, $id, $dataToDelete);
            }
            $this->popuConn()->commit();

            $this->setResponseOk(__FUNCTION__, $this->deleteErrors);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }



    /*********************
     **** EMAIL TASKS ****
     *********************/

    /** @Route("/email-tasks", methods={"GET", "OPTIONS"}) */
    public function getAllEmailTasks(Request $request): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new EmailsModel($this->popuConn());
            $response = $model->getAllEmailTasks();

            $page = $this->getPageFromURL($request);
            if ($page) {
                $response = $model->paginateResult($response, $page, $this->getItemsPerPageFromURL($request));
            }

            $this->setResponseOk(__FUNCTION__, $response);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/email-tasks/{id}", methods={"GET", "OPTIONS"}) */
    public function getEmailTaskById($id): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new EmailsModel($this->popuConn());
            $model->validaEmailTaskId($id, true);

            $this->setResponseOk(__FUNCTION__, $model->getEmailTaskById($id));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/email-tasks/create", methods={"POST", "OPTIONS"}) */
    public function createEmailTask(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);

            $model = new EmailsModel($this->popuConn());
            $model->validaEmailTask($data);
            $id   = $model->createEmailTask($data);
            $data = $model->getEmailTaskById($id);

            $this->saveChangeLog(__FUNCTION__, $id, $data);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/email-tasks/edit/{id}", methods={"PUT", "OPTIONS"}) */
    public function editEmailTask(Request $request, $id): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);
            $data['id'] = $id;

            $model = new EmailsModel($this->popuConn());
            $model->validaEmailTaskId($id, true);
            $model->validaEmailTask($data);
            $oldData = $model->getEmailTaskById($id);
            $model->editEmailTask($data);
            $data = $model->getEmailTaskById($id);

            $this->saveChangeLog(__FUNCTION__, $id, $data, $oldData);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/email-tasks/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteEmailTask(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $ids = $this->getJsonParams($request);

            $model = new EmailsModel($this->popuConn());
            $model->validaEmailTaskIdsToDelete($ids);

            $this->popuConn()->beginTransaction();
            foreach ($ids as $id) {
                $dataToDelete = $model->getEmailTaskById($id);
                try {
                    $model->deleteEmailTask($id);
                } catch (DBALException $e) {
                    $this->deleteErrors[] = $this->parseErrorIfIsFkRestrictException($e->getMessage(), $id);
                    continue;
                }
                $this->saveChangeLog(__FUNCTION__, $id, $dataToDelete);
            }
            $this->popuConn()->commit();

            $this->setResponseOk(__FUNCTION__, $this->deleteErrors);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }



    /*********************
     * EMAIL ATTACHMENTS *
     *********************/

    /** @Route("/email-attachments/add", methods={"POST", "OPTIONS"}) */
    public function addEmailAttachment(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);

            $model = new EmailsModel($this->popuConn());
            $model->validaEmailAttachmentCreation($data);
            $model->addEmailAttachment($data);

            $this->saveChangeLog(__FUNCTION__, [], $data);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/email-attachments/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteEmailAttachment(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);

            $model = new EmailsModel($this->popuConn());
            $model->validaEmailAttachmentDeletion($data);
            $model->deleteEmailAttachment($data);

            $this->saveChangeLog(__FUNCTION__, [], $data);
            $this->setResponseOk(__FUNCTION__);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }
}
