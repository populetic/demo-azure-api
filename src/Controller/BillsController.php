<?php

namespace App\Controller;

use App\Entity\Bill;
use App\Model\BillsModel;
use App\Service\Exceptions\ValidationException;
use Doctrine\DBAL\DBALException;
use ErrorException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use TypeError;

class BillsController extends BaseController
{
    /*********************
     ******* BILLS *******
     *********************/

    /** @Route("/bills", methods={"GET", "OPTIONS"}) */
    public function getAllBills(Request $request): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new BillsModel($this->popuConn());
            $response = $model->getAll();

            $page = $this->getPageFromURL($request);
            if ($page) {
                $response = $model->paginateResult($response, $page, $this->getItemsPerPageFromURL($request));
            }

            $this->setResponseOk(__FUNCTION__, $response);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/bills/{id}", methods={"GET", "OPTIONS"}) */
    public function getBillById($id): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new BillsModel($this->popuConn());
            $model->validaId($id, true);

            $this->setResponseOk(__FUNCTION__, $model->getBillAllDataById($id));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/bills-by-claim/{claimId}", methods={"GET", "OPTIONS"}) */
    public function getBillByClaimId($claimId): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new BillsModel($this->popuConn());
            $model->validaClaimId($claimId, true);

            $this->setResponseOk(__FUNCTION__, $model->getByClaimId($claimId));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/bills/create", methods={"POST", "OPTIONS"}) */
    public function createBill(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $bill = new Bill($this->getJsonParams($request));

            $model = new BillsModel($this->popuConn());
            $model->validaBill($bill);
            $bill = $model->create($bill);

            $this->saveChangeLog(__FUNCTION__, $bill->id, $bill);
            $this->setResponseOk(__FUNCTION__, $bill);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/bills/edit/{id}", methods={"PUT", "OPTIONS"}) */
    public function editBill(Request $request, $id): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $bill = new Bill($this->getJsonParams($request));
            $bill->id = $id;

            $model = new BillsModel($this->popuConn());
            $model->validaId($id, true);
            $model->validaBill($bill);
            $oldBill = $model->getById($id);
            $bill    = $model->edit($bill);

            $this->saveChangeLog(__FUNCTION__, $id, $bill, $oldBill);
            $this->setResponseOk(__FUNCTION__, $bill);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/bills/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteBills(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $ids = $this->getJsonParams($request);

            $model = new BillsModel($this->popuConn());
            $model->validaIdsToDelete($ids);
            $this->deleteSeveralIdsTransaction($this->popuConn(), $model, $ids, __FUNCTION__);

            $this->setResponseOk(__FUNCTION__, $this->deleteErrors);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/bill-next-number-by-series/{series}", methods={"GET", "OPTIONS"}) */
    public function getBillNextNumberBySeries($series): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new BillsModel($this->popuConn());
            $model->validaSeries($series);

            $this->setResponseOk(__FUNCTION__, $model->getBillNextNumberBySeries($series));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }



    /*********************
     * CLAIM BILLING DATA *
     *********************/

    /** @Route("/claims-billing-data", methods={"GET", "OPTIONS"}) */
    public function getAllClaimsBillingData(Request $request): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new BillsModel($this->popuConn());
            $response = $model->getAllClaimsBillingData();

            $page = $this->getPageFromURL($request);
            if ($page) {
                $response = $model->paginateResult($response, $page, $this->getItemsPerPageFromURL($request));
            }

            $this->setResponseOk(__FUNCTION__, $response);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/claims-billing-data/by-claim/{claimId}", methods={"GET", "OPTIONS"}) */
    public function getClaimBillingDataByClaimId($claimId): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new BillsModel($this->popuConn());
            $model->validaClaimId($claimId, true);

            $this->setResponseOk(__FUNCTION__, $model->getClaimBillingDataByClaimId($claimId));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/claims-billing-data/{id}", methods={"GET", "OPTIONS"}) */
    public function getClaimBillingDataById($id): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new BillsModel($this->popuConn());
            $model->validaClaimBillingDataId($id, true);

            $this->setResponseOk(__FUNCTION__, $model->getClaimBillingDataById($id));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/claims-billing-data/create", methods={"POST", "OPTIONS"}) */
    public function createClaimBillingData(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);

            $model = new BillsModel($this->popuConn());
            $model->validaClaimBillingData($data);

            if ($model->existsClaimBillingDataByClaimId($data['claimId'])) {
                $oldData = $model->getClaimBillingDataByClaimId($data['claimId']);
                $data['id'] = $oldData['id'];
                $model->editClaimBillingData($data);
                $data = $model->getClaimBillingDataByClaimId($data['claimId']);

                $this->saveChangeLog(__FUNCTION__, $data['id'], $data, $oldData);
            } else {
                $id   = $model->createClaimBillingData($data);
                $data = $model->getClaimBillingDataById($id);

                $this->saveChangeLog(__FUNCTION__, $id, $data);
            }

            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/claims-billing-data/edit/{id}", methods={"PUT", "OPTIONS"}) */
    public function editClaimBillingData(Request $request, $id): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);
            $data['id'] = $id;

            $model = new BillsModel($this->popuConn());
            $model->validaClaimBillingDataId($id, true);
            $model->validaClaimBillingData($data);
            $oldData = $model->getClaimBillingDataById($id);
            $model->editClaimBillingData($data);
            $data = $model->getClaimBillingDataById($id);

            $this->saveChangeLog(__FUNCTION__, $id, $data, $oldData);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/claims-billing-data/edit-is-valid-by-claim/{claimId}/{isValid}", methods={"PUT", "OPTIONS"}) */
    public function editClaimBillingDataIsValidByClaim(Request $request, $claimId, $isValid): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $model = new BillsModel($this->popuConn());
            $model->validaEditIsValid($claimId, $isValid);
            $oldData = $model->getClaimBillingDataByClaimId($claimId);
            $model->editClaimBillingDataIsValidByClaim($claimId, $isValid);
            $data = $model->getClaimBillingDataByClaimId($claimId);

            $this->saveChangeLog(__FUNCTION__, $data['id'], $data, $oldData);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/claims-billing-data/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteClaimBillingData(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $ids = $this->getJsonParams($request);

            $model = new BillsModel($this->popuConn());
            $model->validaClaimBillingDataIdsToDelete($ids);

            $this->popuConn()->beginTransaction();
            foreach ($ids as $id) {
                $dataToDelete = $model->getClaimBillingDataById($id);
                try {
                    $model->deleteClaimBillingData($id);
                } catch (DBALException $e) {
                    $this->deleteErrors[] = $this->parseErrorIfIsFkRestrictException($e->getMessage(), $id);
                    continue;
                }
                $this->saveChangeLog(__FUNCTION__, $id, $dataToDelete);
            }
            $this->popuConn()->commit();

            $this->setResponseOk(__FUNCTION__, $this->deleteErrors);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }



    /*********************
     ** BILL PREFERENCES **
     *********************/

    /** @Route("/bill-preferences", methods={"GET", "OPTIONS"}) */
    public function getAllBillPreferences(Request $request): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new BillsModel($this->popuConn());
            $response = $model->getAllBillPreferences();

            $page = $this->getPageFromURL($request);
            if ($page) {
                $response = $model->paginateResult($response, $page, $this->getItemsPerPageFromURL($request));
            }

            $this->setResponseOk(__FUNCTION__, $response);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/bill-preferences/{id}", methods={"GET", "OPTIONS"}) */
    public function getBillPreferenceById($id): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new BillsModel($this->popuConn());
            $model->validaBillPreferenceId($id, true);

            $this->setResponseOk(__FUNCTION__, $model->getBillPreferenceById($id));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/bill-preferences/create", methods={"POST", "OPTIONS"}) */
    public function createBillPreference(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);

            $model = new BillsModel($this->popuConn());
            $model->validaBillPreference($data);
            $id   = $model->createBillPreference($data);
            $data = $model->getBillPreferenceById($id);

            $this->saveChangeLog(__FUNCTION__, $id, $data);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/bill-preferences/edit/{id}", methods={"PUT", "OPTIONS"}) */
    public function editBillPreference(Request $request, $id): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);
            $data['id'] = $id;

            $model = new BillsModel($this->popuConn());
            $model->validaBillPreferenceId($id, true);
            $model->validaBillPreference($data);
            $oldData = $model->getBillPreferenceById($id);
            $model->editBillPreference($data);
            $data = $model->getBillPreferenceById($id);

            $this->saveChangeLog(__FUNCTION__, $id, $data, $oldData);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/bill-preferences/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteBillPreference(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $ids = $this->getJsonParams($request);

            $model = new BillsModel($this->popuConn());
            $model->validaBillPreferenceIdsToDelete($ids);

            $this->popuConn()->beginTransaction();
            foreach ($ids as $id) {
                $dataToDelete = $model->getBillPreferenceById($id);
                try {
                    $model->deleteBillPreference($id);
                } catch (DBALException $e) {
                    $this->deleteErrors[] = $this->parseErrorIfIsFkRestrictException($e->getMessage(), $id);
                    continue;
                }
                $this->saveChangeLog(__FUNCTION__, $id, $dataToDelete);
            }
            $this->popuConn()->commit();

            $this->setResponseOk(__FUNCTION__, $this->deleteErrors);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }



    /*********************
     ***** BILL TYPES *****
     *********************/

    /** @Route("/bill-types", methods={"GET", "OPTIONS"}) */
    public function getAllBillTypes(Request $request): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new BillsModel($this->popuConn());
            $response = $model->getAllBillTypes();

            $page = $this->getPageFromURL($request);
            if ($page) {
                $response = $model->paginateResult($response, $page, $this->getItemsPerPageFromURL($request));
            }

            $this->setResponseOk(__FUNCTION__, $response);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/bill-types/{id}", methods={"GET", "OPTIONS"}) */
    public function getBillTypeById($id): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new BillsModel($this->popuConn());
            $model->validaBillTypeId($id, true);

            $this->setResponseOk(__FUNCTION__, $model->getBillTypeById($id));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/bill-types/create", methods={"POST", "OPTIONS"}) */
    public function createBillType(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);

            $model = new BillsModel($this->popuConn());
            $model->validaBillType($data);
            $id   = $model->createBillType($data);
            $data = $model->getBillTypeById($id);

            $this->saveChangeLog(__FUNCTION__, $id, $data);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/bill-types/edit/{id}", methods={"PUT", "OPTIONS"}) */
    public function editBillType(Request $request, $id): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);
            $data['id'] = $id;

            $model = new BillsModel($this->popuConn());
            $model->validaBillTypeId($id, true);
            $model->validaBillType($data);
            $oldData = $model->getBillTypeById($id);
            $model->editBillType($data);
            $data = $model->getBillTypeById($id);

            $this->saveChangeLog(__FUNCTION__, $id, $data, $oldData);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/bill-types/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteBillType(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $ids = $this->getJsonParams($request);

            $model = new BillsModel($this->popuConn());
            $model->validaBillTypeIdsToDelete($ids);

            $this->popuConn()->beginTransaction();
            foreach ($ids as $id) {
                $dataToDelete = $model->getBillTypeById($id);
                try {
                    $model->deleteBillType($id);
                } catch (DBALException $e) {
                    $this->deleteErrors[] = $this->parseErrorIfIsFkRestrictException($e->getMessage(), $id);
                    continue;
                }
                $this->saveChangeLog(__FUNCTION__, $id, $dataToDelete);
            }
            $this->popuConn()->commit();

            $this->setResponseOk(__FUNCTION__, $this->deleteErrors);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }



    /***********************
     * PAYMENTS EMAIL SENT *
     ***********************/

    /** @Route("/payments-email-sent-by-claim/{claimId}", methods={"GET", "OPTIONS"}) */
    public function getPaymentEmailSentByClaimId($claimId): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new BillsModel($this->popuConn());
            $model->validaClaimId($claimId, true);

            $this->setResponseOk(__FUNCTION__, $model->getPaymentEmailSentByClaimId($claimId));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/payments-email-sent-by-claim/add/{claimId}", methods={"POST", "OPTIONS"}) */
    public function addPaymentsTimesSent(Request $request, $claimId): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $model = new BillsModel($this->popuConn());
            $model->validaClaimId($claimId, true);
            $model->addPaymentsTimesSent($claimId);
            $data = $model->getPaymentEmailSentByClaimId($claimId);

            $this->saveChangeLog(__FUNCTION__, $claimId, $data);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/payments-email-sent-by-claim/delete/{claimId}", methods={"DELETE", "OPTIONS"}) */
    public function deletePaymentEmailSent(Request $request, $claimId): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $model = new BillsModel($this->popuConn());
            $model->validaClaimIdHasEmailSent($claimId);
            $dataToDelete = $model->getPaymentEmailSentByClaimId($claimId);
            $model->deletePaymentEmailSent($claimId);

            $this->saveChangeLog(__FUNCTION__, $claimId, $dataToDelete);
            $this->setResponseOk(__FUNCTION__);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }
}
