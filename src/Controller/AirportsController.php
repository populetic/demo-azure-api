<?php

namespace App\Controller;

use App\Entity\Airport;
use App\Model\AirportsModel;
use App\Service\Exceptions\ValidationException;
use Doctrine\DBAL\DBALException;
use ErrorException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use TypeError;

class AirportsController extends BaseController
{
    /** @Route("/airports", methods={"GET", "OPTIONS"}) */
    public function getAllAirports(Request $request): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new AirportsModel($this->popuConn());
            $response = $model->getAll();

            $page = $this->getPageFromURL($request);
            if ($page) {
                $response = $model->paginateResult($response, $page, $this->getItemsPerPageFromURL($request));
            }

            $this->setResponseOk(__FUNCTION__, $response);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/airports/{id}", methods={"GET", "OPTIONS"}) */
    public function getAirportById($id): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new AirportsModel($this->popuConn());
            $model->validaId($id, true);

            $this->setResponseOk(__FUNCTION__, $model->getById($id));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/airports/create", methods={"POST", "OPTIONS"}) */
    public function createAirport(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $airport = new Airport($this->getJsonParams($request));

            $model = new AirportsModel($this->popuConn());
            $model->validaCreation($airport);
            $airport = $model->create($airport);

            $this->saveChangeLog(__FUNCTION__, $airport->id, $airport);
            $this->setResponseOk(__FUNCTION__, $airport);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/airports/edit/{id}", methods={"PUT", "OPTIONS"}) */
    public function editAirport(Request $request, $id): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $airport = new Airport($this->getJsonParams($request));
            $airport->id = $id;

            $model = new AirportsModel($this->popuConn());
            $model->validaEdition($airport);
            $oldAirport = $model->getById($id);
            $airport    = $model->edit($airport);

            $this->saveChangeLog(__FUNCTION__, $id, $airport, $oldAirport);
            $this->setResponseOk(__FUNCTION__, $airport);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/airports/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteAirports(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $ids = $this->getJsonParams($request);

            $model = new AirportsModel($this->popuConn());
            $model->validaIdsToDelete($ids);

            $this->deleteSeveralIdsTransaction($this->popuConn(), $model, $ids, __FUNCTION__);

            $this->setResponseOk(__FUNCTION__, $this->deleteErrors);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }
}
