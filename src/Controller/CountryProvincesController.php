<?php

namespace App\Controller;

use App\Entity\CountryProvince;
use App\Model\CountryProvincesModel;
use App\Service\Exceptions\ValidationException;
use Doctrine\DBAL\DBALException;
use ErrorException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use TypeError;

class CountryProvincesController extends BaseController
{
    /** @Route("/country-provinces", methods={"GET", "OPTIONS"}) */
    public function getAllCountryProvinces(Request $request): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new CountryProvincesModel($this->popuConn());
            $response = $model->getAll();

            $page = $this->getPageFromURL($request);
            if ($page) {
                $response = $model->paginateResult($response, $page, $this->getItemsPerPageFromURL($request));
            }

            $this->setResponseOk(__FUNCTION__, $response);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/country-provinces/{id}", methods={"GET", "OPTIONS"}) */
    public function getCountryProvinceById($id): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new CountryProvincesModel($this->popuConn());
            $model->validaId($id, true);

            $this->setResponseOk(__FUNCTION__, $model->getById($id));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/country-provinces/create", methods={"POST", "OPTIONS"}) */
    public function createCountryProvince(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $province = new CountryProvince($this->getJsonParams($request));

            $model = new CountryProvincesModel($this->popuConn());
            $model->validaCreation($province);
            $province = $model->create($province);

            $this->saveChangeLog(__FUNCTION__, $province->id, $province);
            $this->setResponseOk(__FUNCTION__, $province);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/country-provinces/edit/{id}", methods={"PUT", "OPTIONS"}) */
    public function editCountryProvince(Request $request, $id): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $province = new CountryProvince($this->getJsonParams($request));
            $province->id = $id;

            $model = new CountryProvincesModel($this->popuConn());
            $model->validaEdition($province);
            $oldCountryProvince = $model->getById($id);
            $province    = $model->edit($province);

            $this->saveChangeLog(__FUNCTION__, $id, $province, $oldCountryProvince);
            $this->setResponseOk(__FUNCTION__, $province);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/country-provinces/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteCountryProvinces(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $ids = $this->getJsonParams($request);

            $model = new CountryProvincesModel($this->popuConn());
            $model->validaIdsToDelete($ids);

            $this->deleteSeveralIdsTransaction($this->popuConn(), $model, $ids, __FUNCTION__);

            $this->setResponseOk(__FUNCTION__, $this->deleteErrors);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }
}
