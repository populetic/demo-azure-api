<?php

namespace App\Controller;

use App\Entity\GmailApiDataAccess;
use App\Model\GmailApiModel;
use App\Service\Exceptions\ValidationException;
use Doctrine\DBAL\DBALException;
use ErrorException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use TypeError;

class GmailApiController extends BaseController
{
    //TODO a falta de ver cómo se termina, QUEDARÍA:
    // - Si hay cambios añadidos
    // - Testear
    // - Añadir endpoints a postman
    // - Documentación

    /*************************
     * GMAIL API DATA ACCESS *
     *************************/

    /** @Route("/gmail-api-data-access", methods={"GET", "OPTIONS"}) */
    public function getAllDataAccess(): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new GmailApiModel($this->popuConn());
            $this->setResponseOk(__FUNCTION__, $model->getAll());

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/gmail-api-data-access/{id}", methods={"GET", "OPTIONS"}) */
    public function getDataAccessById($id): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new GmailApiModel($this->popuConn());
            $model->validaDataAccessId($id, true);

            $this->setResponseOk(__FUNCTION__, $model->getById($id));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/gmail-api-data-access-by-email/{email}", methods={"GET", "OPTIONS"}) */
    public function getDataAccessByEmail($email): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new GmailApiModel($this->popuConn());
            $model->validaEmail($email, true, true);

            $this->setResponseOk(__FUNCTION__, $model->getByEmail($email));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/gmail-api-data-access/create", methods={"POST", "OPTIONS"}) */
    public function createDataAccess(Request $request): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $dataAccess = new GmailApiDataAccess($this->getJsonParams($request));

            $model = new GmailApiModel($this->popuConn());
            $model->validaDataAccess($dataAccess);
            $dataAccess = $model->create($dataAccess);

            $this->saveChangeLog(__FUNCTION__, $dataAccess->id, $dataAccess);
            $this->setResponseOk(__FUNCTION__, $dataAccess);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/gmail-api-data-access/edit/{id}", methods={"PUT", "OPTIONS"}) */
    public function editDataAccess(Request $request, $id): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $dataAccess = new GmailApiDataAccess($this->getJsonParams($request));
            $dataAccess->id = $id;

            $model = new GmailApiModel($this->popuConn());
            $model->validaDataAccessId($id, true);
            $model->validaDataAccess($dataAccess);
            $dataAccess = $model->edit($dataAccess);

            $this->saveChangeLog(__FUNCTION__, $dataAccess->id, $dataAccess);
            $this->setResponseOk(__FUNCTION__, $dataAccess);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/gmail-api-data-access/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteDataAccess(Request $request): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $ids = $this->getJsonParams($request);

            $model = new GmailApiModel($this->popuConn());
            $model->validaDataAccessIdsToDelete($ids);

            $this->popuConn()->beginTransaction();
            foreach ($ids as $id) {
                try {
                    $model->delete($id);
                } catch (DBALException $e) {
                    $this->deleteErrors[] = $this->parseErrorIfIsFkRestrictException($e->getMessage(), $id);
                    continue;
                }
            }
            $this->popuConn()->commit();

            $this->setResponseOk(__FUNCTION__, $this->deleteErrors);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }



    /*******************************
     * GMAIL API DISRUPTED FLIGHTS *
     *******************************/

    /** @Route("/gmail-api-disrupted-flights/by-ids", methods={"POST", "OPTIONS"}) */
    public function getGmailApiDisruptedFlightByIds(Request $request): JsonResponse
    {
        //TODO revisar esto porque no se si es necesario ni si se hace bien... porque solo
        //TODO estoy buscando por gmail api data access y flight id y que devuelva un solo resultado
        try {
            $this->checkHostIsAllowed();

            $data = $this->getJsonParams($request);

            $model = new GmailApiModel($this->popuConn());
            $model->validaGmailApiDisruptedFlightExists($data);
            $response = $model->getGmailApiDisruptedFlightByIds($data);

            $this->setResponseOk(__FUNCTION__, $response);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/gmail-api-disrupted-flights/create", methods={"POST", "OPTIONS"}) */
    public function createGmailApiDisruptedFlight(Request $request): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $data = $this->getJsonParams($request);

            $model = new GmailApiModel($this->popuConn());
            $model->validaGmailApiDisruptedFlight($data);

            $this->popuConn()->beginTransaction();
            $model->deleteGmailApiDisruptedFlight($data);
            $model->createGmailApiDisruptedFlight($data);
            $this->popuConn()->commit();

            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/gmail-api-disrupted-flights/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteGmailApiDisruptedFlight(Request $request): JsonResponse
    {
        try {
            //TODO elimina todod solo por gmail api data access id y flight id?
            $this->checkHostIsAllowed();

            $data = $this->getJsonParams($request);

            $model = new GmailApiModel($this->popuConn());
            $model->validaGmailApiDisruptedFlightExists($data);
            $model->deleteGmailApiDisruptedFlight($data);

            $this->setResponseOk(__FUNCTION__);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }
}
