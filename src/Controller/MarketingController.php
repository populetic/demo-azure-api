<?php

namespace App\Controller;

use App\Model\MarketingModel;
use App\Service\Exceptions\ValidationException;
use Doctrine\DBAL\DBALException;
use ErrorException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use TypeError;

class MarketingController extends BaseController
{
    /*********************
     ** ADWORDS TRACKING **
     *********************/

    /** @Route("/adwords-tracking", methods={"GET", "OPTIONS"}) */
    public function getAllAdwordsTrackings(Request $request): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new MarketingModel($this->popuConn());

            $page         = $this->getPageFromURL($request);
            $itemsPerPage = $this->getItemsPerPageFromURL($request);

            $res['totalItems']   = $model->getTotalAdwordsTrackingCount();
            $res['page']         = $page ? $page : 1;
            $res['itemsPerPage'] = $itemsPerPage ? $itemsPerPage : $model::ITEMS_PER_PAGE;
            $res['totalPages']   = ceil($res['totalItems'] / $res['itemsPerPage']);
            $res['items']        = $model->getAdwordsTrackingsByPageAndItemsPerPage($res['page'], $res['itemsPerPage']);

            $this->setResponseOk(__FUNCTION__, $res);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/adwords-tracking/{id}", methods={"GET", "OPTIONS"}) */
    public function getAdwordsTrackingById($id): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new MarketingModel($this->popuConn());
            $model->validaAdwordsTrackingId($id, true);

            $this->setResponseOk(__FUNCTION__, $model->getAdwordsTrackingById($id));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/adwords-tracking/create", methods={"POST", "OPTIONS"}) */
    public function createAdwordsTracking(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);

            $model = new MarketingModel($this->popuConn());
            $model->validaAdwordsTracking($data);
            $id   = $model->createAdwordsTracking($data);
            $data = $model->getAdwordsTrackingById($id);

            $this->saveChangeLog(__FUNCTION__, $id, $data);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/adwords-tracking/edit/{id}", methods={"PUT", "OPTIONS"}) */
    public function editAdwordsTracking(Request $request, $id): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);
            $data['id'] = $id;

            $model = new MarketingModel($this->popuConn());
            $model->validaAdwordsTrackingId($id, true);
            $model->validaAdwordsTracking($data);
            $oldData = $model->getAdwordsTrackingById($id);
            $model->editAdwordsTracking($data);
            $data = $model->getAdwordsTrackingById($id);

            $this->saveChangeLog(__FUNCTION__, $id, $data, $oldData);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/adwords-tracking/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteAdwordsTracking(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $ids = $this->getJsonParams($request);

            $model = new MarketingModel($this->popuConn());
            $model->validaAdwordsTrackingIdsToDelete($ids);

            $this->popuConn()->beginTransaction();
            foreach ($ids as $id) {
                $dataToDelete = $model->getAdwordsTrackingById($id);
                try {
                    $model->deleteAdwordsTracking($id);
                } catch (DBALException $e) {
                    $this->deleteErrors[] = $this->parseErrorIfIsFkRestrictException($e->getMessage(), $id);
                    continue;
                }
                $this->saveChangeLog(__FUNCTION__, $id, $dataToDelete);
            }
            $this->popuConn()->commit();

            $this->setResponseOk(__FUNCTION__, $this->deleteErrors);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }



    /***************************
     * AIRLINES REVENUE IMPACT *
     ***************************/

    /** @Route("/airlines-revenue-impact/{airlineId}/{countryId}", methods={"GET", "OPTIONS"}) */
    public function getAllRevenueImpactsByAirlineAndCountry(Request $request, $airlineId, $countryId): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new MarketingModel($this->popuConn());
            $model->validaAirlineAndCountryAndDataExists($airlineId, $countryId);

            $getLast = $request->query->getInt('last', null);

            if ($getLast) {
                $response = $model->getLastRevenueImpactByAirlineAndCountry($airlineId, $countryId);
            } else {
                $response = $model->getAllRevenueImpactsByAirlineAndCountry($airlineId, $countryId);
            }

            $this->setResponseOk(__FUNCTION__, $response);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/airlines-revenue-impact/create", methods={"POST", "OPTIONS"}) */
    public function createRevenueImpact(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);

            $model = new MarketingModel($this->popuConn());
            $model->validaRevenueImpact($data);
            $id   = $model->createRevenueImpact($data);
            $data = $model->getRevenueImpactById($id);

            $this->saveChangeLog(__FUNCTION__, $id, $data);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/airlines-revenue-impact/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteRevenueImpact(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $ids = $this->getJsonParams($request);

            $model = new MarketingModel($this->popuConn());
            $model->validaRevenueImpactIdsToDelete($ids);

            $this->popuConn()->beginTransaction();
            foreach ($ids as $id) {
                $dataToDelete = $model->getRevenueImpactById($id);
                try {
                    $model->deleteRevenueImpact($id);
                } catch (DBALException $e) {
                    $this->deleteErrors[] = $this->parseErrorIfIsFkRestrictException($e->getMessage(), $id);
                    continue;
                }
                $this->saveChangeLog(__FUNCTION__, $id, $dataToDelete);
            }
            $this->popuConn()->commit();

            $this->setResponseOk(__FUNCTION__, $this->deleteErrors);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }
}
