<?php

namespace App\Controller;

use App\Entity\Court;
use App\Model\CourtsModel;
use App\Service\Exceptions\ValidationException;
use Doctrine\DBAL\DBALException;
use ErrorException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use TypeError;

class CourtsController extends BaseController
{
    /*********************
     ****** COURTS ******
     *********************/

    /** @Route("/courts", methods={"GET", "OPTIONS"}) */
    public function getAllCourts(Request $request): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new CourtsModel($this->popuConn());
            $response = $model->getAll();

            $page = $this->getPageFromURL($request);
            if ($page) {
                $response = $model->paginateResult($response, $page, $this->getItemsPerPageFromURL($request));
            }

            $this->setResponseOk(__FUNCTION__, $response);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/courts/{id}", methods={"GET", "OPTIONS"}) */
    public function getCourtById($id): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new CourtsModel($this->popuConn());
            $model->validaId($id, true);

            $this->setResponseOk(__FUNCTION__, $model->getById($id));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/courts/create", methods={"POST", "OPTIONS"}) */
    public function createCourt(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $court = new Court($this->getJsonParams($request));

            $model = new CourtsModel($this->popuConn());
            $model->validaCourt($court);
            $court = $model->create($court);

            $this->saveChangeLog(__FUNCTION__, $court->id, $court);
            $this->setResponseOk(__FUNCTION__, $court);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/courts/edit/{id}", methods={"PUT", "OPTIONS"}) */
    public function editCourt(Request $request, $id): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $court = new Court($this->getJsonParams($request));
            $court->id = $id;

            $model = new CourtsModel($this->popuConn());
            $model->validaId($id, true);
            $model->validaCourt($court);
            $oldCourt = $model->getById($id);
            $court    = $model->edit($court);

            $this->saveChangeLog(__FUNCTION__, $id, $court, $oldCourt);
            $this->setResponseOk(__FUNCTION__, $court);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/courts/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteCourts(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $ids = $this->getJsonParams($request);

            $model = new CourtsModel($this->popuConn());
            $model->validaIdsToDelete($ids);

            $this->deleteSeveralIdsTransaction($this->popuConn(), $model, $ids, __FUNCTION__);

            $this->setResponseOk(__FUNCTION__, $this->deleteErrors);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }



    /*********************
     **** COURT TYPES ****
     *********************/

    /** @Route("/court-types", methods={"GET", "OPTIONS"}) */
    public function getAllCourtTypes(): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new CourtsModel($this->popuConn());
            $this->setResponseOk(__FUNCTION__, $model->getAllCourtTypes());

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/court-types/{id}", methods={"GET", "OPTIONS"}) */
    public function getCourtTypeById($id): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new CourtsModel($this->popuConn());
            $model->validaCourtTypeId($id, true);

            $this->setResponseOk(__FUNCTION__, $model->getCourtTypeById($id));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/court-types/create", methods={"POST", "OPTIONS"}) */
    public function createCourtType(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);

            $model = new CourtsModel($this->popuConn());
            $model->validaCourtTypeCreation($data);
            $data = $model->createCourtType($data);

            $this->saveChangeLog(__FUNCTION__, $data['id'], $data);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/court-types/edit/{id}", methods={"PUT", "OPTIONS"}) */
    public function editCourtType(Request $request, $id): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);
            $data['id'] = $id;

            $model = new CourtsModel($this->popuConn());
            $model->validaCourtTypeId($id, true);
            $model->validaCourtTypeEdition($data);
            $oldData = $model->getCourtTypeById($id);
            $model->editCourtType($data);
            $data = $model->getCourtTypeById($id);

            $this->saveChangeLog(__FUNCTION__, $id, $data, $oldData);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/court-types/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteCourtType(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $ids = $this->getJsonParams($request);

            $model = new CourtsModel($this->popuConn());
            $model->validaCourtTypeIdsToDelete($ids);

            $this->popuConn()->beginTransaction();
            foreach ($ids as $id) {
                $dataToDelete = $model->getCourtTypeById($id);
                try {
                    $model->deleteCourtType($id);
                } catch (DBALException $e) {
                    $this->deleteErrors[] = $this->parseErrorIfIsFkRestrictException($e->getMessage(), $id);
                    continue;
                }
                $this->saveChangeLog(__FUNCTION__, $id, $dataToDelete);
            }
            $this->popuConn()->commit();

            $this->setResponseOk(__FUNCTION__, $this->deleteErrors);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }



    /*********************
     *** COURTS NUMBER ***
     *********************/

    /** @Route("/courts-number", methods={"GET", "OPTIONS"}) */
    public function getAllCourtsNumber(Request $request): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new CourtsModel($this->popuConn());
            $response = $model->getAllCourtsNumber();

            $page = $this->getPageFromURL($request);
            if ($page) {
                $response = $model->paginateResult($response, $page, $this->getItemsPerPageFromURL($request));
            }


            $this->setResponseOk(__FUNCTION__, $response);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/courts-number/{id}", methods={"GET", "OPTIONS"}) */
    public function getCourtNumberById($id): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new CourtsModel($this->popuConn());
            $model->validaCourtNumberId($id, true);

            $this->setResponseOk(__FUNCTION__, $model->getCourtNumberById($id));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/courts-number/by-court-id/{courtId}", methods={"GET", "OPTIONS"}) */
    public function getCourtsNumberByCourtId($courtId): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new CourtsModel($this->popuConn());
            $model->validaId($courtId, true);

            $this->setResponseOk(__FUNCTION__, $model->getCourtsNumberByCourtId($courtId));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/courts-number/create", methods={"POST", "OPTIONS"}) */
    public function createCourtNumber(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);

            $model = new CourtsModel($this->popuConn());
            $model->validaCourtNumber($data);
            $id   = $model->createCourtNumber($data);
            $data = $model->getCourtNumberById($id);

            $this->saveChangeLog(__FUNCTION__, $id, $data);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/courts-number/edit/{id}", methods={"PUT", "OPTIONS"}) */
    public function editCourtNumber(Request $request, $id): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);
            $data['id'] = $id;

            $model = new CourtsModel($this->popuConn());
            $model->validaCourtNumberId($id, true);
            $model->validaCourtNumber($data);
            $oldData = $model->getCourtNumberById($id);
            $model->editCourtNumber($data);
            $data = $model->getCourtNumberById($id);

            $this->saveChangeLog(__FUNCTION__, $id, $data, $oldData);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/courts-number/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteCourtNumber(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $ids = $this->getJsonParams($request);

            $model = new CourtsModel($this->popuConn());
            $model->validaCourtNumberIdsToDelete($ids);

            $this->popuConn()->beginTransaction();
            foreach ($ids as $id) {
                $dataToDelete = $model->getCourtNumberById($id);
                try {
                    $model->deleteCourtNumber($id);
                } catch (DBALException $e) {
                    $this->deleteErrors[] = $this->parseErrorIfIsFkRestrictException($e->getMessage(), $id);
                    continue;
                }
                $this->saveChangeLog(__FUNCTION__, $id, $dataToDelete);
            }
            $this->popuConn()->commit();

            $this->setResponseOk(__FUNCTION__, $this->deleteErrors);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }
}
