<?php

namespace App\Controller;

use App\Entity\City;
use App\Model\CitiesModel;
use App\Service\Exceptions\ValidationException;
use Doctrine\DBAL\DBALException;
use ErrorException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use TypeError;

class CitiesController extends BaseController
{
    /** @Route("/cities", methods={"GET", "OPTIONS"}) */
    public function getAllCities(Request $request): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new CitiesModel($this->popuConn());
            $response = $model->getAll();

            $page = $this->getPageFromURL($request);
            if ($page) {
                $response = $model->paginateResult($response, $page, $this->getItemsPerPageFromURL($request));
            }

            $this->setResponseOk(__FUNCTION__, $response);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/cities/{id}", methods={"GET", "OPTIONS"}) */
    public function getCityById($id): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new CitiesModel($this->popuConn());
            $model->validaId($id, true);

            $this->setResponseOk(__FUNCTION__, $model->getById($id));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/cities/create", methods={"POST", "OPTIONS"}) */
    public function createCity(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $city = new City($this->getJsonParams($request));

            $model = new CitiesModel($this->popuConn());
            $model->validaCreation($city);
            $city = $model->create($city);

            $this->saveChangeLog(__FUNCTION__, $city->id, $city);
            $this->setResponseOk(__FUNCTION__, $city);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/cities/edit/{id}", methods={"PUT", "OPTIONS"}) */
    public function editCity(Request $request, $id): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $city = new City($this->getJsonParams($request));
            $city->id = $id;

            $model = new CitiesModel($this->popuConn());
            $model->validaEdition($city);
            $oldCity = $model->getById($id);
            $city    = $model->edit($city);

            $this->saveChangeLog(__FUNCTION__, $id, $city, $oldCity);
            $this->setResponseOk(__FUNCTION__, $city);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/cities/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteCities(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $ids = $this->getJsonParams($request);

            $model = new CitiesModel($this->popuConn());
            $model->validaIdsToDelete($ids);

            $this->deleteSeveralIdsTransaction($this->popuConn(), $model, $ids, __FUNCTION__);

            $this->setResponseOk(__FUNCTION__, $this->deleteErrors);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }
}
