<?php

namespace App\Controller\TranslationSystem;

use App\Controller\BaseController;
use App\Model\TranslationSystem\TranslationsModel;
use App\Service\Exceptions\ValidationException;
use Doctrine\DBAL\DBALException;
use ErrorException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use TypeError;

class TranslationsController extends BaseController
{
    /** @Route("/translations/by-app/{app}", methods={"GET", "OPTIONS"}) */
    public function getAllTranslationsByApp($app): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new TranslationsModel($this->translationsConn());
            $model->validaAppHasTranslationSystem($app);
            $languages    = $model->getAllActiveLanguagesByApp($app);
            $translations = $model->getAllTranslationsForAppByLanguages($app, $languages);

            $this->setResponseOk(__FUNCTION__, $translations);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/translations/translate-tag-by-lang-id/{app}", methods={"POST", "OPTIONS"}) */
    public function translateTagByLangId(Request $request, $app): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $params = $this->getJsonParams($request);
            $tagId  = $this->getDataIfKeyExistsOrNull($params, 'tagId');
            $langId = $this->getDataIfKeyExistsOrNull($params, 'langId');

            $model = new TranslationsModel($this->translationsConn());
            $model->validaDataToTranslateTag($app, $langId, $tagId);

            $this->setResponseOk(__FUNCTION__, $model->translateTag($app, $langId, $tagId));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/translations/translate-tag-to-default-language-by-app/{app}/{tagId}", methods={"GET", "OPTIONS"}) */
    public function translateTagToDefaultLanguageByApp($app, $tagId): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new TranslationsModel($this->translationsConn());
            $model->validaDataToTranslateTagToDefault($app, $tagId);

            $this->setResponseOk(__FUNCTION__, $model->getDefaultTranslationByAppAndTag($app, $tagId));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }
}
