<?php

namespace App\Controller\TranslationSystem;

use App\Controller\BaseController;
use App\Entity\Language;
use App\Model\TranslationSystem\LanguagesModel;
use App\Service\Exceptions\ValidationException;
use Doctrine\DBAL\DBALException;
use ErrorException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use TypeError;

class LanguagesController extends BaseController
{
    /*********************
     ***** LANGUAGES *****
     *********************/

    /** @Route("/languages", methods={"GET", "OPTIONS"}) */
    public function getAllLanguages(): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new LanguagesModel($this->translationsConn());
            $this->setResponseOk(__FUNCTION__, $model->getAll());

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/languages/{id}", methods={"GET", "OPTIONS"}) */
    public function getLanguageById($id): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new LanguagesModel($this->translationsConn());
            $model->validaId($id, true);

            $this->setResponseOk(__FUNCTION__, $model->getById($id));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/languages/create", methods={"POST", "OPTIONS"}) */
    public function createLanguage(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $language = new Language($this->getJsonParams($request));

            $model = new LanguagesModel($this->translationsConn());
            $model->validaLanguage($language);
            $language = $model->create($language);

            $this->saveChangeLog(__FUNCTION__, $language->id, $language);
            $this->setResponseOk(__FUNCTION__, $language);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/languages/edit/{id}", methods={"PUT", "OPTIONS"}) */
    public function editLanguage(Request $request, $id): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $language = new Language($this->getJsonParams($request));
            $language->id = $id;

            $model = new LanguagesModel($this->translationsConn());
            $model->validaId($id, true);
            $model->validaLanguage($language);
            $oldLanguage = $model->getById($id);
            $language    = $model->edit($language);

            $this->saveChangeLog(__FUNCTION__, $id, $language, $oldLanguage);
            $this->setResponseOk(__FUNCTION__, $language);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/languages/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteLanguages(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $ids = $this->getJsonParams($request);

            $model = new LanguagesModel($this->translationsConn());
            $model->validaIdsToDelete($ids);
            $this->deleteSeveralIdsTransaction($this->translationsConn(), $model, $ids, __FUNCTION__);

            $this->setResponseOk(__FUNCTION__, $this->deleteErrors);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->translationsConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }



    /*********************
     ** LANGUAGES BY APP **
     *********************/

    /** @Route("/languages-by-app/{appId}", methods={"GET", "OPTIONS"}) */
    public function getAllLanguagesByApp(Request $request, $appId): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new LanguagesModel($this->translationsConn());
            $model->validaAppId($appId);
            $response = $model->getAllLanguagesByApp($appId);

            $page = $this->getPageFromURL($request);
            if ($page) {
                $response = $model->paginateResult($response, $page, $this->getItemsPerPageFromURL($request));
            }

            $this->setResponseOk(__FUNCTION__, $response);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/languages-by-app/edit/{appId}", methods={"POST", "OPTIONS"}) */
    public function editLanguagesByApp(Request $request, $appId): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);

            $model = new LanguagesModel($this->translationsConn());
            $model->validaAppId($appId);
            $model->validaLanguages($data);

            $this->translationsConn()->beginTransaction();
            $model->deleteLanguagesByApp($appId);
            $this->saveChangeLog(__FUNCTION__, $appId, 'Deleted all languages for appId: ' . $appId);
            $model->editLanguagesByApp($appId, $data['languages']);

            $newLanguages = $model->getAllLanguagesByApp($appId);
            foreach ($newLanguages as $lang) {
                $this->saveChangeLog(
                    __FUNCTION__,
                    $appId,
                    'Added language "' . $lang['languageName'] . '" for appId: "' . $appId . '"' .
                    ($lang['isDefault'] ? ' (Default Language).' : '.')
                );
            }
            $this->translationsConn()->commit();

            $this->setResponseOk(__FUNCTION__, $newLanguages);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->translationsConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }
}
