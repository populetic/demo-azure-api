<?php

namespace App\Controller;

use App\Entity\Airline;
use App\Model\AirlinesModel;
use App\Service\Exceptions\ValidationException;
use Doctrine\DBAL\DBALException;
use ErrorException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use TypeError;

class AirlinesController extends BaseController
{
    /*********************
     ****** AIRLINES ******
     *********************/

    /** @Route("/airlines", methods={"GET", "OPTIONS"}) */
    public function getAllAirlines(Request $request): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new AirlinesModel($this->popuConn());
            $response = $model->getAll();

            $page = $this->getPageFromURL($request);
            if ($page) {
                $response = $model->paginateResult($response, $page, $this->getItemsPerPageFromURL($request));
            }

            $this->setResponseOk(__FUNCTION__, $response);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/airlines/{id}", methods={"GET", "OPTIONS"}) */
    public function getAirlineById($id): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new AirlinesModel($this->popuConn());
            $model->validaAirlineId($id, true);

            $this->setResponseOk(__FUNCTION__, $model->getById($id));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/airlines/create", methods={"POST", "OPTIONS"}) */
    public function createAirline(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $airline = new Airline($this->getJsonParams($request));

            $model = new AirlinesModel($this->popuConn());
            $model->validaCreation($airline);
            $airline = $model->create($airline);

            $this->saveChangeLog(__FUNCTION__, $airline->id, $airline);
            $this->setResponseOk(__FUNCTION__, $airline);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/airlines/edit/{id}", methods={"PUT", "OPTIONS"}) */
    public function editAirline(Request $request, $id): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $airline = new Airline($this->getJsonParams($request));
            $airline->id = $id;

            $model = new AirlinesModel($this->popuConn());
            $model->validaEdition($airline);
            $oldAirline = $model->getById($id);
            $airline    = $model->edit($airline);

            $this->saveChangeLog(__FUNCTION__, $id, $airline, $oldAirline);
            $this->setResponseOk(__FUNCTION__, $airline);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/airlines/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteAirlines(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $ids = $this->getJsonParams($request);

            $model = new AirlinesModel($this->popuConn());
            $model->validaIdsToDelete($ids);

            $this->deleteSeveralIdsTransaction($this->popuConn(), $model, $ids, __FUNCTION__);

            $this->setResponseOk(__FUNCTION__, $this->deleteErrors);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }



    /************************************
     ** AIRLINES LEGAL DATA BY COUNTRY **
     ************************************/

    /** @Route("/airlines-legal-data-by-country/{airlineId}", methods={"GET", "OPTIONS"}) */
    public function getAllCountryLegalDataByAirlineId($airlineId): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new AirlinesModel($this->popuConn());
            $model->validaAirlineId($airlineId, true);
            $this->setResponseOk(__FUNCTION__, $model->getAllCountryLegalDataByAirlineId($airlineId));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/airlines-legal-data-by-country/create", methods={"POST", "OPTIONS"}) */
    public function createAirlineLegalDataByCountry(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $legalData = $this->getJsonParams($request);

            $model = new AirlinesModel($this->popuConn());
            $model->validaLegalDataCreation($legalData);
            $model->createLegalData($legalData);

            $this->saveChangeLog(__FUNCTION__, [], $legalData);
            $this->setResponseOk(__FUNCTION__, $legalData);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/airlines-legal-data-by-country/edit", methods={"PUT", "OPTIONS"}) */
    public function editAirlineLegalDataByCountry(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);

            $model = new AirlinesModel($this->popuConn());
            $model->validaLegalDataEdition($data);
            $oldData = $model->getLegalDataByAirlineAndCountry($data['airlineId'], $data['countryId']);
            $model->editLegalData($data);

            $this->saveChangeLog(__FUNCTION__, [$data['airlineId'], $data['countryId']], $data, $oldData);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/airlines-legal-data-by-country/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteAirlineLegalDataByCountry(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $airlineAndCountry = $this->getJsonParams($request);
            $airlineId = $this->getDataIfKeyExistsOrNull($airlineAndCountry, 'airlineId');
            $countryId = $this->getDataIfKeyExistsOrNull($airlineAndCountry, 'countryId');

            $model = new AirlinesModel($this->popuConn());
            $model->validaLegalDataDeletion($airlineId, $countryId);
            $legalDataToDelete = $model->getLegalDataByAirlineAndCountry($airlineId, $countryId);
            $model->deleteLegalData($airlineId, $countryId);

            $this->saveChangeLog(__FUNCTION__, [], $legalDataToDelete);
            $this->setResponseOk(__FUNCTION__);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }



    /************************************
     * AIRLINES REQUIRED DOC BY COUNTRY *
     ************************************/

    /** @Route("/airlines-required-documentation-by-country/{airlineId}", methods={"GET", "OPTIONS"}) */
    public function getAllCountryRequiredDocByAirlineId($airlineId): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new AirlinesModel($this->popuConn());
            $model->validaAirlineId($airlineId, true);
            $this->setResponseOk(__FUNCTION__, $model->getAllCountryRequiredDocByAirlineId($airlineId));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/airlines-required-documentation-by-country/create", methods={"POST", "OPTIONS"}) */
    public function createAirlineRequiredDocByCountry(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $requiredDoc = $this->getJsonParams($request);

            $model = new AirlinesModel($this->popuConn());
            $model->validaAirlineRequiredDocCreation($requiredDoc);
            $model->createAirlineRequiredDoc($requiredDoc);

            $this->saveChangeLog(__FUNCTION__, [], $requiredDoc);
            $this->setResponseOk(__FUNCTION__, $requiredDoc);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/airlines-required-documentation-by-country/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteAirlineRequiredDocByCountry(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $requiredDoc = $this->getJsonParams($request);

            $model = new AirlinesModel($this->popuConn());
            $model->validaAirlineRequiredDocDeletion($requiredDoc);
            $model->deleteAirlineRequiredDoc($requiredDoc);

            $this->saveChangeLog(__FUNCTION__, [], $requiredDoc);
            $this->setResponseOk(__FUNCTION__);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }
}
