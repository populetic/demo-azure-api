<?php

namespace App\Controller;

use App\Entity\Region;
use App\Model\RegionsModel;
use App\Service\Exceptions\ValidationException;
use Doctrine\DBAL\DBALException;
use ErrorException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use TypeError;

class RegionsController extends BaseController
{
    /** @Route("/regions", methods={"GET", "OPTIONS"}) */
    public function getAllRegions(Request $request): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new RegionsModel($this->popuConn());
            $response = $model->getAll();

            $page = $this->getPageFromURL($request);
            if ($page) {
                $response = $model->paginateResult($response, $page, $this->getItemsPerPageFromURL($request));
            }

            $this->setResponseOk(__FUNCTION__, $response);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/regions/{id}", methods={"GET", "OPTIONS"}) */
    public function getRegionById($id): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new RegionsModel($this->popuConn());
            $model->validaId($id, true);

            $this->setResponseOk(__FUNCTION__, $model->getById($id));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/regions/create", methods={"POST", "OPTIONS"}) */
    public function createRegion(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $region = new Region($this->getJsonParams($request));

            $model = new RegionsModel($this->popuConn());
            $model->validaCreation($region);
            $region = $model->create($region);

            $this->saveChangeLog(__FUNCTION__, $region->id, $region);
            $this->setResponseOk(__FUNCTION__, $region);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/regions/edit/{id}", methods={"PUT", "OPTIONS"}) */
    public function editRegion(Request $request, $id): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $region = new Region($this->getJsonParams($request));
            $region->id = $id;

            $model = new RegionsModel($this->popuConn());
            $model->validaEdition($region);
            $oldRegion = $model->getById($id);
            $region    = $model->edit($region);

            $this->saveChangeLog(__FUNCTION__, $id, $region, $oldRegion);
            $this->setResponseOk(__FUNCTION__, $region);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/regions/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteRegions(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $ids = $this->getJsonParams($request);

            $model = new RegionsModel($this->popuConn());
            $model->validaIdsToDelete($ids);

            $this->deleteSeveralIdsTransaction($this->popuConn(), $model, $ids, __FUNCTION__);

            $this->setResponseOk(__FUNCTION__, $this->deleteErrors);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }
}
