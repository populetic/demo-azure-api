<?php

namespace App\Controller;

use App\Entity\User;
use App\Model\UsersModel;
use App\Service\Exceptions\ValidationException;
use App\Service\OurMandrill;
use Doctrine\DBAL\DBALException;
use ErrorException;
use Mandrill_Error;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use TypeError;

class UsersController extends BaseController
{
    /** @Route("/users", methods={"GET", "OPTIONS"}) */
    public function getUsers(Request $request): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new UsersModel($this->popuConn());

            $usernameFilter   = $request->query->get('username', '');
            $departmentFilter = $request->query->get('departmentId', null);
            $lawFirmFilter    = $request->query->get('lawFirmId', null);

            if ($usernameFilter) {
                $model->validaUsernameExists($usernameFilter);
                $response = $model->getByUsername($usernameFilter);
            } elseif ($departmentFilter) {
                $model->validaDepartmentId($departmentFilter);
                $response = $model->getAllByDepartment($departmentFilter);
            } elseif ($lawFirmFilter) {
                $model->validaLawFirmId($lawFirmFilter);
                $response = $model->getAllByLawFirm($lawFirmFilter);
            } else {
                $response = $model->getAll();
                $page     = $this->getPageFromURL($request);
                if ($page) {
                    $response = $model->paginateResult($response, $page, $this->getItemsPerPageFromURL($request));
                }
            }

            $this->setResponseOk(__FUNCTION__, $response);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/users/{id}", methods={"GET", "OPTIONS"}) */
    public function getUserById($id): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new UsersModel($this->popuConn());
            $this->setResponseOk(__FUNCTION__, $model->validaIdAndReturnUser($id, true));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/users/login", methods={"POST", "OPTIONS"}) */
    public function loginUser(Request $request): JsonResponse
    {
        try {
            $params = $this->getJsonParams($request);

            $model       = new UsersModel($this->popuConn());
            $user        = $model->validaLoginAndGetUser($params);
            $user->token = $this->getToken($user);

            $this->userLogged = (array)$user;
            $this->saveChangeLog(__FUNCTION__);
            $this->setResponseOk(__FUNCTION__, $user);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/users/create", methods={"POST", "OPTIONS"}) */
    public function createUser(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $user = new User($this->getJsonParams($request));

            $model = new UsersModel($this->popuConn());
            $model->validaCreation($this->statiaConn(), $this->translationsConn(), $user);
            $user = $model->create($user);

            $this->saveChangeLog(__FUNCTION__, $user->id, $user);
            $this->setResponseOk(__FUNCTION__, $user);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/users/edit/{id}", methods={"PUT", "OPTIONS"}) */
    public function editUser(Request $request, $id): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $user = new User($this->getJsonParams($request));
            $user->id = $id;

            $model = new UsersModel($this->popuConn());
            $model->validaEdition($this->statiaConn(), $this->translationsConn(), $user);
            $oldUser = $model->getById($id);
            $newUser = $model->edit($user);

            $this->saveChangeLog(__FUNCTION__, $user->id, $newUser, $oldUser);
            $this->setResponseOk(__FUNCTION__, $newUser);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/users/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteUser(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $ids = $this->getJsonParams($request);

            $model = new UsersModel($this->popuConn());
            $model->validaIdsToDelete($ids);
            $this->deleteSeveralIdsTransaction($this->popuConn(), $model, $ids, __FUNCTION__);

            $this->setResponseOk(__FUNCTION__, $this->deleteErrors);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /**
     * Sin estar logeado, el usuario hace petición para cambiar su contraseña (forgot password) y
     * se le envía un email con un link al correo de ese usuario para confirmar que se ha hecho la petición
     * @Route("/user-change-password-petition", methods={"GET", "OPTIONS"})
     */
    public function changeUserPasswordPetition(Request $request): JsonResponse
    {
        try {
            $username = $request->query->get('username', '');

            $model = new UsersModel($this->popuConn());
            $user  = $model->getByUsername($username);
            $model->validaIfUserHasEmailOrThrowException($user->email);
            $model->validaIfUserIsActiveOrThrowException($user);

            //TODO hacer email con traducciones
            //TODO hacerlo como HTML (o template)
            $data['subject']      = 'Statia: Petición de cambio de password';
            $data['languageCode'] = $user->languageCode;
            $data['emailTo']      = $user->email;
            $data['nameTo']       = $user->completeName;
            $data['html']         = '
                Alguien, esperamos que tú, solicitó que la contraseña para la cuenta asociada con este
                correo (' . $user->email. ') sea reseteada.

                Si no fuiste tú por favor ignora este correo y no respondas a él.

                En el caso de que quieras continuar el proceso de reseteo de contraseña por favor haz clic
                en el siguiente enlace: ' .
                $this->getBaseURLByEnvironment() . '/user-new-password?username=' . $user->username
            ;

            $ourMandrill = new OurMandrill();
            $ourMandrill->sendText($data);

            $this->userLogged = (array)$user;
            $this->saveChangeLog(__FUNCTION__);
            $this->setResponseOk(__FUNCTION__);

        } catch (DBALException | ErrorException | TypeError | Mandrill_Error $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /**
     * Envía un email con un nuevo password aleatorio
     * @Route("/user-new-password", methods={"GET", "OPTIONS"})
     */
    public function generateUserNewPassword(Request $request)
    {
        try {
            $username = $request->query->get('username', '');

            $model = new UsersModel($this->popuConn());
            $user  = $model->getByUsername($username);
            $model->validaIfUserHasEmailOrThrowException($user->email);
            $model->validaIfUserIsActiveOrThrowException($user);

            $newPassword = $model->generateRandomPassword();
            $model->changePassword($username, $newPassword);

            //TODO hacer email con traducciones
            //TODO hacerlo como HTML (o template)
            //TODO cambiar redirección web o un OK o nada?
            $data['subject']      = 'Statia: Nuevo password generado';
            $data['languageCode'] = $user->languageCode;
            $data['emailTo']      = $user->email;
            $data['nameTo']       = $user->completeName;
            $data['html']         = '
                Tal y como has solicitado, hemos generado una nueva contraseña para tu cuenta.

                Por favor, a partir de ahora utiliza estos datos de acceso:

                        Usuario: ' . $username . '
                        Contraseña: ' . $newPassword . '

                Recuerda que puedes cambiar tu password de nuevo en la sección de edición de perfil.
            ';

            $ourMandrill = new OurMandrill();
            $ourMandrill->sendText($data);

            $this->userLogged = (array)$user;
            $this->saveChangeLog(__FUNCTION__);

        } catch (DBALException | ErrorException | TypeError | Mandrill_Error $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return $this->redirect('http://www.populetic.com');
        }
    }
}
