<?php

namespace App\Controller;

use App\Model\ExtrajudicialModel;
use App\Service\Exceptions\ValidationException;
use Doctrine\DBAL\DBALException;
use ErrorException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use TypeError;

class ExtrajudicialController extends BaseController
{
    /***********************
     * ADDITIONAL EXPENSES *
     ***********************/

    /** @Route("/extrajudicial-additional-expenses", methods={"GET", "OPTIONS"}) */
    public function getAllAdditionalExp(): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new ExtrajudicialModel($this->popuConn());
            $this->setResponseOk(__FUNCTION__, $model->getAllAdditionalExp());

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/extrajudicial-additional-expenses/{id}", methods={"GET", "OPTIONS"}) */
    public function getAdditionalExpById($id): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new ExtrajudicialModel($this->popuConn());
            $model->validaAdditionalExpId($id, true);

            $this->setResponseOk(__FUNCTION__, $model->getAdditionalExpById($id));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/extrajudicial-additional-expenses/create", methods={"POST", "OPTIONS"}) */
    public function createAdditionalExp(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);

            $model = new ExtrajudicialModel($this->popuConn());
            $model->validaAdditionalExp($data);
            $data = $model->createAdditionalExp($data);

            $this->saveChangeLog(__FUNCTION__, $data['id'], $data);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/extrajudicial-additional-expenses/edit/{id}", methods={"PUT", "OPTIONS"}) */
    public function editAdditionalExp(Request $request, $id): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);
            $data['id'] = $id;

            $model = new ExtrajudicialModel($this->popuConn());
            $model->validaAdditionalExpId($id, true);
            $model->validaAdditionalExp($data);
            $oldData = $model->getAdditionalExpById($id);
            $model->editAdditionalExp($data);
            $data = $model->getAdditionalExpById($id);

            $this->saveChangeLog(__FUNCTION__, $id, $data, $oldData);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/extrajudicial-additional-expenses/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteAdditionalExp(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $ids = $this->getJsonParams($request);

            $model = new ExtrajudicialModel($this->popuConn());
            $model->validaAdditionalExpIdsToDelete($ids);

            $this->popuConn()->beginTransaction();
            foreach ($ids as $id) {
                $dataToDelete = $model->getAdditionalExpById($id);
                try {
                    $model->deleteAdditionalExp($id);
                } catch (DBALException $e) {
                    $this->deleteErrors[] = $this->parseErrorIfIsFkRestrictException($e->getMessage(), $id);
                    continue;
                }
                $this->saveChangeLog(__FUNCTION__, $id, $dataToDelete);
            }
            $this->popuConn()->commit();

            $this->setResponseOk(__FUNCTION__, $this->deleteErrors);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }



    /**********************
     **** TYPE SENDING ****
     **********************/

    /** @Route("/extrajudicial-type-sending/by-country/{countryId}", methods={"GET", "OPTIONS"}) */
    public function getAllTypeSendingByCountry($countryId): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new ExtrajudicialModel($this->popuConn());
            $model->validaCountryId($countryId, true);
            $this->setResponseOk(__FUNCTION__, $model->getAllTypeSendingByCountry($countryId));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/extrajudicial-type-sending/{id}", methods={"GET", "OPTIONS"}) */
    public function getTypeSendingById($id): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new ExtrajudicialModel($this->popuConn());
            $model->validaTypeSendingId($id, true);

            $this->setResponseOk(__FUNCTION__, $model->getTypeSendingById($id));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/extrajudicial-type-sending/create", methods={"POST", "OPTIONS"}) */
    public function createTypeSending(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);

            $model = new ExtrajudicialModel($this->popuConn());
            $model->validaTypeSending($data);
            $data = $model->createTypeSending($data);

            $this->saveChangeLog(__FUNCTION__, $data['id'], $data);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/extrajudicial-type-sending/edit/{id}", methods={"PUT", "OPTIONS"}) */
    public function editTypeSending(Request $request, $id): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);
            $data['id'] = $id;

            $model = new ExtrajudicialModel($this->popuConn());
            $model->validaTypeSendingId($id, true);
            $model->validaTypeSending($data);
            $oldData = $model->getTypeSendingById($id);
            $model->editTypeSending($data);
            $data = $model->getTypeSendingById($id);

            $this->saveChangeLog(__FUNCTION__, $id, $data, $oldData);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/extrajudicial-type-sending/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteTypeSending(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $ids = $this->getJsonParams($request);

            $model = new ExtrajudicialModel($this->popuConn());
            $model->validaTypeSendingIdsToDelete($ids);

            $this->popuConn()->beginTransaction();
            foreach ($ids as $id) {
                $dataToDelete = $model->getTypeSendingById($id);
                try {
                    $model->deleteTypeSending($id);
                } catch (DBALException $e) {
                    $this->deleteErrors[] = $this->parseErrorIfIsFkRestrictException($e->getMessage(), $id);
                    continue;
                }
                $this->saveChangeLog(__FUNCTION__, $id, $dataToDelete);
            }
            $this->popuConn()->commit();

            $this->setResponseOk(__FUNCTION__, $this->deleteErrors);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }



    /************************
     **** RELATED CLAIMS ****
     ************************/

    /** @Route("/extrajudicial-related-claims/{claimId}", methods={"GET", "OPTIONS"}) */
    public function getAdditionalExpensesByRelatedClaim($claimId): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new ExtrajudicialModel($this->popuConn());
            $model->validaClaimId($claimId, true);

            $this->setResponseOk(__FUNCTION__, $model->getAdditionalExpensesByRelatedClaim($claimId));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/extrajudicial-related-claims/create", methods={"POST", "OPTIONS"}) */
    public function createRelatedClaims(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);

            $model = new ExtrajudicialModel($this->popuConn());
            $model->validaRelatedClaims($data);
            $model->createRelatedClaims($data);

            $this->saveChangeLog(__FUNCTION__, [], $data);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/extrajudicial-related-claims/delete/{claimId}/{additionalExpensesId}", methods={"DELETE", "OPTIONS"}) */
    public function deleteRelatedClaims(Request $request, $claimId, $additionalExpensesId): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $model = new ExtrajudicialModel($this->popuConn());
            $model->validaRelatedClaimsExists($claimId, $additionalExpensesId);
            $model->deleteRelatedClaims($claimId, $additionalExpensesId);

            $this->saveChangeLog(
                __FUNCTION__,
                [],
                [ 'claimId' => $claimId, 'extrajudicialAdditionalExpensesId' => $additionalExpensesId ]
            );
            $this->setResponseOk(__FUNCTION__);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }
}
