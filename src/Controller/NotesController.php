<?php

namespace App\Controller;

use App\Entity\Note;
use App\Model\NotesModel;
use App\Service\Exceptions\ValidationException;
use Doctrine\DBAL\DBALException;
use ErrorException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use TypeError;

class NotesController extends BaseController
{
    /*********************
     ******* NOTES *******
     *********************/

    /** @Route("/notes/{claimId}", methods={"GET", "OPTIONS"}) */
    public function getAllNotesByClaimId(Request $request, $claimId): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new NotesModel($this->popuConn());
            $model->validaClaimId($claimId, true);
            $response = $model->getAllByClaimId($claimId);

            $page = $this->getPageFromURL($request);
            if ($page) {
                $response = $model->paginateResult($response, $page, $this->getItemsPerPageFromURL($request));
            }

            $this->setResponseOk(__FUNCTION__, $response);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/notes/create", methods={"POST", "OPTIONS"}) */
    public function createNote(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $note = new Note($this->getJsonParams($request));
            $note->userId = $this->userLogged['id'];

            $model = new NotesModel($this->popuConn());
            $model->validaCreation($this->statiaConn(), $note);
            $note = $model->create($note);

            $this->saveChangeLog(__FUNCTION__, $note->id, $note);
            $this->setResponseOk(__FUNCTION__, $note);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/notes/edit/{id}", methods={"PUT", "OPTIONS"}) */
    public function editNote(Request $request, $id): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $note = new Note($this->getJsonParams($request));
            $note->id = $id;

            $model = new NotesModel($this->popuConn());
            $model->validaEdition($this->statiaConn(), $note, $this->userLogged['id']);
            $oldNote = $model->getById($id);
            $note    = $model->edit($note);

            $this->saveChangeLog(__FUNCTION__, $id, $note, $oldNote);
            $this->setResponseOk(__FUNCTION__, $note);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/notes/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteNotes(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $ids = $this->getJsonParams($request);

            $model = new NotesModel($this->popuConn());
            $model->validaIdsToDelete($ids, $this->userLogged['id']);

            $this->deleteSeveralIdsTransaction($this->popuConn(), $model, $ids, __FUNCTION__);

            $this->setResponseOk(__FUNCTION__, $this->deleteErrors);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }



    /**********************
     ***** NOTE TYPES *****
     **********************/

    /** @Route("/note-types", methods={"GET", "OPTIONS"}) */
    public function getAllNoteTypes(): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new NotesModel($this->popuConn());
            $this->setResponseOk(__FUNCTION__, $model->getAllNoteTypes());

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/note-types/create", methods={"POST", "OPTIONS"}) */
    public function createNoteType(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);

            $model = new NotesModel($this->popuConn());
            $model->validaNoteTypeToCreate($data);
            $data = $model->createNoteType($data);

            $this->saveChangeLog(__FUNCTION__, $data['id'], $data);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/note-types/edit/{id}", methods={"PUT", "OPTIONS"}) */
    public function editNoteType(Request $request, $id): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);
            $data['id'] = $id;

            $model = new NotesModel($this->popuConn());
            $model->validaNoteTypeToEdit($data);
            $oldData = $model->getNoteTypeById($id);
            $model->editNoteType($data);

            $this->saveChangeLog(__FUNCTION__, $id, $data, $oldData);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/note-types/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteNoteTypes(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $ids = $this->getJsonParams($request);

            $model = new NotesModel($this->popuConn());
            $model->validaNoteTypeIdsToDelete($ids);

            $this->popuConn()->beginTransaction();
            foreach ($ids as $id) {
                $dataToDelete = $model->getNoteTypeById($id);
                try {
                    $model->deleteNoteType($id);
                } catch (DBALException $e) {
                    $this->deleteErrors[] = $this->parseErrorIfIsFkRestrictException($e->getMessage(), $id);
                    continue;
                }
                $this->saveChangeLog(__FUNCTION__, $id, $dataToDelete);
            }
            $this->popuConn()->commit();

            $this->setResponseOk(__FUNCTION__, $this->deleteErrors);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }



    /************************
     * SOCIAL NETWORK TYPES *
     ************************/

    /** @Route("/social-network-types", methods={"GET", "OPTIONS"}) */
    public function getAllSocialNetworkTypes(): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new NotesModel($this->popuConn());
            $this->setResponseOk(__FUNCTION__, $model->getAllSocialNetworkTypes());

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/social-network-types/create", methods={"POST", "OPTIONS"}) */
    public function createSocialNetworkType(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);

            $model = new NotesModel($this->popuConn());
            $model->validaSocialNetworkTypeToCreate($data);
            $data = $model->createSocialNetworkType($data);

            $this->saveChangeLog(__FUNCTION__, $data['id'], $data);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/social-network-types/edit/{id}", methods={"PUT", "OPTIONS"}) */
    public function editSocialNetworkType(Request $request, $id): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);
            $data['id'] = $id;

            $model = new NotesModel($this->popuConn());
            $model->validaSocialNetworkTypeToEdit($data);
            $oldData = $model->getSocialNetworkTypeById($id);
            $model->editSocialNetworkType($data);

            $this->saveChangeLog(__FUNCTION__, $id, $data, $oldData);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/social-network-types/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteSocialNetworkTypes(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $ids = $this->getJsonParams($request);

            $model = new NotesModel($this->popuConn());
            $model->validaSocialNetworkTypeIdsToDelete($ids);

            $this->popuConn()->beginTransaction();
            foreach ($ids as $id) {
                $dataToDelete = $model->getSocialNetworkTypeById($id);
                try {
                    $model->deleteSocialNetworkType($id);
                } catch (DBALException $e) {
                    $this->deleteErrors[] = $this->parseErrorIfIsFkRestrictException($e->getMessage(), $id);
                    continue;
                }
                $this->saveChangeLog(__FUNCTION__, $id, $dataToDelete);
            }
            $this->popuConn()->commit();

            $this->setResponseOk(__FUNCTION__, $this->deleteErrors);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }
}
