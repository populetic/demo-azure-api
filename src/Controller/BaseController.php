<?php

namespace App\Controller;

use App\Entity\User;
use App\Model\LogsModel;
use App\Service\Exceptions\ValidationException;
use App\Service\JwtAuth;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class BaseController extends AbstractController
{
    const PROD_APP_ENV             = 'prod';
    const DEV_BASE_URL             = 'http://populetic-api.local:8080';
    const PROD_BASE_URL            = 'http://api.populetic.com';
    const OK                       = 'Ok';
    const ERROR                    = 'Error';
    const PARAMS_ERROR             = 'paramsError';
    const HOST_NOT_ALLOWED_ERROR   = 'hostNotAllowedError';
    const FK_ERROR                 = 'fkCannotDeleteId';
    const FK_VIOLATION             = 'Integrity constraint violation: 1451';
    const PAGE_PARAM_KEY           = 'page';
    const ITEMS_PER_PAGE_PARAM_KEY = 'itemsperpage';

    private $keysToAvoidInLog = ['password', 'id', 'accountNumber', 'swift', 'createdAt', 'updatedAt'];

    protected $userLogged;
    protected $response;
    protected $deleteErrors = [];

    public function popuConn(): Connection
    {
        return $this->getDoctrine()->getManager('populetic')->getConnection();
    }

    public function statiaConn(): Connection
    {
        return $this->getDoctrine()->getManager('statia')->getConnection();
    }

    public function translationsConn(): Connection
    {
        return $this->getDoctrine()->getManager('translations')->getConnection();
    }

    public function popuSiteConn(): Connection
    {
        return $this->getDoctrine()->getManager('populeticsite')->getConnection();
    }

    /** @Route("/", methods={"GET", "OPTIONS"}) */
    public function apiStatus(): JsonResponse
    {
        $this->checkHostIsAllowed();
        $allowedHosts = explode(',', getenv('TRUSTED_PROXIES'));

        $documentationPath = $_SERVER['EFS_PATH'].$_SERVER['DOCUMENTATION_PATH'];

        $this->setResponseOk(__FUNCTION__, ['connectionOk' => $this->popuConn() ? 'connOK' : 'connKO',
                                            'allowedHosts' => $allowedHosts,
                                            'HTTP_HOST' => $_SERVER['HTTP_HOST'],
                                            'ALLOWED' => in_array($_SERVER['HTTP_HOST'], $allowedHosts),
                                            'EFS_PATH' => $_SERVER['EFS_PATH'],
                                            'ACCESS_TO_EFS' => is_dir($_SERVER['EFS_PATH']) ]);
        return new JsonResponse($this->response);
    }

    protected function getJsonParams(Request $request): array
    {
        $params = json_decode($request->get('json', null), true);
        if ($params) {
            return $params;
        } else {
            throw new ValidationException(self::PARAMS_ERROR);
        }
    }

    protected function getToken(User $user): string
    {
        return (new JwtAuth())->getToken($user);
    }

    protected function checkTokenAndSetUserLogged(Request $request)
    {
        $this->userLogged = (new JwtAuth())->checkToken($request->headers->get('Authorization'));
    }

    protected function checkHostIsAllowed()
    {
        $allowedHosts = explode(',', getenv('TRUSTED_PROXIES'));
        if (!isset($_SERVER['HTTP_HOST']) || !in_array($_SERVER['HTTP_HOST'], $allowedHosts)) {
            throw new ValidationException(self::HOST_NOT_ALLOWED_ERROR);
        }
    }

    protected function setResponseOk($message = self::OK, $response = [])
    {
        if ($message !== self::OK) {
            $message = $message . self::OK;
        }

        $this->response = [
            'status'   => 1,
            'message'  => $message,
            'response' => (array)$response
        ];
    }

    protected function setResponseError($message = self::ERROR, $errors = [])
    {
        if ($message !== self::ERROR) {
            $message = $message . self::ERROR;
        }

        $this->response = [
            'status'   => 0,
            'message'  => $message,
            'errors'   => $this->putIntoArrayIfIsString($errors)
        ];
    }

    protected function getBaseURLByEnvironment(): string
    {
        return $_SERVER['APP_ENV'] === self::PROD_APP_ENV ? self::PROD_BASE_URL : self::DEV_BASE_URL;
    }

    /**
     * Función estandard para entities (comprobar que sirve para el caso, cuyo model debe tener las funciones
     * getById($id), delete($id)) para eliminar varios ids con transacción. Además setea $this->deleteErrors
     * con los errores de db al eliminar, si es que los hay.
     * @param $conn             -> Conexión a DB (PopuConn, StatiaConn, etc)
     * @param $model            -> Modelo con funciones getById y delete
     * @param $ids              -> ids de los elementos a borrar
     * @param $actionNameToLog  -> nombre de la función Action del controller (__FUNCTION__)
     */
    protected function deleteSeveralIdsTransaction(Connection $conn, $model, $ids, $actionNameToLog)
    {
        $conn->beginTransaction();

        foreach ($ids as $id) {
            $dataToDelete = $model->getById($id);
            try {
                $model->delete($id);
            } catch (DBALException $e) {
                $this->deleteErrors[] = $this->parseErrorIfIsFkRestrictException($e->getMessage(), $id);
                continue;
            }
            $this->saveChangeLog($actionNameToLog, $id, $dataToDelete);
        }

        $conn->commit();
    }

    /**
     * Chequea si el error es por FK Constraint y nos lo parsea con el id implicado.
     * @param $errorMsg string  -> El $e->getMessage() del DBALException
     * @param $id int           -> El id que ha fallado
     */
    protected function parseErrorIfIsFkRestrictException($errorMsg, $id): string
    {
        if (is_string($errorMsg) && strpos($errorMsg, self::FK_VIOLATION) !== false) {
            $errorMsg = self::FK_ERROR . ":'" . $id . "'";
        } elseif (!is_string($errorMsg)) {
            $errorMsg = 'DBALExceptionError';
        }

        return $errorMsg;
    }

    /**
     * @param $params array con key 'ids' que contiene un array de los ids a tratar
     * @return array de ids (no assoc.)
     */
    protected function getIdsFromParams($params): array
    {
        return $this->getDataIfKeyExistsOrEmptyArray($params, 'ids');
    }

    protected function getDataIfKeyExistsOrNull($array, $key)
    {
        return array_key_exists($key, $array) ? $array[$key] : null;
    }

    protected function getDataIfKeyExistsOrZero($array, $key)
    {
        return array_key_exists($key, $array) ? $array[$key] : 0;
    }

    protected function getDataIfKeyExistsOrEmptyArray($array, $key)
    {
        return array_key_exists($key, $array) ? $array[$key] : [];
    }

    protected function getPageFromURL(Request $request): int
    {
        return $request->query->getInt(self::PAGE_PARAM_KEY, null);
    }

    protected function getItemsPerPageFromURL(Request $request): int
    {
        return $request->query->getInt(self::ITEMS_PER_PAGE_PARAM_KEY, null);
    }

    /**
     * Función para logear todos los cambios que se hagan desde la API
     *
     * $methodName       -> el __FUNCTION__ para logear la acción
     * $affectedIds      -> el id (int) o ids (array) editados de la tabla
     * $newDataOrMessage -> nuevos datos (array assoc: key=>value o Entity) o mensaje a logear (string)
     * $oldData          -> datos antiguos (array assoc: key=>value o Entity)
     *
     * 'create' y 'delete': Enviar sólo $newDataOrMessage para logear los datos creados/eliminados. No $oldData.
     * 'edit':              Enviar ambos params $newDataOrMessage y $oldData con las mismas keys para hacer el diff.
     */
    protected function saveChangeLog($methodName, $affectedIds = [], $newDataOrMessage = [], $oldData = [])
    {
        if (!isset($this->userLogged['id'])) $this->userLogged['id'] = null;

        $action = $this->parseAffectedIdsToLogMessage($methodName, $affectedIds);

        if (is_string($newDataOrMessage)) {
            $message = $newDataOrMessage;
        } else {
            $message = $this->parseChanges($newDataOrMessage, $oldData);
        }

        if (!$message) $message = 'No changes';

        $logsModel = new LogsModel($this->popuConn());
        $logsModel->insert($this->userLogged['id'], $action, $message);
    }

    /**
     * Función para logear los cambios relacionados con claims, sobretodo los cambios de status (el claimStatusId
     * se setea automáticamente teniendo el claimId)
     *
     * $action           -> el __FUNCTION__ para logear la acción
     * $claimId int
     * $newDataOrMessage -> mensaje a logear (string) o nuevos datos (array assoc: key=>value o Entity)
     */
    protected function saveClaimStatusLog($action, int $claimId, $newDataOrMessage = null)
    {
        if (!isset($this->userLogged['id'])) $this->userLogged['id'] = 0;

        if (is_string($newDataOrMessage) || $newDataOrMessage === null) {
            $message = $newDataOrMessage;
        } else {
            $message = $this->parseChanges($newDataOrMessage, []);
        }

        $logsModel = new LogsModel($this->popuConn());
        $logsModel->insertClaimStatusByActualClaimStatus($claimId, $this->userLogged['id'], $action, $message);
    }

    private function parseAffectedIdsToLogMessage($methodName, $affectedIds): string
    {
        if ($affectedIds) {
            $affectedIds = is_array($affectedIds) ? implode(', ', $affectedIds) : $affectedIds;
            $affectedIds = ' with id: ' . $affectedIds;
        } else {
            $affectedIds = '';
        }

        return $methodName . $affectedIds;
    }

    private function parseChanges($newData, $oldData): string
    {
        $stringChanges = '';
        $i = 0;

        if ($newData && $oldData) {
            $newData = (array)$newData;
            $oldData = (array)$oldData;

            foreach ($newData as $key => $newValue) {
                if (!in_array($key, $this->keysToAvoidInLog)            &&
                    array_key_exists($key, $oldData)                    &&
                    (!is_array($oldData[$key]) && !is_array($newValue)) &&
                    $oldData[$key] != $newValue
                ) {
                    $oldData[$key]  = $this->returnNoneIfEmptyData($oldData[$key]);
                    $newValue       = $this->returnNoneIfEmptyData($newValue);

                    $stringChanges  = $this->addLineBreakIfNeeded($stringChanges, $i);
                    $stringChanges .= "'" . $key . "' changed from '" . $oldData[$key] . "' to '" . $newValue . "'.";

                    $i++;
                }
            }
        } elseif ($newData) {
            $newData = (array)$newData;

            foreach ($newData as $key => $value) {
                if (!is_array($value) && !in_array($key, $this->keysToAvoidInLog)) {
                    $value = $this->returnNoneIfEmptyData($value);

                    $stringChanges  = $this->addLineBreakIfNeeded($stringChanges, $i);
                    $stringChanges .= "'" . $key . "' value: '" . $value . "'.";

                    $i++;
                }
            }
        }

        return $stringChanges;
    }

    private function addLineBreakIfNeeded($stringChanges, $i): string
    {
        return $stringChanges . ($i === 0 ? "" : "\n");
    }

    private function returnNoneIfEmptyData($data): string
    {
        return ($data === '' || $data === null) ? 'none' : $data;
    }

    private function putIntoArrayIfIsString($data): array
    {
        return (is_array($data)) ? $data : array($data);
    }
}
