<?php

namespace App\Controller;

use App\Model\PopuleticSiteModel;
use App\Service\Exceptions\ValidationException;
use Doctrine\DBAL\DBALException;
use ErrorException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use TypeError;

class PopuleticSiteController extends BaseController
{
    /*********************
     **** ACCESS LOGS ****
     *********************/

    /** @Route("/access-logs", methods={"GET", "OPTIONS"}) */
    public function getAllAccessLogs(Request $request): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new PopuleticSiteModel($this->popuSiteConn());
            $response = $model->getAllAccessLogs();

            $page = $this->getPageFromURL($request);
            if ($page) {
                $response = $model->paginateResult($response, $page, $this->getItemsPerPageFromURL($request));
            }

            $this->setResponseOk(__FUNCTION__, $response);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/access-logs/{id}", methods={"GET", "OPTIONS"}) */
    public function getAccessLogsById($id): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new PopuleticSiteModel($this->popuSiteConn());
            $model->validaAccessLogsId($id, true);

            $this->setResponseOk(__FUNCTION__, $model->getAccessLogsById($id));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/access-logs/create", methods={"POST", "OPTIONS"}) */
    public function createAccessLogs(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);

            $model = new PopuleticSiteModel($this->popuSiteConn());
            $model->validaAccessLogs($data);
            $data = $model->createAccessLogs($data);

            $this->saveChangeLog(__FUNCTION__, $data['id'], $data);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/access-logs/edit/{id}", methods={"PUT", "OPTIONS"}) */
    public function editAccessLogs(Request $request, $id): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);
            $data['id'] = $id;

            $model = new PopuleticSiteModel($this->popuSiteConn());
            $model->validaAccessLogsId($id, true);
            $model->validaAccessLogs($data);
            $oldData = $model->getAccessLogsById($id);
            $model->editAccessLogs($data);
            $data = $model->getAccessLogsById($id);

            $this->saveChangeLog(__FUNCTION__, $id, $data, $oldData);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/access-logs/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteAccessLogs(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $ids = $this->getJsonParams($request);

            $model = new PopuleticSiteModel($this->popuSiteConn());
            $model->validaAccessLogsIdsToDelete($ids);

            $this->popuSiteConn()->beginTransaction();
            foreach ($ids as $id) {
                $dataToDelete = $model->getAccessLogsById($id);
                try {
                    $model->deleteAccessLogs($id);
                } catch (DBALException $e) {
                    $this->deleteErrors[] = $this->parseErrorIfIsFkRestrictException($e->getMessage(), $id);
                    continue;
                }
                $this->saveChangeLog(__FUNCTION__, $id, $dataToDelete);
            }
            $this->popuSiteConn()->commit();

            $this->setResponseOk(__FUNCTION__, $this->deleteErrors);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuSiteConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }



    /*********************
     *** AIRLINES INFO ***
     *********************/

    /** @Route("/airlines-info", methods={"GET", "OPTIONS"}) */
    public function getAllAirlinesInfoTotals(Request $request): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new PopuleticSiteModel($this->popuSiteConn());
            $response = $model->getAllAirlinesInfoTotals();

            $page = $this->getPageFromURL($request);
            if ($page) {
                $response = $model->paginateResult($response, $page, $this->getItemsPerPageFromURL($request));
            }

            $this->setResponseOk(__FUNCTION__, $response);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/airlines-info/{airlineId}", methods={"GET", "OPTIONS"}) */
    public function getAirlineInfoBioByAirlineId($airlineId): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new PopuleticSiteModel($this->popuSiteConn());
            $model->validaAirlineId($airlineId);

            $this->setResponseOk(__FUNCTION__, $model->getAirlineInfoBioByAirlineId($airlineId));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }



    /*********************
     ** DYNAMIC LANDINGS **
     *********************/

    /** @Route("/dynamic-landings", methods={"GET", "OPTIONS"}) */
    public function getAllDynamicLandings(Request $request): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new PopuleticSiteModel($this->popuSiteConn());
            $response = $model->getAllDynamicLandings();

            $page = $this->getPageFromURL($request);
            if ($page) {
                $response = $model->paginateResult($response, $page, $this->getItemsPerPageFromURL($request));
            }

            $this->setResponseOk(__FUNCTION__, $response);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/dynamic-landings/{id}", methods={"GET", "OPTIONS"}) */
    public function getDynamicLandingsById($id): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new PopuleticSiteModel($this->popuSiteConn());
            $model->validaDynamicLandingsId($id);

            $this->setResponseOk(__FUNCTION__, $model->getDynamicLandingsById($id));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }



    /*********************
     ** LANDING AIRLINES **
     *********************/

    /** @Route("/landing-airlines", methods={"GET", "OPTIONS"}) */
    public function getAllLandingAirlines(Request $request): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new PopuleticSiteModel($this->popuSiteConn());
            $response = $model->getAllLandingAirlines();

            $page = $this->getPageFromURL($request);
            if ($page) {
                $response = $model->paginateResult($response, $page, $this->getItemsPerPageFromURL($request));
            }

            $this->setResponseOk(__FUNCTION__, $response);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/landing-airlines/{id}", methods={"GET", "OPTIONS"}) */
    public function getLandingAirlinesById($id): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new PopuleticSiteModel($this->popuSiteConn());
            $model->validaLandingAirlinesId($id);

            $this->setResponseOk(__FUNCTION__, $model->getLandingAirlinesById($id));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/landing-airlines/by-airline/{airlineId}", methods={"GET", "OPTIONS"}) */
    public function getLandingAirlinesByAirlineId($airlineId): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new PopuleticSiteModel($this->popuSiteConn());
            $model->validaAirlineId($airlineId);

            $this->setResponseOk(__FUNCTION__, $model->getLandingAirlinesByAirlineId($airlineId));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }



    /*******************
     ** URL LANGUAGES **
     *******************/

    /** @Route("/url-languages", methods={"GET", "OPTIONS"}) */
    public function getAllUrlLanguages(Request $request): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new PopuleticSiteModel($this->popuSiteConn());
            $response = $model->getAllUrlLanguages();

            $page = $this->getPageFromURL($request);
            if ($page) {
                $response = $model->paginateResult($response, $page, $this->getItemsPerPageFromURL($request));
            }

            $this->setResponseOk(__FUNCTION__, $response);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/url-languages/{id}", methods={"GET", "OPTIONS"}) */
    public function getUrlLanguagesById($id): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new PopuleticSiteModel($this->popuSiteConn());
            $model->validaUrlLanguagesId($id);

            $this->setResponseOk(__FUNCTION__, $model->getUrlLanguagesById($id));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }
}
