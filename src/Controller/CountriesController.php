<?php

namespace App\Controller;

use App\Entity\Country;
use App\Model\CountriesModel;
use App\Service\Exceptions\ValidationException;
use Doctrine\DBAL\DBALException;
use ErrorException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use TypeError;

class CountriesController extends BaseController
{
    /** @Route("/countries", methods={"GET", "OPTIONS"}) */
    public function getAllCountries(Request $request): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new CountriesModel($this->popuConn());
            $response = $model->getAll();

            $page = $this->getPageFromURL($request);
            if ($page) {
                $response = $model->paginateResult($response, $page, $this->getItemsPerPageFromURL($request));
            }

            $this->setResponseOk(__FUNCTION__, $response);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/countries/{id}", methods={"GET", "OPTIONS"}) */
    public function getCountryById($id): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new CountriesModel($this->popuConn());
            $model->validaCountryId($id, true);

            $this->setResponseOk(__FUNCTION__, $model->getById($id));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/countries/create", methods={"POST", "OPTIONS"}) */
    public function createCountry(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $country = new Country($this->getJsonParams($request));

            $model = new CountriesModel($this->popuConn());
            $model->validaCreation($country);
            $country = $model->create($country);

            $this->saveChangeLog(__FUNCTION__, $country->id, $country);
            $this->setResponseOk(__FUNCTION__, $country);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/countries/edit/{id}", methods={"PUT", "OPTIONS"}) */
    public function editCountry(Request $request, $id): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $country = new Country($this->getJsonParams($request));
            $country->id = $id;

            $model = new CountriesModel($this->popuConn());
            $model->validaEdition($country);
            $oldCountry = $model->getById($id);
            $country    = $model->edit($country);

            $this->saveChangeLog(__FUNCTION__, $id, $country, $oldCountry);
            $this->setResponseOk(__FUNCTION__, $country);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/countries/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteCountries(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $ids = $this->getJsonParams($request);

            $model = new CountriesModel($this->popuConn());
            $model->validaIdsToDelete($ids);

            $this->deleteSeveralIdsTransaction($this->popuConn(), $model, $ids, __FUNCTION__);

            $this->setResponseOk(__FUNCTION__, $this->deleteErrors);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }
}
