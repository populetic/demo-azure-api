<?php

namespace App\Controller;

use App\Model\PaymentsModel;
use App\Service\Exceptions\ValidationException;
use Doctrine\DBAL\DBALException;
use ErrorException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use TypeError;

class PaymentsController extends BaseController
{
    /** @Route("/payments-by-claim-status/{claimStatusId}", methods={"GET", "OPTIONS"}) */
    public function getPaymentsByClaimStatus($claimStatusId): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new PaymentsModel($this->popuConn());
            $model->validaClaimStatusId($claimStatusId, true);

            $this->setResponseOk(__FUNCTION__, $model->getPaymentsByClaimStatus($claimStatusId));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/payments-by-claim-status-and-last-sent", methods={"POST", "OPTIONS"}) */
    public function getPaymentsByClaimStatusAndDate(Request $request): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $data = $this->getJsonParams($request);

            $model = new PaymentsModel($this->popuConn());
            $model->validaPaymentsClaimStatusAndDate($data);

            $this->setResponseOk(__FUNCTION__, $model->getPaymentsByClaimStatusAndDate($data));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/payments-get-dates-to-send-email", methods={"GET", "OPTIONS"}) */
    public function getDatesToSendEmailPayment(): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new PaymentsModel($this->popuConn());
            $this->setResponseOk(__FUNCTION__, $model->getDatesToSendEmailPayment());

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/payments-get-claims-grouped-by-account-data/{claimId}", methods={"GET", "OPTIONS"}) */
    public function getClaimsGroupedByAccountData($claimId): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new PaymentsModel($this->popuConn());
            $model->validaClaimId($claimId, true);
            $this->setResponseOk(__FUNCTION__, $model->getClaimsGroupedByAccountData($claimId));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/payments-get-clients-data-with-completed-payment", methods={"GET", "OPTIONS"}) */
    public function getClientsDataWithCompletedPayment(): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new PaymentsModel($this->popuConn());
            $this->setResponseOk(__FUNCTION__, $model->getClientsDataWithCompletedPayment());

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/payments-get-claims-log-status-to-revert", methods={"GET", "OPTIONS"}) */
    public function getClaimsLogsStatusToRevert(): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new PaymentsModel($this->popuConn());
            $this->setResponseOk(__FUNCTION__, $model->getClaimsLogsStatusToRevert());

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }
}
