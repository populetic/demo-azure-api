<?php

namespace App\Controller;

use App\Entity\CompanyData;
use App\Model\CompanyDataModel;
use App\Service\Exceptions\ValidationException;
use Doctrine\DBAL\DBALException;
use ErrorException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use TypeError;

class CompanyDataController extends BaseController
{
    /** @Route("/company-data", methods={"GET", "OPTIONS"}) */
    public function getAllCompanyData(Request $request): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new CompanyDataModel($this->popuConn());
            $response = $model->getAll();

            $page = $this->getPageFromURL($request);
            if ($page) {
                $response = $model->paginateResult($response, $page, $this->getItemsPerPageFromURL($request));
            }

            $this->setResponseOk(__FUNCTION__, $response);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/company-data/by-country/{countryId}", methods={"GET", "OPTIONS"}) */
    public function getCompanyDataByCountryId($countryId): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new CompanyDataModel($this->popuConn());
            $model->validaDataExistsForCountryId($countryId, true);

            $this->setResponseOk(__FUNCTION__, $model->getByCountryId($countryId));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/company-data/create/{countryId}", methods={"POST", "OPTIONS"}) */
    public function createCompanyData(Request $request, $countryId): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $companyData = new CompanyData($this->getJsonParams($request));
            $companyData->countryId = $countryId;

            $model = new CompanyDataModel($this->popuConn());
            $model->validaCompanyData($companyData, true);
            $companyData = $model->create($companyData);

            $this->saveChangeLog(__FUNCTION__, [], $companyData);
            $this->setResponseOk(__FUNCTION__, $companyData);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/company-data/edit/{countryId}", methods={"PUT", "OPTIONS"}) */
    public function editCompanyData(Request $request, $countryId): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $companyData = new CompanyData($this->getJsonParams($request));
            $companyData->countryId = $countryId;

            $model = new CompanyDataModel($this->popuConn());
            $model->validaDataExistsForCountryId($countryId, true);
            $model->validaCompanyData($companyData, false);
            $oldCompanyData = $model->getByCountryId($countryId);
            $companyData    = $model->edit($companyData);

            $this->saveChangeLog(__FUNCTION__, [], $companyData, $oldCompanyData);
            $this->setResponseOk(__FUNCTION__, $companyData);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/company-data/delete/{countryId}", methods={"DELETE", "OPTIONS"}) */
    public function deleteCompanyDataByCountryId(Request $request, $countryId): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $model = new CompanyDataModel($this->popuConn());
            $model->validaDataExistsForCountryId($countryId, true);
            $dataToDelete = $model->getByCountryId($countryId);
            $model->delete($countryId);

            $this->saveChangeLog(__FUNCTION__, [], $dataToDelete);
            $this->setResponseOk(__FUNCTION__);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }
}
