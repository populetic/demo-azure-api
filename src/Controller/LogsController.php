<?php

namespace App\Controller;

use App\Model\LogsModel;
use App\Service\Exceptions\ValidationException;
use Doctrine\DBAL\DBALException;
use ErrorException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use TypeError;

class LogsController extends BaseController
{
    /****************
     ***** LOGS *****
     ****************/

    /** @Route("/logs/{year}", methods={"GET", "OPTIONS"}) */
    public function getAllLogsByYear(Request $request, $year): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new LogsModel($this->popuConn());
            $model->validaYearAndLogsTableExists($year);
            $response = $model->getAllByYear($year);

            $page = $this->getPageFromURL($request);
            if ($page) {
                $response = $model->paginateResult($response, $page, $this->getItemsPerPageFromURL($request));
            }

            $this->setResponseOk(__FUNCTION__, $response);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }



    /*********************
     * LOGS CLAIM STATUS *
     *********************/

    /** @Route("/logs-claim-status/insert", methods={"POST", "OPTIONS"}) */
    public function insertLogClaimStatus(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);
            $data['userId'] = $this->userLogged['id'];

            $model = new LogsModel($this->popuConn());
            $model->validaLogClaimStatus($data);
            $model->insertClaimStatus($data);

            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/logs-claim-status", methods={"GET", "OPTIONS"}) */
    public function getAllLogsClaimStatus(Request $request): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new LogsModel($this->popuConn());
            $response = $model->getAllClaimStatus();

            $page = $this->getPageFromURL($request);
            if ($page) {
                $response = $model->paginateResult($response, $page, $this->getItemsPerPageFromURL($request));
            }

            $this->setResponseOk(__FUNCTION__, $response);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/logs-claim-status/by-claim/{claimId}", methods={"GET", "OPTIONS"}) */
    public function getAllLogsClaimStatusByClaimId(Request $request, $claimId): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new LogsModel($this->popuConn());
            $model->validaClaimId($claimId, true);
            $response = $model->getAllLogsClaimStatusByClaimId($claimId);

            $page = $this->getPageFromURL($request);
            if ($page) {
                $response = $model->paginateResult($response, $page, $this->getItemsPerPageFromURL($request));
            }

            $this->setResponseOk(__FUNCTION__, $response);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }
}
