<?php

namespace App\Controller;

use App\Model\CronsModel;
use App\Service\Exceptions\ValidationException;
use App\Service\OurMandrill;
use Doctrine\DBAL\DBALException;
use ErrorException;
use Mandrill_Error;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use TypeError;

class CronsController extends BaseController
{
    const EMAIL_ADM = 'adm@populetic.com';

    /** @Route("/crons/email-to-adm-with-yesterday-status-to-clientcollect-or-cancelwithcharge", methods={"GET", "OPTIONS"}) */
    public function emailToAdmWithYesterdayStatusToClientCollectOrCancelWithCharge(): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $yesterday = date('d/m/Y', strtotime('-1 days'));

            $model  = new CronsModel($this->popuConn());
            $claims = $model->getYesterdayStatusToClientCollectOrCancelWithCharge();

            if ($claims) {
                $email = [
                    'subject'      => $yesterday . ' - Generar factura',
                    'languageCode' => 'es',
                    'emailTo'      => self::EMAIL_ADM,
                    'nameTo'       => 'Adm',
                    'html'         => $model->parseYesterdayStatusToCollectOrChargeClaimsToHTML($claims, $yesterday)
                ];

                $ourMandrill = new OurMandrill();
                $ourMandrill->sendText($email);
            }

            $this->setResponseOk(__FUNCTION__);

        } catch (DBALException | ErrorException | TypeError | Mandrill_Error $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }
}
