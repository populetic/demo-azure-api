<?php

namespace App\Controller;

use App\Model\ParametersModel;
use App\Service\Exceptions\ValidationException;
use Doctrine\DBAL\DBALException;
use ErrorException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use TypeError;

class ParametersController extends BaseController
{
    /**
     * Controller para tablas auxiliares con campos id y name
     */

    /**********************
     ** DISRUPTION TYPES **
     **********************/

    /** @Route("/disruption-types", methods={"GET", "OPTIONS"}) */
    public function getAllDisruptionTypes(): JsonResponse
    {
        try {
            $model = new ParametersModel($this->popuConn());
            $this->getAll(__FUNCTION__, $model, $model::DISRUPTION_TYPES);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/disruption-types/{id}", methods={"GET", "OPTIONS"}) */
    public function getDisruptionTypesById($id): JsonResponse
    {
        try {
            $model = new ParametersModel($this->popuConn());
            $this->getById(__FUNCTION__, $model, $model::DISRUPTION_TYPES, $id);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/disruption-types/create", methods={"POST", "OPTIONS"}) */
    public function createDisruptionTypes(Request $request): JsonResponse
    {
        try {
            $model = new ParametersModel($this->popuConn());
            $this->create(__FUNCTION__, $request, $model, $model::DISRUPTION_TYPES);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/disruption-types/edit/{id}", methods={"PUT", "OPTIONS"}) */
    public function editDisruptionTypes(Request $request, $id): JsonResponse
    {
        try {
            $model = new ParametersModel($this->popuConn());
            $this->edit(__FUNCTION__, $request, $model, $model::DISRUPTION_TYPES, $id);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/disruption-types/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteDisruptionTypes(Request $request): JsonResponse
    {
        try {
            $model = new ParametersModel($this->popuConn());
            $this->delete(__FUNCTION__, $request, $model, $model::DISRUPTION_TYPES);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }



    /***********************
     **** DELAY OPTIONS ****
     ***********************/

    /** @Route("/delay-options", methods={"GET", "OPTIONS"}) */
    public function getAllDelayOptions(): JsonResponse
    {
        try {
            $model = new ParametersModel($this->popuConn());
            $this->getAll(__FUNCTION__, $model, $model::DELAY_OPTIONS);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/delay-options/{id}", methods={"GET", "OPTIONS"}) */
    public function getDelayOptionsById($id): JsonResponse
    {
        try {
            $model = new ParametersModel($this->popuConn());
            $this->getById(__FUNCTION__, $model, $model::DELAY_OPTIONS, $id);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/delay-options/create", methods={"POST", "OPTIONS"}) */
    public function createDelayOptions(Request $request): JsonResponse
    {
        try {
            $model = new ParametersModel($this->popuConn());
            $this->create(__FUNCTION__, $request, $model, $model::DELAY_OPTIONS);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/delay-options/edit/{id}", methods={"PUT", "OPTIONS"}) */
    public function editDelayOptions(Request $request, $id): JsonResponse
    {
        try {
            $model = new ParametersModel($this->popuConn());
            $this->edit(__FUNCTION__, $request, $model, $model::DELAY_OPTIONS, $id);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/delay-options/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteDelayOptions(Request $request): JsonResponse
    {
        try {
            $model = new ParametersModel($this->popuConn());
            $this->delete(__FUNCTION__, $request, $model, $model::DELAY_OPTIONS);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }



    /*********************
     ** AIRLINE REASONS **
     *********************/

    /** @Route("/airline-reasons", methods={"GET", "OPTIONS"}) */
    public function getAllAirlineReasons(): JsonResponse
    {
        try {
            $model = new ParametersModel($this->popuConn());
            $this->getAll(__FUNCTION__, $model, $model::AIRLINE_REASONS);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/airline-reasons/{id}", methods={"GET", "OPTIONS"}) */
    public function getAirlineReasonsById($id): JsonResponse
    {
        try {
            $model = new ParametersModel($this->popuConn());
            $this->getById(__FUNCTION__, $model, $model::AIRLINE_REASONS, $id);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/airline-reasons/create", methods={"POST", "OPTIONS"}) */
    public function createAirlineReasons(Request $request): JsonResponse
    {
        try {
            $model = new ParametersModel($this->popuConn());
            $this->create(__FUNCTION__, $request, $model, $model::AIRLINE_REASONS);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/airline-reasons/edit/{id}", methods={"PUT", "OPTIONS"}) */
    public function editAirlineReasons(Request $request, $id): JsonResponse
    {
        try {
            $model = new ParametersModel($this->popuConn());
            $this->edit(__FUNCTION__, $request, $model, $model::AIRLINE_REASONS, $id);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/airline-reasons/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteAirlineReasons(Request $request): JsonResponse
    {
        try {
            $model = new ParametersModel($this->popuConn());
            $this->delete(__FUNCTION__, $request, $model, $model::AIRLINE_REASONS);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }



    /**************************
     ** AIRLINE ALTERNATIVES **
     **************************/

    /** @Route("/airline-alternatives", methods={"GET", "OPTIONS"}) */
    public function getAllAirlineAlternatives(): JsonResponse
    {
        try {
            $model = new ParametersModel($this->popuConn());
            $this->getAll(__FUNCTION__, $model, $model::AIRLINE_ALTERNAT);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/airline-alternatives/{id}", methods={"GET", "OPTIONS"}) */
    public function getAirlineAlternativesById($id): JsonResponse
    {
        try {
            $model = new ParametersModel($this->popuConn());
            $this->getById(__FUNCTION__, $model, $model::AIRLINE_ALTERNAT, $id);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/airline-alternatives/create", methods={"POST", "OPTIONS"}) */
    public function createAirlineAlternatives(Request $request): JsonResponse
    {
        try {
            $model = new ParametersModel($this->popuConn());
            $this->create(__FUNCTION__, $request, $model, $model::AIRLINE_ALTERNAT);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/airline-alternatives/edit/{id}", methods={"PUT", "OPTIONS"}) */
    public function editAirlineAlternatives(Request $request, $id): JsonResponse
    {
        try {
            $model = new ParametersModel($this->popuConn());
            $this->edit(__FUNCTION__, $request, $model, $model::AIRLINE_ALTERNAT, $id);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/airline-alternatives/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteAirlineAlternatives(Request $request): JsonResponse
    {
        try {
            $model = new ParametersModel($this->popuConn());
            $this->delete(__FUNCTION__, $request, $model, $model::AIRLINE_ALTERNAT);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }



    /***************************************
     ** AIRLINE COMPENSATION ALTERNATIVES **
     ***************************************/

    /** @Route("/airline-compensation-alternatives", methods={"GET", "OPTIONS"}) */
    public function getAllAirlineCompensationAlternatives(): JsonResponse
    {
        try {
            $model = new ParametersModel($this->popuConn());
            $this->getAll(__FUNCTION__, $model, $model::AIRLINE_COMP_ALTERNAT);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/airline-compensation-alternatives/{id}", methods={"GET", "OPTIONS"}) */
    public function getAirlineCompensationAlternativesById($id): JsonResponse
    {
        try {
            $model = new ParametersModel($this->popuConn());
            $this->getById(__FUNCTION__, $model, $model::AIRLINE_COMP_ALTERNAT, $id);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/airline-compensation-alternatives/create", methods={"POST", "OPTIONS"}) */
    public function createAirlineCompensationAlternatives(Request $request): JsonResponse
    {
        try {
            $model = new ParametersModel($this->popuConn());
            $this->create(__FUNCTION__, $request, $model, $model::AIRLINE_COMP_ALTERNAT);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/airline-compensation-alternatives/edit/{id}", methods={"PUT", "OPTIONS"}) */
    public function editAirlineCompensationAlternatives(Request $request, $id): JsonResponse
    {
        try {
            $model = new ParametersModel($this->popuConn());
            $this->edit(__FUNCTION__, $request, $model, $model::AIRLINE_COMP_ALTERNAT, $id);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/airline-compensation-alternatives/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteAirlineCompensationAlternatives(Request $request): JsonResponse
    {
        try {
            $model = new ParametersModel($this->popuConn());
            $this->delete(__FUNCTION__, $request, $model, $model::AIRLINE_COMP_ALTERNAT);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }



    /**********************
     *** LAWSUITE TYPES ***
     **********************/

    /** @Route("/lawsuite-types", methods={"GET", "OPTIONS"}) */
    public function getAllLawsuiteTypes(): JsonResponse
    {
        try {
            $model = new ParametersModel($this->popuConn());
            $this->getAll(__FUNCTION__, $model, $model::LAWSUITE_TYPES);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/lawsuite-types/{id}", methods={"GET", "OPTIONS"}) */
    public function getLawsuiteTypesById($id): JsonResponse
    {
        try {
            $model = new ParametersModel($this->popuConn());
            $this->getById(__FUNCTION__, $model, $model::LAWSUITE_TYPES, $id);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/lawsuite-types/create", methods={"POST", "OPTIONS"}) */
    public function createLawsuiteTypes(Request $request): JsonResponse
    {
        try {
            $model = new ParametersModel($this->popuConn());
            $this->create(__FUNCTION__, $request, $model, $model::LAWSUITE_TYPES);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/lawsuite-types/edit/{id}", methods={"PUT", "OPTIONS"}) */
    public function editLawsuiteTypes(Request $request, $id): JsonResponse
    {
        try {
            $model = new ParametersModel($this->popuConn());
            $this->edit(__FUNCTION__, $request, $model, $model::LAWSUITE_TYPES, $id);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/lawsuite-types/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteLawsuiteTypes(Request $request): JsonResponse
    {
        try {
            $model = new ParametersModel($this->popuConn());
            $this->delete(__FUNCTION__, $request, $model, $model::LAWSUITE_TYPES);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }



    /*********************
     ** RESOLUTION WAYS **
     *********************/

    /** @Route("/resolution-ways", methods={"GET", "OPTIONS"}) */
    public function getAllResolutionWays(): JsonResponse
    {
        try {
            $model = new ParametersModel($this->popuConn());
            $this->getAll(__FUNCTION__, $model, $model::RESOLUTION_WAYS);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/resolution-ways/{id}", methods={"GET", "OPTIONS"}) */
    public function getResolutionWaysById($id): JsonResponse
    {
        try {
            $model = new ParametersModel($this->popuConn());
            $this->getById(__FUNCTION__, $model, $model::RESOLUTION_WAYS, $id);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/resolution-ways/create", methods={"POST", "OPTIONS"}) */
    public function createResolutionWays(Request $request): JsonResponse
    {
        try {
            $model = new ParametersModel($this->popuConn());
            $this->create(__FUNCTION__, $request, $model, $model::RESOLUTION_WAYS);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/resolution-ways/edit/{id}", methods={"PUT", "OPTIONS"}) */
    public function editResolutionWays(Request $request, $id): JsonResponse
    {
        try {
            $model = new ParametersModel($this->popuConn());
            $this->edit(__FUNCTION__, $request, $model, $model::RESOLUTION_WAYS, $id);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/resolution-ways/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteResolutionWays(Request $request): JsonResponse
    {
        try {
            $model = new ParametersModel($this->popuConn());
            $this->delete(__FUNCTION__, $request, $model, $model::RESOLUTION_WAYS);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }



    /************************
     * LUGGAGE ISSUE MOMENT *
     ************************/

    /** @Route("/luggage-issue-moment", methods={"GET", "OPTIONS"}) */
    public function getAllLuggageIssueMoments(): JsonResponse
    {
        try {
            $model = new ParametersModel($this->popuConn());
            $this->getAll(__FUNCTION__, $model, $model::LUGGAGE_ISSUE_MOMENT);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/luggage-issue-moment/{id}", methods={"GET", "OPTIONS"}) */
    public function getLuggageIssueMomentsById($id): JsonResponse
    {
        try {
            $model = new ParametersModel($this->popuConn());
            $this->getById(__FUNCTION__, $model, $model::LUGGAGE_ISSUE_MOMENT, $id);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/luggage-issue-moment/create", methods={"POST", "OPTIONS"}) */
    public function createLuggageIssueMoments(Request $request): JsonResponse
    {
        try {
            $model = new ParametersModel($this->popuConn());
            $this->create(__FUNCTION__, $request, $model, $model::LUGGAGE_ISSUE_MOMENT);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/luggage-issue-moment/edit/{id}", methods={"PUT", "OPTIONS"}) */
    public function editLuggageIssueMoments(Request $request, $id): JsonResponse
    {
        try {
            $model = new ParametersModel($this->popuConn());
            $this->edit(__FUNCTION__, $request, $model, $model::LUGGAGE_ISSUE_MOMENT, $id);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/luggage-issue-moment/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteLuggageIssueMoments(Request $request): JsonResponse
    {
        try {
            $model = new ParametersModel($this->popuConn());
            $this->delete(__FUNCTION__, $request, $model, $model::LUGGAGE_ISSUE_MOMENT);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }



    /*************************************************************
     * FUNCIONES ESTANDARIZADAS PARA TABLAS CON CAMPOS id Y name *
     *************************************************************/

    private function getAll($function, ParametersModel $model, $table)
    {
        $this->checkHostIsAllowed();
        $this->setResponseOk($function, $model->getAll($table));
    }

    private function getById($function, ParametersModel $model, $table, $id)
    {
        $this->checkHostIsAllowed();
        $model->validaId($table, $id, true);

        $this->setResponseOk($function, $model->getById($table, $id));
    }

    private function create($function, Request $request, ParametersModel $model, $table)
    {
        $this->checkTokenAndSetUserLogged($request);

        $data = $this->getJsonParams($request);

        $model->validaName($table, $data);
        $data = $model->create($table, $data);

        $this->saveChangeLog($function, $data['id'], $data);
        $this->setResponseOk($function, $data);
    }

    private function edit($function, Request $request, ParametersModel $model, $table, $id)
    {
        $this->checkTokenAndSetUserLogged($request);

        $data = $this->getJsonParams($request);
        $data['id'] = $id;

        $model->validaId($table, $id, true);
        $model->validaName($table, $data);
        $oldData = $model->getById($table, $id);
        $model->edit($table, $data);
        $data = $model->getById($table, $id);

        $this->saveChangeLog($function, $id, $data, $oldData);
        $this->setResponseOk($function, $data);
    }

    private function delete($function, Request $request, ParametersModel $model, $table)
    {
        $this->checkTokenAndSetUserLogged($request);

        $ids = $this->getJsonParams($request);

        $model->validaIdsToDelete($table, $ids);

        $this->popuConn()->beginTransaction();
        foreach ($ids as $id) {
            $dataToDelete = $model->getById($table, $id);
            try {
                $model->delete($table, $id);
            } catch (DBALException $e) {
                $this->deleteErrors[] = $this->parseErrorIfIsFkRestrictException($e->getMessage(), $id);
                continue;
            }
            $this->saveChangeLog($function, $id, $dataToDelete);
        }
        $this->popuConn()->commit();

        $this->setResponseOk($function, $this->deleteErrors);
    }
}
