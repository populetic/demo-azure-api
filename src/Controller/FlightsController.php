<?php

namespace App\Controller;

use App\Entity\Flight;
use App\Model\FlightsModel;
use App\Service\Exceptions\ValidationException;
use Doctrine\DBAL\DBALException;
use ErrorException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use TypeError;

class FlightsController extends BaseController
{
    /*********************
     ****** FLIGHTS ******
     *********************/

    /** @Route("/flights", methods={"GET", "OPTIONS"}) */
    public function getAllFlights(Request $request): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new FlightsModel($this->popuConn());

            $page         = $this->getPageFromURL($request);
            $itemsPerPage = $this->getItemsPerPageFromURL($request);

            $res['totalItems']   = $model->getTotalFlightsCount();
            $res['page']         = $page ? $page : 1;
            $res['itemsPerPage'] = $itemsPerPage ? $itemsPerPage : $model::ITEMS_PER_PAGE;
            $res['totalPages']   = ceil($res['totalItems'] / $res['itemsPerPage']);
            $res['items']        = $model->getFlightsByPageAndItemsPerPage($res['page'], $res['itemsPerPage']);

            $this->setResponseOk(__FUNCTION__, $res);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/flights/{id}", methods={"GET", "OPTIONS"}) */
    public function getFlightById($id): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new FlightsModel($this->popuConn());
            $model->validaId($id, true);

            $this->setResponseOk(__FUNCTION__, $model->getById($id));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/flights/create", methods={"POST", "OPTIONS"}) */
    public function createFlight(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $params = $this->getJsonParams($request);

            $flight  = new Flight($params);
            $claimId = $this->getDataIfKeyExistsOrNull($params, 'claimId');

            $model = new FlightsModel($this->popuConn());
            $model->validaCreation($flight);
            if ($claimId) $model->validaClaimId($claimId, true);

            $this->popuConn()->beginTransaction();

            $flight = $model->create($flight);
            $this->saveChangeLog(__FUNCTION__, $flight->id, $flight);

            if ($claimId) {
                $model->editClaimWithNewFlightId($claimId, $flight->id);
                $this->saveChangeLog(__FUNCTION__, $flight->id,
                    'Changed flightId to "' . $flight->id . '" for claimId "' . $claimId . '"'
                );
            }

            $this->popuConn()->commit();

            $this->setResponseOk(__FUNCTION__, $flight);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/flights/edit/{id}", methods={"PUT", "OPTIONS"}) */
    public function editFlight(Request $request, $id): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $flight = new Flight($this->getJsonParams($request));
            $flight->id = $id;

            $model = new FlightsModel($this->popuConn());
            $model->validaEdition($flight);
            $oldFlight = $model->getById($id);
            $flight    = $model->edit($flight, $oldFlight);

            $this->saveChangeLog(__FUNCTION__, $id, $flight, $oldFlight);
            $this->setResponseOk(__FUNCTION__, $flight);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/flights/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteFlights(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $ids = $this->getJsonParams($request);

            $model = new FlightsModel($this->popuConn());
            $model->validaIdsToDelete($ids);

            $this->deleteSeveralIdsTransaction($this->popuConn(), $model, $ids, __FUNCTION__);

            $this->setResponseOk(__FUNCTION__, $this->deleteErrors);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }



    /************************************
     ******** FLIGHT DATA ORIGINS ********
     ************************************/

    /** @Route("/flight-data-origins", methods={"GET", "OPTIONS"}) */
    public function getAllDataOrigins(): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new FlightsModel($this->popuConn());
            $this->setResponseOk(__FUNCTION__, $model->getAllDataOrigins());

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/flight-data-origins/{id}", methods={"GET", "OPTIONS"}) */
    public function getDataOriginById($id): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new FlightsModel($this->popuConn());
            $model->validaDataOriginId($id, true);

            $this->setResponseOk(__FUNCTION__, $model->getDataOriginById($id));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/flight-data-origins/create", methods={"POST", "OPTIONS"}) */
    public function createDataOrigin(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);

            $model = new FlightsModel($this->popuConn());
            $model->validaDataOriginCreation($data);
            $data = $model->createDataOrigin($data);

            $this->saveChangeLog(__FUNCTION__, $data['id'], $data);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/flight-data-origins/edit/{id}", methods={"PUT", "OPTIONS"}) */
    public function editDataOrigin(Request $request, $id): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);
            $data['id'] = $id;

            $model = new FlightsModel($this->popuConn());
            $model->validaDataOriginEdition($data);
            $oldData = $model->getDataOriginById($id);
            $model->editDataOrigin($data);

            $this->saveChangeLog(__FUNCTION__, $id, $data, $oldData);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/flight-data-origins/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteDataOrigins(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $ids = $this->getJsonParams($request);

            $model = new FlightsModel($this->popuConn());
            $model->validaDataOriginIdsToDelete($ids);

            $this->popuConn()->beginTransaction();
            foreach ($ids as $id) {
                $dataToDelete = $model->getDataOriginById($id);
                try {
                    $model->deleteDataOrigin($id);
                } catch (DBALException $e) {
                    $this->deleteErrors[] = $this->parseErrorIfIsFkRestrictException($e->getMessage(), $id);
                    continue;
                }
                $this->saveChangeLog(__FUNCTION__, $id, $dataToDelete);
            }
            $this->popuConn()->commit();

            $this->setResponseOk(__FUNCTION__, $this->deleteErrors);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }



    /************************************
     ******** CONNECTION FLIGHTS ********
     ************************************/

    /** @Route("/connection-flights", methods={"GET", "OPTIONS"}) */
    public function getAllConnectionFlights(): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new FlightsModel($this->popuConn());
            $this->setResponseOk(__FUNCTION__, $model->getAllConnectionFlights());

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/connection-flights/{id}", methods={"GET", "OPTIONS"}) */
    public function getConnectionFlightById($id): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new FlightsModel($this->popuConn());
            $model->validaConnectionFlightId($id, true);

            $this->setResponseOk(__FUNCTION__, $model->getConnectionFlightById($id));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/connection-flights/create", methods={"POST", "OPTIONS"}) */
    public function createConnectionFlight(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);

            $model = new FlightsModel($this->popuConn());
            $model->validaConnectionFlight($data);
            $data = $model->createConnectionFlight($data);

            $this->saveChangeLog(__FUNCTION__, $data['id'], $data);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/connection-flights/edit/{id}", methods={"PUT", "OPTIONS"}) */
    public function editConnectionFlight(Request $request, $id): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);
            $data['id'] = $id;

            $model = new FlightsModel($this->popuConn());
            $model->validaConnectionFlight($data, true);
            $oldData = $model->getConnectionFlightById($id);
            $data = $model->editConnectionFlight($data);

            $this->saveChangeLog(__FUNCTION__, $id, $data, $oldData);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/connection-flights/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteConnectionFlights(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $ids = $this->getJsonParams($request);

            $model = new FlightsModel($this->popuConn());
            $model->validaConnectionFlightIdsToDelete($ids);

            $this->popuConn()->beginTransaction();
            foreach ($ids as $id) {
                $dataToDelete = $model->getConnectionFlightById($id);
                try {
                    $model->deleteConnectionFlight($id);
                } catch (DBALException $e) {
                    $this->deleteErrors[] = $this->parseErrorIfIsFkRestrictException($e->getMessage(), $id);
                    continue;
                }
                $this->saveChangeLog(__FUNCTION__, $id, $dataToDelete);
            }
            $this->popuConn()->commit();

            $this->setResponseOk(__FUNCTION__, $this->deleteErrors);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }



    /************************************
     ***** CLAIM CONNECTION FLIGHTS *****
     ************************************/

    /** @Route("/claim-connection-flights/{claimId}", methods={"GET", "OPTIONS"}) */
    public function getAllClaimConnectionFlights($claimId): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new FlightsModel($this->popuConn());
            $model->validaClaimId($claimId, true);
            $this->setResponseOk(__FUNCTION__, $model->getAllClaimConnectionFlights($claimId));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/claim-connection-flights/create/{claimId}", methods={"POST", "OPTIONS"}) */
    public function createClaimConnectionFlights(Request $request, $claimId): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);

            $model = new FlightsModel($this->popuConn());
            $model->validaClaimId($claimId, true);
            $model->validaClaimConnectionFlightsCreation($data);

            $this->popuConn()->beginTransaction();
            $model->deleteAllClaimConnectionFlights($claimId);
            $model->createClaimConnectionFlights($claimId, $data);

            foreach ($data as $connFlightOrder) {
                $this->saveChangeLog(__FUNCTION__, $claimId, $connFlightOrder);
            }
            $this->popuConn()->commit();

            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/claim-connection-flights/add/{claimId}", methods={"POST", "OPTIONS"}) */
    public function addClaimConnectionFlights(Request $request, $claimId): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);

            $model = new FlightsModel($this->popuConn());
            $model->validaClaimConnectionFlightsAddition($claimId, $data);

            $this->popuConn()->beginTransaction();
            $model->addClaimConnectionFlights($claimId, $data);
            $this->saveChangeLog(__FUNCTION__, $claimId, $data);
            $this->popuConn()->commit();

            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/claim-connection-flights/delete/{claimId}/{connFlightId}", methods={"DELETE", "OPTIONS"}) */
    public function deleteClaimConnectionFlight(Request $request, $claimId, $connFlightId): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $model = new FlightsModel($this->popuConn());
            $model->validaClaimConnectionFlightExists($claimId, $connFlightId);

            $this->popuConn()->beginTransaction();
            $model->deleteClaimConnectionFlight($claimId, $connFlightId);
            $this->saveChangeLog(__FUNCTION__, [], ['claimId' => $claimId, 'connectionFlightId' => $connFlightId]);
            $this->popuConn()->commit();

            $this->setResponseOk(__FUNCTION__, []);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/claim-connection-flights/delete-all/{claimId}", methods={"DELETE", "OPTIONS"}) */
    public function deleteAllClaimConnectionFlights(Request $request, $claimId): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $model = new FlightsModel($this->popuConn());
            $model->validaClaimConnectionFlightExistsForClaim($claimId);

            $this->popuConn()->beginTransaction();
            $model->deleteAllClaimConnectionFlights($claimId);
            $this->saveChangeLog(__FUNCTION__, [], 'Deleted all Claim Connection Flights for Claim id: ' . $claimId);
            $this->popuConn()->commit();

            $this->setResponseOk(__FUNCTION__, []);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }
}
