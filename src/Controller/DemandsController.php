<?php

namespace App\Controller;

use App\Entity\Demand;
use App\Model\DemandsModel;
use App\Service\Exceptions\ValidationException;
use Doctrine\DBAL\DBALException;
use ErrorException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use TypeError;

class DemandsController extends BaseController
{
    /*********************
     ****** DEMANDS ******
     *********************/

    /** @Route("/demands", methods={"GET", "OPTIONS"}) */
    public function getAllDemands(Request $request): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $filters['procedureNumber'] = $request->query->get('procedureNumber', null);
            $filters['cancellated']     = $request->query->get('cancellated', null);
            $filters['lawFirmId']       = $request->query->get('lawFirmId', null);

            $model = new DemandsModel($this->popuConn());
            $model->validaFilters($filters);
            $response = $model->getAll($filters);

            $page = $this->getPageFromURL($request);
            if ($page) {
                $response = $model->paginateResult($response, $page, $this->getItemsPerPageFromURL($request));
            }

            $this->setResponseOk(__FUNCTION__, $response);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/demands/{id}", methods={"GET", "OPTIONS"}) */
    public function getDemandById($id): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new DemandsModel($this->popuConn());
            $model->validaDemandId($id, true);

            $this->setResponseOk(__FUNCTION__, $model->getById($id));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/demands/create", methods={"POST", "OPTIONS"}) */
    public function createDemand(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $demand = new Demand($this->getJsonParams($request));

            $model = new DemandsModel($this->popuConn());
            $model->validaDemand($demand);
            $demand = $model->create($demand);

            $this->saveChangeLog(__FUNCTION__, $demand->id, $demand);
            $this->setResponseOk(__FUNCTION__, $demand);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/demands/edit/{id}", methods={"PUT", "OPTIONS"}) */
    public function editDemand(Request $request, $id): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $demand = new Demand($this->getJsonParams($request));
            $demand->id = $id;

            $model = new DemandsModel($this->popuConn());
            $model->validaDemandId($id, true);
            $model->validaDemand($demand);
            $oldDemand = $model->getById($id);
            $demand    = $model->edit($demand);

            $this->saveChangeLog(__FUNCTION__, $id, $demand, $oldDemand);
            $this->setResponseOk(__FUNCTION__, $demand);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/demands/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteDemands(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $ids = $this->getJsonParams($request);

            $model = new DemandsModel($this->popuConn());
            $model->validaIdsToDelete($ids);
            $this->deleteSeveralIdsTransaction($this->popuConn(), $model, $ids, __FUNCTION__);

            $this->setResponseOk(__FUNCTION__, $this->deleteErrors);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }



    /*********************
     * DEMANDS EXECUTIVE *
     *********************/

    /** @Route("/demands-executive", methods={"GET", "OPTIONS"}) */
    public function getAllDemandsExecutive(Request $request): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new DemandsModel($this->popuConn());
            $response = $model->getAllDemandsExecutive();

            $page = $this->getPageFromURL($request);
            if ($page) {
                $response = $model->paginateResult($response, $page, $this->getItemsPerPageFromURL($request));
            }

            $this->setResponseOk(__FUNCTION__, $response);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/demands-executive/{id}", methods={"GET", "OPTIONS"}) */
    public function getDemandExecutiveById($id): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new DemandsModel($this->popuConn());
            $model->validaDemandExecutiveId($id, true);

            $this->setResponseOk(__FUNCTION__, $model->getDemandExecutiveById($id));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/demands-executive/create", methods={"POST", "OPTIONS"}) */
    public function createDemandExecutive(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);

            $model = new DemandsModel($this->popuConn());
            $model->validaDemandExecutive($data);
            $id   = $model->createDemandExecutive($data);
            $data = $model->getDemandExecutiveById($id);

            $this->saveChangeLog(__FUNCTION__, $id, $data);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/demands-executive/edit/{id}", methods={"PUT", "OPTIONS"}) */
    public function editDemandExecutive(Request $request, $id): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);
            $data['id'] = $id;

            $model = new DemandsModel($this->popuConn());
            $model->validaDemandExecutiveId($id, true);
            $model->validaDemandExecutive($data);
            $oldData = $model->getDemandExecutiveById($id);
            $model->editDemandExecutive($data);
            $data = $model->getDemandExecutiveById($id);

            $this->saveChangeLog(__FUNCTION__, $id, $data, $oldData);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/demands-executive/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteDemandExecutive(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $ids = $this->getJsonParams($request);

            $model = new DemandsModel($this->popuConn());
            $model->validaDemandExecutiveIdsToDelete($ids);

            $this->popuConn()->beginTransaction();
            foreach ($ids as $id) {
                $dataToDelete = $model->getDemandExecutiveById($id);
                try {
                    $model->deleteDemandExecutive($id);
                } catch (DBALException $e) {
                    $this->deleteErrors[] = $this->parseErrorIfIsFkRestrictException($e->getMessage(), $id);
                    continue;
                }
                $this->saveChangeLog(__FUNCTION__, $id, $dataToDelete);
            }
            $this->popuConn()->commit();

            $this->setResponseOk(__FUNCTION__, $this->deleteErrors);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }



    /*********************
     ** DEMAND EXPENSES **
     *********************/

    /** @Route("/demands-expenses", methods={"GET", "OPTIONS"}) */
    public function getAllDemandExpenses(Request $request): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new DemandsModel($this->popuConn());
            $response = $model->getAllDemandExpenses();

            $page = $this->getPageFromURL($request);
            if ($page) {
                $response = $model->paginateResult($response, $page, $this->getItemsPerPageFromURL($request));
            }

            $this->setResponseOk(__FUNCTION__, $response);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/demands-expenses/{id}", methods={"GET", "OPTIONS"}) */
    public function getDemandExpenseById($id): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new DemandsModel($this->popuConn());
            $model->validaDemandExpenseId($id, true);

            $this->setResponseOk(__FUNCTION__, $model->getDemandExpenseById($id));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/demands-expenses/create", methods={"POST", "OPTIONS"}) */
    public function createDemandExpense(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);

            $model = new DemandsModel($this->popuConn());
            $model->validaDemandExpense($data);
            $id   = $model->createDemandExpense($data);
            $data = $model->getDemandExpenseById($id);

            $this->saveChangeLog(__FUNCTION__, $id, $data);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/demands-expenses/edit/{id}", methods={"PUT", "OPTIONS"}) */
    public function editDemandExpense(Request $request, $id): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);
            $data['id'] = $id;

            $model = new DemandsModel($this->popuConn());
            $model->validaDemandExpenseId($id, true);
            $model->validaDemandExpense($data);
            $oldData = $model->getDemandExpenseById($id);
            $model->editDemandExpense($data);
            $data = $model->getDemandExpenseById($id);

            $this->saveChangeLog(__FUNCTION__, $id, $data, $oldData);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/demands-expenses/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteDemandExpense(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $ids = $this->getJsonParams($request);

            $model = new DemandsModel($this->popuConn());
            $model->validaDemandExpenseIdsToDelete($ids);

            $this->popuConn()->beginTransaction();
            foreach ($ids as $id) {
                $dataToDelete = $model->getDemandExpenseById($id);
                try {
                    $model->deleteDemandExpense($id);
                } catch (DBALException $e) {
                    $this->deleteErrors[] = $this->parseErrorIfIsFkRestrictException($e->getMessage(), $id);
                    continue;
                }
                $this->saveChangeLog(__FUNCTION__, $id, $dataToDelete);
            }
            $this->popuConn()->commit();

            $this->setResponseOk(__FUNCTION__, $this->deleteErrors);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }



    /*********************
     **** DEMAND VIEW ****
     *********************/

    /** @Route("/demand-view/{demandId}", methods={"GET", "OPTIONS"}) */
    public function getDemandViewByDemandId($demandId): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new DemandsModel($this->popuConn());
            $model->validaDemandId($demandId, true);

            $this->setResponseOk(__FUNCTION__, $model->getDemandViewByDemandId($demandId));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/demand-view/create", methods={"POST", "OPTIONS"}) */
    public function createDemandView(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);

            $model = new DemandsModel($this->popuConn());
            $model->validaDemandView($data);

            $this->popuConn()->beginTransaction();
            $model->deleteDemandView($data['demandId']);
            $model->createDemandView($data);

            $this->saveChangeLog(__FUNCTION__, [], $data);
            $this->popuConn()->commit();

            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/demand-view/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteDemandView(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $demandsIds = $this->getJsonParams($request);

            $model = new DemandsModel($this->popuConn());
            $model->validaDemandViewToDelete($demandsIds);

            $this->popuConn()->beginTransaction();
            foreach ($demandsIds as $demandId) {
                $demandViewToDelete = $model->getDemandViewByDemandId($demandId);
                try {
                    $model->deleteDemandView($demandId);
                } catch (DBALException $e) {
                    $this->deleteErrors[] = $this->parseErrorIfIsFkRestrictException($e->getMessage(), $demandId);
                    continue;
                }
                $this->saveChangeLog(__FUNCTION__, $demandId, $demandViewToDelete);
            }
            $this->popuConn()->commit();

            $this->setResponseOk(__FUNCTION__, $this->deleteErrors);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }



    /*********************
     *** RELATED CLAIMS ***
     *********************/

    /** @Route("/demand-related-claims/{demandId}", methods={"GET", "OPTIONS"}) */
    public function getRelatedClaimsByDemandId($demandId): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new DemandsModel($this->popuConn());
            $model->validaDemandId($demandId, true);

            $this->setResponseOk(__FUNCTION__, $model->getRelatedClaimsByDemandId($demandId));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/demand-related-claims/create", methods={"POST", "OPTIONS"}) */
    public function createDemandRelatedClaim(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);

            $model = new DemandsModel($this->popuConn());
            $model->validaRelatedClaimCreation($data);
            foreach ($data['claimIds'] as $claimId) {
                $model->createDemandRelatedClaim($data['demandId'], $claimId);
            }

            $this->saveChangeLog(__FUNCTION__, [], $data);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/demand-related-claims/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteDemandRelatedClaim(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);

            $model = new DemandsModel($this->popuConn());
            $model->validaRelatedClaimToDelete($data);
            $model->deleteDemandRelatedClaim($data);

            $this->saveChangeLog(__FUNCTION__, [], $data);
            $this->setResponseOk(__FUNCTION__);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }



    /************************
     * DEMAND NOTIFICATIONS *
     ************************/

    /** @Route("/demand-notifications/{demandId}", methods={"GET", "OPTIONS"}) */
    public function getDemandNotificationsByDemandId($demandId): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new DemandsModel($this->popuConn());
            $model->validaDemandId($demandId, true);

            $this->setResponseOk(__FUNCTION__, $model->getDemandNotificationsByDemandId($demandId));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/demand-notifications/create", methods={"POST", "OPTIONS"}) */
    public function createDemandNotification(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);

            $model = new DemandsModel($this->popuConn());
            $model->validaDemandNotificationCreation($data);
            $model->createDemandNotification($data);

            $this->saveChangeLog(__FUNCTION__, [], $data);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/demand-notifications/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteDemandNotification(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);

            $model = new DemandsModel($this->popuConn());
            $model->validaDemandNotificationToDelete($data);
            $model->deleteDemandNotification($data);

            $this->saveChangeLog(__FUNCTION__, [], $data);
            $this->setResponseOk(__FUNCTION__);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }



    /*********************
     * NOTIFICATION TYPES *
     *********************/

    /** @Route("/judicial-notification-types-by-country", methods={"GET", "OPTIONS"}) */
    public function getAllNotificationTypesByCountry(Request $request): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $countryId = $request->query->getInt('countryid', null);

            $model = new DemandsModel($this->popuConn());
            if ($countryId) $model->validaCountryId($countryId, true);
            $response = $model->getAllNotificationTypesByCountry($countryId);

            $page = $this->getPageFromURL($request);
            if ($page) {
                $response = $model->paginateResult($response, $page, $this->getItemsPerPageFromURL($request));
            }

            $this->setResponseOk(__FUNCTION__, $response);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/judicial-notification-types-by-country/{id}", methods={"GET", "OPTIONS"}) */
    public function getNotificationTypeById($id): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new DemandsModel($this->popuConn());
            $model->validaNotificationTypeId($id, true);

            $this->setResponseOk(__FUNCTION__, $model->getNotificationTypeById($id));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/judicial-notification-types-by-country/create", methods={"POST", "OPTIONS"}) */
    public function createNotificationType(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);

            $model = new DemandsModel($this->popuConn());
            $model->validaNotificationType($data);
            $data = $model->createNotificationType($data);

            $this->saveChangeLog(__FUNCTION__, $data['id'], $data);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/judicial-notification-types-by-country/edit/{id}", methods={"PUT", "OPTIONS"}) */
    public function editNotificationType(Request $request, $id): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);
            $data['id'] = $id;

            $model = new DemandsModel($this->popuConn());
            $model->validaNotificationTypeId($id, true);
            $model->validaNotificationType($data);
            $oldData = $model->getNotificationTypeById($id);
            $model->editNotificationType($data);

            $this->saveChangeLog(__FUNCTION__, $id, $data, $oldData);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/judicial-notification-types-by-country/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteNotificationType(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $ids = $this->getJsonParams($request);

            $model = new DemandsModel($this->popuConn());
            $model->validaNotificationTypeIdsToDelete($ids);

            $this->popuConn()->beginTransaction();
            foreach ($ids as $id) {
                $dataToDelete = $model->getNotificationTypeById($id);
                try {
                    $model->deleteNotificationType($id);
                } catch (DBALException $e) {
                    $this->deleteErrors[] = $this->parseErrorIfIsFkRestrictException($e->getMessage(), $id);
                    continue;
                }
                $this->saveChangeLog(__FUNCTION__, $id, $dataToDelete);
            }
            $this->popuConn()->commit();

            $this->setResponseOk(__FUNCTION__, $this->deleteErrors);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }
}
