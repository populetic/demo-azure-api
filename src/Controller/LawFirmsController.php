<?php

namespace App\Controller;

use App\Entity\LawFirm;
use App\Model\LawFirmsModel;
use App\Service\Exceptions\ValidationException;
use Doctrine\DBAL\DBALException;
use ErrorException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use TypeError;

class LawFirmsController extends BaseController
{
    /*********************
     ***** LAW FIRMS *****
     *********************/

    /** @Route("/law-firms", methods={"GET", "OPTIONS"}) */
    public function getAllLawFirms(Request $request): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new LawFirmsModel($this->popuConn());
            $response = $model->getAll();

            $page = $this->getPageFromURL($request);
            if ($page) {
                $response = $model->paginateResult($response, $page, $this->getItemsPerPageFromURL($request));
            }

            $this->setResponseOk(__FUNCTION__, $response);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/law-firms/{id}", methods={"GET", "OPTIONS"}) */
    public function getLawFirmById($id): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new LawFirmsModel($this->popuConn());
            $model->validaLawFirmId($id, true);

            $this->setResponseOk(__FUNCTION__, $model->getById($id));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/law-firms/create", methods={"POST", "OPTIONS"}) */
    public function createLawFirm(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $lawFirm = new LawFirm($this->getJsonParams($request));

            $model = new LawFirmsModel($this->popuConn());
            $model->validaLawFirm($lawFirm);
            $lawFirm = $model->create($lawFirm);

            $this->saveChangeLog(__FUNCTION__, $lawFirm->id, $lawFirm);
            $this->setResponseOk(__FUNCTION__, $lawFirm);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/law-firms/edit/{id}", methods={"PUT", "OPTIONS"}) */
    public function editLawFirm(Request $request, $id): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $lawFirm = new LawFirm($this->getJsonParams($request));
            $lawFirm->id = $id;

            $model = new LawFirmsModel($this->popuConn());
            $model->validaLawFirmId($id, true);
            $model->validaLawFirm($lawFirm);
            $oldLawFirm = $model->getById($id);
            $lawFirm    = $model->edit($lawFirm);

            $this->saveChangeLog(__FUNCTION__, $id, $lawFirm, $oldLawFirm);
            $this->setResponseOk(__FUNCTION__, $lawFirm);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/law-firms/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteLawFirms(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $ids = $this->getJsonParams($request);

            $model = new LawFirmsModel($this->popuConn());
            $model->validaIdsToDelete($ids);
            $this->deleteSeveralIdsTransaction($this->popuConn(), $model, $ids, __FUNCTION__);

            $this->setResponseOk(__FUNCTION__, $this->deleteErrors);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }



    /*********************************
     * DISRUPTION TYPES BY LAW FIRM *
     *********************************/

    /** @Route("/disruption-types-by-law-firm/{lawFirmId}", methods={"GET", "OPTIONS"}) */
    public function getDisrTypesByLawFirm($lawFirmId): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new LawFirmsModel($this->popuConn());
            $model->validaLawFirmId($lawFirmId, true);

            $this->setResponseOk(__FUNCTION__, $model->getDisrTypesByLawFirm($lawFirmId));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/disruption-types-by-law-firm/create", methods={"POST", "OPTIONS"}) */
    public function createDisrTypeByLawFirm(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);

            $model = new LawFirmsModel($this->popuConn());
            $model->validaDisrTypeByLawFirmCreation($data);
            $model->createDisrTypeByLawFirm($data);

            $this->saveChangeLog(__FUNCTION__, [], $data);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/disruption-types-by-law-firm/edit", methods={"PUT", "OPTIONS"}) */
    public function editDisrTypeByLawFirm(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);

            $model = new LawFirmsModel($this->popuConn());
            $model->validaDisrTypeByLawFirmEdition($data);
            $oldData = $model->getDisrTypesByLawFirm($data['lawFirmId'], $data['disruptionTypeId']);
            $model->editDisrTypeByLawFirm($data);

            $this->saveChangeLog(__FUNCTION__, [$data['lawFirmId'], $data['disruptionTypeId']], $data, $oldData);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/disruption-types-by-law-firm/delete/{lawFirmId}/{disruptionTypeId}", methods={"DELETE", "OPTIONS"}) */
    public function deleteDisrTypeByLawFirm(Request $request, $lawFirmId, $disruptionTypeId): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $model = new LawFirmsModel($this->popuConn());
            $model->validaDisrTypeByLawFirmExists($lawFirmId, $disruptionTypeId);
            $dataToDelete = $model->getDisrTypesByLawFirm($lawFirmId, $disruptionTypeId);
            $model->deleteDisrTypeByLawFirm($lawFirmId, $disruptionTypeId);

            $this->saveChangeLog(__FUNCTION__, [], $dataToDelete);
            $this->setResponseOk(__FUNCTION__);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /*********************
     * LAW FIRM ADMINISTRATORS *
     *********************/

     /** @Route("/law-firm-administrators", methods={"GET", "OPTIONS"}) */
    public function getAllLawFirmAdministrators(Request $request): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new LawFirmsModel($this->popuConn());
            $response = $model->getAllLawFirmAdministrators();

            $page = $this->getPageFromURL($request);
            if ($page) {
                $response = $model->paginateResult($response, $page, $this->getItemsPerPageFromURL($request));
            }

            $this->setResponseOk(__FUNCTION__, $response);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/law-firm-administrators/{id}", methods={"GET", "OPTIONS"}) */
    public function getAllLawFirmAdministratorById($id): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new LawFirmsModel($this->popuConn());
            $model->validaLawFirmId($id, true);

            $this->setResponseOk(__FUNCTION__, $model->getAllLawFirmAdministratorById($id));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/law-firm-administrators/create", methods={"POST", "OPTIONS"}) */
    public function createLawFirmAdministrator(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);

            $model = new LawFirmsModel($this->popuConn());
            $model->validaLawFirmAdministratorCreation($data);
            $model->createLawFirmAdministrator($data);

            $this->saveChangeLog(__FUNCTION__, [], $data);
            $this->setResponseOk(__FUNCTION__, $data);
            //devoler el admin creado
        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    //actualizar Docu
    /** @Route("/law-firm-administrators/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteLawFirmAdministrator(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);

            $model = new LawFirmsModel($this->popuConn());
            $model->validaLawFirmAdministratorToDelete($data);
            $model->deleteLawFirmAdministrator($data);

            $this->saveChangeLog(__FUNCTION__, [], $data);
            $this->setResponseOk(__FUNCTION__);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }
}
