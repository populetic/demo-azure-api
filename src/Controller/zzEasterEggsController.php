<?php

namespace App\Controller;

use App\Service\Exceptions\ValidationException;
use ErrorException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use TypeError;

class zzEasterEggsController extends BaseController
{
    const ERROR           = 'Necesitas un tutorial de arrays';
    const ANIMOMOUS_ERROR = 'VG9wbyBkZXRlY3RlZA==';

    /** @Route("/animomous/{password}", methods={"POST", "OPTIONS"}) */
    public function animomous(Request $request, $password): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $response = self::ANIMOMOUS_ERROR;
            if ($this->checkPassword($password)) {
                $answers = $this->megaShuffle($this->getAnimomousAnswers());
                $response = array_shift($answers);
            }

            $this->setResponseOk(__FUNCTION__, base64_decode($response));

        } catch (ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, self::ERROR);
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/decisiones-de-la-bida", methods={"POST", "OPTIONS"}) */
    public function decisionesDeLaBida(Request $request): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $decisions = $this->megaShuffle($this->getJsonParams($request));
            $winner = array_shift($decisions);
            $this->setResponseOk(__FUNCTION__, [
                'El destino ha decidido que: ' => $winner,
                'Hoy no, mañaaaana...'         => $decisions
            ]);
        } catch (ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, self::ERROR);
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/8-ball", methods={"POST", "OPTIONS"}) */
    public function eightBall(Request $request): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $yourQuestion = $this->getJsonParams($request);
            $this->makeNothingWithYourQuestion($yourQuestion);

            $luckyAnswer = [
                'Sí',
                'No',
                'Va ser que no',
                'Lo más seguro es que sí, pero no se... prueba a ver...',
                'Ni lo sueñes',
                'Puedes apostar a ello',
                'Hoy no, mañaaaana... ',
                'A ti no puedo mentirte................adios',
                'La conexión no se ha podido estable....ES BROMA! Pero, no...',
                'Nononononono.....bueno sí',
                'Habiamo uccito a fumare, contacti en 5 minuti (mejor contacta en 20)',
                'Tienes menos posibilidades que el fari en la nba',
                'Fijo... pero fijo eh? FIJO',
                'Nueve de cada diez dentistas dicen que sí',
                'Once de cada diez dentistas dicen que no'
            ];

            $luckyAnswer = $this->megaShuffle($luckyAnswer);
            $this->setResponseOk(__FUNCTION__, array_shift($luckyAnswer));

        } catch (ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, self::ERROR);
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/check-consciencia-global", methods={"POST", "OPTIONS"}) */
    public function checkConscienciaGlobal(Request $request): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $concept       = $request->query->get('concepto', null);
            $countOnes     = $countZeros = 0;
            $maxIterations = 100000;

            if ($concept) {
                $maxIterations = $this->getMaxIterationsByNumerology($concept);
            }

            for ($i = 0; $i < $maxIterations; $i++) {
                if (rand(0, 1) === 0) $countZeros++;
                else $countOnes++;
            }

            $percOnes  = ($countOnes * 100) / $maxIterations;
            $percZeros = ($countZeros * 100) / $maxIterations;

            $this->setResponseOk(__FUNCTION__, [
                'Concepto'             => $concept,
                'Porcentaje de 0'      => round($percZeros, 2) . '%',
                'Porcentaje de 1'      => round($percOnes, 2) . '%',
                'Resultado Desviación' => ($percZeros > 51 || $percOnes > 51) ? 'VAIS A MORIR TODOS' : 'TODO OK CHAVALES'
            ]);
        } catch (ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, self::ERROR);
        } finally {
            return new JsonResponse($this->response);
        }
    }

    private function megaShuffle($array): array
    {
        for ($x = 0; $x <= 10; $x++) {
            shuffle($array);
        }

        return $array;
    }

    private function getMaxIterationsByNumerology($concept): int
    {
        $sum = 0;
        foreach (str_split($concept) as $letter) {
            if (strpos('ajs', $letter) !== false) $sum += 1;
            if (strpos('bkt', $letter) !== false) $sum += 2;
            if (strpos('clu', $letter) !== false) $sum += 3;
            if (strpos('dmv', $letter) !== false) $sum += 4;
            if (strpos('enñw', $letter) !== false) $sum += 5;
            if (strpos('fox', $letter) !== false) $sum += 6;
            if (strpos('gpy', $letter) !== false) $sum += 7;
            if (strpos('hqz', $letter) !== false) $sum += 8;
            if (strpos('ir', $letter) !== false) $sum += 9;
        }

        return $sum * 1111;
    }

    private function makeNothingWithYourQuestion($itDoesntMatter)
    {
        return null;
    }

    private function checkPassword($pass): bool
    {
        return is_string($pass) && base64_encode($pass) === 'dGV0YXM=';
    }
}
