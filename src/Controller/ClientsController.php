<?php

namespace App\Controller;

use App\Entity\Client;
use App\Entity\LegalGuardian;
use App\Model\ClientsModel;
use App\Service\Exceptions\ValidationException;
use Doctrine\DBAL\DBALException;
use ErrorException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use TypeError;

class ClientsController extends BaseController
{
    /*********************
     ****** CLIENTS ******
     *********************/

    /** @Route("/clients", methods={"GET", "OPTIONS"}) */
    public function getAllClients(Request $request): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $filters['finished'] = $request->query->get('finished', null);

            $model = new ClientsModel($this->popuConn());
            $model->validaFilters($filters);

            $this->setResponseOk(__FUNCTION__, $model->getAll($filters));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/clients/{id}", methods={"GET", "OPTIONS"}) */
    public function getClientById($id): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new ClientsModel($this->popuConn());
            $model->validaId($id, true);

            $this->setResponseOk(__FUNCTION__, $model->getById($id));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/client-by-claim-id/{claimId}", methods={"GET", "OPTIONS"}) */
    public function getClientByClaimId($claimId): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new ClientsModel($this->popuConn());
            $model->validaClaimId($claimId, true);

            $this->setResponseOk(__FUNCTION__, $model->getByClaimId($claimId));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/client-related-claims/{id}", methods={"GET", "OPTIONS"}) */
    public function getClientRelatedClaims($id): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new ClientsModel($this->popuConn());
            $model->validaId($id, true);

            $this->setResponseOk(__FUNCTION__, $model->getRelatedClaimsById($id));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/client-related-demands/{id}", methods={"GET", "OPTIONS"}) */
    public function getClientRelatedDemands($id): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new ClientsModel($this->popuConn());
            $model->validaId($id, true);

            $this->setResponseOk(__FUNCTION__, $model->getRelatedDemandsById($id));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/client-related-bills/{id}", methods={"GET", "OPTIONS"}) */
    public function getClientRelatedBills($id): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new ClientsModel($this->popuConn());
            $model->validaId($id, true);

            $this->setResponseOk(__FUNCTION__, $model->getRelatedBillsById($id));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/clients/create", methods={"POST", "OPTIONS"}) */
    public function createClient(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $client = new Client($this->getJsonParams($request));

            $model = new ClientsModel($this->popuConn());
            $model->validaCreation($client);
            $client = $model->create($client);

            $this->saveChangeLog(__FUNCTION__, $client->id, $client);
            $this->setResponseOk(__FUNCTION__, $client);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/clients/edit/{id}", methods={"PUT", "OPTIONS"}) */
    public function editClient(Request $request, $id): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $client = new Client($this->getJsonParams($request));
            $client->id = $id;

            $model = new ClientsModel($this->popuConn());
            $model->validaEdition($client);
            $oldClient = $model->getById($id);
            $client    = $model->edit($client);

            $this->saveChangeLog(__FUNCTION__, $id, $client, $oldClient);
            $this->setResponseOk(__FUNCTION__, $client);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/clients/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteClients(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $ids = $this->getJsonParams($request);

            $model = new ClientsModel($this->popuConn());
            $model->validaIdsToDelete($ids);

            $this->deleteSeveralIdsTransaction($this->popuConn(), $model, $ids, __FUNCTION__);

            $this->setResponseOk(__FUNCTION__, $this->deleteErrors);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/clients-vip-payment", methods={"GET", "OPTIONS"}) */
    public function getAllVIPPaymentClients(Request $request): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new ClientsModel($this->popuConn());
            $this->setResponseOk(__FUNCTION__, $model->getAllVIPPaymentClients());

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/clients-payment-between-dates", methods={"POST", "OPTIONS"}) */
    public function getPaymentClientsBetweenDates(Request $request): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $data  = $this->getJsonParams($request);
            $start = $this->getDataIfKeyExistsOrNull($data, 'start');
            $end   = $this->getDataIfKeyExistsOrNull($data, 'end');

            $model = new ClientsModel($this->popuConn());
            $model->validaTimestamp($start, true);
            $model->validaTimestamp($end, true);
            $this->setResponseOk(__FUNCTION__, $model->getPaymentClientsBetweenDates($start, $end));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }



    /*********************
     ** LEGAL GUARDIANS **
     *********************/

    /** @Route("/legal-guardians", methods={"GET", "OPTIONS"}) */
    public function getAllLegalGuardians(Request $request): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new ClientsModel($this->popuConn());
            $response = $model->getAllLegalGuardians();

            $page = $this->getPageFromURL($request);
            if ($page) {
                $response = $model->paginateResult($response, $page, $this->getItemsPerPageFromURL($request));
            }

            $this->setResponseOk(__FUNCTION__, $response);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/legal-guardians/{id}", methods={"GET", "OPTIONS"}) */
    public function getLegalGuardianById($id): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new ClientsModel($this->popuConn());
            $model->validaLegalGuardianId($id, true);

            $this->setResponseOk(__FUNCTION__, $model->getLegalGuardianById($id));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/legal-guardians/create", methods={"POST", "OPTIONS"}) */
    public function createLegalGuardian(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $legalGuardian = new LegalGuardian($this->getJsonParams($request));

            $model = new ClientsModel($this->popuConn());
            $model->validaLegalGuardianCreation($legalGuardian);
            $legalGuardian = $model->createLegalGuardian($legalGuardian);

            $this->saveChangeLog(__FUNCTION__, $legalGuardian->id, $legalGuardian);
            $this->setResponseOk(__FUNCTION__, $legalGuardian);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/legal-guardians/edit/{id}", methods={"PUT", "OPTIONS"}) */
    public function editLegalGuardian(Request $request, $id): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $legalGuardian = new LegalGuardian($this->getJsonParams($request));
            $legalGuardian->id = $id;

            $model = new ClientsModel($this->popuConn());
            $model->validaLegalGuardianEdition($legalGuardian);
            $oldLegalGuardian = $model->getLegalGuardianById($id);
            $legalGuardian    = $model->editLegalGuardian($legalGuardian);

            $this->saveChangeLog(__FUNCTION__, $id, $legalGuardian, $oldLegalGuardian);
            $this->setResponseOk(__FUNCTION__, $legalGuardian);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/legal-guardians/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteLegalGuardians(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $ids = $this->getJsonParams($request);

            $model = new ClientsModel($this->popuConn());
            $model->validaLegalGuardianIdsToDelete($ids);

            $this->popuConn()->beginTransaction();
            foreach ($ids as $id) {
                $dataToDelete = $model->getLegalGuardianById($id);
                try {
                    $model->deleteLegalGuardian($id);
                } catch (DBALException $e) {
                    $this->deleteErrors[] = $this->parseErrorIfIsFkRestrictException($e->getMessage(), $id);
                    continue;
                }
                $this->saveChangeLog(__FUNCTION__, $id, $dataToDelete);
            }
            $this->popuConn()->commit();

            $this->setResponseOk(__FUNCTION__, $this->deleteErrors);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }
}
