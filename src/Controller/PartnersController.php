<?php

namespace App\Controller;

use App\Entity\Partner;
use App\Model\PartnersModel;
use App\Service\Exceptions\ValidationException;
use Doctrine\DBAL\DBALException;
use ErrorException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use TypeError;

class PartnersController extends BaseController
{
    /*********************
     ****** PARTNERS ******
     *********************/

    /** @Route("/partners", methods={"GET", "OPTIONS"}) */
    public function getAllPartners(Request $request): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new PartnersModel($this->popuConn());
            $response = $model->getAll();

            $page = $this->getPageFromURL($request);
            if ($page) {
                $response = $model->paginateResult($response, $page, $this->getItemsPerPageFromURL($request));
            }

            $this->setResponseOk(__FUNCTION__, $response);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/partners/{id}", methods={"GET", "OPTIONS"}) */
    public function getPartnerById($id): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new PartnersModel($this->popuConn());
            $model->validaId($id, true);

            $this->setResponseOk(__FUNCTION__, $model->getById($id));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/partner-by-claim/{claimId}", methods={"GET", "OPTIONS"}) */
    public function getPartnerByClaimId($claimId): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new PartnersModel($this->popuConn());
            $model->validaClaimId($claimId, true);

            $this->setResponseOk(__FUNCTION__, $model->getByClaimId($claimId));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/partners/create", methods={"POST", "OPTIONS"}) */
    public function createPartner(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $partner = new Partner($this->getJsonParams($request));

            $model = new PartnersModel($this->popuConn());
            $model->validaCreation($partner);
            $partner = $model->create($partner);

            $this->saveChangeLog(__FUNCTION__, $partner->id, $partner);
            $this->setResponseOk(__FUNCTION__, $partner);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/partners/edit/{id}", methods={"PUT", "OPTIONS"}) */
    public function editPartner(Request $request, $id): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $partner = new Partner($this->getJsonParams($request));
            $partner->id = $id;

            $model = new PartnersModel($this->popuConn());
            $model->validaEdition($partner);
            $oldPartner = $model->getById($id);
            $partner    = $model->edit($partner);

            $this->saveChangeLog(__FUNCTION__, $id, $partner, $oldPartner);
            $this->setResponseOk(__FUNCTION__, $partner);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/partners/delete", methods={"DELETE", "OPTIONS"}) */
    public function deletePartners(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $ids = $this->getJsonParams($request);

            $model = new PartnersModel($this->popuConn());
            $model->validaIdsToDelete($ids);

            $this->deleteSeveralIdsTransaction($this->popuConn(), $model, $ids, __FUNCTION__);

            $this->setResponseOk(__FUNCTION__, $this->deleteErrors);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }



    /*********************
     ** PARTNERS CLAIMS **
     *********************/

    /** @Route("/partners-claims/by-partner/{partnerId}", methods={"GET", "OPTIONS"}) */
    public function getAllPartnersClaimsByPartnerId($partnerId): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new PartnersModel($this->popuConn());
            $model->validaId($partnerId, true);

            $this->setResponseOk(__FUNCTION__, $model->getAllPartnersClaimsByPartnerId($partnerId));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/partners-claims/by-claim-and-partner/{claimId}/{partnerId}", methods={"GET", "OPTIONS"}) */
    public function getPartnerClaimByClaimAndPartner($claimId, $partnerId): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new PartnersModel($this->popuConn());
            $model->validaClaimIdAndPartnerId($claimId, $partnerId);

            $this->setResponseOk(__FUNCTION__, $model->getPartnerClaimByClaimAndPartner($claimId, $partnerId));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/partners-claims/create", methods={"POST", "OPTIONS"}) */
    public function createPartnerClaim(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);

            $model = new PartnersModel($this->popuConn());
            $model->validaPartnerClaim($data);
            $model->throwExceptionIfPartnerClaimAlreadyExists($data['claimId'], $data['partnerId']);
            $model->createPartnerClaim($data);

            $this->saveChangeLog(__FUNCTION__, [], $data);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/partners-claims/edit", methods={"PUT", "OPTIONS"}) */
    public function editPartnerClaim(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);

            $model = new PartnersModel($this->popuConn());
            $model->validaPartnerClaim($data);
            $model->validaIfPartnerClaimExistsOrThrowException($data['claimId'], $data['partnerId']);
            $oldData = $model->getPartnerClaimByClaimAndPartner($data['claimId'], $data['partnerId']);
            $model->editPartnerClaim($data);

            $this->saveChangeLog(__FUNCTION__, [$data['claimId'], $data['partnerId']], $data, $oldData);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/partners-claims/delete", methods={"DELETE", "OPTIONS"}) */
    public function deletePartnerClaim(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $dataToDelete = $this->getJsonParams($request);
            $claimId      = $this->getDataIfKeyExistsOrNull($dataToDelete, 'claimId');
            $partnerId    = $this->getDataIfKeyExistsOrNull($dataToDelete, 'partnerId');

            $model = new PartnersModel($this->popuConn());
            $model->validaClaimIdAndPartnerId($claimId, $partnerId);

            $model->deletePartnerClaim($claimId, $partnerId);
            $this->saveChangeLog(__FUNCTION__, [], $dataToDelete);

            $this->setResponseOk(__FUNCTION__);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }
}
