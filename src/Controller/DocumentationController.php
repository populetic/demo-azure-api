<?php

namespace App\Controller;

use App\Entity\Documentation;
use App\Model\DocumentationModel;
use App\Service\Exceptions\ValidationException;
use Doctrine\DBAL\DBALException;
use ErrorException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use TypeError;

class DocumentationController extends BaseController
{
    /*********************
     *** DOCUMENTATION ***
     *********************/

    /** @Route("/documentation", methods={"GET", "OPTIONS"}) */
    public function getAllDocumentation(Request $request): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new DocumentationModel($this->popuConn());
            $response = $model->getAll();

            $page = $this->getPageFromURL($request);
            if ($page) {
                $response = $model->paginateResult($response, $page, $this->getItemsPerPageFromURL($request));
            }

            $this->setResponseOk(__FUNCTION__, $response);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/documentation/{id}", methods={"GET", "OPTIONS"}) */
    public function getDocumentationById($id): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new DocumentationModel($this->popuConn());
            $model->validaId($id, true);

            $data = (array)$model->getById($id);
            $data['dataFromGoogleVisio'] = $model->getDocGoogleVisioByDocumentationId($id);
            unset($data['dataFromGoogleVisio']['documentationId']);

            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/documentation/create", methods={"POST", "OPTIONS"}) */
    public function createDocumentation(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $documentation = new Documentation($this->getJsonParams($request));

            $model = new DocumentationModel($this->popuConn());
            $model->validaDocumentation($documentation);
            $documentation = $model->create($documentation);

            $this->saveChangeLog(__FUNCTION__, $documentation->id, $documentation);
            $this->setResponseOk(__FUNCTION__, $documentation);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/documentation/edit/{id}", methods={"PUT", "OPTIONS"}) */
    public function editDocumentation(Request $request, $id): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $documentation = new Documentation($this->getJsonParams($request));
            $documentation->id = $id;

            $model = new DocumentationModel($this->popuConn());
            $model->validaId($id, true);
            $model->validaDocumentation($documentation);
            $oldDocumentation = $model->getById($id);
            $documentation    = $model->edit($documentation);

            $this->saveChangeLog(__FUNCTION__, $id, $documentation, $oldDocumentation);
            $this->setResponseOk(__FUNCTION__, $documentation);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/documentation/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteDocumentation(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $ids = $this->getJsonParams($request);

            $model = new DocumentationModel($this->popuConn());
            $model->validaIdsToDelete($ids);

            $this->deleteSeveralIdsTransaction($this->popuConn(), $model, $ids, __FUNCTION__);

            $this->setResponseOk(__FUNCTION__, $this->deleteErrors);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }



    /***********************
     * DOCUMENTATION TYPES *
     ***********************/

    /** @Route("/documentation-types", methods={"GET", "OPTIONS"}) */
    public function getAllDocumentationTypes(): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new DocumentationModel($this->popuConn());
            $this->setResponseOk(__FUNCTION__, $model->getAllDocumentationTypes());

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/documentation-types/{id}", methods={"GET", "OPTIONS"}) */
    public function getDocumentationTypeById($id): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new DocumentationModel($this->popuConn());
            $model->validaDocumentationTypeId($id, true);

            $this->setResponseOk(__FUNCTION__, $model->getDocumentationTypeById($id));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/documentation-types/create", methods={"POST", "OPTIONS"}) */
    public function createDocumentationType(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);

            $model = new DocumentationModel($this->popuConn());
            $model->validaDocumentationType($data);
            $data = $model->createDocumentationType($data);

            $this->saveChangeLog(__FUNCTION__, $data['id'], $data);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/documentation-types/edit/{id}", methods={"PUT", "OPTIONS"}) */
    public function editDocumentationType(Request $request, $id): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);
            $data['id'] = $id;

            $model = new DocumentationModel($this->popuConn());
            $model->validaDocumentationTypeId($id, true);
            $model->validaDocumentationType($data);
            $oldData = $model->getDocumentationTypeById($id);
            $model->editDocumentationType($data);
            $data = $model->getDocumentationTypeById($id);

            $this->saveChangeLog(__FUNCTION__, $id, $data, $oldData);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/documentation-types/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteDocumentationType(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $ids = $this->getJsonParams($request);

            $model = new DocumentationModel($this->popuConn());
            $model->validaDocumentationTypeIdsToDelete($ids);

            $this->popuConn()->beginTransaction();
            foreach ($ids as $id) {
                $dataToDelete = $model->getDocumentationTypeById($id);
                try {
                    $model->deleteDocumentationType($id);
                } catch (DBALException $e) {
                    $this->deleteErrors[] = $this->parseErrorIfIsFkRestrictException($e->getMessage(), $id);
                    continue;
                }
                $this->saveChangeLog(__FUNCTION__, $id, $dataToDelete);
            }
            $this->popuConn()->commit();

            $this->setResponseOk(__FUNCTION__, $this->deleteErrors);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }



    /*********************
     ** DOC GOOGLE VISIO **
     *********************/

    /** @Route("/documentation-data-google-visio/create/{documentationId}", methods={"POST", "OPTIONS"}) */
    public function createDocGoogleVisio(Request $request, $documentationId): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);

            $model = new DocumentationModel($this->popuConn());
            $model->validaId($documentationId, true);
            $model->validaDocGoogleVisio($data);

            $this->popuConn()->beginTransaction();
            $model->deleteDocGoogleVisioByDocumentationId($documentationId);
            $model->createDocGoogleVisio($documentationId, $data);
            $data = $model->getDocGoogleVisioByDocumentationId($documentationId);
            $this->saveChangeLog(__FUNCTION__, $documentationId, $data);
            $this->popuConn()->commit();

            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/documentation-data-google-visio/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteDocGoogleVisio(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $documentationIds = $this->getJsonParams($request);

            $model = new DocumentationModel($this->popuConn());
            $model->validaDocGoogleVisioIdsToDelete($documentationIds);

            $this->popuConn()->beginTransaction();
            foreach ($documentationIds as $id) {
                $dataToDelete = $model->getDocGoogleVisioByDocumentationId($id);
                try {
                    $model->deleteDocGoogleVisioByDocumentationId($id);
                } catch (DBALException $e) {
                    $this->deleteErrors[] = $this->parseErrorIfIsFkRestrictException($e->getMessage(), $id);
                    continue;
                }
                $this->saveChangeLog(__FUNCTION__, $id, $dataToDelete);
            }
            $this->popuConn()->commit();

            $this->setResponseOk(__FUNCTION__, $this->deleteErrors);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }



    /***********************
     * DOCU PDF TO CONVERT *
     ***********************/

    /** @Route("/documentation-pdf-to-convert/{claimId}", methods={"GET", "OPTIONS"}) */
    public function getDocuPdfToConvertByClaimId($claimId): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new DocumentationModel($this->popuConn());
            $model->validaClaimId($claimId, true);

            $this->setResponseOk(__FUNCTION__, $model->getDocuPdfToConvertByClaimId($claimId));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/documentation-pdf-to-convert/create", methods={"POST", "OPTIONS"}) */
    public function createDocuPdfToConvert(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);

            $model = new DocumentationModel($this->popuConn());
            $model->validaDocuPdfToConvertCreation($data);
            $model->createDocuPdfToConvert($data);

            $this->saveChangeLog(__FUNCTION__, [], $data);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/documentation-pdf-to-convert/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteDocuPdfToConvert(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $dataToDelete = $this->getJsonParams($request);

            $model = new DocumentationModel($this->popuConn());
            $model->validaDocuPdfToConvertDeletion($dataToDelete);
            $model->deleteDocuPdfToConvert($dataToDelete);

            $this->saveChangeLog(__FUNCTION__, [], $dataToDelete);
            $this->setResponseOk(__FUNCTION__);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }
}
