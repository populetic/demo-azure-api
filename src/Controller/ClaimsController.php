<?php

namespace App\Controller;

use App\Entity\Claim;
use App\Model\ClaimsModel;
use App\Service\Exceptions\ValidationException;
use Doctrine\DBAL\DBALException;
use ErrorException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use TypeError;

class ClaimsController extends BaseController
{
    /********************
     ****** CLAIMS ******
     ********************/

    /** @Route("/claims", methods={"GET", "OPTIONS"}) */
    public function getAllClaims(Request $request): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $filters['userId']         = $request->query->get('userId', null);
            $filters['finished']       = $request->query->get('finished', null);
            $filters['closed']         = $request->query->get('closed', null);
            $filters['languageId']     = $request->query->get('languageId', null);
            $filters['lawFirmId']      = $request->query->get('lawFirmId', null);
            $filters['claimStatusIds'] = $request->query->get('claimStatusIds', null);

            $model = new ClaimsModel($this->popuConn());
            $model->validaFilters($filters);

            $this->setResponseOk(__FUNCTION__, $model->getAll($filters));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/claims-by-email-and-status", methods={"GET", "OPTIONS"}) */
    public function getAllByEmailAndStatus(Request $request): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $email         = $request->query->get('email', null);
            $claimStatusId = $request->query->get('claimStatusId', null);

            $model = new ClaimsModel($this->popuConn());
            $model->validaEmailAndStatusId($email, $claimStatusId);

            $this->setResponseOk(__FUNCTION__, $model->getAllByEmailAndStatus($email, $claimStatusId));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/claims/{id}", methods={"GET", "OPTIONS"}) */
    public function getClaimById($id): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new ClaimsModel($this->popuConn());
            $model->validaClaimId($id, true);

            $this->setResponseOk(__FUNCTION__, $model->getByIdExtraData($id));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/claim-by-ref/{ref}", methods={"GET", "OPTIONS"}) */
    public function getClaimByRef($ref): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new ClaimsModel($this->popuConn());
            $model->validaRef($ref, true, true);

            $this->setResponseOk(__FUNCTION__, $model->getByRef($ref));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/claims-get-all-companions/{id}", methods={"GET", "OPTIONS"}) */
    public function getAllCompanionsByMainClaimId($id): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new ClaimsModel($this->popuConn());
            $model->validaClaimId($id, true);

            $this->setResponseOk(__FUNCTION__, $model->getAllCompanionsByMainClaimId($id));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/claims-get-all-possible-same-flight-travelers/{id}", methods={"GET", "OPTIONS"}) */
    public function getAllPossibleSameFlightTravelersByClaimId($id): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new ClaimsModel($this->popuConn());
            $model->validaClaimId($id, true);

            $this->setResponseOk(__FUNCTION__, $model->getAllPossibleSameFlightTravelersByClaimId($id));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/claims-get-all-demands/{id}", methods={"GET", "OPTIONS"}) */
    public function getAllDemandsByClaimId($id): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new ClaimsModel($this->popuConn());
            $model->validaClaimId($id, true);

            $this->setResponseOk(__FUNCTION__, $model->getAllDemandsByClaimId($id));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/claims-get-all-bills/{id}", methods={"GET", "OPTIONS"}) */
    public function getAllBillsByClaimId($id): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new ClaimsModel($this->popuConn());
            $model->validaClaimId($id, true);

            $this->setResponseOk(__FUNCTION__, $model->getAllBillsByClaimId($id));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/claims/create", methods={"POST", "OPTIONS"}) */
    public function createClaim(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $claim = new Claim($this->getJsonParams($request));

            $model = new ClaimsModel($this->popuConn());
            $model->validaClaim($claim);

            $this->popuConn()->beginTransaction();
            $claim = $model->create($claim);
            $claim = $model->getAndUpdateLawsuiteTypeAndInsertAdditionalInformation($claim);
            $model->setClaimCompetencesAndPlacementsByClaimId($claim->id);
            $this->popuConn()->commit();

            $this->saveChangeLog(__FUNCTION__, $claim->id, $claim);
            $this->setResponseOk(__FUNCTION__, $claim);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/claims/edit/{id}", methods={"PUT", "OPTIONS"}) */
    public function editClaim(Request $request, $id): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $claim = new Claim($this->getJsonParams($request));
            $claim->id = $id;

            $model = new ClaimsModel($this->popuConn());
            $model->validaClaimId($id, true);
            $model->validaClaim($claim);

            $this->popuConn()->beginTransaction();
            $oldClaim = $model->getById($id);
            $claim    = $model->edit($claim);
            $model->setClaimCompetencesAndPlacementsByClaimId($claim->id);
            $this->popuConn()->commit();

            $this->saveChangeLog(__FUNCTION__, $id, $claim, $oldClaim);
            $this->setResponseOk(__FUNCTION__, $claim);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/claims/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteClaims(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $ids = $this->getJsonParams($request);

            $model = new ClaimsModel($this->popuConn());
            $model->validaClaimsIds($ids, true);
            $this->deleteSeveralIdsTransaction($this->popuConn(), $model, $ids, __FUNCTION__);

            $this->setResponseOk(__FUNCTION__, $this->deleteErrors);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/claims/calculateEstimatedCompensation", methods={"POST", "OPTIONS"}) */
    public function calculateEstimatedCompensation(Request $request): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $data = $this->getJsonParams($request);

            $model = new ClaimsModel($this->popuConn());
            $model->validaCalculateEstimatedCompensationData($data);
            $amountEstimated = $model->calculateEstimatedCompensation(
                $data['disruptionTypeId'],
                $data['distance'],
                array_key_exists('delay', $data) ? $data['delay'] : 0,
                array_key_exists('luggageDelay', $data) ? $data['luggageDelay'] : 0
            );

            $this->setResponseOk(__FUNCTION__, [ 'amountEstimated' => $amountEstimated ]);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/claims-count-by-claim-status/{claimStatusId}", methods={"GET", "OPTIONS"}) */
    public function countClaimsByClaimStatusId($claimStatusId): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new ClaimsModel($this->popuConn());
            $model->validaClaimStatusId($claimStatusId, true);

            $this->setResponseOk(__FUNCTION__, $model->countClaimsByClaimStatusId($claimStatusId));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/claims-get-oldest-date-status-collected", methods={"POST", "OPTIONS"}) */
    public function getOldestDateWithStatusPopuleticCollected(Request $request): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $data    = $this->getJsonParams($request);
            $maxDate = $this->getDataIfKeyExistsOrNull($data, 'maxDate');

            $model = new ClaimsModel($this->popuConn());
            $model->validaTimestamp($maxDate, true);

            $this->setResponseOk(__FUNCTION__, $model->getOldestDateWithStatusPopuleticCollected($maxDate));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/claims-change-status", methods={"PUT", "OPTIONS"}) */
    public function changeClaimStatusAndLog(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);

            $model = new ClaimsModel($this->popuConn());
            $model->validaChangeClaimStatus($data);

            $this->popuConn()->beginTransaction();
            $model->changeClaimStatus($data['claimId'], $data['claimStatusId'], $this->userLogged['id']);
            $this->saveClaimStatusLog(__FUNCTION__, $data['claimId']);
            $this->popuConn()->commit();

            $this->setResponseOk(__FUNCTION__);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

     /** @Route("/claims-change-law-firm", methods={"PUT", "OPTIONS"}) */
     public function claimsChangeLawFirmAndLog(Request $request): JsonResponse
     {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);

            $model = new ClaimsModel($this->popuConn());
            $model->validaChangeClaimLawFirm($data);

            $this->popuConn()->beginTransaction();
            foreach ($data['claimIds'] as $id) {
                $model->changeClaimLawFirm($id, $data['lawFirmId']);
                $this->saveClaimStatusLog(__FUNCTION__, $id);
            }
            $this->popuConn()->commit();

            $this->setResponseOk(__FUNCTION__);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
     }

    /** @Route("/claims-create-companion", methods={"POST", "OPTIONS"}) */
    public function createCompanionClaim(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);

            $model = new ClaimsModel($this->popuConn());
            $model->validaCreateCompanionClaim($data);

            $this->popuConn()->beginTransaction();
            $claim = $model->createCompanionClaim($data);
            $model->setClaimCompetencesAndPlacementsByClaimId($claim->id);
            $this->popuConn()->commit();

            $this->saveChangeLog(__FUNCTION__, $claim->id, $claim);
            $this->setResponseOk(__FUNCTION__, $claim);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/claims-set-companion", methods={"PUT", "OPTIONS"}) */
    public function setCompanionsForClaim(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);

            $model = new ClaimsModel($this->popuConn());
            $model->validaSetCompanionsForClaim($data);

            $this->popuConn()->beginTransaction();
            foreach ($data['companionClaimsIds'] as $companionClaimId) {
                $model->setCompanionForClaim($data['mainClaimId'], $companionClaimId);
                $this->saveChangeLog(__FUNCTION__, [],
                    'mainClaimId set to "' . $data['mainClaimId'] . '" for claim with id "' . $companionClaimId . '"'
                );
            }
            $this->popuConn()->commit();

            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }



    /**********************
     **** CLAIM STATUS ****
     **********************/

    /** @Route("/claim-status", methods={"GET", "OPTIONS"}) */
    public function getAllClaimStatus(): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new ClaimsModel($this->popuConn());
            $this->setResponseOk(__FUNCTION__, $model->getAllClaimStatus());

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/claim-status-by-role-and-country", methods={"GET", "OPTIONS"}) */
    public function getAllClaimStatusByRoleAndCountry(Request $request): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $roleId    = $request->query->get('roleId', null);
            $countryId = $request->query->get('countryId', null);

            $model = new ClaimsModel($this->popuConn());
            $model->validaRoleIdAndCountryId($roleId, $countryId);

            $this->setResponseOk(__FUNCTION__, $model->getAllClaimStatusByRoleAndCountry($roleId, $countryId));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/claim-status/{id}", methods={"GET", "OPTIONS"}) */
    public function getClaimStatusById($id): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new ClaimsModel($this->popuConn());
            $model->validaClaimStatusId($id, true);

            $this->setResponseOk(__FUNCTION__, $model->getClaimStatusById($id));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/claim-status/create", methods={"POST", "OPTIONS"}) */
    public function createClaimStatus(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);

            $model = new ClaimsModel($this->popuConn());
            $model->validaClaimStatus($data);
            $data = $model->createClaimStatus($data);

            $this->saveChangeLog(__FUNCTION__, $data['id'], $data);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/claim-status/edit/{id}", methods={"PUT", "OPTIONS"}) */
    public function editClaimStatus(Request $request, $id): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);
            $data['id'] = $id;

            $model = new ClaimsModel($this->popuConn());
            $model->validaClaimStatusId($id, true);
            $model->validaClaimStatus($data);
            $oldData = $model->getClaimStatusById($id);
            $model->editClaimStatus($data);
            $data = $model->getClaimStatusById($id);

            $this->saveChangeLog(__FUNCTION__, $id, $data, $oldData);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/claim-status/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteClaimStatus(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $ids = $this->getJsonParams($request);

            $model = new ClaimsModel($this->popuConn());
            $model->validaClaimStatusIdsToDelete($ids);

            $this->popuConn()->beginTransaction();
            foreach ($ids as $id) {
                $dataToDelete = $model->getClaimStatusById($id);
                try {
                    $model->deleteClaimStatus($id);
                } catch (DBALException $e) {
                    $this->deleteErrors[] = $this->parseErrorIfIsFkRestrictException($e->getMessage(), $id);
                    continue;
                }
                $this->saveChangeLog(__FUNCTION__, $id, $dataToDelete);
            }
            $this->popuConn()->commit();

            $this->setResponseOk(__FUNCTION__, $this->deleteErrors);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }



    /**************************
     * CLAIM STATUS COUNTRIES *
     **************************/

    /** @Route("/claim-status-countries", methods={"GET", "OPTIONS"}) */
    public function getAllClaimStatusCountries(): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new ClaimsModel($this->popuConn());
            $this->setResponseOk(__FUNCTION__, $model->getAllClaimStatusCountries());

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/claim-status-countries/{id}", methods={"GET", "OPTIONS"}) */
    public function getClaimStatusCountryById($id): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new ClaimsModel($this->popuConn());
            $model->validaClaimStatusCountryId($id, true);

            $this->setResponseOk(__FUNCTION__, $model->getClaimStatusCountryById($id));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/claim-status-countries/create", methods={"POST", "OPTIONS"}) */
    public function createClaimStatusCountry(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);

            $model = new ClaimsModel($this->popuConn());
            $model->validaClaimStatusCountry($data);
            $data = $model->createClaimStatusCountry($data);

            $this->saveChangeLog(__FUNCTION__, $data['id'], $data);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/claim-status-countries/edit/{id}", methods={"PUT", "OPTIONS"}) */
    public function editClaimStatusCountry(Request $request, $id): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);
            $data['id'] = $id;

            $model = new ClaimsModel($this->popuConn());
            $model->validaClaimStatusCountryId($id, true);
            $model->validaClaimStatusCountry($data);
            $oldData = $model->getClaimStatusCountryById($id);
            $model->editClaimStatusCountry($data);
            $data = $model->getClaimStatusCountryById($id);

            $this->saveChangeLog(__FUNCTION__, $id, $data, $oldData);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/claim-status-countries/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteClaimStatusCountry(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $ids = $this->getJsonParams($request);

            $model = new ClaimsModel($this->popuConn());
            $model->validaClaimStatusCountryIdsToDelete($ids);

            $this->popuConn()->beginTransaction();
            foreach ($ids as $id) {
                $dataToDelete = $model->getClaimStatusCountryById($id);
                try {
                    $model->deleteClaimStatusCountry($id);
                } catch (DBALException $e) {
                    $this->deleteErrors[] = $this->parseErrorIfIsFkRestrictException($e->getMessage(), $id);
                    continue;
                }
                $this->saveChangeLog(__FUNCTION__, $id, $dataToDelete);
            }
            $this->popuConn()->commit();

            $this->setResponseOk(__FUNCTION__, $this->deleteErrors);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }



    /**************************
     ** CLAIM STATUS BY ROLE **
     **************************/

    /** @Route("/claim-status-by-role", methods={"GET", "OPTIONS"}) */
    public function getAllClaimStatusByRole(): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new ClaimsModel($this->popuConn());
            $this->setResponseOk(__FUNCTION__, $model->getAllClaimStatusByRole());

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/claim-status-by-role/{claimStatusCountryId}/{roleId}", methods={"GET", "OPTIONS"}) */
    public function getClaimStatusByRole($claimStatusCountryId, $roleId): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new ClaimsModel($this->popuConn());
            $model->validaClaimStatusByRoleExists($claimStatusCountryId, $roleId, true);

            $this->setResponseOk(__FUNCTION__, $model->getClaimStatusByRole($claimStatusCountryId, $roleId));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/claim-status-by-role/create", methods={"POST", "OPTIONS"}) */
    public function createClaimStatusByRole(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);

            $model = new ClaimsModel($this->popuConn());
            $model->validaClaimStatusByRole($data, true);
            $model->createClaimStatusByRole($data);

            $this->saveChangeLog(__FUNCTION__, [], $data);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/claim-status-by-role/edit", methods={"PUT", "OPTIONS"}) */
    public function editClaimStatusByRole(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);

            $model = new ClaimsModel($this->popuConn());
            $model->validaClaimStatusByRole($data, false);
            $oldData = $model->getClaimStatusByRole($data['claimStatusCountryId'], $data['roleId']);
            $model->editClaimStatusByRole($data);

            $this->saveChangeLog(__FUNCTION__, [$data['claimStatusCountryId'], $data['roleId']], $data, $oldData);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/claim-status-by-role/delete/{claimStatusCountryId}/{roleId}", methods={"DELETE", "OPTIONS"}) */
    public function deleteClaimStatusByRole(Request $request, $claimStatusCountryId, $roleId): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $model = new ClaimsModel($this->popuConn());
            $model->validaClaimStatusByRoleExists($claimStatusCountryId, $roleId, true);
            $dataToDelete = $model->getClaimStatusByRole($claimStatusCountryId, $roleId);
            $model->deleteClaimStatusByRole($claimStatusCountryId, $roleId);

            $this->saveChangeLog(__FUNCTION__, [], $dataToDelete);
            $this->setResponseOk(__FUNCTION__);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }



    /*************************
     *** CLAIMS COMPETENCE ***
     *************************/

    /** @Route("/claims-competence/{claimId}", methods={"GET", "OPTIONS"}) */
    public function getAllClaimsCompetence($claimId): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new ClaimsModel($this->popuConn());
            $model->validaClaimId($claimId, true);
            $this->setResponseOk(__FUNCTION__, $model->getAllClaimsCompetence($claimId));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/claims-competence/{claimId}/{countryId}", methods={"GET", "OPTIONS"}) */
    public function getClaimCompetence($claimId, $countryId): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new ClaimsModel($this->popuConn());
            $model->validaClaimCompetenceExists($claimId, $countryId, true);

            $this->setResponseOk(__FUNCTION__, $model->getClaimCompetence($claimId, $countryId));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/claims-competence/create", methods={"POST", "OPTIONS"}) */
    public function createClaimCompetence(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);

            $model = new ClaimsModel($this->popuConn());
            $model->validaClaimCompetence($data, true);
            $model->createClaimCompetence($data);
            $data = $model->getClaimCompetence($data['claimId'], $data['countryId']);

            $this->saveChangeLog(__FUNCTION__, [], $data);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/claims-competence/edit", methods={"PUT", "OPTIONS"}) */
    public function editClaimCompetence(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);

            $model = new ClaimsModel($this->popuConn());
            $model->validaClaimCompetence($data, false);
            $oldData = $model->getClaimCompetence($data['claimId'], $data['countryId']);
            $model->editClaimCompetence($data);
            $data = $model->getClaimCompetence($data['claimId'], $data['countryId']);

            $this->saveChangeLog(__FUNCTION__, [$data['claimId'], $data['countryId']], $data, $oldData);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/claims-competence/delete/{claimId}/{countryId}", methods={"DELETE", "OPTIONS"}) */
    public function deleteClaimCompetence(Request $request, $claimId, $countryId): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $model = new ClaimsModel($this->popuConn());
            $model->validaClaimCompetenceExists($claimId, $countryId, true);
            $dataToDelete = $model->getClaimCompetence($claimId, $countryId);
            $model->deleteClaimCompetence($claimId, $countryId);

            $this->saveChangeLog(__FUNCTION__, [], $dataToDelete);
            $this->setResponseOk(__FUNCTION__);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }



    /************************
     *** CLAIMS PLACEMENT ***
     ************************/

    /** @Route("/claims-placement/{claimId}", methods={"GET", "OPTIONS"}) */
    public function getAllClaimsPlacement($claimId): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new ClaimsModel($this->popuConn());
            $model->validaClaimId($claimId, true);
            $this->setResponseOk(__FUNCTION__, $model->getAllClaimsPlacement($claimId));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/claims-placement/{claimId}/{countryId}", methods={"GET", "OPTIONS"}) */
    public function getClaimPlacement($claimId, $countryId): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new ClaimsModel($this->popuConn());
            $model->validaClaimPlacementExists($claimId, $countryId, true);

            $this->setResponseOk(__FUNCTION__, $model->getClaimPlacement($claimId, $countryId));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/claims-placement/create", methods={"POST", "OPTIONS"}) */
    public function createClaimPlacement(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);

            $model = new ClaimsModel($this->popuConn());
            $model->validaClaimPlacement($data, true);
            $model->createClaimPlacement($data);

            $this->saveChangeLog(__FUNCTION__, [], $data);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/claims-placement/edit", methods={"PUT", "OPTIONS"}) */
    public function editClaimPlacement(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);

            $model = new ClaimsModel($this->popuConn());
            $model->validaClaimPlacement($data, false);
            $oldData = $model->getClaimPlacement($data['claimId'], $data['countryId']);
            $model->editClaimPlacement($data);

            $this->saveChangeLog(__FUNCTION__, [$data['claimId'], $data['countryId']], $data, $oldData);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/claims-placement/delete/{claimId}/{countryId}", methods={"DELETE", "OPTIONS"}) */
    public function deleteClaimPlacement(Request $request, $claimId, $countryId): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $model = new ClaimsModel($this->popuConn());
            $model->validaClaimPlacementExists($claimId, $countryId, true);
            $dataToDelete = $model->getClaimPlacement($claimId, $countryId);
            $model->deleteClaimPlacement($claimId, $countryId);

            $this->saveChangeLog(__FUNCTION__, [], $dataToDelete);
            $this->setResponseOk(__FUNCTION__);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }



    /**********************
     ** CLAIM DOC STATUS **
     **********************/

    /** @Route("/claim-documentation-status", methods={"GET", "OPTIONS"}) */
    public function getAllClaimDocStatus(): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new ClaimsModel($this->popuConn());
            $this->setResponseOk(__FUNCTION__, $model->getAllClaimDocStatus());

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/claim-documentation-status/{id}", methods={"GET", "OPTIONS"}) */
    public function getClaimDocStatusById($id): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new ClaimsModel($this->popuConn());
            $model->validaClaimDocStatusId($id, true);

            $this->setResponseOk(__FUNCTION__, $model->getClaimDocStatusById($id));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/claim-documentation-status/create", methods={"POST", "OPTIONS"}) */
    public function createClaimDocStatus(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);

            $model = new ClaimsModel($this->popuConn());
            $model->validaClaimDocStatus($data);
            $data = $model->createClaimDocStatus($data);

            $this->saveChangeLog(__FUNCTION__, $data['id'], $data);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/claim-documentation-status/edit/{id}", methods={"PUT", "OPTIONS"}) */
    public function editClaimDocStatus(Request $request, $id): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);
            $data['id'] = $id;

            $model = new ClaimsModel($this->popuConn());
            $model->validaClaimDocStatusId($id, true);
            $model->validaClaimDocStatus($data);
            $oldData = $model->getClaimDocStatusById($id);
            $model->editClaimDocStatus($data);
            $data = $model->getClaimDocStatusById($id);

            $this->saveChangeLog(__FUNCTION__, $id, $data, $oldData);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/claim-documentation-status/delete", methods={"DELETE", "OPTIONS"}) */
    public function deleteClaimDocStatus(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $ids = $this->getJsonParams($request);

            $model = new ClaimsModel($this->popuConn());
            $model->validaClaimDocStatusIdsToDelete($ids);

            $this->popuConn()->beginTransaction();
            foreach ($ids as $id) {
                $dataToDelete = $model->getClaimDocStatusById($id);
                try {
                    $model->deleteClaimDocStatus($id);
                } catch (DBALException $e) {
                    $this->deleteErrors[] = $this->parseErrorIfIsFkRestrictException($e->getMessage(), $id);
                    continue;
                }
                $this->saveChangeLog(__FUNCTION__, $id, $dataToDelete);
            }
            $this->popuConn()->commit();

            $this->setResponseOk(__FUNCTION__, $this->deleteErrors);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            $this->popuConn()->rollBack();
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }



    /****************************
     * CLAIM DOC STATUS BY ROLE *
     ****************************/

    /** @Route("/claim-documentation-status-by-role/{roleId}", methods={"GET", "OPTIONS"}) */
    public function getClaimDocStatusByRole($roleId): JsonResponse
    {
        try {
            $this->checkHostIsAllowed();

            $model = new ClaimsModel($this->popuConn());
            $model->validaRoleId($roleId, true);

            $this->setResponseOk(__FUNCTION__, $model->getAllClaimDocStatusByRole($roleId));

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/claim-documentation-status-by-role/create", methods={"POST", "OPTIONS"}) */
    public function createClaimDocStatusByRole(Request $request): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $data = $this->getJsonParams($request);

            $model = new ClaimsModel($this->popuConn());
            $model->validaNewClaimDocStatusByRole($data);
            $model->createClaimDocStatusByRole($data);

            $this->saveChangeLog(__FUNCTION__, [], $data);
            $this->setResponseOk(__FUNCTION__, $data);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }

    /** @Route("/claim-documentation-status-by-role/delete/{claimDocStatusId}/{roleId}", methods={"DELETE", "OPTIONS"}) */
    public function deleteClaimDocStatusByRole(Request $request, $claimDocStatusId, $roleId): JsonResponse
    {
        try {
            $this->checkTokenAndSetUserLogged($request);

            $model = new ClaimsModel($this->popuConn());
            $model->validaClaimDocStatusByRoleExists($claimDocStatusId, $roleId, true);
            $model->deleteClaimDocStatusByRole($claimDocStatusId, $roleId);

            $this->saveChangeLog(__FUNCTION__, [], ['claimDocumentationStatusId' => $claimDocStatusId, 'roleId' => $roleId]);
            $this->setResponseOk(__FUNCTION__);

        } catch (DBALException | ErrorException | TypeError $e) {
            $this->setResponseError(__FUNCTION__, $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
        } catch (ValidationException $e) {
            $this->setResponseError(__FUNCTION__, $e->getErrors());
        } finally {
            return new JsonResponse($this->response);
        }
    }
}
