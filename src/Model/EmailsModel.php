<?php

namespace App\Model;

use App\Entity\EmailCustom;
use App\Entity\EmailMandrill;
use App\Model\TranslationSystem\LanguagesModel;
use App\Repository\ClaimsRepository;
use App\Repository\EmailsRepository;
use Doctrine\DBAL\Connection;

class EmailsModel extends BaseModel
{
    const MANDRILL = 'mandrill';
    const CUSTOM   = 'custom';

    const VAR_SEP   = '[var]';
    const EMOJI_SEP = '[emo]';

    private $popuDB;
    private $allowedTypes = [ self::MANDRILL, self::CUSTOM ];

    private $mandrillEmailsToSendParams = ['claimId', 'emailTo', 'nameTo', 'templateVars', 'templateName'];
    private $customEmailsToSendParams   = ['claimId', 'emailTo', 'nameTo', 'subject', 'html', 'languageId' ];

    public function __construct(Connection $populeticDB)
    {
        parent::__construct();
        $this->repository = new EmailsRepository($populeticDB);
        $this->popuDB = $populeticDB;
    }

    /*********************
     ****** EMAILS *******
     *********************/

    public function getAllByType($type): array
    {
        return $this->repository->getAllByType($type);
    }

    public function getAllByClaimId($claimId): array
    {
        $emails[self::MANDRILL] = $this->repository->getAllByTypeAndClaimId(self::MANDRILL, $claimId);
        $emails[self::CUSTOM]   = $this->repository->getAllByTypeAndClaimId(self::CUSTOM, $claimId);

        return $emails;
    }

    public function getMandrillById($id): EmailMandrill
    {
        $email = new EmailMandrill($this->repository->getByIdAndType($id, self::MANDRILL));
        $this->checkIfIdExistsOrThrowException($email->id);

        return $email;
    }

    public function getCustomById($id): EmailCustom
    {
        $email = new EmailCustom($this->repository->getByIdAndType($id, self::CUSTOM));
        $this->checkIfIdExistsOrThrowException($email->id);

        if ($email->body) {
            $email->body = base64_decode($email->body);
        }

        return $email;
    }

    private function checkIfIdExistsOrThrowException($id)
    {
        if (!$id) $this->jValidator->setError('emailId', $this->jValidator::E_DOESNT_EXISTS);
        $this->jValidator->checkErrors();
    }

    public function getEmailMandrillByMandrillId($mandrillId): EmailMandrill
    {
        return new EmailMandrill($this->repository->getEmailMandrillByMandrillId($mandrillId));
    }

    public function insertMandrill(EmailMandrill $email): EmailMandrill
    {
        $id = $this->repository->insertMandrill($email);
        return $this->getMandrillById($id);
    }

    public function insertCustom(EmailCustom $email): EmailCustom
    {
        $email->body = base64_encode($email->body);

        $id = $this->repository->insertCustom($email);
        return $this->getCustomById($id);
    }

    public function updateMandrillTrackingInfoByMandrillId($data)
    {
        $email             = new EmailMandrill();
        $email->status     = $data['state'];
        $email->opens      = $data['opens'];
        $email->clicks     = $data['clicks'];
        $email->mandrillId = $data['_id'];

        $this->repository->updateMandrillInfo($email);
    }

    public function validaMandrillEmailsToSend($emails)
    {
        foreach ($emails as $email) {
            $this->validaParams($email, $this->mandrillEmailsToSendParams);
            $this->validaClaimId($email['claimId']);
            if (!is_array($email['templateVars'])) {
                $this->jValidator->setError('templateVars', $this->jValidator::E_NOARRAY);
            }
            $this->validaMandrillTemplateNameAndCheckThatExists($email['templateName']);
            $this->validaCommonDataEmailsToSend($email);
        }

        $this->jValidator->checkErrors();
    }

    public function validaCustomEmailsToSend($emails)
    {
        foreach ($emails as $email) {
            $this->validaParams($email, $this->customEmailsToSendParams);
            $this->validaClaimId($email['claimId'], false, false);
            $this->jValidator->validaString($email['subject'], true, 'subject', 1, 250);
            $this->jValidator->validaString($email['html'], true, 'html', 1);
            $this->validaIdAndExistsInTable($email['languageId'], 'languageId', 'translation_system.languages', true);
            $this->validaCommonDataEmailsToSend($email);
        }

        $this->jValidator->checkErrors();
    }

    private function validaCommonDataEmailsToSend($email)
    {
        $this->jValidator->validaEmail($email['emailTo'], true, 'emailTo');
        $this->jValidator->validaString($email['nameTo'], true, 'nameTo', 1, 50);

        if (array_key_exists('attachments', $email)) {
            foreach ($email['attachments'] as $attachment) {
                $this->validaParams($attachment, ['path', 'filename', 'extension']);
                $this->jValidator->validaString($attachment['path'], true, 'path', 1);
                $this->jValidator->validaString($attachment['filename'], true, 'filename', 1);
                $this->jValidator->validaString($attachment['extension'], true, 'extension', 1, 3);
            }
        }
        if (array_key_exists('sendAt', $email)) {
            $this->jValidator->validaDate($email['sendAt'], true, 'sendAt');
        }
    }

    public function validaMandrill(EmailMandrill $email)
    {
        $this->validaClaimId($email->claimId);
        $this->jValidator->validaString($email->status, true, 'status', 1, 50);
        $this->validaEmailMandrillTemplateId($email->emailMandrillTemplateId);
        $this->jValidator->validaInt($email->opens, true, 'opens', 0);
        $this->jValidator->validaInt($email->clicks, true, 'clicks', 0);
        $this->jValidator->validaString($email->mandrillId, true, 'mandrillId', 1, 250);
        $this->validaDocsIds($email->docsIds);
        $this->jValidator->checkErrors();
    }

    public function validaCustom(EmailCustom $email)
    {
        $this->validaClaimId($email->claimId, false, false);
        $this->jValidator->validaEmail($email->email, true, 'email');
        $this->jValidator->validaString($email->status, true, 'status', 1, 50);
        $this->jValidator->validaString($email->subject, true, 'subject', 1, 250);
        $this->jValidator->validaString($email->body, true, 'body');
        $this->validaDocsIds($email->docsIds);
        $this->jValidator->checkErrors();
    }

    private function validaDocsIds($ids)
    {
        if (is_array($ids)) {
            foreach ($ids as $id) {
                $this->validaIdAndExistsInTable($id, 'documentationId', 'documentation', true);
            }
        } else {
            $this->jValidator->setError('docsIds', $this->jValidator::E_NOARRAY, $ids);
        }
    }

    public function validaId($id, $checkErrors = true)
    {
        $this->jValidator->validaInt($id, true, 'emailId', 1);
        if ($checkErrors) $this->jValidator->checkErrors();
    }

    public function validaMandrillId($mandrillId)
    {
        $this->jValidator->validaString($mandrillId, true, 'mandrillId', 1, 250);
        $this->jValidator->checkErrors();
    }

    public function validaType($type, $checkErrors = false)
    {
        if (!in_array($type, $this->allowedTypes)) {
            $this->jValidator->setError('emailType', $this->jValidator::E_DOESNT_EXISTS, $type);
        }

        if ($checkErrors) $this->jValidator->checkErrors();
    }

    public function parseDataToSendMandrillEmail($email, $subject): array
    {
        $email['subject']      = $this->translateSubject($email, $subject);
        $email['languageCode'] = $this->getLanguageCodeFromTemplateName($email['templateName']);
        $email['attachments']  = $this->parseAttachmentsForMandrill($email);

        return $email;
    }

    public function parseDataToSendCustomEmail($translationsConn, $email): array
    {
        $languageModel = new LanguagesModel($translationsConn);
        $language      = $languageModel->getById($email['languageId']);

        $email['languageCode'] = $language->code;
        $email['attachments']  = $this->parseAttachmentsForMandrill($email);

        return $email;
    }

    /**
     * Función para poner emojis y variables en el subject, ya que éste está guardado
     * en db con keys a cambiar por datos
     *
     * @param $email    -> un array con keys claimId y templateVars. Este último serán variables a insertar en
     *                    el template de mailchimp y en el subject de DB, p.e.:
     *                            template -> ...la aerolinea {{NOMBRE_AEROLINEA}}
     *                            subject  -> ...la aerolinea [var]NOMBRE_AEROLINEA[var]
     * @param $subject  -> Subject del email con variables o emojis a traducir
     * @return string
     */
    private function translateSubject($email, $subject): string
    {
        if (!array_key_exists('REF', $email['templateVars'])) {
            $email['templateVars']['REF'] = (new ClaimsRepository($this->popuDB))->getRefByClaimId($email['claimId']);
        }

        $subject = $this->translateSubjectVariables($subject, $email['templateVars']);
        $subject = $this->translateSubjectEmojis($subject);

        return $subject;
    }

    private function translateSubjectVariables($subject, $emailInfo): string
    {
        $wordsToTranslate = $this->getAllWordsBetweenSeparator(self::VAR_SEP, $subject);
        foreach ($wordsToTranslate as $word) {
            if (array_key_exists($word, $emailInfo)) {
                $subject = str_replace(self::VAR_SEP . $word . self::VAR_SEP, $emailInfo[$word], $subject);
            }
        }

        return $subject;
    }

    private function translateSubjectEmojis($subject): string
    {
        $emojisToTranslate = $this->getAllWordsBetweenSeparator(self::EMOJI_SEP, $subject);
        if ($emojisToTranslate) {
            $emojis = json_decode(file_get_contents(self::JSON_FOLDER . 'jsonEmojisUTF16.json'), true);
            foreach ($emojisToTranslate as $word) {
                if (array_key_exists($word, $emojis)) {
                    $subject = str_replace(self::EMOJI_SEP . $word . self::EMOJI_SEP, $emojis[$word], $subject);
                }
            }
        }

        return $subject;
    }

    private function getLanguageCodeFromTemplateName($templateName): string
    {
        return strtolower(str_replace(substr($templateName, 0, -2), '', $templateName));
    }

    public function setEmailMandrillEntityWithData($email, $mandrillTemplate, $mandrillResponse): EmailMandrill
    {
        return new EmailMandrill([
            'claimId'                 => $email['claimId'],
            'emailMandrillTemplateId' => $mandrillTemplate['id'],
            'status'                  => $mandrillResponse[0]['status'],
            'mandrillId'              => $mandrillResponse[0]['_id']
        ]);
    }

    public function setEmailCustomEntityWithData($email, $mandrillResponse): EmailCustom
    {
        return new EmailCustom([
            'claimId' => $email['claimId'],
            'email'   => $email['emailTo'],
            'status'  => $mandrillResponse[0]['status'],
            'subject' => $email['subject'],
            'body'    => $email['html']
        ]);
    }

    private function parseAttachmentsForMandrill($email): array
    {
//TODO comprobar que el content se coge bien así cuando este el sistema EFS montado y claro
//TODO el path me trae ya el basePath o hay que añadirlo según environment?

        $attachmentsForMandrill = [];

//        if (array_key_exists('attachments', $email)) {
//            foreach ($email['attachments'] as $attachment) {
//                $attachmentsForMandrill[] = [
//                    'type'    => $this->getMimeTypeByExtension($attachment['extension']),
//                    'name'    => $attachment['filename'],
//                    'content' => file_get_contents(base64_encode(
//                        $attachment['path'] . $attachment['filename'] . '.' . $attachment['extension']
//                    ))
//                ];
//            }
//        }

        return $attachmentsForMandrill;
    }

    public function getLanguageCodeByLangId($languageId): string
    {
        $languageModel = new LanguagesModel($this->popuDB);
        $language      = $languageModel->getById($languageId);

        return $language->code;
    }

    private function getMimeTypeByExtension($extension): string
    {
        switch ($extension) {
            case 'jpeg':
            case 'jpg':
                $mimeType = 'image/jpeg';
                break;
            case 'doc':
                $mimeType = 'application/msword';
                break;
            case 'pdf':
                $mimeType = 'application/pdf';
                break;
            default:
                $mimeType = 'application/octet-stream';
        }

        return $mimeType;
    }




    /****************************
     * EMAIL MANDRILL TEMPLATES *
     ****************************/

    private $neededParamsMandrillTemplates = ['templateName', 'subject'];

    public function getAllMandrillTemplates(): array
    {
        return $this->repository->getAllMandrillTemplates();
    }

    public function getMandrillTemplateById($id): array
    {
        return $this->repository->getMandrillTemplateById($id);
    }

    public function getMandrillTemplateByName($name): array
    {
        return $this->repository->getMandrillTemplateByName($name);
    }

    public function createMandrillTemplate($data): int
    {
        return $this->repository->createMandrillTemplate($data);
    }

    public function editMandrillTemplate($data)
    {
        $this->repository->editMandrillTemplate($data);
    }

    public function deleteMandrillTemplate($id)
    {
        $this->repository->deleteMandrillTemplate($id);
    }

    public function validaMandrillTemplate($data)
    {
        $id = array_key_exists('id', $data) ? $data['id'] : null;

        $this->validaParams($data, $this->neededParamsMandrillTemplates);
        $this->validaMandrillTemplateNameAndDoesntExists($data['templateName'], $id);
        $this->jValidator->validaString($data['subject'], true, 'subject', 1, 250);

        $this->jValidator->checkErrors();
    }

    public function validaMandrillTemplateIdsToDelete($ids)
    {
        foreach ($ids as $id) {
            $this->validaEmailMandrillTemplateId($id);
        }

        $this->jValidator->checkErrors();
    }

    public function validaEmailMandrillTemplateId($id, $checkErrors = false)
    {
        $this->validaIdAndExistsInTable($id, 'emailMandrillTemplateId', 'email_mandrill_templates', true, false, $checkErrors);
    }

    public function validaMandrillTemplateNameAndCheckThatExists($name, $checkErrors = false)
    {
        $this->jValidator->validaString($name, true, 'mandrillTemplateName', 1, 100);
        if ($name && !$this->repository->mandrillTemplateNameExists($name)) {
            $this->jValidator->setError('mandrillTemplateName', $this->jValidator::E_DOESNT_EXISTS, $name);
        }

        if ($checkErrors) $this->jValidator->checkErrors();
    }

    public function validaMandrillTemplateNameAndDoesntExists($name, $id = null)
    {
        $this->jValidator->validaString($name, true, 'mandrillTemplateName', 1, 100);
        if ($name && $this->repository->mandrillTemplateNameExists($name, $id)) {
            $this->jValidator->setError('mandrillTemplateName', $this->jValidator::E_ALREADY_EXISTS, $name);
        }
    }



    /*********************
     **** EMAIL TASKS ****
     *********************/

    private $neededParamsEmailTasks = ['emailMandrillTemplateId', 'dateField', 'hoursToSend'];

    public function getAllEmailTasks(): array
    {
        return $this->repository->getAllEmailTasks();
    }

    public function getEmailTaskById($id): array
    {
        return $this->repository->getEmailTaskById($id);
    }

    public function createEmailTask($data): int
    {
        return $this->repository->createEmailTask($data);
    }

    public function editEmailTask($data)
    {
        $this->repository->editEmailTask($data);
    }

    public function deleteEmailTask($id)
    {
        $this->repository->deleteEmailTask($id);
    }

    public function validaEmailTask($data)
    {
        $this->validaParams($data, $this->neededParamsEmailTasks);
        $this->validaEmailMandrillTemplateId($data['emailMandrillTemplateId']);
        $this->jValidator->validaString($data['dateField'], true, 'dateField', 1, 50);
        $this->jValidator->validaInt($data['hoursToSend'], true, 'hoursToSend');

        if (array_key_exists('isActive', $data)) {
            $this->jValidator->validaBoolean($data['isActive'], true, 'isActive');
        }

        $this->jValidator->checkErrors();
    }

    public function validaEmailTaskIdsToDelete($ids)
    {
        foreach ($ids as $id) {
            $this->validaEmailTaskId($id);
        }

        $this->jValidator->checkErrors();
    }

    public function validaEmailTaskId($id, $checkErrors = false)
    {
        $this->jValidator->validaInt($id, true, 'emailTaskId', 1);
        if ($id && !$this->repository->idExistsInTable('email_tasks', $id)) {
            $this->jValidator->setError('emailTaskId', $this->jValidator::E_DOESNT_EXISTS, $id);
        }

        if ($checkErrors) $this->jValidator->checkErrors();
    }



    /*********************
     * EMAIL ATTACHMENTS *
     *********************/

    private $neededParamsEmailAttachments = ['emailId', 'emailType', 'documentationId'];

    public function addEmailAttachment($data)
    {
        $this->repository->addEmailAttachment($data['emailId'], $data['emailType'], $data['documentationId']);
    }

    public function deleteEmailAttachment($data)
    {
        $this->repository->deleteEmailAttachment($data);
    }

    public function validaEmailAttachmentCreation($data)
    {
        $this->validaParams($data, $this->neededParamsEmailAttachments);
        $this->validaType($data['emailType'], true);

        $this->validaEmailId($data['emailType'], $data['emailId']);
        $this->validaDocumentationId($data['documentationId']);

        $this->jValidator->checkErrors();

        if ($this->repository->emailAttachmentExists($data)) {
            $this->jValidator->setError('emailAttachment', $this->jValidator::E_ALREADY_EXISTS);
        }

        $this->jValidator->checkErrors();
    }

    public function validaEmailAttachmentDeletion($data)
    {
        $this->validaParams($data, $this->neededParamsEmailAttachments);
        $this->validaType($data['emailType'], true);

        $this->validaId($data['emailId'], false);
        $this->jValidator->validaInt($data['documentationId'], true, 'documentationId', 1);

        $this->jValidator->checkErrors();

        if (!$this->repository->emailAttachmentExists($data)) {
            $this->jValidator->setError('emailAttachment', $this->jValidator::E_DOESNT_EXISTS);
        }

        $this->jValidator->checkErrors();
    }

    private function validaEmailId($type, $id)
    {
        $this->validaId($id, false);
        if ($id && !$this->repository->idExistsInTable('emails_' . $type, $id)) {
            $this->jValidator->setError('emailId', $this->jValidator::E_DOESNT_EXISTS, $id);
        }
    }

    private function validaDocumentationId($id)
    {
        $this->jValidator->validaInt($id, true, 'documentationId', 1);
        if ($id && !$this->repository->idExistsInTable('documentation', $id)) {
            $this->jValidator->setError('documentationId', $this->jValidator::E_DOESNT_EXISTS, $id);
        }
    }
}