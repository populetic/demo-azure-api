<?php

namespace App\Model;

use App\Entity\Client;
use App\Entity\LegalGuardian;
use App\Repository\ClientsRepository;
use Doctrine\DBAL\Connection;

class ClientsModel extends BaseModel
{
    const MALE_KEY   = 'M';
    const FEMALE_KEY = 'F';

    public function __construct(Connection $populeticDB)
    {
        parent::__construct();
        $this->repository = new ClientsRepository($populeticDB);
    }

    /*********************
     ****** CLIENTS ******
     *********************/

    public function getAll($filters): array
    {
        return $this->repository->getAll($filters);
    }

    public function getById($id): Client
    {
        return new Client($this->repository->getById($id));
    }

    public function getByClaimId($claimId): Client
    {
        return new Client($this->repository->getByClaimId($claimId));
    }

    public function getRelatedClaimsById($id): array
    {
        return $this->repository->getRelatedClaimsById($id);
    }

    public function getRelatedDemandsById($id): array
    {
        $claimsIds = $this->parseToOnlyClaimsIdsList($this->getRelatedClaimsById($id));
        return $this->repository->getRelatedDemandsByClaimsIds($claimsIds);
    }

    public function getRelatedBillsById($id): array
    {
        $claimsIds = $this->parseToOnlyClaimsIdsList($this->getRelatedClaimsById($id));
        return $this->repository->getRelatedBillsByClaimsIds($claimsIds);
    }

    private function parseToOnlyClaimsIdsList($claims): array
    {
        return array_column($claims, 'claimId');
    }

    public function create(Client $client): Client
    {
        $id = $this->repository->create($client);
        return $this->getById($id);
    }

    public function edit(Client $client): Client
    {
        $this->repository->edit($client);
        return $this->getById($client->id);
    }

    public function delete($id)
    {
        $this->repository->delete($id);
    }

    public function getAllVIPPaymentClients(): array
    {
        return $this->repository->getAllVIPPaymentClients();
    }

    public function getPaymentClientsBetweenDates($start, $end): array
    {
        return $this->repository->getPaymentClientsBetweenDates($start, $end);
    }

    public function validaFilters($filters)
    {
        if ($filters['finished'] !== null) {
            $this->jValidator->validaBoolean($filters['finished'], true, 'finished');
        }

        $this->jValidator->checkErrors();
    }

    public function validaCreation(Client $client)
    {
        $this->validaCommonFields($client);
        $this->jValidator->checkErrors();
    }

    public function validaEdition(Client $client)
    {
        $this->validaId($client->id);
        $this->validaCommonFields($client);
        $this->jValidator->checkErrors();
    }

    private function validaCommonFields(Client $client)
    {
        $this->jValidator->validaString($client->name, true, 'name', 1, 250);
        $this->jValidator->validaString($client->surnames, true, 'surnames', 1, 250);
        $this->jValidator->validaEmail($client->email, true, 'email');
        $this->jValidator->validaString($client->phone, false, 'phone', 1, 250);
        $this->jValidator->validaString($client->city, false, 'city', 1, 250);
        $this->jValidator->validaString($client->address, false, 'address', 1, 250);
        $this->validaCountryProvinceIdIfIsSet($client->countryProvinceId);
        $this->validaCountryId($client->countryId, false, false);
        $this->validaLanguageId($client->languageId);
        $this->jValidator->validaString($client->idCard, false, 'idCard', 1, 250);
        $this->validaDocumentationTypeId($client->documentationTypeId);
        $this->validaIdCardExpiresAt($client->idCardExpiresAt);
        $this->validaLegalGuardianId($client->legalGuardianId, false, false);
        $this->validaGenderIfIsSet($client->gender);
        $this->jValidator->validaIP($client->ip, false, 'ip');
        $this->jValidator->validaString($client->userAgent, false, 'userAgent', 1, 250);
    }

    private function validaCountryProvinceIdIfIsSet($id)
    {
        if ($id !== null) {
            $this->validaIdAndExistsInTable($id, 'countryProvinceId', 'country_provinces', true, true);
        }
    }

    private function validaLanguageId($id)
    {
        $this->validaIdAndExistsInTable($id, 'languageId', 'translation_system.languages', true);
    }

    private function validaDocumentationTypeId($id)
    {
        $this->validaIdAndExistsInTable($id, 'documentationTypeId', 'documentation_types', false);
    }

    private function validaIdCardExpiresAt($idCardExpiresAt)
    {
        $this->jValidator->validaDate($idCardExpiresAt, false, 'idCardExpiresAt', self::DATE_FORMAT);
    }

    private function validaGenderIfIsSet($gender)
    {
        if ($gender !== null && ($gender !== self::MALE_KEY && $gender !== self::FEMALE_KEY)) {
            $this->jValidator->setError('gender', $this->jValidator::E_INVALID_FORMAT, $gender);
        }
    }

    public function validaIdsToDelete($ids)
    {
        foreach ($ids as $id) {
            $this->validaId($id);
        }

        $this->jValidator->checkErrors();
    }

    public function validaId($id, $checkErrors = false)
    {
        $this->validaIdAndExistsInTable($id, 'clientId', 'clients', true, false, $checkErrors);
    }



    /*********************
     ** LEGAL GUARDIANS **
     *********************/

    public function getAllLegalGuardians(): array
    {
        return $this->repository->getAllLegalGuardians();
    }

    public function getLegalGuardianById($id): LegalGuardian
    {
        return new LegalGuardian($this->repository->getLegalGuardianById($id));
    }

    public function createLegalGuardian(LegalGuardian $legalGuardian): LegalGuardian
    {
        $id = $this->repository->createLegalGuardian($legalGuardian);
        return $this->getLegalGuardianById($id);
    }

    public function editLegalGuardian(LegalGuardian $legalGuardian): LegalGuardian
    {
        $this->repository->editLegalGuardian($legalGuardian);
        return $this->getLegalGuardianById($legalGuardian->id);
    }

    public function deleteLegalGuardian($id)
    {
        $this->repository->deleteLegalGuardian($id);
    }

    public function validaLegalGuardianCreation(LegalGuardian $legalGuardian)
    {
        $this->validaLegalGuardianCommonFields($legalGuardian);
        $this->jValidator->checkErrors();
    }

    public function validaLegalGuardianEdition(LegalGuardian $legalGuardian)
    {
        $this->validaLegalGuardianId($legalGuardian->id);
        $this->validaLegalGuardianCommonFields($legalGuardian);
        $this->jValidator->checkErrors();
    }

    private function validaLegalGuardianCommonFields(LegalGuardian $legalGuardian)
    {
        $this->jValidator->validaString($legalGuardian->name, true, 'name', 1, 250);
        $this->jValidator->validaString($legalGuardian->surnames, true, 'surnames', 1, 250);
        $this->jValidator->validaString($legalGuardian->idCard, true, 'idCard', 1, 250);
        $this->validaIdAndExistsInTable($legalGuardian->documentationTypeId, 'documentationTypeId', 'documentation_types', false);
        $this->validaIdCardExpiresAt($legalGuardian->idCardExpiresAt);
    }

    public function validaLegalGuardianIdsToDelete($ids)
    {
        foreach ($ids as $id) {
            $this->validaLegalGuardianId($id);
        }

        $this->jValidator->checkErrors();
    }

    public function validaLegalGuardianId($id, $checkErrors = false, $required = true)
    {
        $this->validaIdAndExistsInTable($id, 'legalGuardianId', 'legal_guardians', $required, false, $checkErrors);
    }
}