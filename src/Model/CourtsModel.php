<?php

namespace App\Model;

use App\Entity\Court;
use App\Repository\CourtsRepository;
use Doctrine\DBAL\Connection;

class CourtsModel extends BaseModel
{
    public function __construct(Connection $populeticDB)
    {
        parent::__construct();
        $this->repository = new CourtsRepository($populeticDB);
    }

    /*********************
     ****** COURTS ******
     *********************/

    public function getAll(): array
    {
        return $this->repository->getAll();
    }

    public function getById($id): Court
    {
        return new Court($this->repository->getById($id));
    }

    public function create(Court $court): Court
    {
        $id = $this->repository->create($court);
        return $this->getById($id);
    }

    public function edit(Court $court): Court
    {
        $this->repository->edit($court);
        return $this->getById($court->id);
    }

    public function delete($id)
    {
        $this->repository->delete($id);
    }

    public function validaCourt(Court $court)
    {
        $this->validaCourtTypeIdExistsForCountry($court->courtTypeId, $court->countryId);
        $this->jValidator->validaString($court->address, false, 'address', 1, 250);
        $this->jValidator->validaString($court->postalCode, false, 'postalCode', 1, 15);
        $this->validaCityIdExistsForCountry($court->cityId, $court->countryId);
        $this->validaCountryId($court->countryId);
        $this->validaTypeAndCityDoesntExists($court->courtTypeId, $court->cityId, $court->id);
        $this->jValidator->checkErrors();
    }

    private function validaCourtTypeIdExistsForCountry($id, $countryId)
    {
        $this->jValidator->validaInt($id, true, 'courtTypeId', 1);
        if (!$this->repository->courtTypeIdExistsForCountry($id, $countryId)) {
            $this->jValidator->setError('courtTypeId', $this->jValidator::E_DOESNT_EXISTS . 'ForCountry', $id);
        }
    }

    private function validaCityIdExistsForCountry($id, $countryId)
    {
        $this->jValidator->validaInt($id, true, 'cityId', 1);
        if ($id && !$this->repository->cityIdExistsForCountry($id, $countryId)) {
            $this->jValidator->setError('cityId', $this->jValidator::E_DOESNT_EXISTS . 'ForCountry', $id);
        }
    }

    private function validaTypeAndCityDoesntExists($courtTypeId, $cityId, $id)
    {
        if ($this->repository->typeAndCityExists($courtTypeId, $cityId, $id)) {
            $this->jValidator->setError('typeAndCity', $this->jValidator::E_ALREADY_EXISTS);
        }
    }

    public function validaIdsToDelete($ids)
    {
        foreach ($ids as $id) {
            $this->validaId($id);
        }

        $this->jValidator->checkErrors();
    }

    public function validaId($id, $checkErrors = false)
    {
        $this->validaIdAndExistsInTable($id, 'courtId', 'courts', true, false, $checkErrors);
    }



    /*********************
     **** COURT TYPES ****
     *********************/

    private $neededParamsCourtType = ['name', 'countryId'];

    public function getAllCourtTypes(): array
    {
        return $this->repository->getAllCourtTypes();
    }

    public function getCourtTypeById($id): array
    {
        return $this->repository->getCourtTypeById($id);
    }

    public function createCourtType($data): array
    {
        $data['id'] = $this->repository->createCourtType($data);
        return $data;
    }

    public function editCourtType($data)
    {
        $this->repository->editCourtType($data);
    }

    public function deleteCourtType($id)
    {
        $this->repository->deleteCourtType($id);
    }

    public function validaCourtTypeCreation($data)
    {
        $this->validaParams($data, $this->neededParamsCourtType);
        $this->validaCourtTypeName($data['name'], $data['countryId']);
        $this->validaCountryId($data['countryId']);
        $this->jValidator->checkErrors();
    }

    public function validaCourtTypeEdition($data)
    {
        $this->validaParams($data, $this->neededParamsCourtType);
        $this->validaCourtTypeName($data['name'], $data['countryId'], $data['id']);
        $this->validaCountryId($data['countryId']);
        $this->jValidator->checkErrors();
    }

    private function validaCourtTypeName($name, $countryId, $id = null)
    {
        $this->jValidator->validaString($name, true, 'name', 1, 250);
        if ($this->repository->courtTypeNameExistsForCountry($name, $countryId, $id)) {
            $this->jValidator->setError('name', $this->jValidator::E_ALREADY_EXISTS . 'ForCountry', $name);
        }
    }

    public function validaCourtTypeIdsToDelete($ids)
    {
        foreach ($ids as $id) {
            $this->validaCourtTypeId($id);
        }

        $this->jValidator->checkErrors();
    }

    public function validaCourtTypeId($id, $checkErrors = false)
    {
        $this->validaIdAndExistsInTable($id, 'courtTypeId', 'court_types', true, false, $checkErrors);
    }



    /*********************
     *** COURTS NUMBER ***
     *********************/

    private $neededParamsCourtNumber = ['courtId', 'number', 'fax', 'phone'];

    public function getAllCourtsNumber(): array
    {
        return $this->repository->getAllCourtsNumber();
    }

    public function getCourtsNumberByCourtId($courtId): array
    {
        return $this->repository->getCourtsNumberByCourtId($courtId);
    }

    public function getCourtNumberById($id): array
    {
        return $this->repository->getCourtNumberById($id);
    }

    public function createCourtNumber($data): int
    {
        return $this->repository->createCourtNumber($data);
    }

    public function editCourtNumber($data)
    {
        $this->repository->editCourtNumber($data);
    }

    public function deleteCourtNumber($id)
    {
        $this->repository->deleteCourtNumber($id);
    }

    public function validaCourtNumber($data)
    {
        $id = array_key_exists('id', $data) ? $data['id'] : null;

        $this->validaParams($data, $this->neededParamsCourtNumber);
        $this->validaId($data['courtId']);
        $this->jValidator->validaInt($data['number'], true, 'number', 1);
        $this->validaNumberDoesntExistsForCourtId($data['courtId'], $data['number'], $id);
        $this->jValidator->validaString($data['fax'], false, 'fax', 1, 250);
        $this->jValidator->validaString($data['phone'], false, 'phone', 1, 250);
        $this->jValidator->checkErrors();
    }

    public function validaCourtNumberIdsToDelete($ids)
    {
        foreach ($ids as $id) {
            $this->validaCourtNumberId($id);
        }

        $this->jValidator->checkErrors();
    }

    public function validaCourtNumberId($id, $checkErrors = false)
    {
        $this->validaIdAndExistsInTable($id, 'courtNumberId', 'courts_number', true, false, $checkErrors);
    }

    private function validaNumberDoesntExistsForCourtId($courtId, $number, $id = null)
    {
        if ($this->repository->numberExistsForCourtId($courtId, $number, $id)) {
            $this->jValidator->setError('number', $this->jValidator::E_ALREADY_EXISTS, $number);
        }
    }
}