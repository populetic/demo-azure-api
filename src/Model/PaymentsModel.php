<?php

namespace App\Model;

use App\Repository\PaymentsRepository;
use Doctrine\DBAL\Connection;

class PaymentsModel extends BaseModel
{
    private $populeticDB;

    public function __construct(Connection $populeticDB)
    {
        parent::__construct();
        $this->repository  = new PaymentsRepository($populeticDB);
        $this->populeticDB = $populeticDB;
    }

    public function getPaymentsByClaimStatus($claimStatusId): array
    {
        $billsModel = new BillsModel($this->populeticDB);
        $payments   = $this->repository->getPaymentsByClaimStatus($claimStatusId);

        foreach ($payments as $key => $data) {
            $payments[$key] = $billsModel->decryptClaimBillingData($data);
        }

        return $payments;
    }

    public function getPaymentsByClaimStatusAndDate($data): array
    {
        $billsModel = new BillsModel($this->populeticDB);
        $payments   = $this->repository->getPaymentsByClaimStatusAndDate($data['claimStatusId'], $data['lastSentAt']);

        foreach ($payments as $key => $data) {
            $payments[$key] = $billsModel->decryptClaimBillingData($data);
        }

        return $payments;
    }

    public function getDatesToSendEmailPayment(): array
    {
        return $this->repository->getDatesToSendEmailPayment();
    }

    public function getClaimsGroupedByAccountData($claimId): array
    {
        $groupedClaims = $this->repository->getClaimsGroupedByAccountData($claimId);

        $claimsArray = [];
        foreach ($groupedClaims as $claim) {
            $claimsArray[] = $claim['claimId'];
        }

        return $claimsArray;
    }

    public function getClientsDataWithCompletedPayment(): array
    {
        return $this->repository->getClientsDataWithCompletedPayment();
    }

    public function getClaimsLogsStatusToRevert(): array
    {
        return $this->repository->getClaimsLogsStatusToRevert();
    }

    public function validaPaymentsClaimStatusAndDate($data)
    {
        $this->validaParams($data, ['claimStatusId', 'lastSentAt']);
        $this->validaClaimStatusId($data['claimStatusId']);
        $this->jValidator->validaDate($data['lastSentAt'], true, 'lastSentAt', ConstantsModel::DATE_FORMAT);
        $this->jValidator->checkErrors();
    }

    public function validaClaimStatusId($id, $checkErrors = false)
    {
        $this->validaIdAndExistsInTable($id, 'claimStatusId', 'claim_status', true);
        if ($checkErrors) $this->jValidator->checkErrors();
    }
}