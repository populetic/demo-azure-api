<?php

namespace App\Model\Statia;

use App\Model\BaseModel;
use App\Repository\Statia\RolesRepository;
use Doctrine\DBAL\Connection;

class RolesModel extends BaseModel
{
    private $neededParams = ['name', 'departmentId', 'mainMenuId', 'isAdmin'];

    public function __construct(Connection $statiaDB)
    {
        parent::__construct();
        $this->repository = new RolesRepository($statiaDB);
    }

    public function getAll(): array
    {
        return $this->repository->getAll();
    }

    public function getById($id): array
    {
        return $this->repository->getById($id);
    }

    public function create($data): array
    {
        $data['id'] = $this->repository->create($data);
        return $data;
    }

    public function edit($data)
    {
        $this->repository->edit($data);
    }

    public function delete($id)
    {
        $this->repository->delete($id);
    }

    public function validaCreation($data)
    {
        $this->validaParams($data, $this->neededParams);
        $this->validaCommonFields($data);
        $this->checkIfNameAlreadyExists($data['name']);
        $this->jValidator->checkErrors();
    }

    public function validaEdition($data)
    {
        $this->validaParams($data, $this->neededParams);
        $this->validaId($data['id']);
        $this->validaCommonFields($data);
        $this->checkIfNameAlreadyExists($data['name'], $data['id']);
        $this->jValidator->checkErrors();
    }

    public function validaIdsToDelete($ids)
    {
        foreach ($ids as $id) {
            $this->validaId($id, false);
        }
        $this->jValidator->checkErrors();
    }

    public function validaId($id, $checkErrors = true)
    {
        $this->validaIdAndExistsInTable($id, 'roleId', 'roles', true, false, $checkErrors);
    }

    private function validaCommonFields($data)
    {
        $this->jValidator->validaString($data['name'], true, 'name', 1, 50);
        $this->validaIdAndExistsInTable($data['departmentId'], 'departmentId', 'departments', true);
        $this->validaIdAndExistsInTable($data['mainMenuId'], 'mainMenuId', 'menus', false);
        $this->jValidator->validaBoolean($data['isAdmin'], true, 'isAdmin');
    }

    private function checkIfNameAlreadyExists($name, $id = null)
    {
        if ($this->repository->nameExistsInTable('roles', $name, $id)) {
            $this->jValidator->setError('name', $this->jValidator::E_ALREADY_EXISTS, $name);
        }
    }
}