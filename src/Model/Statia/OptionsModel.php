<?php

namespace App\Model\Statia;

use App\Model\BaseModel;
use App\Repository\Statia\OptionsRepository;
use App\Service\Exceptions\ValidationException;
use Doctrine\DBAL\Connection;

class OptionsModel extends BaseModel
{
    private $neededParams            = ['name', 'icon', 'onHeaderMenu', 'onTableMenu'];
    private $roleMenusNeededParams   = ['roleId', 'menusOptions'];
    private $menuOptionsNeededParams = ['menuId', 'optionIds'];

    public function __construct(Connection $statiaDB)
    {
        parent::__construct();
        $this->repository = new OptionsRepository($statiaDB);
    }

    /*********************
     ****** OPTIONS ******
     *********************/

    public function getAll(): array
    {
        return $this->repository->getAll();
    }

    public function getById($id): array
    {
        return $this->repository->getById($id);
    }

    public function getByMenuAndRole($menuId, $roleId): array
    {
        $data = [];

        if ($menuId) {
            $data = $this->repository->getByMenuAndRole($menuId, $roleId);
        } else {
            foreach ($this->repository->getAllMenusByRole($roleId) as $menu) {
                $data[] = [
                    'menu'    => $menu['id'],
                    'options' => $this->repository->getByMenuAndRole($menu['id'], $roleId)
                ];
            }
        }

        return $data;
    }

    public function create($data): array
    {
        $data['id'] = $this->repository->create($data);
        return $data;
    }

    public function edit($data)
    {
        $this->repository->edit($data);
    }

    public function delete($id)
    {
        $this->repository->delete($id);
    }

    public function validaCreation($data)
    {
        $this->validaParams($data, $this->neededParams);
        $this->validaCommonFields($data);
        $this->checkIfNameAlreadyExists($data['name']);
        $this->jValidator->checkErrors();
    }

    public function validaEdition($data)
    {
        $this->validaParams($data, $this->neededParams);
        $this->validaId($data['id']);
        $this->validaCommonFields($data);
        $this->checkIfNameAlreadyExists($data['name'], $data['id']);
        $this->jValidator->checkErrors();
    }

    public function validaIdsToDelete($ids)
    {
        foreach ($ids as $id) {
            $this->validaId($id, false);
        }
        $this->jValidator->checkErrors();
    }

    public function validaId($id, $checkErrors = true)
    {
        $this->validaIdAndExistsInTable($id, 'optionId', 'options', true, false, $checkErrors);
    }

    private function validaCommonFields($data)
    {
        $this->jValidator->validaString($data['name'], true, 'name', 1, 15);
        $this->jValidator->validaString($data['icon'], false, 'icon', 1, 25);
        $this->jValidator->validaBoolean($data['onHeaderMenu'], true, 'onHeaderMenu');
        $this->jValidator->validaBoolean($data['onTableMenu'], true, 'onTableMenu');
    }

    private function checkIfNameAlreadyExists($name, $id = null)
    {
        if ($this->repository->nameExistsInTable('options', $name, $id)) {
            $this->jValidator->setError('name', $this->jValidator::E_ALREADY_EXISTS, $name);
        }
    }



    /*********************
     * MENU ROLE OPTIONS *
     *********************/

    public function createMenuRole($menuId, $roleId, $quickaccess)
    {
        $this->repository->createMenuRole($menuId, $roleId, $quickaccess);
    }
    public function createMenuRoleOption($menuId, $roleId, $optionId)
    {
        $this->repository->createMenuRoleOption($menuId, $roleId, $optionId);
    }

    public function deleteAllMenusRolesOptionsByRoleId($roleId)
    {
        $this->repository->deleteAllMenusRolesOptionsByRoleId($roleId);
    }
    public function deleteAllMenusRolesByRoleId($roleId)
    {
        $this->repository->deleteAllMenusRolesByRoleId($roleId);
    }

    public function validaMenuAndRoleToGet($menu, $role)
    {
        $this->jValidator->validaInt($menu, false, 'menuId', 1);
        $this->jValidator->validaInt($role, true, 'roleId', 1);
        $this->jValidator->checkErrors();
    }

    public function validaEditRoleMenusOptions($params)
    {
        $this->validaParams($params, $this->roleMenusNeededParams);

        if (!is_array($params['menusOptions'])) {
            throw new ValidationException('menusOptions' . $this->jValidator::E_NOARRAY);
        }

        $this->validaIdAndExistsInTable($params['roleId'], 'roleId', 'roles', true);
        foreach ($params['menusOptions'] as $menuOptions) {
            $this->validaParams($menuOptions, $this->menuOptionsNeededParams);
            $quickaccess = array_key_exists('quickaccess', $menuOptions) ? $menuOptions['quickaccess'] : 0;

            if (!is_array($menuOptions['optionIds'])) {
                throw new ValidationException('optionIds' . $this->jValidator::E_NOARRAY);
            }

            $this->validaIdAndExistsInTable($menuOptions['menuId'], 'menuId', 'menus', true);
            $this->jValidator->validaBoolean($quickaccess, true, 'quickaccess');

            foreach ($menuOptions['optionIds'] as $optionId) {
                $this->validaId($optionId, false);
            }
        }

        $this->jValidator->checkErrors();
    }
}