<?php

namespace App\Model\Statia;

use App\Model\BaseModel;
use App\Repository\Statia\MenusRepository;
use Doctrine\DBAL\Connection;

class MenusModel extends BaseModel
{
    const FIELD_MAX_LENGTH = 70;

    public function __construct(Connection $statiaDB)
    {
        parent::__construct();
        $this->repository = new MenusRepository($statiaDB);
    }

    /*********************
     ******* MENUS *******
     *********************/

    private $neededParams = ['name', 'parentId', 'position', 'icon', 'hasCheckbox', 'hasTableMenu', 'hasMultipleDelete'];

    public function getAll(): array
    {
        $menuWithOptions = [];

        $menus = $this->repository->getAll();
        foreach ($menus as $menu) {
            $menuWithOptions[] = $this->addMenuOptionsAndProperties($menu);
        }

        return $menuWithOptions;
    }

    public function getById($id): array
    {
        $menu = $this->repository->getById($id);
        return $this->addMenuOptionsAndProperties($menu);
    }

    public function getByRole($roleId): array
    {
        return $this->repository->getByRole($roleId);
    }

    public function create($data): array
    {
        $data['id'] = $this->repository->create($data);
        return $data;
    }

    public function edit($data)
    {
        $this->repository->edit($data);
    }

    public function delete($id)
    {
        $this->repository->delete($id);
    }

    public function filterMainMenus(array $menus): array
    {
        $mainMenus = [];
        foreach ($menus as $menu) {
            if ($menu['parentId'] === null) {
                $mainMenus[] = $menu;
            }
        }

        return $mainMenus;
    }

    public function validaCreation($data)
    {
        $this->validaParams($data, $this->neededParams);
        $this->validaCommonFields($data);
        $this->checkIfNameAlreadyExists($data['name']);
        $this->jValidator->checkErrors();
    }

    public function validaEdition($data)
    {
        $this->validaParams($data, $this->neededParams);
        $this->validaId($data['id']);
        $this->validaCommonFields($data);
        $this->checkIfNameAlreadyExists($data['name'], $data['id']);
        $this->jValidator->checkErrors();
    }

    public function validaIdsToDelete($ids)
    {
        foreach ($ids as $id) {
            $this->validaId($id, false);
        }
        $this->jValidator->checkErrors();
    }

    public function validaId($id, $checkErrors = true)
    {
        $this->validaIdAndExistsInTable($id, 'menuId', 'menus', true, false, $checkErrors);
    }

    private function validaCommonFields($data)
    {
        $this->jValidator->validaString($data['name'], true, 'name', 1, 40);
        $this->jValidator->validaInt($data['parentId'], false, 'parentId', 1);
        $this->jValidator->validaInt($data['position'], false, 'position', 1);
        $this->jValidator->validaString($data['icon'], false, 'icon', 1, 40);
        $this->jValidator->validaBoolean($data['hasCheckbox'], true, 'hasCheckbox');
        $this->jValidator->validaBoolean($data['hasTableMenu'], true, 'hasTableMenu');
        $this->jValidator->validaBoolean($data['hasMultipleDelete'], true, 'hasMultipleDelete');

        if ($data['parentId'] && !$this->repository->idExistsInTable('menus', $data['parentId'])) {
            $this->jValidator->setError('parentId', $this->jValidator::E_DOESNT_EXISTS, $data['parentId']);
        }
    }

    private function checkIfNameAlreadyExists($name, $id = null)
    {
        if ($this->repository->nameExistsInTable('menus', $name, $id)) {
            $this->jValidator->setError('name', $this->jValidator::E_ALREADY_EXISTS, $name);
        }
    }

    private function addMenuOptionsAndProperties($menu): array
    {
        $menu['options']    = $this->repository->getMenuOptionsByMenuId($menu['id']);
        $menu['properties'] = $this->repository->getMenuPropertiesByMenuId($menu['id']);

        return $menu;
    }



    /*********************
     **** MENU OPTIONS ****
     *********************/

    public function createMenuOptions($menuId, $optionIds)
    {
        foreach ($optionIds as $optionId) {
            $this->repository->createMenuOption($menuId, $optionId);
        }
    }

    public function deleteAllMenuOptions($menuId)
    {
        $this->repository->deleteAllMenuOptions($menuId);
    }

    public function validaMenuOptions($optionIds)
    {
        foreach ($optionIds as $optionId) {
            $this->validaOptionId($optionId);
        }
        $this->jValidator->checkErrors();
    }

    private function validaOptionId($id)
    {
        $this->validaIdAndExistsInTable($id, 'optionId', 'options', true);
    }



    /*********************
     ** MENU PROPERTIES **
     *********************/

    private $menuPropertyNeededParams = [
        'menuId',
        'field',
        'sort',
        'filter',
        'isAdvancedFilter',
        'isFilterSelected',
        'isBoolean',
        'isNumber',
        'isDate',
        'canBeTranslated',
        'onlyForAdmin',
        'isSimpleFilter'
    ];
    private $deleteMenuPropertyNeededParams = ['menuId', 'field'];

    public function createMenuProperty($menuProperty)
    {
        $this->repository->createMenuProperty($menuProperty);
    }

    public function editMenuProperty($menuProperty): array
    {
        $this->repository->editMenuProperty($menuProperty);

        $menuProperty['field'] = $menuProperty['fieldNewName'];
        unset($menuProperty['fieldNewName']);

        return $menuProperty;
    }

    public function getMenuPropertyByFieldName($fieldName): array
    {
        return $this->repository->getMenuPropertyByFieldName($fieldName);
    }

    public function deleteMenuProperty($menuProperty)
    {
        $this->repository->deleteMenuProperty($menuProperty['menuId'], $menuProperty['field']);
    }

    public function validaMenuPropertyToCreate($menuProperty)
    {
        $this->validaParams($menuProperty, $this->menuPropertyNeededParams);
        $this->validaMenuPropertyFields($menuProperty);
        $this->checkIfFieldAlreadyExistsForMenuProperty($menuProperty['menuId'], $menuProperty['field']);
        $this->jValidator->checkErrors();
    }

    public function validaMenuPropertyToEdit($menuProperty)
    {
        $this->validaParams($menuProperty, array_merge($this->menuPropertyNeededParams, ['fieldNewName']));
        $this->validaMenuPropertyFields($menuProperty);
        if (!$this->repository->fieldExistsForMenuProperty($menuProperty['menuId'], $menuProperty['field'])) {
            $this->jValidator->setError('field', $this->jValidator::E_DOESNT_EXISTS, $menuProperty['field']);
        }
        if ($menuProperty['fieldNewName'] !== $menuProperty['field']) {
            $this->checkIfFieldAlreadyExistsForMenuProperty($menuProperty['menuId'], $menuProperty['fieldNewName']);
        }
        $this->jValidator->checkErrors();
    }

    public function validaMenuPropertyToDelete($menuProperty)
    {
        $this->validaParams($menuProperty, $this->deleteMenuPropertyNeededParams);
        $this->validaId($menuProperty['menuId']);
        $this->jValidator->validaString($menuProperty['field'], true, 'field', 1, self::FIELD_MAX_LENGTH);
        if (!$this->repository->fieldExistsForMenuProperty($menuProperty['menuId'], $menuProperty['field'])) {
            $this->jValidator->setError('field', $this->jValidator::E_DOESNT_EXISTS, $menuProperty['field']);
        }
        $this->jValidator->checkErrors();
    }

    public function validaMenuPropertyFields($menuProperty)
    {
        $this->validaId($menuProperty['menuId']);
        $this->jValidator->validaString($menuProperty['field'], true, 'field', 1, self::FIELD_MAX_LENGTH);
        if (!empty($menuProperty['fieldNewName'])) {
            $this->jValidator->validaString($menuProperty['fieldNewName'], true, 'fieldNewName', 1, self::FIELD_MAX_LENGTH);
        }
        $this->jValidator->validaInt($menuProperty['sort'], true, 'sort');
        $this->jValidator->validaBoolean($menuProperty['filter'], true, 'filter');
        $this->jValidator->validaBoolean($menuProperty['isAdvancedFilter'], true, 'isAdvancedFilter');
        $this->jValidator->validaBoolean($menuProperty['isFilterSelected'], true, 'isFilterSelected');
        $this->jValidator->validaBoolean($menuProperty['isBoolean'], true, 'isBoolean');
        $this->jValidator->validaBoolean($menuProperty['isNumber'], true, 'isNumber');
        $this->jValidator->validaBoolean($menuProperty['isDate'], true, 'isDate');
        $this->jValidator->validaBoolean($menuProperty['canBeTranslated'], true, 'canBeTranslated');
        $this->jValidator->validaBoolean($menuProperty['onlyForAdmin'], true, 'onlyForAdmin');
        $this->jValidator->validaBoolean($menuProperty['isSimpleFilter'], true, 'isSimpleFilter');
    }

    private function checkIfFieldAlreadyExistsForMenuProperty($menuId, $field)
    {
        if ($this->repository->fieldExistsForMenuProperty($menuId, $field)) {
            $this->jValidator->setError('field', $this->jValidator::E_ALREADY_EXISTS, $field);
        }
    }
}