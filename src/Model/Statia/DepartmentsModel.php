<?php

namespace App\Model\Statia;

use App\Model\BaseModel;
use App\Repository\Statia\DepartmentsRepository;
use Doctrine\DBAL\Connection;

class DepartmentsModel extends BaseModel
{
    private $neededParams = ['name'];

    public function __construct(Connection $statiaDB)
    {
        parent::__construct();
        $this->repository = new DepartmentsRepository($statiaDB);
    }

    public function getAll(): array
    {
        return $this->repository->getAll();
    }

    public function getById($id): array
    {
        return $this->repository->getById($id);
    }

    public function create($data): array
    {
        $data['id'] = $this->repository->create($data['name']);
        return $data;
    }

    public function edit($data)
    {
        $this->repository->edit($data);
    }

    public function delete($id)
    {
        $this->repository->delete($id);
    }

    public function validaCreation($data)
    {
        $this->validaParams($data, $this->neededParams);
        $this->validaCommonData($data['name']);
        $this->jValidator->checkErrors();
    }

    public function validaEdition($data)
    {
        $this->validaParams($data, $this->neededParams);
        $this->validaId($data['id']);
        $this->validaCommonData($data['name'], $data['id']);
        $this->jValidator->checkErrors();
    }

    public function validaIdsToDelete($ids)
    {
        foreach ($ids as $id) {
            $this->validaId($id, false);
        }
        $this->jValidator->checkErrors();
    }

    public function validaId($id, $checkErrors = true)
    {
        $this->validaIdAndExistsInTable($id, 'departmentId', 'departments', true, false, $checkErrors);
    }

    private function validaCommonData($name, $id = null)
    {
        $this->jValidator->validaString($name, true, 'name', 1, 100);
        if ($this->repository->nameExistsInTable('departments', $name, $id)) {
            $this->jValidator->setError('name', $this->jValidator::E_ALREADY_EXISTS, $name);
        }
    }
}