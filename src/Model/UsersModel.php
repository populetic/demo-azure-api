<?php

namespace App\Model;

use App\Entity\User;
use App\Repository\Statia\RolesRepository;
use App\Repository\TranslationSystem\TranslationsRepository;
use App\Repository\UsersRepository;
use App\Service\Exceptions\ValidationException;
use Doctrine\DBAL\Connection;

class UsersModel extends BaseModel
{
    const USER_DOESNT_EXISTS   = 'userDoesntExists';
    const USER_INACTIVE        = 'userInactive';
    const USER_HASNT_EMAIL     = 'userHasntEmail';
    const PASSWORD_INVALID     = 'passwordInvalid';
    const USERNAME_MAX_LENGTH  = 45;

    private $loginNeededParams = ['username', 'password'];
    private $statiaDB;
    private $translationsDB;

    public function __construct(Connection $populeticDB)
    {
        parent::__construct();
        $this->repository = new UsersRepository($populeticDB);
    }

    public function getAll(): array
    {
        return $this->repository->getAll();
    }

    public function getAllByDepartment($departmentId): array
    {
        return $this->repository->getAllByDepartment($departmentId);
    }

    public function getAllByLawFirm($lawFirmId): array
    {
        return $this->repository->getAllByLawFirm($lawFirmId);
    }

    public function getById($id, $getExtraData = false): User
    {
        $userData = $this->repository->getById($id, $getExtraData);
        $this->checkUserExistsOrThrowException($userData);

        $user = new User($userData);

        return $this->getUserWithPasswordHidden($user);
    }

    public function getByUsername($username, $getPassword = false): User
    {
        $userData = $this->repository->getByUsername($username, $getPassword);
        $this->checkUserExistsOrThrowException($userData);

        $userData['password'] = $getPassword ? $userData['password'] : '';

        return new User($userData);
    }

    public function validaLoginAndGetUser($params): User
    {
        $this->validaParams($params, $this->loginNeededParams);
        $this->jValidator->validaString($params['username'], true, 'username', 1, self::USERNAME_MAX_LENGTH);
        $this->jValidator->checkErrors();

        $user = $this->getByUsername($params['username'], true);
        $this->validaIfUserIsActiveOrThrowException($user);

        if (!$this->verifyPassword($params['password'], $user->password)) {
            throw new ValidationException(self::PASSWORD_INVALID);
        }

        return $this->getUserWithPasswordHidden($user);
    }

    public function validaIfUserHasEmailOrThrowException($email)
    {
        if (!$email) {
            throw new ValidationException(self::USER_HASNT_EMAIL);
        }
    }

    public function validaIfUserIsActiveOrThrowException(User $user)
    {
        if ($user->isActive == 0) {
            throw new ValidationException(self::USER_INACTIVE);
        }
    }

    public function validaCreation(Connection $statiaDB, Connection $translationsDB, User $user)
    {
        $this->statiaDB       = $statiaDB;
        $this->translationsDB = $translationsDB;

        $this->jValidator->validaPassword($user->password, true, 'password', 2, 6, 25);
        $this->validaUserData($user);

        $this->jValidator->checkErrors();
    }

    public function validaEdition(Connection $statiaDB, Connection $translationsDB, User $user)
    {
        $this->statiaDB       = $statiaDB;
        $this->translationsDB = $translationsDB;

        $this->validaIdAndReturnUser($user->id);
        $this->jValidator->validaPassword($user->password, false, 'password', 2, 6, 25);
        $this->validaUserData($user);

        $this->jValidator->checkErrors();
    }

    public function validaIdAndReturnUser($id, $getExtraData = false): User
    {
        $this->jValidator->validaInt($id, true, 'userId', 1);
        $this->jValidator->checkErrors();

        return $this->getById($id, $getExtraData);
    }

    public function create(User $user): User
    {
        $user->password = $this->hashPassword($user->password);
        $id = $this->repository->create($user);

        return $this->getById($id);
    }

    public function edit(User $user): User
    {
        if ($user->password) {
            $user->password = $this->hashPassword($user->password);
        }
        $this->repository->edit($user);

        return $this->getById($user->id);
    }

    public function delete($id)
    {
        $this->repository->delete($id);
    }

    public function validaUsernameExists($username)
    {
        $this->jValidator->validaString($username, true, 'username', 1, self::USERNAME_MAX_LENGTH);
        if (!$this->repository->usernameExists($username)) {
            $this->jValidator->setError('username', $this->jValidator::E_DOESNT_EXISTS, $username);
        }

        $this->jValidator->checkErrors();
    }

    public function validaDepartmentId($id)
    {
        $this->validaIdAndExistsInTable($id, 'departmentId', 'statia.departments', true, false, true);
    }

    public function validaLawFirmId($id)
    {
        $this->validaIdAndExistsInTable($id, 'lawFirmId', 'law_firms', true, false, true);
    }

    public function changePassword($username, $newPassword)
    {
        $newPassword = $this->hashPassword($newPassword);
        $this->repository->changePassword($username, $newPassword);
    }

    private function validaUserData(User $user)
    {
        $this->validaUsername($user->username, $user->id);
        $this->jValidator->validaEmail($user->email);
        $this->jValidator->validaString($user->name, true, 'name', 1, 100);
        $this->jValidator->validaString($user->surnames, false, 'surname', 1, 250);
        $this->validaCountryId($user->countryId);
        $this->validaLanguageId($user->languageId);
        $this->validaRoleId($user->roleId);
        $this->validaLawFirmIdIfIsSet($user->lawFirmId);
        $this->jValidator->validaBoolean($user->isSystem, true, 'isSystem');
        $this->jValidator->validaBoolean($user->isActive, true, 'isActive');
    }

    private function validaUsername($username, $userId)
    {
        $this->jValidator->validaString($username, true, 'username', 1, self::USERNAME_MAX_LENGTH);
        if ($this->repository->usernameExists($username, $userId)) {
            $this->jValidator->setError('username', $this->jValidator::E_ALREADY_EXISTS, $username);
        }
    }

    private function validaLanguageId($languageId)
    {
        $this->jValidator->validaInt($languageId, true, 'languageId', 1);
        $languagesRepo = new TranslationsRepository($this->translationsDB);
        if ($languageId && !$languagesRepo->idExistsInTable('languages', $languageId)) {
            $this->jValidator->setError('languageId', $this->jValidator::E_DOESNT_EXISTS, $languageId);
        }
    }

    private function validaRoleId($roleId)
    {
        $this->jValidator->validaInt($roleId, true, 'roleId', 1);
        $rolesRepo = new RolesRepository($this->statiaDB);
        if ($roleId && !$rolesRepo->idExistsInTable('roles', $roleId)) {
            $this->jValidator->setError('roleId', $this->jValidator::E_DOESNT_EXISTS, $roleId);
        }
    }

    private function validaLawFirmIdIfIsSet($id)
    {
        if ($id !== null) {
            $this->validaIdAndExistsInTable($id, 'lawFirmId', 'law_firms', true);
        }
    }

    private function checkUserExistsOrThrowException($userData)
    {
        if (empty($userData)) {
            throw new ValidationException(self::USER_DOESNT_EXISTS);
        }
    }

    private function getUserWithPasswordHidden(User $user): User
    {
        $user->password = '';
        return $user;
    }

    public function validaIdsToDelete($ids)
    {
        foreach ($ids as $id) {
            $this->validaUserId($id);
        }

        $this->jValidator->checkErrors();
    }

    public function validaUserId($id, $checkErrors = false)
    {
        $this->validaIdAndExistsInTable($id, 'userId', 'users', true, false, $checkErrors);
    }
}