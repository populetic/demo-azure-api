<?php

namespace App\Model;

use App\Entity\Note;
use App\Repository\NotesRepository;
use App\Repository\Statia\DepartmentsRepository;
use App\Service\Exceptions\ValidationException;
use Doctrine\DBAL\Connection;

class NotesModel extends BaseModel
{
    const CONTENT_MAX_LENGTH = 500;
    const CANT_EDIT_OR_DELETE_OTHER_USER_NOTES = 'cantEditOrDeleteOtherUserNotes';

    private $statiaDB;
    private $neededParamsNoteType = ['name'];
    private $neededParamsSocialNetworkType = ['name'];

    public function __construct(Connection $populeticDB)
    {
        parent::__construct();
        $this->repository = new NotesRepository($populeticDB);
    }

    /*********************
     ******* NOTES *******
     *********************/

    public function getAllByClaimId($claimId): array
    {
        return $this->repository->getAllByClaimId($claimId);
    }

    public function getById($id): Note
    {
        return new Note($this->repository->getById($id));
    }

    public function create(Note $note): Note
    {
        $id = $this->repository->create($note);
        return $this->getById($id);
    }

    public function edit($note): Note
    {
        $this->repository->edit($note);
        return $this->getById($note->id);
    }

    public function delete($id)
    {
        $this->repository->delete($id);
    }

    public function validaCreation(Connection $statiaDB, Note $note)
    {
        $this->statiaDB = $statiaDB;

        $this->validaClaimId($note->claimId, false);
        $this->validaCommonFields($note);
        $this->jValidator->checkErrors();
    }

    public function validaEdition(Connection $statiaDB, Note $note, $userLoggedId)
    {
        $this->statiaDB = $statiaDB;

        $this->validaIdAndCheckErrors($note->id);
        $this->checkIfUserLoggedIsOwnerOrThrowException($note->id, $userLoggedId);
        $this->validaCommonFields($note);
        $this->jValidator->checkErrors();
    }

    private function validaCommonFields(Note $note)
    {
        $this->jValidator->validaString($note->content, true, 'content', 1, self::CONTENT_MAX_LENGTH);
        $this->validaNoteTypeIdExistsAndIsActive($note->noteTypeId);
        $this->validaDepartmentForIdIfIsSet($note->departmentForId);
        if ($note->socialNetworkTypeId !== null) {
            $this->validaSocialNetworkTypeIdExistsAndIsActive($note->socialNetworkTypeId);
        }
        $this->jValidator->validaString($note->clientSocialNetworkId, false, 'clientSocialNetworkId', 1, 255);
    }

    private function validaNoteTypeIdExistsAndIsActive($id)
    {
        $this->validaNoteTypeId($id);
        if (!$this->repository->idIsActiveInTable('note_types', $id)) {
            $this->jValidator->setError('noteTypeId', $this->jValidator::E_DISABLED, $id);
        }
    }

    private function validaSocialNetworkTypeIdExistsAndIsActive($id)
    {
        $this->validaSocialNetworkTypeId($id);
        if (!$this->repository->idIsActiveInTable('social_network_types', $id)) {
            $this->jValidator->setError('socialNetworkTypeId', $this->jValidator::E_DISABLED, $id);
        }
    }

    public function validaIdsToDelete($ids, $userLoggedId)
    {
        foreach ($ids as $id) {
            $this->validaIdAndCheckErrors($id);
            $this->checkIfUserLoggedIsOwnerOrThrowException($id, $userLoggedId);
        }
    }

    private function validaIdAndCheckErrors($id)
    {
        $this->validaIdAndExistsInTable($id, 'noteId', 'notes', true, false, true);
    }

    private function checkIfUserLoggedIsOwnerOrThrowException($id, $userLoggedId)
    {
        $note = $this->repository->getById($id);
        if ($note['userId'] !== $userLoggedId) {
            throw new ValidationException(self::CANT_EDIT_OR_DELETE_OTHER_USER_NOTES);
        }
    }

    private function validaDepartmentForIdIfIsSet($id)
    {
        if ($id !== null) {
            $this->jValidator->validaInt($id, true, 'departmentForId', 1);
            $departmentRepo = new DepartmentsRepository($this->statiaDB);
            if (!$departmentRepo->idExistsInTable('departments', $id)) {
                $this->jValidator->setError('departmentForId', $this->jValidator::E_DOESNT_EXISTS, $id);
            }
        }
    }



    /**********************
     ***** NOTE TYPES *****
     **********************/

    public function getAllNoteTypes(): array
    {
        return $this->repository->getAllNoteTypes();
    }

    public function getNoteTypeById($id): array
    {
        return $this->repository->getNoteTypeById($id);
    }

    public function createNoteType($noteType): array
    {
        $noteType['id'] = $this->repository->createNoteType($noteType);
        return $noteType;
    }

    public function editNoteType($noteType)
    {
        $this->repository->editNoteType($noteType);
    }

    public function deleteNoteType($id)
    {
        $this->repository->deleteNoteType($id);
    }

    public function validaNoteTypeToCreate($noteType)
    {
        $this->validaParams($noteType, $this->neededParamsNoteType);
        $this->validaNoteTypeName($noteType['name']);
        $this->jValidator->checkErrors();
    }

    public function validaNoteTypeToEdit($noteType)
    {
        $this->validaParams($noteType, $this->neededParamsNoteType);
        $this->validaNoteTypeId($noteType['id']);
        $this->validaNoteTypeName($noteType['name'], $noteType['id']);
        if (array_key_exists('isActive', $noteType)) {
            $this->jValidator->validaBoolean($noteType['isActive'], true, 'isActive');
        }
        $this->jValidator->checkErrors();
    }

    public function validaNoteTypeIdsToDelete($ids)
    {
        foreach ($ids as $id) {
            $this->validaNoteTypeId($id);
        }
        $this->jValidator->checkErrors();
    }

    private function validaNoteTypeId($id)
    {
        $this->validaIdAndExistsInTable($id, 'noteTypeId', 'note_types', true);
    }

    private function validaNoteTypeName($name, $noteTypeId = null)
    {
        $this->jValidator->validaString($name, true, 'noteTypeName', 1, 50);
        if ($this->repository->nameExistsInTable('note_types', $name, $noteTypeId)) {
            $this->jValidator->setError('noteTypeName', $this->jValidator::E_ALREADY_EXISTS, $name);
        }
    }



    /************************
     * SOCIAL NETWORK TYPES *
     ************************/

    public function getAllSocialNetworkTypes(): array
    {
        return $this->repository->getAllSocialNetworkTypes();
    }

    public function getSocialNetworkTypeById($id): array
    {
        return $this->repository->getSocialNetworkTypeById($id);
    }

    public function createSocialNetworkType($socialNetworkType): array
    {
        $socialNetworkType['id'] = $this->repository->createSocialNetworkType($socialNetworkType);
        return $socialNetworkType;
    }

    public function editSocialNetworkType($socialNetworkType)
    {
        $this->repository->editSocialNetworkType($socialNetworkType);
    }

    public function deleteSocialNetworkType($id)
    {
        $this->repository->deleteSocialNetworkType($id);
    }

    public function validaSocialNetworkTypeToCreate($socialNetworkType)
    {
        $this->validaParams($socialNetworkType, $this->neededParamsSocialNetworkType);
        $this->validaSocialNetworkTypeName($socialNetworkType['name']);
        $this->jValidator->checkErrors();
    }

    public function validaSocialNetworkTypeToEdit($socialNetworkType)
    {
        $this->validaParams($socialNetworkType, $this->neededParamsSocialNetworkType);
        $this->validaSocialNetworkTypeId($socialNetworkType['id']);
        $this->validaSocialNetworkTypeName($socialNetworkType['name'], $socialNetworkType['id']);
        if (array_key_exists('isActive', $socialNetworkType)) {
            $this->jValidator->validaBoolean($socialNetworkType['isActive'], true, 'isActive');
        }
        $this->jValidator->checkErrors();
    }

    public function validaSocialNetworkTypeIdsToDelete($ids)
    {
        foreach ($ids as $id) {
            $this->validaSocialNetworkTypeId($id);
        }
        $this->jValidator->checkErrors();
    }

    private function validaSocialNetworkTypeId($id)
    {
        $this->validaIdAndExistsInTable($id, 'socialNetworkTypeId', 'social_network_types', true);
    }

    private function validaSocialNetworkTypeName($name, $socialNetworkTypeId = null)
    {
        $this->jValidator->validaString($name, true, 'socialNetworkTypeName', 1, 50);
        if ($this->repository->nameExistsInTable('social_network_types', $name, $socialNetworkTypeId)) {
            $this->jValidator->setError('socialNetworkTypeName', $this->jValidator::E_ALREADY_EXISTS, $name);
        }
    }
}