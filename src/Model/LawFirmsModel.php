<?php

namespace App\Model;

use App\Entity\LawFirm;
use App\Repository\LawFirmsRepository;
use Doctrine\DBAL\Connection;

class LawFirmsModel extends BaseModel
{
    const NOT_CORRESPOND_TO_USER   = 'NotCorrespondToUser';

    public function __construct(Connection $populeticDB)
    {
        parent::__construct();
        $this->repository  = new LawFirmsRepository($populeticDB);
    }

    /*********************
     ***** LAW FIRMS *****
     *********************/

    public function getAll(): array
    {
        return $this->repository->getAll();
    }

    public function getById($id): LawFirm
    {
        return new LawFirm($this->repository->getById($id));
    }

    public function create(LawFirm $lawFirm): LawFirm
    {
        $id = $this->repository->create($lawFirm);
        return $this->getById($id);
    }

    public function edit(LawFirm $lawFirm): LawFirm
    {
        $this->repository->edit($lawFirm);
        return $this->getById($lawFirm->id);
    }

    public function delete($id)
    {
        $this->repository->delete($id);
    }

    public function validaLawFirm(LawFirm $lawFirm)
    {
        $this->jValidator->validaString($lawFirm->name, true, 'name', 1, 250);
        $this->checkIfNameAlreadyExists($lawFirm->name, $lawFirm->id);
        $this->validaCountryId($lawFirm->countryId);
        $this->jValidator->validaEmail($lawFirm->email, false);
        $this->jValidator->validaString($lawFirm->phone, false, 'phone', 1, 250);
        $this->jValidator->validaString($lawFirm->fax, false, 'fax', 1, 250);
        $this->jValidator->validaString($lawFirm->address, false, 'address', 0, 250);
        $this->jValidator->validaURL($lawFirm->website, false, 'website');
        $this->jValidator->checkErrors();
    }

    private function checkIfNameAlreadyExists($name, $id = null)
    {
        if ($this->repository->nameExistsInTable('law_firms', $name, $id)) {
            $this->jValidator->setError('name', $this->jValidator::E_ALREADY_EXISTS, $name);
        }
    }

    public function validaIdsToDelete($ids)
    {
        foreach ($ids as $id) {
            $this->validaLawFirmId($id);
        }

        $this->jValidator->checkErrors();
    }

    public function validaLawFirmId($id, $checkErrors = false)
    {
        $this->validaIdAndExistsInTable($id, 'lawFirmId', 'law_firms', true, false, $checkErrors);
    }



    /*********************************
     * DISRUPTION TYPES BY LAW FIRM *
     *********************************/

    private $neededParamsDisrTypesByLawFirm = ['lawFirmId', 'disruptionTypeId', 'name'];

    public function getDisrTypesByLawFirm($lawFirmId, $disruptionTypeId = null): array
    {
        return $this->repository->getDisrTypesByLawFirm($lawFirmId, $disruptionTypeId);
    }

    public function createDisrTypeByLawFirm($data)
    {
        $this->repository->createDisrTypeByLawFirm($data);
    }

    public function editDisrTypeByLawFirm($data)
    {
        $this->repository->editDisrTypeByLawFirm($data);
    }

    public function deleteDisrTypeByLawFirm($lawFirmId, $disruptionTypeId)
    {
        $this->repository->deleteDisrTypeByLawFirm($lawFirmId, $disruptionTypeId);
    }

    public function validaDisrTypeByLawFirmCreation($data)
    {
        $this->validaParams($data, $this->neededParamsDisrTypesByLawFirm);
        $this->validaLawFirmId($data['lawFirmId']);
        $this->validaIdAndExistsInTable($data['disruptionTypeId'], 'disruptionTypeId', 'disruption_types', true);
        $this->jValidator->checkErrors();

        if ($this->repository->disrTypeByLawFirmExists($data['lawFirmId'], $data['disruptionTypeId'])) {
            $this->jValidator->setError('disruptionTypeByLawFirm', $this->jValidator::E_ALREADY_EXISTS);
        }
        $this->jValidator->checkErrors();

        $this->jValidator->validaString($data['name'], true, 'name', 1, 250);
        $this->jValidator->checkErrors();
    }

    public function validaDisrTypeByLawFirmEdition($data)
    {
        $this->validaParams($data, $this->neededParamsDisrTypesByLawFirm);
        $this->jValidator->validaInt($data['lawFirmId'], true, 'lawFirmId', 1);
        $this->jValidator->validaInt($data['disruptionTypeId'], true, 'disruptionTypeId', 1);
        $this->jValidator->checkErrors();

        $this->validaDisrTypeByLawFirmExists($data['lawFirmId'], $data['disruptionTypeId']);

        $this->jValidator->validaString($data['name'], true, 'name', 1, 250);
        $this->jValidator->checkErrors();
    }

    public function validaDisrTypeByLawFirmExists($lawFirmId, $disruptionTypeId)
    {
        if (!$this->repository->disrTypeByLawFirmExists($lawFirmId, $disruptionTypeId)) {
            $this->jValidator->setError('disruptionTypeByLawFirm', $this->jValidator::E_DOESNT_EXISTS);
        }
        $this->jValidator->checkErrors();
    }

    /*********************
     * LAW FIRM ADMINISTRATORS *
     *********************/

    private $neededParamsLawFirmAdministrators = ['lawFirmId', 'userId'];

    public function getAllLawFirmAdministrators(): array
    {
        return $this->repository->getAllLawFirmAdministrators();
    }

    public function getAllLawFirmAdministratorById($lawFirmId): array
    {
        return $this->repository->getAllLawFirmAdministratorById($lawFirmId);
    }

    public function createLawFirmAdministrator($data)
    {
        $this->repository->createLawFirmAdministrator($data);
    }

    public function deleteLawFirmAdministrator($data)
    {
        $this->repository->deleteLawFirmAdministrator($data['lawFirmId'], $data['userId']);
    }

    public function validaLawFirmAdministratorCreation($data)
    {
        $this->validaParams($data, $this->neededParamsLawFirmAdministrators);
        $this->validaLawFirmId($data['lawFirmId']);
        $this->validaUserId($data['userId']);
        $this->validaCorrespondenceLawFirmIdToUserId($data['lawFirmId'], $data['userId']);
        if ($this->repository->lawFirmAdministratorExists($data['lawFirmId'], $data['userId'])) {
            $this->jValidator->setError('lawFirmAdministrator', $this->jValidator::E_ALREADY_EXISTS, $data['lawFirmId']);
        }

        $this->jValidator->checkErrors();
    }

    public function validaLawFirmAdministratorToDelete($data)
    {
        $this->validaParams($data, $this->neededParamsLawFirmAdministrators);
        $this->validaLawFirmId($data['lawFirmId']);
        $this->validaIdAndExistsInTable($data['userId'], 'userId', 'users', true, true);
        if (!$this->repository->lawFirmAdministratorExists($data['lawFirmId'], $data['userId'])) {
            $this->jValidator->setError('lawFirmAdministrator', $this->jValidator::E_DOESNT_EXISTS, $data['lawFirmId']);
        }

        $this->jValidator->checkErrors();
    }

    public function validaUserId($userId)
    {
        $this->validaIdAndExistsInTable($userId, 'userId', 'users', true, true);
        if ($this->repository->lawFirmAdministratorExists(null, $userId)) {
            $this->jValidator->setError('userId', $this->jValidator::E_ALREADY_EXISTS, $userId);
        }
    }

    public function validaCorrespondenceLawFirmIdToUserId($lawFirmId, $userId)
    {
        if (!$this->repository->validaCorrespondenceLawFirmIdToUserId($lawFirmId, $userId)) {
            $this->jValidator->setError('lawFirmId', self::NOT_CORRESPOND_TO_USER, $lawFirmId);
        }
    }
}

