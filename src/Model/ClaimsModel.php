<?php

namespace App\Model;

use App\Entity\Claim;
use App\Repository\ClaimsRepository;
use Doctrine\DBAL\Connection;

class ClaimsModel extends BaseModel
{
    const CLAIM_COMPETENCE_SUFIX = 'competence';
    const CLAIM_PLACEMENT_SUFIX  = 'placement';

    private $populeticDB;

    public function __construct(Connection $populeticDB)
    {
        parent::__construct();
        $this->repository  = new ClaimsRepository($populeticDB);
        $this->populeticDB = $populeticDB;
    }

    /********************
     ****** CLAIMS ******
     ********************/

    private $additionalInformationFields = [
        'amountInitialCompensation',
        'airlineAlternativeId',
        'luggageIssueMomentId',
        'luggageDamageDescription',
        'luggageReceivedAt'
    ];
    private $calculateEstimatedCompensationFields = ['disruptionTypeId', 'distance'];
    private $changeClaimStatusFields              = ['claimId', 'claimStatusId'];
    private $changeClaimLawFirmFields             = ['claimIds', 'lawFirmId'];

    public function getAll($filters): array
    {
        return $this->repository->getAll($filters);
    }

    public function getAllByEmailAndStatus($email, $claimStatusId): array
    {
        return $this->repository->getAllByEmailAndStatus($email, $claimStatusId);
    }

    public function getById($id): Claim
    {
        return new Claim($this->repository->getById($id));
    }

    public function getByIdExtraData($id): array
    {
        return $this->repository->getById($id, true);
    }

    public function getByRef($ref): array
    {
        return $this->repository->getByRef($ref);
    }

    public function getAllCompanionsByMainClaimId($id): array
    {
        return $this->repository->getAllCompanionsByMainClaimId($id);
    }

    public function getAllPossibleSameFlightTravelersByClaimId($id): array
    {
        $flightModel = new FlightsModel($this->populeticDB);

        $flightData     = $flightModel->getFlightDataByClaimId($id);
        $sameFlightsIds = $flightModel->getSameFlightsIdsByFlightData($flightData);

        return $this->repository->getAllPossibleSameFlightTravelersByClaimId($id, $flightData, $sameFlightsIds);
    }

    public function getAllDemandsByClaimId($id): array
    {
        return $this->repository->getAllDemandsByClaimId($id);
    }

    public function getAllBillsByClaimId($id): array
    {
        return $this->repository->getAllBillsByClaimId($id);
    }

    public function create(Claim $claim): Claim
    {
        $claim->id = $this->repository->create($claim);
        return $claim;
    }

    public function getAndUpdateLawsuiteTypeAndInsertAdditionalInformation(Claim $claim): Claim
    {
        return $this->edit($claim);
    }

    public function edit(Claim $claim): Claim
    {
        $claim->lawsuiteTypeId = $this->getLawsuiteTypeId(
            $claim->id,
            $claim->disruptionTypeId,
            $claim->flightId,
            $claim->hasConnectionFlight
        );

        $this->repository->edit($claim);
        $editedClaim = $this->getById($claim->id);
        $editedClaim->additionalInformation = $claim->additionalInformation;

        return $editedClaim;
    }

    public function delete($id)
    {
        $this->repository->delete($id);
    }

    public function getLawsuiteTypeId($claimId, $disruptionType, $flightId, $hasConnectionFlight = 0): int
    {
        $lawsuiteTypeId = self::LAWSUITE_TYPE_ID_MONTREAL;

        $flightsModel = new FlightsModel($this->populeticDB);
        $flight = $flightsModel->getById($flightId);

        $airport['departureId'] = $flight->airportDepartureId;
        $airport['arrivalId']   = $flight->airportArrivalId;

        if ($hasConnectionFlight) {
            $airport = $this->overwriteAirportDataIfHasConnectionFlight($flightsModel, $airport, $claimId);
        }

        $airportsModel            = new AirportsModel($this->populeticDB);
        $departureAirportInUE     = $airportsModel->isAirportInUE($airport['departureId']);
        $arrivalAirportInUE       = $airportsModel->isAirportInUE($airport['arrivalId']);

        $airlinesModel            = new AirlinesModel($this->populeticDB);
        $airline                  = $airlinesModel->getById($flight->airlineId);
        $airlineHasOfficeInEurope = $airlinesModel->airlineHasOfficeInEurope($flight->airlineId);

        if (!in_array($disruptionType, $this->luggageDisruptionTypes) &&
            ($departureAirportInUE || ($arrivalAirportInUE && ($airline->hasCommunityLicense || $airlineHasOfficeInEurope)))
        ) {
            $lawsuiteTypeId = self::LAWSUITE_TYPE_ID_EUROPEAN;
        }

        return $lawsuiteTypeId;
    }

    private function overwriteAirportDataIfHasConnectionFlight(FlightsModel $flightsModel, $airport, $claimId): array
    {
        $connFlights      = $flightsModel->getAllClaimConnectionFlights($claimId);
        $countConnections = count($connFlights);

        if ($countConnections > 1) {
            $airport['departureId'] = $connFlights[0]['airportDepartureId'];
            $airport['arrivalId']   = $connFlights[$countConnections-1]['airportArrivalId'];
        } elseif ($countConnections === 1) {
            $airport['departureId'] = $connFlights[0]['airportDepartureId'];
            $airport['arrivalId']   = $connFlights[0]['airportArrivalId'];
        }

        return $airport;
    }

    public function calculateEstimatedCompensation($disruptionTypeId, $distance, $delay = 0, $luggageDelay = 0): float
    {
        $compensation     = 0;
        $compensationType = $this->calculateDistanceCompensationType($distance);

        switch ($disruptionTypeId) {
            case self::DISR_TYPE_ID_DELAY:
                if ($delay >= self::DELAY_MINUTES) {
                    switch ($compensationType) {
                        case self::MIN:
                            $compensation = 250;
                            break;
                        case self::MIDDLE:
                            $compensation = 400;
                            break;
                        case self::MAX:
                            $compensation = 600;
                            break;
                    }
                } elseif ($this->isNeverArrived($delay)) {
                    $compensation = 600;
                }
                break;
            case self::DISR_TYPE_ID_CANCELLATION:
            case self::DISR_TYPE_ID_OVERBOOKING:
                switch ($compensationType) {
                    case self::MIN:
                        if (!$this->isNeverArrived($delay) && $delay < 120) {
                            $compensation = 125;
                        } else {
                            $compensation = 250;
                        }
                        break;
                    case self::MIDDLE:
                        if (!$this->isNeverArrived($delay) && $delay < 175) {
                            $compensation = 200;
                        } else {
                            $compensation = 400;
                        }
                        break;
                    case self::MAX:
                        if (!$this->isNeverArrived($delay) && $delay < 240) {
                            $compensation = 300;
                        } else {
                            $compensation = 600;
                        }
                        break;
                }
                break;
            case self::DISR_TYPE_ID_LUGGAGE_LOST:
                $compensation = 1500;
                break;
            case self::DISR_TYPE_ID_LUGGAGE_DELAY:
                $compensation = $this->calculateAmountWithLuggageReceivedDate($luggageDelay);
                break;
            case self::DISR_TYPE_ID_LUGGAGE_DAMAGE:
                $compensation = 200;
                break;
        }

        return $compensation;
    }

    private function isNeverArrived($delay)
    {
        return $delay === null;
    }

    private function calculateDistanceCompensationType($flightDistance): string
    {
        if ($flightDistance > 1500 && $flightDistance <= 3500) {
            $compensationType = self::MIDDLE;
        } else if ($flightDistance > 3500) {
            $compensationType = self::MAX;
        } else {
            $compensationType = self::MIN;
        }

        return $compensationType;
    }

    private function calculateAmountWithLuggageReceivedDate($luggageDelay): float
    {
        $maxAmountMinus21Days = 1131;
        $limitDays            = 21;

        if ($luggageDelay <= 0) {
            $amount = 0;
        } elseif ($luggageDelay < $limitDays) {
            $amount = round(($maxAmountMinus21Days / $limitDays) * $luggageDelay * 1.23, 2);
        } else {
            $amount = 1500;
        }

        return $amount;
    }

//TODO cuando hacer este check y qué conlleva, marcar como dupl en db?
//TODO hacen falta los campos de client? o también los saco de la db teniendo el claimId?
    public function checkIfDuplicatedClaim($claimId, $clientName, $clientSurnames, $clientEmail): int
    {
        $duplClaimId = 0;
        $claimData   = $this->repository->getClaimFlightDataToCheckIfDuplicated($claimId);

        if ($claimData) {
            $possibleDupl = $this->repository->getIfPossibleDuplicatedClaim(
                $claimId, $clientName, $clientSurnames, $clientEmail, $claimData
            );

            if ($possibleDupl) {
                $duplClaimId = $possibleDupl[0]['id'];
            }
        }

        return $duplClaimId;
    }

    public function countClaimsByClaimStatusId($claimStatusId): array
    {
        return [ 'count' => $this->repository->countClaimsByClaimStatusId($claimStatusId) ];
    }

    public function getOldestDateWithStatusPopuleticCollected($maxDate = null): array
    {
        return $this->repository->getOldestDateWithStatusPopuleticCollected($maxDate);
    }

    public function changeClaimStatus($claimId, $claimStatusId, $userId)
    {
        switch ($claimStatusId) {
            case ConstantsModel::CLAIM_STATUS_EXTRAJUD_AGREEMENT:
            case ConstantsModel::CLAIM_STATUS_CANCELED_WITHOUT_CHARGE:
            case ConstantsModel::CLAIM_STATUS_CANCELED_WITH_50_E:
            case ConstantsModel::CLAIM_STATUS_CANCELED_WITH_25_PERC:
                $this->repository->detachClaimLawFirm($claimId);
                break;
            case ConstantsModel::CLAIM_STATUS_ASSIGNED_LEGAL_DEPT:
                $this->repository->assignClaimToUserId($claimId, $userId);
                break;
        }

        $this->repository->changeClaimStatus($claimId, $claimStatusId);
    }

    public function changeClaimLawFirm($claimId, $lawFirmId)
    {
        $this->repository->changeClaimLawFirm($claimId, $lawFirmId);
    }

    public function createCompanionClaim($data): Claim
    {
        $mainClaim        = $this->getById($data['mainClaimId']);
        $companionClaimId = $this->repository->createCompanionClaim($mainClaim, $data['clientId']);
        $this->repository->duplicateClaimAdditionalInfoForCompanion($mainClaim->id, $companionClaimId);

        return $this->getById($companionClaimId);
    }

    public function setCompanionForClaim($mainClaimId, $companionClaimId)
    {
        $this->repository->setCompanionForClaim($mainClaimId, $companionClaimId);
    }

    public function validaFilters($filters)
    {
        $this->validaIdAndExistsInTable($filters['userId'], 'userId', 'users', true, true);

        if ($filters['finished'] !== null) {
            $this->jValidator->validaBoolean($filters['finished'], true, 'finished');
        }
        if ($filters['closed'] !== null) {
            $this->jValidator->validaBoolean($filters['closed'], true, 'closed');
        }
        if ($filters['languageId'] !== null) {
            $this->validaIdAndExistsInTable($filters['languageId'], 'languageId', 'translation_system.languages', true);
        }
        if ($filters['lawFirmId'] !== null && $filters['lawFirmId'] != 0) {
            $this->validaIdAndExistsInTable($filters['lawFirmId'], 'lawFirmId', 'law_firms', true);
        }
        if ($filters['claimStatusIds'] !== null) {
            foreach (explode(',', $filters['claimStatusIds']) as $claimStatusId) {
                $this->validaIdAndExistsInTable($claimStatusId, 'claimStatusId', 'claim_status', true);
            }
        }

        $this->jValidator->checkErrors();
    }

    public function validaEmailAndStatusId($email, $claimStatus)
    {
        $this->jValidator->validaEmail($email, true, 'email');
        $this->validaClaimStatusId($claimStatus);
        $this->jValidator->checkErrors();
    }

    public function validaClaim(Claim $claim)
    {
        $this->validaRef($claim->ref);
        $this->validaIdAndExistsInTable($claim->clientId, 'clientId', 'clients', false);
        $this->validaIdAndExistsInTable($claim->claimStatusId, 'claimStatusId', 'claim_status', true);
        $this->validaIdAndExistsInTable($claim->claimDocumentationStatusId, 'claimDocumentationStatusId', 'claim_documentation_status', true);
        $this->validaIdAndExistsInTable($claim->flightId, 'flightId', 'flights', true);
        $this->jValidator->validaBoolean($claim->isOnboardingFlight, true, 'isOnboardingFlight');
        $this->jValidator->validaString($claim->reservationNumber, false, 'reservationNumber', 1, 75);
        $this->validaIdAndExistsInTable($claim->disruptionTypeId, 'disruptionTypeId', 'disruption_types', true);
        $this->jValidator->validaFloat($claim->amountEstimated, true, 'amountEstimated');
        $this->jValidator->validaFloat($claim->amountTickets, false, 'amountTickets');
        $this->jValidator->validaFloat($claim->amountReceived, false, 'amountReceived');
        $this->jValidator->validaFloat($claim->fee, true, 'fee', 0, 100);
        $this->jValidator->validaBoolean($claim->hasConnectionFlight, true, 'hasConnectionFlight');
        $this->validaIdAndExistsInTable($claim->delayOptionId, 'delayOptionId', 'delay_options', true);
        $this->validaIdAndExistsInTable($claim->airlineReasonId, 'airlineReasonId', 'airline_reasons', true);
        $this->jValidator->validaString($claim->description, false, 'description');
        $this->validaIdAndExistsInTable($claim->mainClaimId, 'mainClaimId', 'claims', false);
        $this->jValidator->validaBoolean($claim->isRemarketingEnabled, true, 'isRemarketingEnabled');
        $this->jValidator->validaBoolean($claim->isFirstRemarketingSent, true, 'isFirstRemarketingSent');
        $this->jValidator->validaBoolean($claim->isClaimable, true, 'isClaimable');
        $this->jValidator->validaBoolean($claim->isExtrajudicialAirlineNegative, true, 'isExtrajudicialAirlineNegative');
        $this->validaIdAndExistsInTable($claim->resolutionWayId, 'resolutionWayId', 'resolution_ways', false);
        $this->validaIdAndExistsInTable($claim->lawFirmId, 'lawFirmId', 'law_firms', false);
        $this->validaIdAndExistsInTable($claim->userId, 'userId', 'users', false, true);
        $this->jValidator->validaDate($claim->finishedAt, false, 'finishedAt');
        $this->jValidator->validaDate($claim->extrajudicialSentAt, false, 'extrajudicialSentAt');

        $isLuggageDelay = $claim->disruptionTypeId === self::DISR_TYPE_ID_LUGGAGE_DELAY;

        if ($isLuggageDelay || is_array($claim->additionalInformation) ) {
            $this->validaAdditionalInformation($claim->additionalInformation, $isLuggageDelay);
        }

        $this->jValidator->checkErrors();
    }

    public function validaRef($ref, $required = false, $checkErrors = false)
    {
        $this->jValidator->validaString($ref, $required, 'ref', 1, 10);
        if ($checkErrors) $this->jValidator->checkErrors();
    }

    public function validaAdditionalInformation($addInfo, $isLuggageDelay = false)
    {
        $this->validaParams($addInfo, $this->additionalInformationFields);
        $this->jValidator->validaFloat($addInfo['amountInitialCompensation'], false, 'amountInitialCompensation');
        $this->validaIdAndExistsInTable($addInfo['airlineAlternativeId'], 'airlineAlternativeId', 'airline_alternatives', false);
        $this->validaIdAndExistsInTable($addInfo['luggageIssueMomentId'], 'luggageIssueMomentId', 'luggage_issue_moment', false);
        $this->jValidator->validaString($addInfo['luggageDamageDescription'], false, 'luggageDamageDescription');
        $this->jValidator->validaDate($addInfo['luggageReceivedAt'], $isLuggageDelay, 'luggageReceivedAt', self::DATE_FORMAT);
    }

    public function validaClaimsIds($ids, $checkErrors = false)
    {
        foreach ($ids as $id) {
            $this->validaClaimId($id);
        }

        if ($checkErrors) $this->jValidator->checkErrors();
    }

    public function validaCalculateEstimatedCompensationData($data)
    {
        $this->validaParams($data, $this->calculateEstimatedCompensationFields);
        $this->validaIdAndExistsInTable($data['disruptionTypeId'], 'disruptionTypeId', 'disruption_types', true);
        $this->jValidator->validaFloat($data['distance'], true, 'distance');
        if (array_key_exists('delay', $data) && $data['delay'] !== null) {
            $this->jValidator->validaInt($data['delay'], true, 'delay');
        }
        if (array_key_exists('luggageDelay', $data)) $this->jValidator->validaInt($data['luggageDelay'], true, 'luggageDelay');
        $this->jValidator->checkErrors();
    }

    public function validaChangeClaimStatus($data)
    {
        $this->validaParams($data, $this->changeClaimStatusFields);
        $this->validaClaimId($data['claimId']);
        $this->validaClaimStatusId($data['claimStatusId']);

        if ($data['claimStatusId'] == ConstantsModel::CLAIM_STATUS_PENDING_LEGAL_DEPT) {
            $this->validaDocumentationIsValidByClaimId($data['claimId']);
        }

        $this->jValidator->checkErrors();
    }

    public function validaChangeClaimLawFirm($data)
    {
        $this->validaParams($data, $this->changeClaimLawFirmFields);
        $this->validaClaimsIds($data['claimIds']);
        $this->validaIdAndExistsInTable($data['lawFirmId'], 'lawFirmId', 'law_firms', true);
        $this->jValidator->checkErrors();
    }

    public function validaCreateCompanionClaim($data)
    {
        $this->validaParams($data, ['clientId', 'mainClaimId']);
        $this->validaIdAndExistsInTable($data['clientId'], 'clientId', 'clients', true);
        $this->validaClaimId($data['mainClaimId']);
        $this->jValidator->checkErrors();
    }

    public function validaSetCompanionsForClaim($data)
    {
        $this->validaParams($data, ['mainClaimId', 'companionClaimsIds']);

        $this->validaClaimId($data['mainClaimId']);
        foreach ($data['companionClaimsIds'] as $companionClaimId) {
            $this->validaClaimId($companionClaimId);
        }

        $this->jValidator->checkErrors();
    }

    private function validaDocumentationIsValidByClaimId($claimId)
    {
        if ($this->repository->claimHasInvalidDocumentation($claimId)) {
            $this->jValidator->setError('claimId', 'HasInvalidDocumentation');
        }
    }



    /**********************
     **** CLAIM STATUS ****
     **********************/

    private $neededParamsClaimStatus = ['name', 'closed'];

    public function getAllClaimStatus(): array
    {
        return $this->repository->getAllClaimStatus();
    }

    public function getClaimStatusById($id): array
    {
        return $this->repository->getClaimStatusById($id);
    }

    public function getAllClaimStatusByRoleAndCountry($roleId, $countryId): array
    {
        return $this->repository->getAllClaimStatusByRoleAndCountry($roleId, $countryId);
    }

    public function createClaimStatus($data): array
    {
        $data['id'] = $this->repository->createClaimStatus($data);
        return $data;
    }

    public function editClaimStatus($data)
    {
        $this->repository->editClaimStatus($data);
    }

    public function deleteClaimStatus($id)
    {
        $this->repository->deleteClaimStatus($id);
    }

    public function validaClaimStatus($data)
    {
        $id = array_key_exists('id', $data) ? $data['id'] : null;

        $this->validaParams($data, $this->neededParamsClaimStatus);
        $this->validaClaimStatusName($data['name'], $id);
        $this->jValidator->validaBoolean($data['closed'], true, 'closed');
        $this->jValidator->checkErrors();
    }

    private function validaClaimStatusName($name, $id = null)
    {
        $this->jValidator->validaString($name, true, 'name', 1, 50);
        if ($this->repository->nameExistsInTable('claim_status', $name, $id)) {
            $this->jValidator->setError('name', $this->jValidator::E_ALREADY_EXISTS, $name);
        }
    }

    public function validaClaimStatusIdsToDelete($ids)
    {
        foreach ($ids as $id) {
            $this->validaClaimStatusId($id);
        }

        $this->jValidator->checkErrors();
    }

    public function validaClaimStatusId($id, $checkErrors = false)
    {
        $this->validaIdAndExistsInTable($id, 'claimStatusId', 'claim_status', true, false, $checkErrors);
    }

    public function validaRoleIdAndCountryId($roleId, $countryId)
    {
        $this->validaIdAndExistsInTable($countryId, 'countryId', 'countries', true);
        $this->validaIdAndExistsInTable($roleId, 'roleId', 'statia.roles', true);
        $this->jValidator->checkErrors();
    }


    /**************************
     * CLAIM STATUS COUNTRIES *
     **************************/

    private $neededParamsClaimStatusCountries = ['countryId', 'claimStatusId', 'order'];

    public function getAllClaimStatusCountries(): array
    {
        return $this->repository->getAllClaimStatusCountries();
    }

    public function getClaimStatusCountryById($id): array
    {
        return $this->repository->getClaimStatusCountryById($id);
    }

    public function createClaimStatusCountry($data): array
    {
        $data['id'] = $this->repository->createClaimStatusCountry($data);
        return $data;
    }

    public function editClaimStatusCountry($data)
    {
        $this->repository->editClaimStatusCountry($data);
    }

    public function deleteClaimStatusCountry($id)
    {
        $this->repository->deleteClaimStatusCountry($id);
    }

    public function validaClaimStatusCountry($data)
    {
        $id = array_key_exists('id', $data) ? $data['id'] : null;

        $this->validaParams($data, $this->neededParamsClaimStatusCountries);
        $this->validaCountryId($data['countryId']);
        $this->validaClaimStatusId($data['claimStatusId']);
        $this->jValidator->validaInt($data['order'], true, 'order', 0);
        $this->jValidator->checkErrors();

        $this->validaClaimStatusAlreadyExistsForCountry($data['countryId'], $data['claimStatusId'], $id);
        $this->jValidator->checkErrors();
    }

    private function validaClaimStatusAlreadyExistsForCountry($countryId, $claimStatusId, $id = null)
    {
        if ($this->repository->claimStatusExistsForCountry($countryId, $claimStatusId, $id)) {
            $this->jValidator->setError('claimStatus', $this->jValidator::E_ALREADY_EXISTS . 'ForCountryId');
        }
    }

    public function validaClaimStatusCountryIdsToDelete($ids)
    {
        foreach ($ids as $id) {
            $this->validaClaimStatusCountryId($id);
        }

        $this->jValidator->checkErrors();
    }

    public function validaClaimStatusCountryId($id, $checkErrors = false)
    {
        $this->validaIdAndExistsInTable($id, 'claimStatusCountryId', 'claim_status_countries', true, false, $checkErrors);
    }



    /**************************
     ** CLAIM STATUS BY ROLE **
     **************************/

    private $neededParamsClaimStatusByRole = ['claimStatusCountryId', 'roleId', 'isReadable', 'isEditable'];

    public function getAllClaimStatusByRole(): array
    {
        return $this->repository->getAllClaimStatusByRole();
    }

    public function getClaimStatusByRole($claimStatusCountryId, $roleId): array
    {
        return $this->repository->getClaimStatusByRole($claimStatusCountryId, $roleId);
    }

    public function createClaimStatusByRole($data)
    {
        $this->repository->createClaimStatusByRole($data);
    }

    public function editClaimStatusByRole($data)
    {
        $this->repository->editClaimStatusByRole($data);
    }

    public function deleteClaimStatusByRole($claimStatusCountryId, $roleId)
    {
        $this->repository->deleteClaimStatusByRole($claimStatusCountryId, $roleId);
    }

    public function validaClaimStatusByRole($data, $new = true)
    {
        $this->validaParams($data, $this->neededParamsClaimStatusByRole);
        $this->validaIdAndExistsInTable($data['claimStatusCountryId'], 'claimStatusCountryId', 'claim_status_countries', true);
        $this->validaIdAndExistsInTable($data['roleId'], 'roleId', 'statia.roles', true);
        $this->jValidator->validaBoolean($data['isReadable'], true, 'isReadable');
        $this->jValidator->validaBoolean($data['isEditable'], true, 'isEditable');
        $this->jValidator->checkErrors();

        if ($new && $this->repository->claimStatusByRoleExists($data['claimStatusCountryId'], $data['roleId'])) {
            $this->jValidator->setError('claimStatusByRole', $this->jValidator::E_ALREADY_EXISTS);
        } elseif (!$new && !$this->repository->claimStatusByRoleExists($data['claimStatusCountryId'], $data['roleId'])) {
            $this->jValidator->setError('claimStatusByRole', $this->jValidator::E_DOESNT_EXISTS);
        }
        $this->jValidator->checkErrors();
    }

    public function validaClaimStatusByRoleExists($claimStatusCountryId, $roleId, $checkErrors = false)
    {
        $this->jValidator->validaInt($claimStatusCountryId, true, 'claimStatusCountryId', 1);
        $this->validaRoleId($roleId);
        if ($checkErrors) $this->jValidator->checkErrors();

        if (!$this->repository->claimStatusByRoleExists($claimStatusCountryId, $roleId)) {
            $this->jValidator->setError('claimStatusByRole', $this->jValidator::E_DOESNT_EXISTS);
        }
        if ($checkErrors) $this->jValidator->checkErrors();
    }

    public function validaRoleId($id, $checkErrors = false)
    {
        $this->jValidator->validaInt($id, true, 'roleId', 1);
        if ($checkErrors) $this->jValidator->checkErrors();
    }



    /***********************************
     * CLAIMS COMPETENCE AND PLACEMENT *
     ***********************************/

    private $paramsClaimCompetenceAndPlacement = ['claimId', 'countryId'];

    /** COMPETENCE */
    public function getAllClaimsCompetence($claimId): array
    {
        return $this->repository->getAllClaimsCompetenceOrPlacement($claimId, self::CLAIM_COMPETENCE_SUFIX);
    }

    public function getClaimCompetence($claimId, $countryId): array
    {
        return $this->repository->getClaimCompetenceOrPlacement($claimId, $countryId, self::CLAIM_COMPETENCE_SUFIX);
    }

    public function createClaimCompetence($data)
    {
        $this->repository->createClaimCompetenceOrPlacement($data, self::CLAIM_COMPETENCE_SUFIX);
    }

    public function editClaimCompetence($data)
    {
        $this->repository->editClaimCompetenceOrPlacement($data, self::CLAIM_COMPETENCE_SUFIX);
    }

    public function deleteClaimCompetence($claimId, $countryId)
    {
        $this->repository->deleteClaimCompetenceOrPlacement($claimId, $countryId, self::CLAIM_COMPETENCE_SUFIX);
    }

    public function validaClaimCompetence($data, $new = true)
    {
        $this->validaClaimCompetenceAndPlacementCommonFields($data);

        if ($new && $this->repository->claimCompetenceOrPlacementExists(
                $data['claimId'],
                $data['countryId'],
                self::CLAIM_COMPETENCE_SUFIX
            )
        ) {
            $this->jValidator->setError('claimCompetence', $this->jValidator::E_ALREADY_EXISTS);

        } elseif (!$new && !$this->repository->claimCompetenceOrPlacementExists(
                $data['claimId'],
                $data['countryId'],
                self::CLAIM_COMPETENCE_SUFIX
            )
        ) {
            $this->jValidator->setError('claimCompetence', $this->jValidator::E_DOESNT_EXISTS);
        }

        $this->jValidator->checkErrors();
    }

    public function validaClaimCompetenceExists($claimId, $countryId, $checkErrors = false)
    {
        $this->jValidator->validaInt($claimId, true, 'claimId', 1);
        $this->jValidator->validaInt($countryId, true, 'countryId', 1);
        if ($checkErrors) $this->jValidator->checkErrors();

        if (!$this->repository->claimCompetenceOrPlacementExists($claimId, $countryId, self::CLAIM_COMPETENCE_SUFIX)) {
            $this->jValidator->setError('claimCompetence', $this->jValidator::E_DOESNT_EXISTS);
        }
        if ($checkErrors) $this->jValidator->checkErrors();
    }

    private function validaClaimCompetenceAndPlacementCommonFields($data)
    {
        $this->validaParams($data, $this->paramsClaimCompetenceAndPlacement);
        $this->validaClaimId($data['claimId']);
        $this->validaCountryId($data['countryId']);
        if (array_key_exists('isSelected', $data)) {
            $this->jValidator->validaBoolean($data['isSelected'], true, 'isSelected');
        }
        if (array_key_exists('isSelectable', $data)) {
            $this->jValidator->validaBoolean($data['isSelectable'], true, 'isSelectable');
        }

        $this->jValidator->checkErrors();
    }

    /** PLACEMENT */
    public function getAllClaimsPlacement($claimId): array
    {
        return $this->repository->getAllClaimsCompetenceOrPlacement($claimId, self::CLAIM_PLACEMENT_SUFIX);
    }

    public function getClaimPlacement($claimId, $countryId): array
    {
        return $this->repository->getClaimCompetenceOrPlacement($claimId, $countryId, self::CLAIM_PLACEMENT_SUFIX);
    }

    public function createClaimPlacement($data)
    {
        $this->repository->createClaimCompetenceOrPlacement($data, self::CLAIM_PLACEMENT_SUFIX);
    }

    public function editClaimPlacement($data)
    {
        $this->repository->editClaimCompetenceOrPlacement($data, self::CLAIM_PLACEMENT_SUFIX);
    }

    public function deleteClaimPlacement($claimId, $countryId)
    {
        $this->repository->deleteClaimCompetenceOrPlacement($claimId, $countryId, self::CLAIM_PLACEMENT_SUFIX);
    }

    public function validaClaimPlacement($data, $new = true)
    {
        $this->validaClaimCompetenceAndPlacementCommonFields($data);

        if ($new && $this->repository->claimCompetenceOrPlacementExists(
                $data['claimId'],
                $data['countryId'],
                self::CLAIM_PLACEMENT_SUFIX
            )
        ) {
            $this->jValidator->setError('claimPlacement', $this->jValidator::E_ALREADY_EXISTS);

        } elseif (!$new && !$this->repository->claimCompetenceOrPlacementExists(
                $data['claimId'],
                $data['countryId'],
                self::CLAIM_PLACEMENT_SUFIX
            )
        ) {
            $this->jValidator->setError('claimPlacement', $this->jValidator::E_DOESNT_EXISTS);
        }

        $this->jValidator->checkErrors();
    }

    public function validaClaimPlacementExists($claimId, $countryId, $checkErrors = false)
    {
        $this->jValidator->validaInt($claimId, true, 'claimId', 1);
        $this->jValidator->validaInt($countryId, true, 'countryId', 1);
        if ($checkErrors) $this->jValidator->checkErrors();

        if (!$this->repository->claimCompetenceOrPlacementExists($claimId, $countryId, self::CLAIM_PLACEMENT_SUFIX)) {
            $this->jValidator->setError('claimPlacement', $this->jValidator::E_DOESNT_EXISTS);
        }
        if ($checkErrors) $this->jValidator->checkErrors();
    }

    /** SET COMPETENCES/PLACEMENT SEGÚN CLAIM */
    public function setClaimCompetencesAndPlacementsByClaimId($claimId)
    {
        $countriesCompetences = $this->getCountriesWeHaveCompetenceByClaimId($claimId);
        $countriesPlacement   = $this->getCountriesWeHavePlacementByClaimId($claimId);

        $this->repository->deleteAllClaimCompetencesAndPlacements($claimId);
        $this->insertCompetencesAndPlacements($claimId, $countriesCompetences, $countriesPlacement);
        $this->updateWhichCompetenceAndPlacementIsSelected($claimId, $countriesCompetences, $countriesPlacement);
        $this->updateWhichCompetencesAreNotSelectable($claimId, $countriesCompetences, $countriesPlacement);
        $this->updateWhichPlacementsAreNotSelectable($claimId, $countriesCompetences, $countriesPlacement);
    }

    private function getCountriesWeHaveCompetenceByClaimId($claimId): array
    {
        $flightsModel = new FlightsModel($this->populeticDB);

        $claim  = $this->getByIdExtraData($claimId);
        $flight = $flightsModel->getById($claim['flightId']);

        return $this->countriesWeHaveCompetenceByClaimData($claim['lawsuiteTypeId'], $claim['clientCountryId'], $flight);
    }

    private function getCountriesWeHavePlacementByClaimId($claimId): array
    {
        $claim = $this->getByIdExtraData($claimId);
        return $this->countriesWeHavePlacementByAirlineId($claim['airlineId']);
    }

    private function insertCompetencesAndPlacements($claimId, $countriesCompetences, $countriesPlacement)
    {
        foreach ($countriesCompetences as $countryId) {
            $this->createClaimCompetence([ 'claimId' => $claimId, 'countryId' => $countryId ]);
        }

        foreach ($countriesPlacement as $countryId) {
            $this->createClaimPlacement([ 'claimId' => $claimId, 'countryId' => $countryId ]);
        }
    }

    private function updateWhichCompetenceAndPlacementIsSelected($claimId, $countriesCompetences, $countriesPlacement)
    {
        $countriesInBoth = array_values(array_intersect($countriesCompetences, $countriesPlacement));
        if (count($countriesInBoth) > 0) {
            $this->editClaimCompetence([
                'claimId'      => $claimId,
                'countryId'    => $countriesInBoth[0],
                'isSelected'   => 1
            ]);
            $this->editClaimPlacement([
                'claimId'      => $claimId,
                'countryId'    => $countriesInBoth[0],
                'isSelected'   => 1
            ]);
        }
    }

    private function updateWhichCompetencesAreNotSelectable($claimId, $countriesCompetences, $countriesPlacement)
    {
        $countriesCompetenceNotInPlacement = array_diff($countriesCompetences, $countriesPlacement);
        foreach ($countriesCompetenceNotInPlacement as $countryId) {
            $this->editClaimCompetence([
                'claimId'      => $claimId,
                'countryId'    => $countryId,
                'isSelectable' => 0
            ]);
        }
    }

    private function updateWhichPlacementsAreNotSelectable($claimId, $countriesCompetences, $countriesPlacement)
    {
        $countriesPlacementNotInCompetence = array_diff($countriesPlacement, $countriesCompetences);
        foreach ($countriesPlacementNotInCompetence as $countryId) {
            $this->editClaimPlacement([
                'claimId'      => $claimId,
                'countryId'    => $countryId,
                'isSelectable' => 0
            ]);
        }
    }

    private function countriesWeHaveCompetenceByClaimData($lawsuiteTypeId, $clientCountryId, $flight): array
    {
        $countriesWeHaveCompetenceForThisClaim = [];

        $countriesModel = new CountriesModel($this->populeticDB);
        $airlinesModel  = new AirlinesModel($this->populeticDB);

        foreach ($countriesModel->getCountriesWeHaveCompetence() as $countryId) {
            if ($airlinesModel->airlineHasHeadquartersAddressInCountry($flight->airlineId, $countryId)) {
                $countriesWeHaveCompetenceForThisClaim[] = $countryId;

            } else {
                if ($flight->airportArrivalCountryId === $countryId) {
                    $countriesWeHaveCompetenceForThisClaim[] = $countryId;

                } else {
                    if ($countryId !== self::COUNTRY_ID_SPAIN &&
                        $lawsuiteTypeId !== self::LAWSUITE_TYPE_ID_MONTREAL
                    ) {
                        if ($flight->airportDepartureCountryId === $countryId) {
                            $countriesWeHaveCompetenceForThisClaim[] = $countryId;
                        }

                    } elseif ($countryId === self::COUNTRY_ID_SPAIN) {
                        if ($flight->airportDepartureCountryId === $countryId || $clientCountryId === $countryId) {
                            $countriesWeHaveCompetenceForThisClaim[] = $countryId;
                        }
                    }
                }
            }
        }

        return $countriesWeHaveCompetenceForThisClaim;
    }

    private function countriesWeHavePlacementByAirlineId($airlineId): array
    {
        $countriesWeHavePlacementForThisAirline = [];

        $countriesModel = new CountriesModel($this->populeticDB);
        $airlinesModel  = new AirlinesModel($this->populeticDB);

        foreach ($countriesModel->getCountriesWeHaveCompetence() as $countryId) {
            if ($airlinesModel->airlineHasBranchAddressInCountry($airlineId, $countryId)) {
                $countriesWeHavePlacementForThisAirline[] = $countryId;
            }
        }

        return $countriesWeHavePlacementForThisAirline;
    }



    /**********************
     ** CLAIM DOC STATUS **
     **********************/

    private $neededParamsClaimDocStatus = ['name'];

    public function getAllClaimDocStatus(): array
    {
        return $this->repository->getAllClaimDocStatus();
    }

    public function getClaimDocStatusById($id): array
    {
        return $this->repository->getClaimDocStatusById($id);
    }

    public function createClaimDocStatus($data): array
    {
        $data['id'] = $this->repository->createClaimDocStatus($data);
        return $data;
    }

    public function editClaimDocStatus($data)
    {
        $this->repository->editClaimDocStatus($data);
    }

    public function deleteClaimDocStatus($id)
    {
        $this->repository->deleteClaimDocStatus($id);
    }

    public function validaClaimDocStatus($data)
    {
        $id = array_key_exists('id', $data) ? $data['id'] : null;

        $this->validaParams($data, $this->neededParamsClaimDocStatus);
        $this->validaClaimDocStatusName($data['name'], $id);
        $this->jValidator->checkErrors();
    }

    private function validaClaimDocStatusName($name, $id = null)
    {
        $this->jValidator->validaString($name, true, 'name', 1, 50);
        if ($this->repository->nameExistsInTable('claim_documentation_status', $name, $id)) {
            $this->jValidator->setError('name', $this->jValidator::E_ALREADY_EXISTS, $name);
        }
    }

    public function validaClaimDocStatusIdsToDelete($ids)
    {
        foreach ($ids as $id) {
            $this->validaClaimDocStatusId($id);
        }

        $this->jValidator->checkErrors();
    }

    public function validaClaimDocStatusId($id, $checkErrors = false)
    {
        $this->validaIdAndExistsInTable(
            $id, 'claimDocumentationStatusId', 'claim_documentation_status', true, false, $checkErrors
        );
    }



    /****************************
     * CLAIM DOC STATUS BY ROLE *
     ****************************/

    private $neededParamsClaimDocStatusByRole = ['claimDocumentationStatusId', 'roleId'];

    public function getAllClaimDocStatusByRole($roleId): array
    {
        return $this->repository->getAllClaimDocStatusByRole($roleId);
    }

    public function createClaimDocStatusByRole($data)
    {
        $this->repository->createClaimDocStatusByRole($data);
    }

    public function deleteClaimDocStatusByRole($claimDocStatusId, $roleId)
    {
        $this->repository->deleteClaimDocStatusByRole($claimDocStatusId, $roleId);
    }

    public function validaNewClaimDocStatusByRole($data)
    {
        $this->validaParams($data, $this->neededParamsClaimDocStatusByRole);
        $this->validaIdAndExistsInTable(
            $data['claimDocumentationStatusId'],
            'claimDocumentationStatusId',
            'claim_documentation_status',
            true
        );
        $this->validaIdAndExistsInTable($data['roleId'], 'roleId', 'statia.roles', true);
        $this->jValidator->checkErrors();

        if ($this->repository->claimDocStatusByRoleExists($data['claimDocumentationStatusId'], $data['roleId'])) {
            $this->jValidator->setError('claimDocumentationStatusByRole', $this->jValidator::E_ALREADY_EXISTS);
        }
        $this->jValidator->checkErrors();
    }

    public function validaClaimDocStatusByRoleExists($claimDocStatusId, $roleId, $checkErrors = false)
    {
        $this->jValidator->validaInt($claimDocStatusId, true, 'claimDocumentationStatusId', 1);
        $this->validaRoleId($roleId);
        if ($checkErrors) $this->jValidator->checkErrors();

        if (!$this->repository->claimDocStatusByRoleExists($claimDocStatusId, $roleId)) {
            $this->jValidator->setError('claimDocumentationStatusByRole', $this->jValidator::E_DOESNT_EXISTS);
        }
        if ($checkErrors) $this->jValidator->checkErrors();
    }
}