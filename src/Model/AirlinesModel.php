<?php

namespace App\Model;

use App\Entity\Airline;
use App\Repository\AirlinesRepository;
use Doctrine\DBAL\Connection;

class AirlinesModel extends BaseModel
{
    public function __construct(Connection $populeticDB)
    {
        parent::__construct();
        $this->repository = new AirlinesRepository($populeticDB);
    }

    /*********************
     ****** AIRLINES ******
     *********************/

    public function getAll(): array
    {
        return $this->repository->getAll();
    }

    public function getById($id): Airline
    {
        return new Airline($this->repository->getById($id));
    }

    public function create(Airline $airline): Airline
    {
        $id = $this->repository->create($airline);
        return $this->getById($id);
    }

    public function edit(Airline $airline): Airline
    {
        $this->repository->edit($airline);
        return $this->getById($airline->id);
    }

    public function delete($id)
    {
        $this->repository->delete($id);
    }

    public function validaCreation(Airline $airline)
    {
        $this->validaCommonFields($airline);
        $this->jValidator->validaBoolean($airline->isActive, false, 'isActive');
        $this->jValidator->checkErrors();
    }

    public function validaEdition(Airline $airline)
    {
        $this->validaAirlineId($airline->id);
        $this->validaCommonFields($airline);
        $this->jValidator->validaBoolean($airline->isActive, true, 'isActive');
        $this->jValidator->checkErrors();
    }

    private function validaCommonFields(Airline $airline)
    {
        $this->jValidator->validaString($airline->name, true, 'name', 1, 250);
        $this->jValidator->validaString($airline->iata, false, 'iata', 1, 250);
        $this->jValidator->validaString($airline->icao, false, 'icao', 1, 250);
        $this->jValidator->validaString($airline->flightstatsIata, false, 'flightstatsIata', 1, 250);
        $this->jValidator->validaBoolean($airline->isBankruptcy, true, 'isBankruptcy');
        $this->jValidator->validaBoolean($airline->hasCommunityLicense, true, 'hasCommunityLicense');
    }

    public function validaIdsToDelete($ids)
    {
        foreach ($ids as $id) {
            $this->validaAirlineId($id);
        }

        $this->jValidator->checkErrors();
    }



    /************************************
     ** AIRLINES LEGAL DATA BY COUNTRY **
     ************************************/

    private $neededParamsAirlineCountryLegalData = [
        'airlineId',
        'countryId',
        'languageId',
        'email',
        'fax',
        'branchAddress',
        'headquartersAddress',
        'isJudicialEnabled',
        'hasNonInterestAgreement'
    ];

    public function getAllCountryLegalDataByAirlineId($airlineId): array
    {
        return $this->repository->getAllCountryLegalDataByAirlineId($airlineId);
    }

    public function getLegalDataByAirlineAndCountry($airlineId, $countryId): array
    {
        return $this->repository->getLegalDataByAirlineAndCountry($airlineId, $countryId);
    }

    public function createLegalData($data)
    {
        $this->repository->createLegalData($data);
    }

    public function editLegalData($data)
    {
        $this->repository->editLegalData($data);
    }

    public function deleteLegalData($airlineId, $countryId)
    {
        $this->repository->deleteLegalData($airlineId, $countryId);
    }

    public function airlineHasOfficeInEurope($airlineId): bool
    {
        return $this->repository->hasAirlineOfficeInEurope($airlineId);
    }

    public function airlineHasHeadquartersAddressInCountry($airlineId, $countryId): bool
    {
        return $this->repository->airlineHasAddressInCountryByAddressType($airlineId, $countryId, 'headquarters_address');
    }

    public function airlineHasBranchAddressInCountry($airlineId, $countryId): bool
    {
        return $this->repository->airlineHasAddressInCountryByAddressType($airlineId, $countryId, 'branch_address');
    }

    public function validaLegalDataCreation($data)
    {
        $this->validaParams($data, $this->neededParamsAirlineCountryLegalData);
        $this->validaAirlineId($data['airlineId']);
        $this->validaCountryId($data['countryId']);
        $this->validaLegalDataDoesntExistsForAirlineAndCountry($data['airlineId'], $data['countryId']);
        $this->validaLegalCommonData($data);
        $this->jValidator->checkErrors();
    }

    public function validaLegalDataEdition($data)
    {
        $this->validaParams($data, $this->neededParamsAirlineCountryLegalData);
        $this->validaLegalDataAlreadyExistsForAirlineAndCountry($data['airlineId'], $data['countryId']);
        $this->validaLegalCommonData($data);
        $this->jValidator->checkErrors();
    }

    public function validaLegalDataDeletion($airlineId, $countryId)
    {
        $this->jValidator->validaInt($airlineId, true, 'airlineId', 1);
        $this->jValidator->validaInt($countryId, true, 'countryId', 1);
        $this->validaLegalDataAlreadyExistsForAirlineAndCountry($airlineId, $countryId);
        $this->jValidator->checkErrors();
    }

    private function validaLegalCommonData($data)
    {
        $this->validaLanguageId($data['languageId']);
        $this->jValidator->validaEmail($data['email'], false, 'email');
        $this->jValidator->validaString($data['fax'], false, 'fax', 1, 50);
        $this->jValidator->validaString($data['branchAddress'], false, 'branchAddress', 1, 250);
        $this->jValidator->validaString($data['headquartersAddress'], false, 'headquartersAddress', 1, 250);
        $this->jValidator->validaBoolean($data['isJudicialEnabled'], true, 'isJudicialEnabled');
        $this->jValidator->validaBoolean($data['hasNonInterestAgreement'], true, 'hasNonInterestAgreement');
    }

    private function validaLanguageId($id)
    {
        $this->validaIdAndExistsInTable($id, 'languageId', 'translation_system.languages', true);
    }

    private function validaLegalDataDoesntExistsForAirlineAndCountry($airlineId, $countryId)
    {
        if ($this->repository->legalDataExistsForAirlineAndCountry($airlineId, $countryId)) {
            $this->jValidator->setError('legalData', $this->jValidator::E_ALREADY_EXISTS);
        }
    }

    private function validaLegalDataAlreadyExistsForAirlineAndCountry($airlineId, $countryId)
    {
        if (!$this->repository->legalDataExistsForAirlineAndCountry($airlineId, $countryId)) {
            $this->jValidator->setError('legalData', $this->jValidator::E_DOESNT_EXISTS);
        }
    }



    /************************************
     * AIRLINES REQUIRED DOC BY COUNTRY *
     ************************************/

    private $neededParamsAirlineRequiredDoc = ['airlineId', 'countryId', 'documentationTypeId'];

    public function getAllCountryRequiredDocByAirlineId($airlineId): array
    {
        $data = [];
        foreach ($this->repository->getAllCountryRequiredDocByAirlineId($airlineId) as $doc) {
            $data[$doc['countryId']]['countryName'] = $doc['countryName'];
            $data[$doc['countryId']]['documentationTypes'][$doc['documentationTypeId']] = $doc['documentationTypeName'];
        }

        return $data;
    }

    public function createAirlineRequiredDoc($data)
    {
        $this->repository->createAirlineRequiredDoc($data);
    }

    public function deleteAirlineRequiredDoc($data)
    {
        $this->repository->deleteAirlineRequiredDoc($data);
    }

    public function validaAirlineRequiredDocCreation($data)
    {
        $this->validaParams($data, $this->neededParamsAirlineRequiredDoc);
        $this->validaAirlineId($data['airlineId']);
        $this->validaCountryId($data['countryId']);
        $this->validaDocumentationTypeId($data['documentationTypeId']);
        $this->validaAirlineRequiredDocDoesntExists($data);
        $this->jValidator->checkErrors();
    }

    public function validaAirlineRequiredDocDeletion($data)
    {
        $this->jValidator->validaInt($data['airlineId'], true, 'airlineId', 1);
        $this->jValidator->validaInt($data['countryId'], true, 'countryId', 1);
        $this->jValidator->validaInt($data['documentationTypeId'], true, 'documentationTypeId', 1);
        $this->validaAirlineRequiredDocAlreadyExists($data);
        $this->jValidator->checkErrors();
    }

    private function validaDocumentationTypeId($id)
    {
        $this->jValidator->validaInt($id, true, 'documentationTypeId', 1);
        if ($id && !$this->repository->idExistsInTable('documentation_types', $id)) {
            $this->jValidator->setError('documentationTypeId', $this->jValidator::E_DOESNT_EXISTS, $id);
        }
    }

    private function validaAirlineRequiredDocDoesntExists($data)
    {
        if ($this->repository->airlineRequiredDocExists($data)) {
            $this->jValidator->setError('airlineRequiredDoc', $this->jValidator::E_ALREADY_EXISTS);
        }
    }

    private function validaAirlineRequiredDocAlreadyExists($data)
    {
        if (!$this->repository->airlineRequiredDocExists($data)) {
            $this->jValidator->setError('airlineRequiredDoc', $this->jValidator::E_DOESNT_EXISTS);
        }
    }
}