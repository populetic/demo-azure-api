<?php

namespace App\Model;

use App\Entity\Region;
use App\Repository\RegionsRepository;
use Doctrine\DBAL\Connection;

class RegionsModel extends BaseModel
{
    const CODE_LENGTH = 3;

    public function __construct(Connection $populeticDB)
    {
        parent::__construct();
        $this->repository = new RegionsRepository($populeticDB);
    }

    public function getAll(): array
    {
        return $this->repository->getAll();
    }

    public function getById($id): Region
    {
        return new Region($this->repository->getById($id));
    }

    public function create(Region $region): Region
    {
        $id = $this->repository->create($region);
        return $this->getById($id);
    }

    public function edit(Region $region): Region
    {
        $this->repository->edit($region);
        return $this->getById($region->id);
    }

    public function delete($id)
    {
        $this->repository->delete($id);
    }

    public function validaCreation(Region $region)
    {
        $this->validaCommonFields($region);
        $this->jValidator->checkErrors();
    }

    public function validaEdition(Region $region)
    {
        $this->validaId($region->id);
        $this->validaCommonFields($region);
        $this->jValidator->validaBoolean($region->isActive, true, 'isActive');
        $this->jValidator->checkErrors();
    }

    private function validaCommonFields(Region $region)
    {
        $this->jValidator->validaString($region->name, true, 'name', 1, 250);
        $this->checkIfNameAlreadyExists($region->name, $region->id);
        $this->jValidator->validaString($region->code, true, 'code', self::CODE_LENGTH, self::CODE_LENGTH);
        $this->checkIfCodeAlreadyExists($region->code, $region->id);
        $this->jValidator->validaString($region->shortname, true, 'shortname', 1, 100);
        $this->checkIfShortnameAlreadyExists($region->shortname, $region->id);
        $this->jValidator->validaString($region->metaregion, true, 'metaregion', 1, 25);
    }

    private function checkIfNameAlreadyExists($name, $id = null)
    {
        if ($this->repository->nameExistsInTable('regions', $name, $id)) {
            $this->jValidator->setError('name', $this->jValidator::E_ALREADY_EXISTS, $name);
        }
    }

    private function checkIfCodeAlreadyExists($code, $id = null)
    {
        if ($this->repository->codeAlreadyExists($code, $id)) {
            $this->jValidator->setError('code', $this->jValidator::E_ALREADY_EXISTS, $code);
        }
    }

    private function checkIfShortnameAlreadyExists($shortname, $id = null)
    {
        if ($this->repository->shortnameAlreadyExists($shortname, $id)) {
            $this->jValidator->setError('shortname', $this->jValidator::E_ALREADY_EXISTS, $shortname);
        }
    }

    public function validaIdsToDelete($ids)
    {
        foreach ($ids as $id) {
            $this->validaId($id);
        }

        $this->jValidator->checkErrors();
    }

    public function validaId($id, $checkErrors = false)
    {
        $this->validaIdAndExistsInTable($id, 'regionId', 'regions', true, false, $checkErrors);
    }
}