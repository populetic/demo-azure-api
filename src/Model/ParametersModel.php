<?php

namespace App\Model;

use App\Repository\ParametersRepository;
use Doctrine\DBAL\Connection;

class ParametersModel extends BaseModel
{
    /**
     * Model para tablas auxiliares con campos id y name
     */

    const DISRUPTION_TYPES      = 'disruption_types';
    const DELAY_OPTIONS         = 'delay_options';
    const AIRLINE_REASONS       = 'airline_reasons';
    const AIRLINE_ALTERNAT      = 'airline_alternatives';
    const AIRLINE_COMP_ALTERNAT = 'airline_compensation_alternatives';
    const LAWSUITE_TYPES        = 'lawsuite_types';
    const RESOLUTION_WAYS       = 'resolution_ways';
    const LUGGAGE_ISSUE_MOMENT  = 'luggage_issue_moment';

    public function __construct(Connection $populeticDB)
    {
        parent::__construct();
        $this->repository  = new ParametersRepository($populeticDB);
    }

    private $neededParams = ['name'];

    public function getAll($table): array
    {
        return $this->repository->getAll($table);
    }

    public function getById($table, $id): array
    {
        return $this->repository->getById($table, $id);
    }

    public function create($table, $data): array
    {
        $data['id'] = $this->repository->create($table, $data);
        return $data;
    }

    public function edit($table, $data)
    {
        $this->repository->edit($table, $data);
    }

    public function delete($table, $id)
    {
        $this->repository->delete($table, $id);
    }

    public function validaName($table, $data)
    {
        $id = array_key_exists('id', $data) ? $data['id'] : null;

        $this->validaParams($data, $this->neededParams);

        $nameLength = ($table === self::AIRLINE_REASONS) ? 100 : 45;
        $this->jValidator->validaString($data['name'], true, 'name', 1, $nameLength);
        if ($this->repository->nameExistsInTable($table, $data['name'], $id)) {
            $this->jValidator->setError('name', $this->jValidator::E_ALREADY_EXISTS, $data['name']);
        }

        $this->jValidator->checkErrors();
    }

    public function validaIdsToDelete($table, $ids)
    {
        foreach ($ids as $id) {
            $this->validaId($table, $id);
        }

        $this->jValidator->checkErrors();
    }

    public function validaId($table, $id, $checkErrors = false)
    {
        $this->validaIdAndExistsInTable($id, 'id', $table, true, false, $checkErrors);
    }
}