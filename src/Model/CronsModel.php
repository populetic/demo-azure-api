<?php

namespace App\Model;

use App\Repository\CronsRepository;
use Doctrine\DBAL\Connection;

class CronsModel extends BaseModel
{
    public function __construct(Connection $populeticDB)
    {
        parent::__construct();
        $this->repository = new CronsRepository($populeticDB);
    }

    public function getYesterdayStatusToClientCollectOrCancelWithCharge(): array
    {
        return $this->repository->getYesterdayStatusToClientCollectOrCancelWithCharge();
    }

    public function parseYesterdayStatusToCollectOrChargeClaimsToHTML($claims, $yesterday): string
    {
        $claimsList = '
            <html><body>
                <p>Listado de reclamaciones con cambios de estado a Cobrado Cliente y Cancelación con 
                cargo del día anterior, ' . $yesterday . ':</p>
                <table>
                    <tr>
                      <td><strong>Id Reclamación</strong></td>
                      <td><strong>Nombre</strong></td>
                      <td><strong>Email</strong></td>
                    </tr>';

        foreach ($claims as $claim) {
            $claimsList .= '
                <tr>
                    <td>' . $claim['claimId'] . '</td>
                    <td>' . $claim['clientName'] . $claim['clientSurnames'] . '</td>
                    <td>' . $claim['email'] . '</td>
                </tr>
            ';
        }

        return $claimsList . '</table></body></html>';
    }
}