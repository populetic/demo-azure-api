<?php

namespace App\Model;

use App\Entity\Flight;
use App\Repository\FlightsRepository;
use Doctrine\DBAL\Connection;

class FlightsModel extends BaseModel
{
    const FLIGHTS_TABLE = 'flights';

    private $populeticDB;

    public function __construct(Connection $populeticDB)
    {
        parent::__construct();
        $this->repository = new FlightsRepository($populeticDB);
        $this->populeticDB = $populeticDB;
    }

    /*********************
     ****** FLIGHTS ******
     *********************/

    public function getFlightsByPageAndItemsPerPage($page, $itemsPerPage): array
    {
        return $this->repository->getFlightsByPageAndItemsPerPage($page, $itemsPerPage);
    }

    public function getTotalFlightsCount(): int
    {
        return $this->repository->getTotalCountByTable(self::FLIGHTS_TABLE);
    }

    public function getById($id): Flight
    {
        return new Flight($this->repository->getById($id));
    }

    public function getFlightDataByClaimId($claimId): array
    {
        return $this->repository->getFlightDataByClaimId($claimId);
    }

    public function getSameFlightsIdsByFlightData($flightData): array
    {
        return $this->repository->getSameFlightsIdsByFlightData($flightData);
    }

    public function create(Flight $flight): Flight
    {
        $flight->distance = $this->getDistanceBetweenAirports($flight->airportDepartureId, $flight->airportArrivalId);

        $id = $this->repository->create($flight);
        return $this->getById($id);
    }

    public function editClaimWithNewFlightId($claimId, $newFlightId)
    {
        $this->repository->editClaimWithNewFlightId($claimId, $newFlightId);
    }

    public function edit(Flight $flight, Flight $oldFlight): Flight
    {
        if ($flight->airportDepartureId === null) {
            $flight->airportDepartureId = $oldFlight->airportDepartureId;
        }
        if ($flight->airportArrivalId === null) {
            $flight->airportArrivalId = $oldFlight->airportArrivalId;
        }

        $flight->distance = $this->getDistanceBetweenAirports($flight->airportDepartureId, $flight->airportArrivalId);

        $this->repository->edit($flight);
        return $this->getById($flight->id);
    }

    private function getDistanceBetweenAirports($airportDepartureId, $airportArrivalId): float
    {
        $airportsModel = new AirportsModel($this->populeticDB);
        return $airportsModel->calculateDistance($airportDepartureId, $airportArrivalId);
    }

    public function delete($id)
    {
        $this->repository->delete($id);
    }

    public function validaCreation(Flight $flight)
    {
        $this->validaAirportId($flight->airportDepartureId, 'airportDepartureId');
        $this->validaAirportId($flight->airportArrivalId, 'airportArrivalId');
        $this->validaFlightDataOriginId($flight->flightDataOriginId);
        $this->validaCommonFields($flight);
        $this->jValidator->checkErrors();
    }

    public function validaEdition(Flight $flight)
    {
        $this->validaId($flight->id, false, true);
        $this->validaAirportId($flight->airportDepartureId, 'airportDepartureId', false);
        $this->validaAirportId($flight->airportArrivalId, 'airportArrivalId', false);
        $this->validaCommonFields($flight);
        $this->jValidator->checkErrors();
    }

    private function validaCommonFields(Flight $flight)
    {
        $this->jValidator->validaDate($flight->date, true, 'date', self::DATE_FORMAT);
        $this->jValidator->validaString($flight->number, true, 'number', 1, 250);
        $this->jValidator->validaString($flight->status, true, 'status', 1, 50);
        $this->validaAirlineId($flight->airlineId);
        $this->jValidator->validaDate($flight->departureScheduledTime, false, 'departureScheduledTime');
        $this->jValidator->validaDate($flight->departureActualTime, false, 'departureActualTime');
        $this->jValidator->validaDate($flight->arrivalScheduledTime, false, 'arrivalScheduledTime');
        $this->jValidator->validaDate($flight->arrivalActualTime, false, 'arrivalActualTime');
        $this->jValidator->validaInt($flight->delay, false, 'delay');
        $this->jValidator->validaBoolean($flight->isDisrupted, true, 'isDisrupted');
    }

    public function validaIdsToDelete($ids)
    {
        foreach ($ids as $id) {
            $this->validaId($id, false, true);
        }

        $this->jValidator->checkErrors();
    }

    public function validaId($id, $checkErrors = false, $checkIfExists = false)
    {
        $this->validaIdAndExistsInTable($id, 'id', self::FLIGHTS_TABLE, true, false, $checkErrors);
    }

    private function validaAirportId($id, $key, $required = true)
    {
        $this->validaIdAndExistsInTable($id, $key, 'airports', $required);
    }

    private function validaFlightDataOriginId($id)
    {
        $this->validaIdAndExistsInTable($id, 'flightDataOriginId', 'flight_data_origins', true);
    }



    /************************************
     ******** FLIGHT DATA ORIGINS ********
     ************************************/

    private $neededParamsDataOrigins = ['name'];

    public function getAllDataOrigins(): array
    {
        return $this->repository->getAllDataOrigins();
    }

    public function getDataOriginById($id): array
    {
        return $this->repository->getDataOriginById($id);
    }

    public function createDataOrigin($data): array
    {
        $data['id'] = $this->repository->createDataOrigin($data);
        return $data;
    }

    public function editDataOrigin($data)
    {
        $this->repository->editDataOrigin($data);
    }

    public function deleteDataOrigin($id)
    {
        $this->repository->deleteDataOrigin($id);
    }

    public function validaDataOriginId($id, $checkErrors = false)
    {
        $this->validaIdAndExistsInTable($id, 'id', 'connection_flights', true, false, $checkErrors);
    }

    public function validaDataOriginCreation($data)
    {
        $this->validaParams($data, $this->neededParamsDataOrigins);
        $this->checkIfDataOriginNameAlreadyExists($data['name']);
        $this->jValidator->validaString($data['name'], true, 'name', 1, 30);
        $this->jValidator->checkErrors();
    }

    public function validaDataOriginEdition($data)
    {
        $this->validaParams($data, $this->neededParamsDataOrigins);
        $this->checkIfDataOriginNameAlreadyExists($data['name'], $data['id']);
        $this->validaDataOriginId($data['id']);
        $this->jValidator->validaString($data['name'], true, 'name', 1, 30);
        $this->jValidator->checkErrors();
    }

    public function validaDataOriginIdsToDelete($ids)
    {
        foreach ($ids as $id) {
            $this->validaDataOriginId($id);
        }

        $this->jValidator->checkErrors();
    }

    private function checkIfDataOriginNameAlreadyExists($name, $id = null)
    {
        if ($this->repository->nameExistsInTable('flight_data_origins', $name, $id)) {
            $this->jValidator->setError('name', $this->jValidator::E_ALREADY_EXISTS, $name);
        }
    }



    /************************************
     ******** CONNECTION FLIGHTS ********
     ************************************/

    private $neededParamsConnectionFlights = [
        'airportDepartureId',
        'airportArrivalId',
        'arrivalScheduledTime',
        'arrivalActualTime',
        'isDisrupted'
    ];

    public function getAllConnectionFlights(): array
    {
        return $this->repository->getAllConnectionFlights();
    }

    public function getConnectionFlightById($id): array
    {
        return $this->repository->getConnectionFlightById($id);
    }

    public function createConnectionFlight($data): array
    {
        $data['distance'] = $this->getDistanceBetweenAirports($data['airportDepartureId'], $data['airportArrivalId']);

        $data['id'] = $this->repository->createConnectionFlight($data);
        return $data;
    }

    public function editConnectionFlight($data)
    {
        $data['distance'] = $this->getDistanceBetweenAirports($data['airportDepartureId'], $data['airportArrivalId']);

        $this->repository->editConnectionFlight($data);
        return $data;
    }

    public function deleteConnectionFlight($id)
    {
        $this->repository->deleteConnectionFlight($id);
    }

    public function validaConnectionFlightId($id, $checkErrors = false)
    {
        $this->validaIdAndExistsInTable($id, 'connectionFlightId', 'connection_flights', true, false, $checkErrors);
    }

    public function validaConnectionFlight($data, $edit = false)
    {
        $this->validaParams($data, $this->neededParamsConnectionFlights);
        if ($edit) $this->validaConnectionFlightId($data['id']);
        $this->validaAirportId($data['airportDepartureId'], 'airportDepartureId');
        $this->validaAirportId($data['airportArrivalId'], 'airportArrivalId');
        $this->jValidator->validaDate($data['arrivalScheduledTime'], true, 'arrivalScheduledTime');
        $this->jValidator->validaDate($data['arrivalActualTime'], false, 'arrivalActualTime');
        $this->jValidator->validaBoolean($data['isDisrupted'], true, 'isDisrupted');
        $this->jValidator->checkErrors();
    }

    public function validaConnectionFlightIdsToDelete($ids)
    {
        foreach ($ids as $id) {
            $this->validaConnectionFlightId($id);
        }

        $this->jValidator->checkErrors();
    }



    /************************************
     ***** CLAIM CONNECTION FLIGHTS *****
     ************************************/

    private $claimConnectionFlightsFields = ['connectionFlightId', 'order'];

    public function getAllClaimConnectionFlights($claimId): array
    {
        $connections = [];
        foreach ($this->repository->getAllClaimConnectionFlights($claimId) as $order) {
            unset($order['id']);
            $connections[] = $order;
        }

        return $connections;
    }

    public function createClaimConnectionFlights($claimId, $data)
    {
        foreach ($data as $connFlightOrder) {
            $this->repository->createClaimConnectionFlight($claimId, $connFlightOrder);
        }

        if ($data) $this->repository->setHasConnectionFlightByClaimId($claimId, 1);
    }

    public function addClaimConnectionFlights($claimId, $data)
    {
        $this->repository->createClaimConnectionFlight($claimId, $data);
        $this->repository->setHasConnectionFlightByClaimId($claimId, 1);
    }

    public function deleteClaimConnectionFlight($claimId, $connFlightId)
    {
        $this->repository->deleteClaimConnectionFlight($claimId, $connFlightId);
        if (!$this->repository->connectionFlightExistsByClaimId($claimId)) {
            $this->repository->setHasConnectionFlightByClaimId($claimId, 0);
        }
    }

    public function deleteAllClaimConnectionFlights($claimId)
    {
        $this->repository->deleteAllClaimConnectionFlights($claimId);
        $this->repository->setHasConnectionFlightByClaimId($claimId, 0);
    }

    public function validaClaimConnectionFlightsCreation($data)
    {
        $orderNumsAlreadyInUse = [];
        if (count($data) === 0) {
            $this->jValidator->setError('connectionFlightsData', $this->jValidator::E_EMPTY);
        } else {
            foreach ($data as $connFlightOrder) {
                $this->validaParams($connFlightOrder, $this->claimConnectionFlightsFields);
                $this->validaConnectionFlightId($connFlightOrder['connectionFlightId']);
                $this->jValidator->validaInt($connFlightOrder['order'], true, 'order', 1);
                if (in_array($connFlightOrder['order'], $orderNumsAlreadyInUse)) {
                    $this->jValidator->setError('order', $this->jValidator::E_ALREADY_EXISTS, $connFlightOrder['order']);
                } else {
                    $orderNumsAlreadyInUse[] = $connFlightOrder['order'];
                }
            }
        }
        $this->jValidator->checkErrors();
    }

    public function validaClaimConnectionFlightsAddition($claimId, $data)
    {
        $this->validaParams($data, $this->claimConnectionFlightsFields);
        $this->validaClaimId($claimId);
        $this->validaConnectionFlightId($data['connectionFlightId']);
        $this->jValidator->validaInt($data['order'], true, 'order', 1);

        if ($this->repository->connFlightIdExistsForClaimConnectionFlight($claimId, $data['connectionFlightId'])) {
            $this->jValidator->setError('connectionFlightId', $this->jValidator::E_ALREADY_EXISTS . 'ForClaim', $data['connectionFlightId']);
        }

        if ($this->repository->orderExistsForClaimConnectionFlight($claimId, $data['order'])) {
            $this->jValidator->setError('order', $this->jValidator::E_ALREADY_EXISTS . 'ForClaim', $data['order']);
        }

        $this->jValidator->checkErrors();
    }

    public function validaClaimConnectionFlightExists($claimId, $connFlightId)
    {
        $this->validaClaimId($claimId);
        $this->validaConnectionFlightId($connFlightId);
        if (!$this->repository->connFlightIdExistsForClaimConnectionFlight($claimId, $connFlightId)) {
            $this->jValidator->setError('connectionFlightId', $this->jValidator::E_DOESNT_EXISTS . 'ForClaim', $connFlightId);
        }

        $this->jValidator->checkErrors();
    }

    public function validaClaimConnectionFlightExistsForClaim($claimId)
    {
        $this->validaClaimId($claimId);
        if (!$this->repository->connectionFlightExistsByClaimId($claimId)) {
            $this->jValidator->setError('claimConnectionFlight', $this->jValidator::E_DOESNT_EXISTS . 'ForClaim', $claimId);
        }
        $this->jValidator->checkErrors();
    }
}