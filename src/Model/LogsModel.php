<?php

namespace App\Model;

use App\Repository\LogsRepository;
use Doctrine\DBAL\Connection;

class LogsModel extends BaseModel
{
    const LOGS_TABLE = 'logs';

    public function __construct(Connection $populeticDB)
    {
        parent::__construct();
        $this->repository = new LogsRepository($populeticDB);
    }

    /****************
     ***** LOGS *****
     ****************/

    public function getAllByYear($year): array
    {
        return $this->repository->getAllByYear($year);
    }

    public function insert($userId, $action, $changes)
    {
        $logsTable = 'logs_' . date('Y');
        if (!$this->repository->tableExists($logsTable)) {
            $this->repository->createTableLogsForCurrentYear();
            $this->repository->insert($userId, 'LogsModel - ' . __FUNCTION__, $this->getAlertByTable($logsTable));
        }

        $this->repository->insert($userId, $action, $changes);
    }

    public function validaYearAndLogsTableExists($year)
    {
        $this->validaYear($year, true);
        $this->validaLogsTableExists($year);
    }

    public function validaLogsTableExists($year)
    {
        $tableName = self::LOGS_TABLE . '_' . $year;
        if (!$this->repository->tableExists($tableName)) {
            $this->jValidator->setError('table', $this->jValidator::E_DOESNT_EXISTS, $tableName);
        }

        $this->jValidator->checkErrors();
    }

    private function getAlertByTable($tableName): string
    {
        return "ALERT!!: The cron that creates '$tableName' (each year) doesn't seem to work. Created automatically.";
    }



    /*********************
     * LOGS CLAIM STATUS *
     *********************/

    public function getAllClaimStatus(): array
    {
        return $this->repository->getAllClaimStatus();
    }

    public function getAllLogsClaimStatusByClaimId($claimId): array
    {
        return $this->repository->getAllLogsClaimStatusByClaimId($claimId);
    }

    public function insertClaimStatus($data)
    {
        $this->repository->insertClaimStatus(
            $data['claimId'], $data['claimStatusId'], $data['userId'], $data['action'], $data['changes']
        );
    }

    public function insertClaimStatusByActualClaimStatus($claimId, $userId, $action, $changes)
    {
        $this->repository->insertClaimStatusByActualClaimStatus($claimId, $userId, $action, $changes);
    }

    public function validaLogClaimStatus($data)
    {
        $this->validaParams($data, ['claimId', 'claimStatusId', 'action', 'changes']);
        $this->validaClaimId($data['claimId']);
        $this->validaIdAndExistsInTable($data['claimStatusId'], 'claimStatusId', 'claim_status', true);
        $this->jValidator->validaString($data['action'], false, 'action', 1, 250);
        $this->jValidator->validaString($data['changes'], false, 'changes');
        $this->jValidator->checkErrors();
    }
}