<?php

namespace App\Model;

use App\Entity\CompanyData;
use App\Repository\CompanyDataRepository;
use Doctrine\DBAL\Connection;

class CompanyDataModel extends BaseModel
{
    public function __construct(Connection $populeticDB)
    {
        parent::__construct();
        $this->repository  = new CompanyDataRepository($populeticDB);
    }

    public function getAll(): array
    {
        return $this->repository->getAll();
    }

    public function getByCountryId($countryId): CompanyData
    {
        return new CompanyData($this->repository->getByCountryId($countryId));
    }

    public function create(CompanyData $companyData): CompanyData
    {
        $this->repository->create($companyData);
        return $this->getByCountryId($companyData->countryId);
    }

    public function edit(CompanyData $companyData): CompanyData
    {
        $this->repository->edit($companyData);
        return $this->getByCountryId($companyData->countryId);
    }

    public function delete($countryId)
    {
        $this->repository->delete($countryId);
    }

    public function validaCompanyData(CompanyData $companyData, $new = true)
    {
        if ($new) {
            $this->validaCountryId($companyData->countryId);
            if ($companyData->countryId && $this->repository->companyDataExistsForCountryId($companyData->countryId)) {
                $this->jValidator->setError(
                    'companyData',
                    $this->jValidator::E_ALREADY_EXISTS . 'ForCountryId',
                    $companyData->countryId
                );
            }
        }

        $this->jValidator->validaString($companyData->fiscalName, false, 'fiscalName', 1, 100);
        $this->jValidator->validaString($companyData->businessName, false, 'businessName', 1, 100);
        $this->jValidator->validaString($companyData->address, false, 'address', 1, 250);
        $this->jValidator->validaPhone($companyData->phone, false);
        $this->jValidator->validaEmail($companyData->businessEmail, false, 'businessEmail');
        $this->jValidator->validaEmail($companyData->adminEmail, false, 'adminEmail');
        $this->jValidator->validaInt($companyData->adwordsCookiesExpireDays, true, 'adwordsCookiesExpireDays');
        $this->jValidator->validaFloat($companyData->fee, true, 'fee', 0, 100);
        $this->jValidator->checkErrors();
    }

    public function validaDataExistsForCountryId($id, $checkErrors = false)
    {
        $this->jValidator->validaInt($id, true, 'countryId', 1);
        if ($id && !$this->repository->companyDataExistsForCountryId($id)) {
            $this->jValidator->setError('companyData', $this->jValidator::E_DOESNT_EXISTS . 'ForCountryId', $id);
        }

        if ($checkErrors) $this->jValidator->checkErrors();
    }
}