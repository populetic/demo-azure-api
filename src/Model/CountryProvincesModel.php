<?php

namespace App\Model;

use App\Entity\CountryProvince;
use App\Repository\CountryProvincesRepository;
use Doctrine\DBAL\Connection;

class CountryProvincesModel extends BaseModel
{
    public function __construct(Connection $populeticDB)
    {
        parent::__construct();
        $this->repository = new CountryProvincesRepository($populeticDB);
    }

    public function getAll(): array
    {
        return $this->repository->getAll();
    }

    public function getById($id): CountryProvince
    {
        return new CountryProvince($this->repository->getById($id));
    }

    public function create(CountryProvince $province): CountryProvince
    {
        $id = $this->repository->create($province);
        return $this->getById($id);
    }

    public function edit(CountryProvince $province): CountryProvince
    {
        $this->repository->edit($province);
        return $this->getById($province->id);
    }

    public function delete($id)
    {
        $this->repository->delete($id);
    }

    public function validaCreation(CountryProvince $province)
    {
        $this->validaCommonFields($province);
        $this->jValidator->checkErrors();
    }

    public function validaEdition(CountryProvince $province)
    {
        $this->validaId($province->id);
        $this->validaCommonFields($province);
        $this->jValidator->validaBoolean($province->isActive, true, 'isActive');
        $this->jValidator->checkErrors();
    }

    private function validaCommonFields(CountryProvince $province)
    {
        $this->validaCountryId($province->countryId);
        $this->jValidator->validaString($province->name, true, 'name', 1, 250);
        $this->checkIfNameAlreadyExists($province->name, $province->id);
    }

    private function checkIfNameAlreadyExists($name, $id = '')
    {
        if ($this->repository->nameExistsInTable('country_provinces', $name, $id)) {
            $this->jValidator->setError('name', $this->jValidator::E_ALREADY_EXISTS, $name);
        }
    }

    public function validaIdsToDelete($ids)
    {
        foreach ($ids as $id) {
            $this->validaId($id);
        }

        $this->jValidator->checkErrors();
    }

    public function validaId($id, $checkErrors = false)
    {
        $this->validaIdAndExistsInTable($id, 'countryProvinceId', 'country_provinces', true, false, $checkErrors);
    }
}