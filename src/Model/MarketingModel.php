<?php

namespace App\Model;

use App\Repository\MarketingRepository;
use Doctrine\DBAL\Connection;

class MarketingModel extends BaseModel
{
    const AD_TRACK_TABLE = 'adwords_tracking';

    public function __construct(Connection $populeticDB)
    {
        parent::__construct();
        $this->repository  = new MarketingRepository($populeticDB);
    }

    /*********************
     ** ADWORDS TRACKING **
     *********************/

    private $neededParamsAdwordsTrackings = [
        'email',
        'claimId',
        'gclid',
        'utmMedium',
        'utmSource',
        'utmCampaign',
        'ip',
        'userAgent'
    ];

    public function getAdwordsTrackingsByPageAndItemsPerPage($page, $itemsPerPage): array
    {
        return $this->repository->getAdwordsTrackingsByPageAndItemsPerPage($page, $itemsPerPage);
    }

    public function getAdwordsTrackingById($id): array
    {
        return $this->repository->getAdwordsTrackingById($id);
    }

    public function getTotalAdwordsTrackingCount(): int
    {
        return $this->repository->getTotalCountByTable(self::AD_TRACK_TABLE);
    }

    public function createAdwordsTracking($data): int
    {
        return $this->repository->createAdwordsTracking($data);
    }

    public function editAdwordsTracking($data)
    {
        $this->repository->editAdwordsTracking($data);
    }

    public function deleteAdwordsTracking($id)
    {
        $this->repository->deleteAdwordsTracking($id);
    }

    public function validaAdwordsTracking($data)
    {
        $this->validaParams($data, $this->neededParamsAdwordsTrackings);
        $this->jValidator->validaEmail($data['email'], true, 'email');
        $this->validaClaimId($data['claimId'], false, false);
        $this->jValidator->validaString($data['gclid'], false, 'gclid', 1, 250);
        $this->jValidator->validaString($data['utmMedium'], false, 'utmMedium', 1, 100);
        $this->jValidator->validaString($data['utmSource'], false, 'utmSource', 1, 100);
        $this->jValidator->validaString($data['utmCampaign'], false, 'utmCampaign', 1, 100);
        $this->jValidator->validaIP($data['ip'], true);
        $this->jValidator->validaString($data['userAgent'], true, 'userAgent', 1, 250);
        $this->jValidator->checkErrors();
    }

    public function validaAdwordsTrackingIdsToDelete($ids)
    {
        foreach ($ids as $id) {
            $this->validaAdwordsTrackingId($id);
        }

        $this->jValidator->checkErrors();
    }

    public function validaAdwordsTrackingId($id, $checkErrors = false)
    {
        $this->validaIdAndExistsInTable($id, 'adwordsTrackingId', self::AD_TRACK_TABLE, true, false, $checkErrors);
    }



    /***************************
     * AIRLINES REVENUE IMPACT *
     ***************************/

    private $neededParamsRevenueImpacts = ['airlineId', 'countryId', 'revenueImpact'];

    public function getAllRevenueImpactsByAirlineAndCountry($airlineId, $countryId): array
    {
        return $this->repository->getRevenueImpactsByAirlineAndCountry($airlineId, $countryId, false);
    }

    public function getLastRevenueImpactByAirlineAndCountry($airlineId, $countryId): array
    {
        return $this->repository->getRevenueImpactsByAirlineAndCountry($airlineId, $countryId, true);
    }

    public function getRevenueImpactById($id): array
    {
        return $this->repository->getRevenueImpactById($id);
    }

    public function createRevenueImpact($data): int
    {
        return $this->repository->createRevenueImpact($data);
    }

    public function deleteRevenueImpact($id)
    {
        $this->repository->deleteRevenueImpact($id);
    }

    public function validaAirlineAndCountryAndDataExists($airlineId, $countryId)
    {
        $this->jValidator->validaInt($airlineId, true, 'airlineId', 1);
        $this->jValidator->validaInt($countryId, true, 'countryId', 1);
        if (!$this->repository->airlineRevenueImpactExists($airlineId, $countryId)) {
            $this->jValidator->setError('airlineRevenueImpact', $this->jValidator::E_DOESNT_EXISTS);
        }

        $this->jValidator->checkErrors();
    }

    public function validaRevenueImpact($data)
    {
        $this->validaParams($data, $this->neededParamsRevenueImpacts);
        $this->validaAirlineId($data['airlineId']);
        $this->validaCountryId($data['countryId']);
        $this->jValidator->validaFloat($data['revenueImpact'], true, 'revenueImpact');
        $this->jValidator->checkErrors();
    }

    public function validaRevenueImpactIdsToDelete($ids)
    {
        foreach ($ids as $id) {
            $this->validaRevenueImpactId($id);
        }

        $this->jValidator->checkErrors();
    }

    public function validaRevenueImpactId($id, $checkErrors = false)
    {
        $this->validaIdAndExistsInTable($id, 'revenueImpactId', 'airlines_revenue_impact', true, false, $checkErrors);
    }
}