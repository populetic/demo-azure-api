<?php

namespace App\Model;

use App\Repository\ExtrajudicialRepository;
use Doctrine\DBAL\Connection;

class ExtrajudicialModel extends BaseModel
{
    public function __construct(Connection $populeticDB)
    {
        parent::__construct();
        $this->repository  = new ExtrajudicialRepository($populeticDB);
    }

    /***********************
     * ADDITIONAL EXPENSES *
     ***********************/

    private $neededParamsAdditionalExp = ['extrajudicialTypeSendingId', 'expenses', 'comments', 'sentAt'];

    public function getAllAdditionalExp(): array
    {
        return $this->repository->getAllAdditionalExp();
    }

    public function getAdditionalExpById($id): array
    {
        return $this->repository->getAdditionalExpById($id);
    }

    public function createAdditionalExp($data): array
    {
        $id = $this->repository->createAdditionalExp($data);
        return $this->getAdditionalExpById($id);
    }

    public function editAdditionalExp($data)
    {
        $this->repository->editAdditionalExp($data);
    }

    public function deleteAdditionalExp($id)
    {
        $this->repository->deleteAdditionalExp($id);
    }

    public function validaAdditionalExp($data)
    {
        $this->validaParams($data, $this->neededParamsAdditionalExp);
        $this->validaTypeSendingId($data['extrajudicialTypeSendingId']);
        $this->jValidator->validaFloat($data['expenses'], false, 'expenses', 0);
        $this->jValidator->validaString($data['comments'], false, 'comments');
        $this->jValidator->validaDate($data['sentAt'], true, 'date', 'Y-m-d');
        $this->jValidator->checkErrors();
    }

    public function validaAdditionalExpIdsToDelete($ids)
    {
        foreach ($ids as $id) {
            $this->validaAdditionalExpId($id);
        }

        $this->jValidator->checkErrors();
    }

    public function validaAdditionalExpId($id, $checkErrors = false)
    {
        $this->validaIdAndExistsInTable(
            $id,
            'extrajudicialAdditionalExpensesId',
            'extrajudicial_additional_expenses',
            true, false, $checkErrors
        );
    }



    /**********************
     **** TYPE SENDING ****
     **********************/

    private $neededParamsTypeSending = ['name', 'countryId'];

    public function getAllTypeSendingByCountry($countryId): array
    {
        return $this->repository->getAllTypeSendingByCountry($countryId);
    }

    public function getTypeSendingById($id): array
    {
        return $this->repository->getTypeSendingById($id);
    }

    public function createTypeSending($data): array
    {
        $id = $this->repository->createTypeSending($data);
        return $this->getTypeSendingById($id);
    }

    public function editTypeSending($data)
    {
        $this->repository->editTypeSending($data);
    }

    public function deleteTypeSending($id)
    {
        $this->repository->deleteTypeSending($id);
    }

    public function validaTypeSending($data)
    {
        $id = array_key_exists('id', $data) ? $data['id'] : null;

        $this->validaParams($data, $this->neededParamsTypeSending);
        $this->jValidator->validaString($data['name'], true, 'name', 1, 50);
        $this->validaIfTypeSendingNameAlreadyExists($data['name'], $id);
        $this->validaCountryId($data['countryId']);
        $this->jValidator->checkErrors();
    }

    private function validaIfTypeSendingNameAlreadyExists($name, $id = null)
    {
        if ($this->repository->typeSendingNameExists($name, $id)) {
            $this->jValidator->setError('name', $this->jValidator::E_ALREADY_EXISTS, $name);
        }
    }

    public function validaTypeSendingIdsToDelete($ids)
    {
        foreach ($ids as $id) {
            $this->validaTypeSendingId($id);
        }

        $this->jValidator->checkErrors();
    }

    public function validaTypeSendingId($id, $checkErrors = false)
    {
        $this->validaIdAndExistsInTable(
            $id, 'extrajudicialTypeSendingId', 'extrajudicial_type_sending', true, false, $checkErrors
        );
    }



    /************************
     **** RELATED CLAIMS ****
     ************************/

    private $neededParamsRelatedClaims = ['extrajudicialAdditionalExpensesId', 'claimId'];

    public function getAdditionalExpensesByRelatedClaim($claimId): array
    {
        return $this->repository->getAdditionalExpensesByRelatedClaim($claimId);
    }

    public function createRelatedClaims($data)
    {
        $this->repository->createRelatedClaims($data);
    }

    public function deleteRelatedClaims($claimId, $extrajudicialAdditionalExpensesId)
    {
        $this->repository->deleteRelatedClaims($claimId, $extrajudicialAdditionalExpensesId);
    }

    public function validaRelatedClaims($data)
    {
        $this->validaParams($data, $this->neededParamsRelatedClaims);
        $this->validaIdAndExistsInTable(
            $data['extrajudicialAdditionalExpensesId'],
            'extrajudicialAdditionalExpensesId',
            'extrajudicial_additional_expenses',
            true
        );
        $this->validaClaimId($data['claimId']);
        $this->jValidator->checkErrors();

        if ($this->repository->extrajudicialRelatedClaimExists($data['claimId'], $data['extrajudicialAdditionalExpensesId'])) {
            $this->jValidator->setError('extrajudicialRelatedClaim', $this->jValidator::E_ALREADY_EXISTS);
        }
        $this->jValidator->checkErrors();
    }

    public function validaRelatedClaimsExists($claimId, $additionalExpensesId)
    {
        $this->jValidator->validaInt($claimId, true, 'claimId', 1);
        $this->jValidator->validaInt($additionalExpensesId, true, 'extrajudicialAdditionalExpensesId', 1);
        $this->jValidator->checkErrors();

        if (!$this->repository->extrajudicialRelatedClaimExists($claimId, $additionalExpensesId)) {
            $this->jValidator->setError('extrajudicialRelatedClaim', $this->jValidator::E_DOESNT_EXISTS);
        }
        $this->jValidator->checkErrors();
    }
}