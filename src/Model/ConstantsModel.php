<?php

namespace App\Model;

class ConstantsModel
{
    const PROD_APP_ENV = 'prod';
    const JSON_FOLDER  = __DIR__ . '/Json/';

    const ITEMS_PER_PAGE  = 25;
    const PASSWORD_LENGTH = 8;
    const DATE_FORMAT     = 'Y-m-d';
    const TIMESTAMP_DATE  = 'Y-m-d H:i:s';

    const KEYSPACE       = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    const ENCRYPT_METHOD = 'AES-256-CBC';
    const SECRET_KEY     = 'BHEJLNWWQVQWGJSQS52D1QW32E52XWQE8';

    const DELAY_MINUTES       = 180;
    const EARTH_KMS_BY_DEGREE = 111.13384; // 1 degree = 111.13384 km, based on Earth average diameter (12.735 km)
    const MIN                 = 'min';
    const MIDDLE              = 'middle';
    const MAX                 = 'max';

    const DISR_TYPE_ID_DELAY          = 1;
    const DISR_TYPE_ID_CANCELLATION   = 2;
    const DISR_TYPE_ID_OVERBOOKING    = 3;
    const DISR_TYPE_ID_LUGGAGE_LOST   = 4;
    const DISR_TYPE_ID_LUGGAGE_DELAY  = 5;
    const DISR_TYPE_ID_LUGGAGE_DAMAGE = 6;
    protected $luggageDisruptionTypes = [
        self::DISR_TYPE_ID_LUGGAGE_LOST,
        self::DISR_TYPE_ID_LUGGAGE_DELAY,
        self::DISR_TYPE_ID_LUGGAGE_DAMAGE
    ];

    const CLAIM_STATUS_PENDING_TO_ASSIGN       = 1;
    const CLAIM_STATUS_EXTRAJUD_AGREEMENT      = 5;
    const CLAIM_STATUS_LACK_JUDICIAL_COMP      = 6;
    const CLAIM_STATUS_PENDING_LEGAL_DEPT      = 7;
    const CLAIM_STATUS_ASSIGNED_LEGAL_DEPT     = 8;
    const CLAIM_STATUS_POPULETIC_COLLECTED     = 18;
    const CLAIM_STATUS_CUSTOMER_COLLECTED      = 19;
    const CLAIM_STATUS_CANCELED_WITHOUT_CHARGE = 23;
    const CLAIM_STATUS_CANCELED_WITH_50_E      = 24;
    const CLAIM_STATUS_CANCELED_WITH_25_PERC   = 35;
    const CLAIM_STATUS_COMPLETED               = 26;
    const CLAIM_STATUS_VIP_PAYMENT             = 40;
    const CLAIM_STATUS_REQUEST_PAYMENT_DATA    = 41;
    const CLAIM_STATUS_READY_TO_PAY            = 42;

    const CLAIM_DOC_STATUS_REVIEW_PENDING = 1;

    const ROLE_ID_JUNIOR_LAWYER = 24;

    const DEPARTMENT_ID_LEGAL            = 4;
    const DEPARTMENT_ID_CUSTOMER_SERVICE = 5;

    const LAWSUITE_TYPE_ID_EUROPEAN = 1;
    const LAWSUITE_TYPE_ID_MONTREAL = 2;

    const COUNTRY_ID_SPAIN = 3;
}