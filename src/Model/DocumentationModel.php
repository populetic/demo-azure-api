<?php

namespace App\Model;

use App\Entity\Documentation;
use App\Repository\DocumentationRepository;
use Doctrine\DBAL\Connection;

class DocumentationModel extends BaseModel
{
    public function __construct(Connection $populeticDB)
    {
        parent::__construct();
        $this->repository = new DocumentationRepository($populeticDB);
    }

    /*********************
     *** DOCUMENTATION ***
     *********************/

    public function getAll(): array
    {
        return $this->repository->getAll();
    }

    public function getById($id): Documentation
    {
        return new Documentation($this->repository->getById($id));
    }

    public function create(Documentation $documentation): Documentation
    {
        $id = $this->repository->create($documentation);
        return $this->getById($id);
    }

    public function edit(Documentation $documentation): Documentation
    {
        $this->repository->edit($documentation);
        return $this->getById($documentation->id);
    }

    public function delete($id)
    {
        $this->repository->delete($id);
    }

    public function validaDocumentation(Documentation $documentation)
    {
        $this->validaClaimId($documentation->claimId);
        $this->validaDocumentationTypeId($documentation->documentationTypeId);
        $this->jValidator->validaBoolean($documentation->isValid, true, 'isValid');
        $this->jValidator->validaBoolean($documentation->toDelete, true, 'toDelete');
        $this->jValidator->validaBoolean($documentation->notAvailable, true, 'notAvailable');
        $this->jValidator->validaString($documentation->extension, true, 'extension', 2, 4);
        $this->jValidator->checkErrors();
    }

    public function validaIdsToDelete($ids)
    {
        foreach ($ids as $id) {
            $this->validaId($id);
        }

        $this->jValidator->checkErrors();
    }

    public function validaId($id, $checkErrors = false)
    {
        $this->validaIdAndExistsInTable($id, 'documentationId', 'documentation', true, false, $checkErrors);
    }



    /***********************
     * DOCUMENTATION TYPES *
     ***********************/

    private $neededParamsDocType = ['name'];

    public function getAllDocumentationTypes(): array
    {
        return $this->repository->getAllDocumentationTypes();
    }

    public function getDocumentationTypeById($id): array
    {
        return $this->repository->getDocumentationTypeById($id);
    }

    public function createDocumentationType($data): array
    {
        $data['id'] = $this->repository->createDocumentationType($data);
        return $data;
    }

    public function editDocumentationType($data)
    {
        $this->repository->editDocumentationType($data);
    }

    public function deleteDocumentationType($id)
    {
        $this->repository->deleteDocumentationType($id);
    }

    public function validaDocumentationType($data)
    {
        $id = array_key_exists('id', $data) ? $data['id'] : null;

        $this->validaParams($data, $this->neededParamsDocType);
        $this->validaDocumentationTypeName($data['name'], $id);
        $this->jValidator->checkErrors();
    }

    private function validaDocumentationTypeName($name, $id = null)
    {
        $this->jValidator->validaString($name, true, 'name', 1, 50);
        if ($this->repository->nameExistsInTable('documentation_types', $name, $id)) {
            $this->jValidator->setError('name', $this->jValidator::E_ALREADY_EXISTS, $name);
        }
    }

    public function validaDocumentationTypeIdsToDelete($ids)
    {
        foreach ($ids as $id) {
            $this->validaDocumentationTypeId($id);
        }

        $this->jValidator->checkErrors();
    }

    public function validaDocumentationTypeId($id, $checkErrors = false)
    {
        $this->validaIdAndExistsInTable($id, 'documentationTypeId', 'documentation_types', true, false, $checkErrors);
    }



    /*********************
     ** DOC GOOGLE VISIO **
     *********************/

    private $neededParamsDocGoogleVisio = ['texts', 'matches', 'textsAccurance', 'matchesAccurance'];

    public function getDocGoogleVisioByDocumentationId($documentationId): array
    {
        return $this->repository->getDocGoogleVisioByDocumentationId($documentationId);
    }

    public function createDocGoogleVisio($documentationId, $data)
    {
        $this->repository->createDocGoogleVisio($documentationId, $data);
    }

    public function deleteDocGoogleVisioByDocumentationId($documentationId)
    {
        $this->repository->deleteDocGoogleVisioByDocumentationId($documentationId);
    }

    public function validaDocGoogleVisio($data)
    {
        $this->validaParams($data, $this->neededParamsDocGoogleVisio);
        $this->jValidator->validaString($data['texts'], false, 'texts');
        $this->jValidator->validaString($data['matches'], false, 'matches');
        $this->jValidator->validaFloat($data['textsAccurance'], true, 'textsAccurance', 0, null, 3);
        $this->jValidator->validaFloat($data['matchesAccurance'], true, 'matchesAccurance', 0, null, 3);
        $this->jValidator->checkErrors();
    }

    public function validaDocGoogleVisioIdsToDelete($documentationIds)
    {
        foreach ($documentationIds as $id) {
            $this->jValidator->validaInt($id, true, 'documentationId', 1);
            if ($id && !$this->repository->docIdExistsForGoogleVisio($id)) {
                $this->jValidator->setError('documentationId', 'NoDataForGoogleVisio', $id);
            }
        }

        $this->jValidator->checkErrors();
    }



    /***********************
     * DOCU PDF TO CONVERT *
     ***********************/

    private $neededParamsDocuPdfToConvert = ['claimId', 'folder'];

    public function getDocuPdfToConvertByClaimId($claimId): array
    {
        return $this->repository->getDocuPdfToConvertByClaimId($claimId);
    }

    public function createDocuPdfToConvert($data)
    {
        $this->repository->createDocuPdfToConvert($data);
    }

    public function deleteDocuPdfToConvert($data)
    {
        $this->repository->deleteDocuPdfToConvert($data);
    }

    public function validaDocuPdfToConvertCreation($data)
    {
        $this->validaParams($data, $this->neededParamsDocuPdfToConvert);
        $this->validaClaimId($data['claimId']);
        $this->jValidator->validaString($data['folder'], true, 'folder', 1, 250);
        if ($this->repository->docuPdfToConvertExists($data)) {
            $this->jValidator->setError('docuPdfToConvert', $this->jValidator::E_ALREADY_EXISTS);
        }

        $this->jValidator->checkErrors();
    }

    public function validaDocuPdfToConvertDeletion($data)
    {
        $this->validaParams($data, $this->neededParamsDocuPdfToConvert);
        $this->jValidator->validaInt($data['claimId'], true, 'claimId', 1);
        $this->jValidator->validaString($data['folder'], true, 'folder', 1, 250);
        if (!$this->repository->docuPdfToConvertExists($data)) {
            $this->jValidator->setError('docuPdfToConvert', $this->jValidator::E_DOESNT_EXISTS);
        }

        $this->jValidator->checkErrors();
    }
}