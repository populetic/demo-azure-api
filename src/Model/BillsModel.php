<?php

namespace App\Model;

use App\Entity\Bill;
use App\Repository\BillsRepository;
use Doctrine\DBAL\Connection;

class BillsModel extends BaseModel
{
    private $populeticDB;

    public function __construct(Connection $populeticDB)
    {
        parent::__construct();
        $this->repository  = new BillsRepository($populeticDB);
        $this->populeticDB = $populeticDB;
    }

    /*********************
     ******* BILLS *******
     *********************/

    public function getAll(): array
    {
        return $this->repository->getAll();
    }

    public function getById($id): Bill
    {
        return new Bill($this->repository->getById($id));
    }

    public function getBillAllDataById($id): array
    {
        $flightsModel = new FlightsModel($this->populeticDB);
        $clientsModel = new ClientsModel($this->populeticDB);

        $bill = (array)$this->getById($id);

        $bill['flightData']  = (array)$flightsModel->getById($bill['flightId']);
        $bill['clientData']  = (array)$clientsModel->getById($bill['clientId']);
        $bill['billingData'] = $this->repository->getClaimBillingDataById($bill['claimBillingDataId']);

        return $bill;
    }

    public function getByClaimId($claimId): array
    {
        return $this->repository->getByClaimId($claimId);
    }

    public function create(Bill $bill): Bill
    {
        $id = $this->repository->create($bill);
        return $this->getById($id);
    }

    public function edit(Bill $bill): Bill
    {
        $this->repository->edit($bill);
        return $this->getById($bill->id);
    }

    public function delete($id)
    {
        $this->repository->delete($id);
    }

    public function validaBill(Bill $bill)
    {
        $this->jValidator->validaInt($bill->number, true, 'number', 1, 999999);
        $this->validaNumberForBillTypeDoesntExists($bill->number, $bill->billTypeId, $bill->id);
        $this->validaClaimId($bill->claimId);
        $this->validaClaimBillingDataIdIfIsSet($bill->claimBillingDataId);
        $this->validaBillTypeIdIfIsSet($bill->billTypeId);
        $this->jValidator->validaFloat($bill->base, true, 'base', null, null, 5);
        $this->jValidator->validaFloat($bill->ivaTotal, true, 'ivaTotal', null, null, 5);
        $this->jValidator->validaFloat($bill->total, true, 'total', null, null, 5);
        $this->jValidator->validaString($bill->comments, false, 'comments');
        $this->validaParentBillIdIfIsSet($bill->parentBillId);
        $this->validaBillPreferenceId($bill->billPreferenceId);
        $this->jValidator->validaDate($bill->expiresAt, true, 'expiresAt');
        $this->jValidator->checkErrors();
    }

    private function validaNumberForBillTypeDoesntExists($number, $billTypeId, $id)
    {
        if ($this->repository->numberForBillTypeExists($number, $billTypeId, $id)) {
            $this->jValidator->setError('number', $this->jValidator::E_ALREADY_EXISTS . 'ForBillType', $number);
        }
    }

    private function validaClaimBillingDataIdIfIsSet($id)
    {
        if ($id !== null) {
            $this->validaIdAndExistsInTable($id, 'claimBillingDataId', 'claims_billing_data', true);
        }
    }

    private function validaBillTypeIdIfIsSet($id)
    {
        if ($id !== null) {
            $this->validaBillTypeId($id);
        }
    }

    private function validaParentBillIdIfIsSet($id)
    {
        if ($id !== null) {
            $this->validaIdAndExistsInTable($id, 'parentBillId', 'bills', true);
        }
    }

    public function validaBillPreferenceId($id, $checkErrors = false)
    {
        $this->validaIdAndExistsInTable($id, 'billPreferenceId', 'bill_preferences', true, false, $checkErrors);
    }

    public function validaIdsToDelete($ids)
    {
        foreach ($ids as $id) {
            $this->validaId($id);
        }

        $this->jValidator->checkErrors();
    }

    public function validaId($id, $checkErrors = false)
    {
        $this->validaIdAndExistsInTable($id, 'billId', 'bills', true, false, $checkErrors);
    }



    /*********************
     * CLAIM BILLING DATA *
     *********************/

    private $neededParamsClaimsBillingData = ['claimId', 'accountHolder', 'accountNumber', 'swift'];

    public function getAllClaimsBillingData(): array
    {
        $allData = $this->repository->getAllClaimsBillingData();
        foreach ($allData as $key => $data) {
            $allData[$key] = $this->decryptClaimBillingData($data);
        }

        return $allData;
    }

    public function getClaimBillingDataByClaimId($claimId): array
    {
        $data = $this->repository->getClaimBillingDataByClaimId($claimId);
        return $data ? $this->decryptClaimBillingData($data) : [];
    }

    public function getClaimBillingDataById($id): array
    {
        $data = $this->repository->getClaimBillingDataById($id);
        return $this->decryptClaimBillingData($data);
    }

    public function existsClaimBillingDataByClaimId($claimId): bool
    {
        return $this->repository->getClaimBillingDataByClaimId($claimId) ? true : false;
    }

    public function createClaimBillingData($data): int
    {
        $data = $this->encryptClaimBillingData($data);
        return $this->repository->createClaimBillingData($data);
    }

    public function editClaimBillingData($data)
    {
        $data = $this->encryptClaimBillingData($data);
        $this->repository->editClaimBillingData($data);
    }

    public function editClaimBillingDataIsValidByClaim($claimId, $isValid)
    {
        $this->repository->editClaimBillingDataIsValidByClaim($claimId, $isValid);
    }

    public function deleteClaimBillingData($id)
    {
        $this->repository->deleteClaimBillingData($id);
    }

    public function validaClaimBillingData($data)
    {
        $this->validaParams($data, $this->neededParamsClaimsBillingData);
        $this->validaClaimId($data['claimId']);
        $this->jValidator->validaString($data['accountHolder'], true, 'accountHolder', 1, 250);
        $this->jValidator->validaString($data['accountNumber'], true, 'accountNumber', 1, 100);
        $this->jValidator->validaString($data['swift'], false, 'swift', 1, 100);
        if (array_key_exists('isValid', $data)) $this->jValidator->validaBoolean($data['isValid'], true, 'isValid');
        $this->jValidator->checkErrors();
    }

    public function validaEditIsValid($claimId, $isValid)
    {
        $this->validaClaimId($claimId);
        $this->jValidator->validaBoolean($isValid, true, 'isValid');
        $this->jValidator->checkErrors();
    }

    public function validaClaimBillingDataIdsToDelete($ids)
    {
        foreach ($ids as $id) {
            $this->validaClaimBillingDataId($id);
        }

        $this->jValidator->checkErrors();
    }

    public function validaClaimBillingDataId($id, $checkErrors = false)
    {
        $this->validaIdAndExistsInTable($id, 'claimBillingDataId', 'claims_billing_data', true, false, $checkErrors);
    }

    public function encryptClaimBillingData($data): array
    {
        $data['accountHolder'] = $this->encryptText($data['accountHolder']);
        $data['accountNumber'] = $this->encryptText($data['accountNumber']);
        $data['swift']         = $this->encryptText($data['swift']);

        return $data;
    }

    public function decryptClaimBillingData($data): array
    {
        $data['accountHolder'] = $this->decryptText($data['accountHolder']);
        $data['accountNumber'] = $this->decryptText($data['accountNumber']);
        $data['swift']         = $this->decryptText($data['swift']);

        return $data;
    }



    /*********************
     ** BILL PREFERENCES **
     *********************/

    private $neededParamsBillPreferences = [
        'countryId',
        'iva',
        'expirationDaysClient',
        'expirationDaysPopuletic',
        'expirationDaysCancellation',
        'amountCancellation'
    ];

    public function getAllBillPreferences(): array
    {
        return $this->repository->getAllBillPreferences();
    }

    public function getBillPreferenceById($id): array
    {
        return $this->repository->getBillPreferenceById($id);
    }

    public function createBillPreference($data): int
    {
        return $this->repository->createBillPreference($data);
    }

    public function editBillPreference($data)
    {
        $this->repository->editBillPreference($data);
    }

    public function deleteBillPreference($id)
    {
        $this->repository->deleteBillPreference($id);
    }

    public function validaBillPreference($data)
    {
        $this->validaParams($data, $this->neededParamsBillPreferences);
        $this->validaCountryId($data['countryId']);
        $this->jValidator->validaInt($data['iva'], true, 'iva', 0, 100);
        $this->jValidator->validaInt($data['expirationDaysClient'], false, 'expirationDaysClient');
        $this->jValidator->validaInt($data['expirationDaysPopuletic'], false, 'expirationDaysPopuletic');
        $this->jValidator->validaInt($data['expirationDaysCancellation'], false, 'expirationDaysCancellation');
        $this->jValidator->validaFloat($data['amountCancellation'], false, 'amountCancellation', null, null, 5);
        if (array_key_exists('isActive', $data)) {
            $this->jValidator->validaBoolean($data['isActive'], true, 'isActive');
        }

        $this->jValidator->checkErrors();
    }

    public function validaBillPreferenceIdsToDelete($ids)
    {
        foreach ($ids as $id) {
            $this->validaBillPreferenceId($id);
        }

        $this->jValidator->checkErrors();
    }



    /*********************
     ***** BILL TYPES *****
     *********************/

    private $neededParamsBillTypes = ['name', 'series'];

    public function getAllBillTypes(): array
    {
        return $this->repository->getAllBillTypes();
    }

    public function getBillTypeById($id): array
    {
        return $this->repository->getBillTypeById($id);
    }

    public function createBillType($data): int
    {
        return $this->repository->createBillType($data);
    }

    public function editBillType($data)
    {
        $this->repository->editBillType($data);
    }

    public function deleteBillType($id)
    {
        $this->repository->deleteBillType($id);
    }

    public function getBillNextNumberBySeries($series): array
    {
        return $this->repository->getBillNextNumberBySeries($series);
    }

    public function validaBillType($data)
    {
        $id = array_key_exists('id', $data) ? $data['id'] : null;

        $this->validaParams($data, $this->neededParamsBillTypes);
        $this->jValidator->validaString($data['name'], true, 'name', 1, 45);
        if ($this->repository->nameExistsInTable('bill_types', $data['name'], $id)) {
            $this->jValidator->setError('name', $this->jValidator::E_ALREADY_EXISTS, $data['name']);
        }

        $this->jValidator->validaString($data['series'], true, 'series', 1, 15);
        if ($this->repository->seriesExists($data['series'], $id)) {
            $this->jValidator->setError('series', $this->jValidator::E_ALREADY_EXISTS, $data['series']);
        }

        $this->jValidator->checkErrors();
    }

    public function validaBillTypeIdsToDelete($ids)
    {
        foreach ($ids as $id) {
            $this->validaBillTypeId($id);
        }

        $this->jValidator->checkErrors();
    }

    public function validaBillTypeId($id, $checkErrors = false)
    {
        $this->validaIdAndExistsInTable($id, 'billTypeId', 'bill_types', true, false, $checkErrors);
    }

    public function validaSeries($series)
    {
        $this->jValidator->validaString($series, true, 'series', 1, 15);
        if (!$this->repository->seriesExists($series)) {
            $this->jValidator->setError('series', $this->jValidator::E_DOESNT_EXISTS, $series);
        }

        $this->jValidator->checkErrors();
    }



    /***********************
     * PAYMENTS EMAIL SENT *
     ***********************/

    public function getPaymentEmailSentByClaimId($claimId): array
    {
        $data = $this->repository->getPaymentEmailSentByClaimId($claimId);

        if (!$data) {
            $data = [
                'claimId'           => $claimId,
                'numberOfTimesSent' => 0,
                'createdAt'         => null
            ];
        }

        return $data;
    }

    public function addPaymentsTimesSent($claimId)
    {
        $this->repository->addPaymentsTimesSent($claimId);
    }

    public function deletePaymentEmailSent($claimId)
    {
        $this->repository->deletePaymentEmailSent($claimId);
    }

    public function validaClaimIdHasEmailSent($claimId)
    {
        $this->jValidator->validaInt($claimId, true, 'claimId', 1);
        if (!$this->repository->claimIdHasEmailSent($claimId)) {
            $this->jValidator->setError('claimId', 'HasNoEmailSent', $claimId);
        }

        $this->jValidator->checkErrors();
    }
}