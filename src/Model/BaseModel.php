<?php

namespace App\Model;

use App\Service\JValidator;
use Knp\Component\Pager\Paginator;

class BaseModel extends ConstantsModel
{
    /** @var JValidator */
    protected $jValidator;
    protected $repository;

    public function __construct()
    {
        $this->jValidator = new JValidator();
    }

    public function paginateResult($result, $page, $itemsPerPage = null): array
    {
        $itemsPerPage = $itemsPerPage ? $itemsPerPage : self::ITEMS_PER_PAGE;

        $paginator  = new Paginator();
        $pagination = $paginator->paginate($result, $page, $itemsPerPage);

        $response['totalItems']     = $pagination->getTotalItemCount();
        $response['page']           = $page;
        $response['itemsPerPage']   = $itemsPerPage;
        $response['totalPages']     = ceil($response['totalItems'] / $itemsPerPage);
        $response['items']          = $pagination->getItems();

        return $response;
    }

    public function generateRandomPassword(): string
    {
        $str = '';
        $max = mb_strlen(self::KEYSPACE, '8bit') - 1;

        for ($i = 0; $i < self::PASSWORD_LENGTH; ++$i) {
            try {
                $str .= self::KEYSPACE[random_int(0, $max)];
            } catch (\Exception $e) {
                $str .= substr(md5(microtime()), rand(0, 26), self::PASSWORD_LENGTH);
            }
        }

        return $str;
    }

    public function validaIdAndExistsInTable($id, $key, $table, $required, $checkActive = false, $checkErrors = false)
    {
        $this->jValidator->validaInt($id, $required, $key, 1);
        if ($id && !$this->repository->idExistsInTable($table, $id)) {
            $this->jValidator->setError($key, $this->jValidator::E_DOESNT_EXISTS, $id);
        } elseif ($checkActive && $id && !$this->repository->idIsActiveInTable($table, $id)) {
            $this->jValidator->setError($key, $this->jValidator::E_DISABLED, $id);
        }

        if ($checkErrors) $this->jValidator->checkErrors();
    }

    public function validaClaimId($id, $checkErrors = false, $required = true)
    {
        $this->validaIdAndExistsInTable($id, 'claimId', 'populetic.claims', $required, false, $checkErrors);
    }

    public function validaCountryId($id, $checkErrors = false, $required = true)
    {
        $this->validaIdAndExistsInTable($id, 'countryId', 'populetic.countries', $required, true, $checkErrors);
    }

    public function validaAirlineId($id, $checkErrors = false, $required = true)
    {
        $this->validaIdAndExistsInTable($id, 'airlineId', 'populetic.airlines', $required, true, $checkErrors);
    }

    public function validaTimestamp($date, $checkErrors = false, $required = true)
    {
        $this->jValidator->validaDate($date, $required, 'date');
        if ($checkErrors) $this->jValidator->checkErrors();
    }

    public function validaYear($year, $checkErrors = false, $required = true)
    {
        $this->jValidator->validaInt($year, $required, 'year', 2000, 2100);
        if ($checkErrors) $this->jValidator->checkErrors();
    }

    /**
     * Comprueba que todas las keys requeridas para esa petición nos están llegando o nos devuelve
     * los errores de validación
     * @param $params array json convertido a array que nos llega en la petición
     * @param $paramNamesToHave array de strings con los parámetros que se esperan para esa petición
     */
    protected function validaParams($params, $paramNamesToHave)
    {
        foreach ($paramNamesToHave as $paramToCheck) {
            if (!array_key_exists($paramToCheck, $params)) {
                $this->jValidator->setError($paramToCheck, $this->jValidator::E_PARAM_NOT_FOUND);
            }
        }

        $this->jValidator->checkErrors();
    }

    protected function hashPassword($password): string
    {
        return password_hash($password, PASSWORD_DEFAULT);
    }

    protected function verifyPassword($inputPassword, $hashInDB): bool
    {
        return password_verify($inputPassword, $hashInDB);
    }

    /**
     * Se pasa un separador y un string que los contenga y devuelve un array con todas las palabras entre dos separadores
     * @param $separator
     * @param $str
     */
    protected function getAllWordsBetweenSeparator($separator, $str): array
    {
        preg_match_all("~\\$separator(.*?)\\$separator~", $str, $result);
        return (!empty($result[1])) ? $result[1] : array();
    }

    /** Función de encriptar usada en remesas */
    protected function encryptText($text)
    {
        if ($text) {
            $key = hash('sha256', self::SECRET_KEY);
            $iv  = substr(hash('sha256', self::SECRET_KEY), 0, 16);

            return base64_encode(openssl_encrypt($text, self::ENCRYPT_METHOD, $key, 0, $iv));
        } else {
            return $text;
        }
    }

    /** Función de desencriptar usada en remesas */
    protected function decryptText($encryptedText)
    {
        if ($encryptedText) {
            $key = hash('sha256', self::SECRET_KEY);
            $iv  = substr(hash('sha256', self::SECRET_KEY), 0, 16);

            return openssl_decrypt(base64_decode($encryptedText), self::ENCRYPT_METHOD, $key, 0, $iv);
        } else {
            return $encryptedText;
        }
    }
}