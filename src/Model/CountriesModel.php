<?php

namespace App\Model;

use App\Entity\Country;
use App\Repository\CountriesRepository;
use Doctrine\DBAL\Connection;

class CountriesModel extends BaseModel
{
    const ISO_LENGTH = 2;

    public function __construct(Connection $populeticDB)
    {
        parent::__construct();
        $this->repository = new CountriesRepository($populeticDB);
    }

    public function getAll(): array
    {
        return $this->repository->getAll();
    }

    public function getById($id): Country
    {
        return new Country($this->repository->getById($id));
    }

    public function create(Country $country): Country
    {
        $id = $this->repository->create($country);
        return $this->getById($id);
    }

    public function edit(Country $country): Country
    {
        $this->repository->edit($country);
        return $this->getById($country->id);
    }

    public function delete($id)
    {
        $this->repository->delete($id);
    }

    public function getCountriesWeHaveCompetence(): array
    {
        $countries = $this->repository->getCountriesWeHaveCompetence();
        return array_column($countries, 'id');
    }

    public function validaCreation(Country $country)
    {
        $this->validaCommonFields($country);
        $this->jValidator->checkErrors();
    }

    public function validaEdition(Country $country)
    {
        $this->validaCountryId($country->id);
        $this->validaCommonFields($country);
        $this->jValidator->validaBoolean($country->isActive, true, 'isActive');
        $this->jValidator->checkErrors();
    }

    private function validaCommonFields(Country $country)
    {
        $this->jValidator->validaString($country->iso, true, 'iso', self::ISO_LENGTH, self::ISO_LENGTH);
        $this->checkIfIsoAlreadyExists($country->iso, $country->id);
        $this->jValidator->validaString($country->name, true, 'name', 1, 250);
        $this->checkIfNameAlreadyExists($country->name, $country->id);
        $this->ifRegionIdIsSetValidaIfExistsAndIsEnabled($country->regionId);
        $this->jValidator->validaBoolean($country->isUe, true, 'isUe');
        $this->jValidator->validaInt($country->competencePriority, true, 'competencePriority', 0, 50);
    }

    private function checkIfIsoAlreadyExists($iso, $id = null)
    {
        if ($this->repository->isoAlreadyExists($iso, $id)) {
            $this->jValidator->setError('iso', $this->jValidator::E_ALREADY_EXISTS, $iso);
        }
    }

    private function checkIfNameAlreadyExists($name, $id = null)
    {
        if ($this->repository->nameExistsInTable('countries', $name, $id)) {
            $this->jValidator->setError('name', $this->jValidator::E_ALREADY_EXISTS, $name);
        }
    }

    private function ifRegionIdIsSetValidaIfExistsAndIsEnabled($id)
    {
        if ($id !== null) {
            $this->validaIdAndExistsInTable($id, 'regionId', 'regions', true, true);
        }
    }

    public function validaIdsToDelete($ids)
    {
        foreach ($ids as $id) {
            $this->validaCountryId($id);
        }

        $this->jValidator->checkErrors();
    }
}