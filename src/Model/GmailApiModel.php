<?php

namespace App\Model;

use App\Entity\GmailApiDataAccess;
use App\Repository\GmailApiRepository;
use Doctrine\DBAL\Connection;

class GmailApiModel extends BaseModel
{
    public function __construct(Connection $populeticDB)
    {
        parent::__construct();
        $this->repository = new GmailApiRepository($populeticDB);
    }

    /*************************
     * GMAIL API DATA ACCESS *
     *************************/

    public function getAll(): array
    {
        return $this->repository->getAll();
    }

    public function getById($id): GmailApiDataAccess
    {
        return new GmailApiDataAccess($this->repository->getById($id));
    }

    public function getByEmail($email): GmailApiDataAccess
    {
        return new GmailApiDataAccess($this->repository->getByEmail($email));
    }

    public function create(GmailApiDataAccess $dataAccess): GmailApiDataAccess
    {
        $id = $this->repository->create($dataAccess);
        return $this->getById($id);
    }

    public function edit(GmailApiDataAccess $dataAccess): GmailApiDataAccess
    {
        $this->repository->edit($dataAccess);
        return $this->getById($dataAccess->id);
    }

    public function delete($id)
    {
        $this->repository->delete($id);
    }

    public function validaDataAccess(GmailApiDataAccess $dataAccess)
    {
        $this->validaEmail($dataAccess->email, true);
        $this->checkIfEmailAlreadyExists($dataAccess->email, $dataAccess->id);
        $this->jValidator->validaString($dataAccess->accessToken, true, 'accessToken', 1, 250);
        $this->jValidator->validaString($dataAccess->refreshToken, true, 'refreshToken', 1, 250);
        $this->validaClientId($dataAccess->clientId);
        $this->jValidator->validaDate($dataAccess->lastScanAt, true, 'lastScanAt');
        $this->jValidator->checkErrors();
    }

    public function validaEmail($email, $required = false, $checkErrors = false)
    {
        $this->jValidator->validaEmail($email, $required, 'email');
        if ($checkErrors) $this->jValidator->checkErrors();
    }

    public function checkIfEmailAlreadyExists($email, $id = null)
    {
        if ($this->repository->emailExistsInTable('gmail_api_data_access', $email, $id)) {
            $this->jValidator->setError('email', $this->jValidator::E_ALREADY_EXISTS, $email);
        }
    }

    private function validaClientId($id)
    {
        $this->validaIdAndExistsInTable($id, 'clientId', 'clients', false);
    }

    public function validaDataAccessIdsToDelete($ids)
    {
        foreach ($ids as $id) {
            $this->validaDataAccessId($id);
        }

        $this->jValidator->checkErrors();
    }

    public function validaDataAccessId($id, $checkErrors = false)
    {
        $this->validaIdAndExistsInTable($id, 'dataAccessId', 'gmail_api_data_access', true, false, $checkErrors);
    }



    /*******************************
     * GMAIL API DISRUPTED FLIGHTS *
     *******************************/

    private $neededParamsDisruptedFlights = ['dataAccessId', 'flightId', 'claimId'];

    public function getGmailApiDisruptedFlightByIds($data): array
    {
        return $this->repository->getGmailApiDisruptedFlightByIds($data);
    }

    public function createGmailApiDisruptedFlight($data)
    {
        $this->repository->createGmailApiDisruptedFlight($data);
    }

    public function deleteGmailApiDisruptedFlight($data)
    {
        $this->repository->deleteGmailApiDisruptedFlight($data);
    }

    public function validaGmailApiDisruptedFlight($data)
    {
        $this->validaParams($data, $this->neededParamsDisruptedFlights);
        $this->validaIdAndExistsInTable($data['dataAccessId'], 'dataAccessId', 'gmail_api_data_access', true);
        $this->validaIdAndExistsInTable($data['flightId'], 'flightId', 'flights', true);
        $this->validaClaimId($data['claimId'], false, false);
        $this->jValidator->checkErrors();
    }

    public function validaGmailApiDisruptedFlightExists($data)
    {
        $this->jValidator->validaInt($data['dataAccessId'], true, 'dataAccessId', 1);
        $this->jValidator->validaInt($data['flightId'], true, 'flightId', 1);
        $this->jValidator->checkErrors();

        if (!$this->repository->gmailApiDisruptedFlightExists($data)) {
            $this->jValidator->setError('gmailApiDisruptedFlight', $this->jValidator::E_DOESNT_EXISTS);
            $this->jValidator->checkErrors();
        }
    }
}