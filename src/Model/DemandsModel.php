<?php

namespace App\Model;

use App\Entity\Demand;
use App\Repository\DemandsRepository;
use Doctrine\DBAL\Connection;

class DemandsModel extends BaseModel
{
    public function __construct(Connection $populeticDB)
    {
        parent::__construct();
        $this->repository = new DemandsRepository($populeticDB);
    }

    /*********************
     ****** DEMANDS ******
     *********************/

    public function getAll($filters): array
    {
        return $this->repository->getAll($filters);
    }

    public function getById($id): Demand
    {
        return new Demand($this->repository->getById($id));
    }

    public function create(Demand $demand): Demand
    {
        $id = $this->repository->create($demand);
        return $this->getById($id);
    }

    public function edit(Demand $demand): Demand
    {
        $this->repository->edit($demand);
        return $this->getById($demand->id);
    }

    public function delete($id)
    {
        $this->repository->delete($id);
    }

    public function validaFilters($filters)
    {
        if ($filters['procedureNumber'] !== null) {
            $this->jValidator->validaBoolean($filters['procedureNumber'], true, 'procedureNumber');
        }
        if ($filters['cancellated'] !== null) {
            $this->jValidator->validaBoolean($filters['cancellated'], true, 'cancellated');
        }
        if ($filters['lawFirmId'] !== null && $filters['lawFirmId'] != 0) {
            $this->validaIdAndExistsInTable($filters['lawFirmId'], 'lawFirmId', 'law_firms', true);
        }

        $this->jValidator->checkErrors();
    }

    public function validaDemand(Demand $demand)
    {
        $this->jValidator->validaString($demand->procedureNumber, false, 'procedureNumber', 1, 250);
        $this->jValidator->validaString($demand->judgementNumber, false, 'judgementNumber', 1, 250);
        $this->validaCourtNumberId($demand->courtNumberId);
        $this->jValidator->validaFloat($demand->amountRequested, true, 'amountRequested', null, null, 5);
        $this->jValidator->validaFloat($demand->amountReceived, false, 'amountReceived', null, null, 5);
        $this->jValidator->validaFloat($demand->amountInterests, false, 'amountInterests', null, null, 5);
        $this->jValidator->validaString($demand->comments, false, 'comments');
        $this->jValidator->validaBoolean($demand->isNoShow, true, 'isNoShow');
        $this->jValidator->validaBoolean($demand->isOverseas, true, 'isOverseas');
        $this->jValidator->validaBoolean($demand->hasFlightstats, true, 'hasFlightstats');
        $this->jValidator->validaDate($demand->presentedAt, false, 'presentedAt');
        $this->jValidator->validaDate($demand->admitedAt, false, 'admitedAt');
        $this->jValidator->validaDate($demand->judgedAt, false, 'judgedAt');
        $this->jValidator->validaDate($demand->cancellatedAt, false, 'cancellatedAt');
        $this->jValidator->checkErrors();
    }

    private function validaCourtNumberId($id)
    {
        $this->validaIdAndExistsInTable($id, 'courtNumberId', 'courts_number', false);
    }

    public function validaIdsToDelete($ids)
    {
        foreach ($ids as $id) {
            $this->validaDemandId($id);
        }

        $this->jValidator->checkErrors();
    }

    public function validaDemandId($id, $checkErrors = false)
    {
        $this->validaIdAndExistsInTable($id, 'demandId', 'demands', true, false, $checkErrors);
    }



    /*********************
     * DEMANDS EXECUTIVE *
     *********************/

    private $neededParamsDemandsExecutive = [
        'demandId', 'procedureNumber', 'presentedAt', 'amountRequested', 'amountReceived', 'amountInterests', 'comments'
    ];

    public function getAllDemandsExecutive(): array
    {
        return $this->repository->getAllDemandsExecutive();
    }

    public function getDemandExecutiveById($id): array
    {
        return $this->repository->getDemandExecutiveById($id);
    }

    public function createDemandExecutive($data): int
    {
        return $this->repository->createDemandExecutive($data);
    }

    public function editDemandExecutive($data)
    {
        $this->repository->editDemandExecutive($data);
    }

    public function deleteDemandExecutive($id)
    {
        $this->repository->deleteDemandExecutive($id);
    }

    public function validaDemandExecutive($data)
    {
        $this->validaParams($data, $this->neededParamsDemandsExecutive);
        $this->validaDemandId($data['demandId']);
        $this->jValidator->validaString($data['procedureNumber'], false, 'procedureNumber', 1, 250);
        $this->jValidator->validaDate($data['presentedAt'], false, 'presentedAt');
        $this->jValidator->validaFloat($data['amountRequested'], false, 'amountRequested', null, null, 5);
        $this->jValidator->validaFloat($data['amountReceived'], false, 'amountReceived', null, null, 5);
        $this->jValidator->validaFloat($data['amountInterests'], false, 'amountInterests', null, null, 5);
        $this->jValidator->validaString($data['comments'], false, 'comments');
        $this->jValidator->checkErrors();
    }

    public function validaDemandExecutiveIdsToDelete($ids)
    {
        foreach ($ids as $id) {
            $this->validaDemandExecutiveId($id);
        }

        $this->jValidator->checkErrors();
    }

    public function validaDemandExecutiveId($id, $checkErrors = false)
    {
        $this->validaIdAndExistsInTable($id, 'id', 'demands_executive', true, false, $checkErrors);
    }



    /*********************
     ** DEMAND EXPENSES **
     *********************/

    private $neededParamsDemandExpenses = ['demandId', 'amount', 'description', 'userId'];

    public function getAllDemandExpenses(): array
    {
        return $this->repository->getAllDemandExpenses();
    }

    public function getDemandExpenseById($id): array
    {
        return $this->repository->getDemandExpenseById($id);
    }

    public function createDemandExpense($data): int
    {
        return $this->repository->createDemandExpense($data);
    }

    public function editDemandExpense($data)
    {
        $this->repository->editDemandExpense($data);
    }

    public function deleteDemandExpense($id)
    {
        $this->repository->deleteDemandExpense($id);
    }

    public function validaDemandExpense($data)
    {
        $this->validaParams($data, $this->neededParamsDemandExpenses);
        $this->validaDemandId($data['demandId']);
        $this->jValidator->validaFloat($data['amount'], true, 'amount', null, null, 5);
        $this->jValidator->validaString($data['description'], false, 'description', 1, 250);
        $this->validaUserId($data['userId']);
        $this->jValidator->checkErrors();
    }

    private function validaUserId($id)
    {
        $this->validaIdAndExistsInTable($id, 'userId', 'users', true, true);
    }

    public function validaDemandExpenseIdsToDelete($ids)
    {
        foreach ($ids as $id) {
            $this->validaDemandExpenseId($id);
        }

        $this->jValidator->checkErrors();
    }

    public function validaDemandExpenseId($id, $checkErrors = false)
    {
        $this->validaIdAndExistsInTable($id, 'id', 'demand_expenses', true, false, $checkErrors);
    }



    /*********************
     **** DEMAND VIEW ****
     *********************/

    private $neededParamsDemandView = ['demandId', 'location', 'viewDate'];

    public function getDemandViewByDemandId($demandId): array
    {
        return $this->repository->getDemandViewByDemandId($demandId);
    }

    public function createDemandView($data)
    {
        $this->repository->createDemandView($data);
    }

    public function deleteDemandView($demandId)
    {
        $this->repository->deleteDemandView($demandId);
    }

    public function validaDemandView($data)
    {
        $this->validaParams($data, $this->neededParamsDemandView);
        $this->validaDemandId($data['demandId']);
        $this->jValidator->validaString($data['location'], true, 'location', 1, 250);
        $this->jValidator->validaDate($data['viewDate'], true, 'viewDate');
        $this->jValidator->checkErrors();
    }

    public function validaDemandViewToDelete($demandsIds)
    {
        foreach ($demandsIds as $id) {
            $this->jValidator->validaInt($id, true, 'demandId', 1);
            if ($id && !$this->repository->demandViewExists($id)) {
                $this->jValidator->setError('demandId', $this->jValidator::E_DOESNT_EXISTS, $id);
            }
        }

        $this->jValidator->checkErrors();
    }



    /*********************
     *** RELATED CLAIMS ***
     *********************/

    private $neededParamsRelatedClaims = ['demandId', 'claimId'];

    public function getRelatedClaimsByDemandId($demandId): array
    {
        return $this->repository->getRelatedClaimsByDemandId($demandId);
    }

    public function createDemandRelatedClaim($demandId, $claimId)
    {
        $this->repository->createDemandRelatedClaim($demandId, $claimId);
    }

    public function deleteDemandRelatedClaim($data)
    {
        $this->repository->deleteDemandRelatedClaim($data['demandId'], $data['claimId']);
    }

    public function validaRelatedClaimCreation($data)
    {
        $this->validaParams($data, ['demandId', 'claimIds']);
        $this->validaDemandId($data['demandId']);
        foreach ($data['claimIds'] as $claimId) {
            $this->validaClaimId($claimId);
            if ($this->repository->demandRelatedClaimExists($data['demandId'], $claimId)) {
                $this->jValidator->setError('relatedClaim', $this->jValidator::E_ALREADY_EXISTS, $claimId);
            }
        }

        $this->jValidator->checkErrors();
    }

    public function validaRelatedClaimToDelete($data)
    {
        $this->validaParams($data, $this->neededParamsRelatedClaims);
        $this->jValidator->validaInt($data['demandId'], true, 'demandId', 1);
        $this->jValidator->validaInt($data['claimId'], true, 'claimId', 1);
        if (!$this->repository->demandRelatedClaimExists($data['demandId'], $data['claimId'])) {
            $this->jValidator->setError('relatedClaim', $this->jValidator::E_DOESNT_EXISTS);
        }
        $this->jValidator->checkErrors();
    }



    /************************
     * DEMAND NOTIFICATIONS *
     ************************/

    private $neededParamsNotificationsCreation = ['demandId', 'judicialNotificationTypeId', 'daysToNotify'];
    private $neededParamsNotificationsToDelete = ['demandId', 'judicialNotificationTypeId', 'createdAt'];

    public function getDemandNotificationsByDemandId($demandId): array
    {
        return $this->repository->getDemandNotificationsByDemandId($demandId);
    }

    public function createDemandNotification($data)
    {
        $this->repository->createDemandNotification($data);
    }

    public function deleteDemandNotification($data)
    {
        $this->repository->deleteDemandNotification($data);
    }

    public function validaDemandNotificationCreation($data)
    {
        $this->validaParams($data, $this->neededParamsNotificationsCreation);
        $this->validaDemandId($data['demandId']);
        $this->validaJudicialNotificationTypeId($data['judicialNotificationTypeId']);
        $this->jValidator->validaInt($data['daysToNotify'], true, 'daysToNotify', 1, 180);
        $this->jValidator->checkErrors();
    }

    private function validaJudicialNotificationTypeId($id)
    {
        $this->validaIdAndExistsInTable($id, 'judicialNotificationTypeId', 'judicial_notification_types_by_country', true);
    }

    public function validaDemandNotificationToDelete($data)
    {
        $this->validaParams($data, $this->neededParamsNotificationsToDelete);
        $this->jValidator->validaInt($data['demandId'], true, 'demandId', 1);
        $this->jValidator->validaInt($data['judicialNotificationTypeId'], true, 'judicialNotificationTypeId', 1);
        $this->jValidator->validaDate($data['createdAt'], true, 'createdAt');
        if (!$this->repository->demandNotificationExists($data)) {
            $this->jValidator->setError('demandNotification', $this->jValidator::E_DOESNT_EXISTS);
        }
        $this->jValidator->checkErrors();
    }



    /*********************
     * NOTIFICATION TYPES *
     *********************/

    private $neededParamsNotificationTypes = ['name', 'countryId'];

    public function getAllNotificationTypesByCountry($countryId): array
    {
        return $this->repository->getAllNotificationTypesByCountry($countryId);
    }

    public function getNotificationTypeById($id): array
    {
        return $this->repository->getNotificationTypeById($id);
    }

    public function createNotificationType($data): array
    {
        $data['id'] = $this->repository->createNotificationType($data);
        return $data;
    }

    public function editNotificationType($data)
    {
        $this->repository->editNotificationType($data);
    }

    public function deleteNotificationType($id)
    {
        $this->repository->deleteNotificationType($id);
    }

    public function validaNotificationType($data)
    {
        $id = array_key_exists('id', $data) ? $data['id'] : null;

        $this->validaParams($data, $this->neededParamsNotificationTypes);
        $this->validaNotificationTypeName($data['name'], $data['countryId'], $id);
        $this->validaCountryId($data['countryId']);
        $this->jValidator->checkErrors();
    }

    private function validaNotificationTypeName($name, $countryId, $id = null)
    {
        $this->jValidator->validaString($name, true, 'name', 1, 100);
        if ($this->repository->notificationTypeNameExistsForCountry($name, $countryId, $id)) {
            $this->jValidator->setError('name', $this->jValidator::E_ALREADY_EXISTS . 'ForCountry', $name);
        }
    }

    public function validaNotificationTypeIdsToDelete($ids)
    {
        foreach ($ids as $id) {
            $this->validaNotificationTypeId($id);
        }

        $this->jValidator->checkErrors();
    }

    public function validaNotificationTypeId($id, $checkErrors = false)
    {
        $this->validaIdAndExistsInTable($id, 'id', 'judicial_notification_types_by_country', true, false, $checkErrors);
    }
}