<?php

namespace App\Model\TranslationSystem;

use App\Entity\Language;
use App\Model\BaseModel;
use App\Repository\TranslationSystem\LanguagesRepository;
use Doctrine\DBAL\Connection;

class LanguagesModel extends BaseModel
{
    public function __construct(Connection $translationsDB)
    {
        parent::__construct();
        $this->repository = new LanguagesRepository($translationsDB);
    }

    /*********************
     ***** LANGUAGES *****
     *********************/

    public function getAll(): array
    {
        return $this->repository->getAll();
    }

    public function getById($id): Language
    {
        return new Language($this->repository->getById($id));
    }

    public function create(Language $language): Language
    {
        $id = $this->repository->create($language);
        return $this->getById($id);
    }

    public function edit(Language $language): Language
    {
        $this->repository->edit($language);
        return $this->getById($language->id);
    }

    public function delete($id)
    {
        $this->repository->delete($id);
    }

    public function validaLanguage(Language $language)
    {
        $this->validaCode($language->code, $language->id);
        $this->validaName($language->name, $language->id);
        $this->validaClaimReference($language->claimReference, $language->id);
        $this->jValidator->checkErrors();
    }

    private function validaCode($code, $id = null)
    {
        $this->jValidator->validaString($code, true, 'code', 2, 2);
        if ($this->repository->codeExists($code, $id)) {
            $this->jValidator->setError('code', $this->jValidator::E_ALREADY_EXISTS, $code);
        }
    }

    private function validaName($name, $id = null)
    {
        $this->jValidator->validaString($name, true, 'name', 1, 100);
        if ($this->repository->nameExistsInTable('languages', $name, $id)) {
            $this->jValidator->setError('name', $this->jValidator::E_ALREADY_EXISTS, $name);
        }
    }

    private function validaClaimReference($claimReference, $id = null)
    {
        $this->jValidator->validaInt($claimReference, true, 'claimReference', 1);
        if ($this->repository->claimReferenceExists($claimReference, $id)) {
            $this->jValidator->setError('claimReference', $this->jValidator::E_ALREADY_EXISTS, $claimReference);
        }
    }

    public function validaIdsToDelete($ids)
    {
        foreach ($ids as $id) {
            $this->validaId($id);
        }

        $this->jValidator->checkErrors();
    }

    public function validaId($id, $checkErrors = false)
    {
        $this->validaIdAndExistsInTable($id, 'languageId', 'languages', true, false, $checkErrors);
    }



    /*********************
     ** LANGUAGES BY APP **
     *********************/

    private $neededParamsLanguagesByApp = ['languageId', 'isDefault'];

    public function getAllLanguagesByApp($appId): array
    {
        return $this->repository->getAllLanguagesByApp($appId);
    }

    public function editLanguagesByApp($appId, $languages)
    {
        foreach ($languages as $lang) {
            $this->repository->editLanguagesByApp($appId, $lang['languageId'], $lang['isDefault']);
        }
    }

    public function deleteLanguagesByApp($appId)
    {
        $this->repository->deleteLanguagesByApp($appId);
    }

    public function validaAppId($id)
    {
        $this->validaIdAndExistsInTable($id, 'appId', 'apps', true, false, true);
    }

    public function validaLanguages($data)
    {
        $this->validaParams($data, ['languages']);

        if (!empty($data['languages'])) {
            $countDefaults = 0;

            foreach ($data['languages'] as $lang) {
                $this->validaParams($lang, $this->neededParamsLanguagesByApp);
                $this->validaId($lang['languageId']);
                $this->jValidator->validaBoolean($lang['isDefault'], true, 'isDefault');

                if ($lang['isDefault']) $countDefaults++;
            }

            if ($countDefaults > 1) {
                $this->jValidator->setError('languages', $this->jValidator::E_MORE_THAN_ONE_DEFAULT);
            } elseif ($countDefaults < 1) {
                $this->jValidator->setError('languages', $this->jValidator::E_NO_DEFAULT_SELECTED);
            }

            $this->jValidator->checkErrors();
        }
    }
}