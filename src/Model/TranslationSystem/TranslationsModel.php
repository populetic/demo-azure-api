<?php

namespace App\Model\TranslationSystem;

use App\Model\BaseModel;
use App\Repository\TranslationSystem\TranslationsRepository;
use Doctrine\DBAL\Connection;

class TranslationsModel extends BaseModel
{
    public function __construct(Connection $translationsDB)
    {
        parent::__construct();
        $this->repository = new TranslationsRepository($translationsDB);
    }

    public function validaAppHasTranslationSystem($app)
    {
        if (!$this->repository->hasAppTranslationSystem($app)) {
            $this->jValidator->setError('appTranslationSystem', $this->jValidator::E_DOESNT_EXISTS, $app);
        }
        $this->jValidator->checkErrors();
    }

    public function validaDataToTranslateTag($app, $langId, $tagId)
    {
        $this->validaAppHasTranslationSystem($app);

        $languages = $this->getAllActiveLanguagesByApp($app);
        if (is_numeric($langId)) {
            $this->jValidator->validaInt($langId, true, 'langId', 1);

            if (!in_array($langId, array_keys($languages))) {
                $this->jValidator->setError('langId', $this->jValidator::E_DOESNT_EXISTS, $langId);
            }
        } else {
            $this->jValidator->validaString($langId, true, 'langId', 2, 2);

            if (!in_array($langId, array_values($languages))) {
                $this->jValidator->setError('langId', $this->jValidator::E_DOESNT_EXISTS, $langId);
            }
        }

        $this->jValidator->validaString($tagId, true, 'tagId', 1, 250);

        $this->jValidator->checkErrors();
    }

    public function validaDataToTranslateTagToDefault($app, $tagId)
    {
        $this->validaAppHasTranslationSystem($app);
        $this->jValidator->validaString($tagId, true, 'tagId', 1, 250);
        $this->jValidator->checkErrors();
    }

    public function translateTag($app, $langId, $tagId): array
    {
        if (is_numeric($langId)) {
            $langCode = $this->repository->getLangCodeById($langId);
        } else {
            $langCode = $langId;
            $langId   = $this->repository->getLangIdByCode($langCode);
        }

        $translations = $this->repository->getTranslationsByAppAndLangIdAndTagId($app, $langId, $tagId);
        $result[$langCode] = $this->parseTranslationsToJsonEncode($translations);

        return $result;
    }

    public function getDefaultTranslationByAppAndTag($app, $tagId): array
    {
        $translations = $this->repository->getDefaultTranslationByAppAndTag($app, $tagId);
        return $this->parseTranslationsToJsonEncode($translations);
    }

    public function getAllActiveLanguagesByApp($app): array
    {
        $languages = $this->repository->getAllActiveLanguagesByApp($app);
        return array_column($languages, 'code', 'id');
    }

    public function getAllTranslationsForAppByLanguages($app, $languages): array
    {
        $translations = [];
        foreach ($languages as $langId => $langCode) {
            $translations[$langCode] = $this->parseTranslationsToJsonEncode(
                $this->repository->getAllTranslationsForAppByLangId($app, $langId)
            );
        }
        return $translations;
    }

    private function parseTranslationsToJsonEncode($translations): array
    {
        $parsed = [];
        foreach ($translations as $translation) {
            $subtagOrder = $translation['subtagOrder'] ? ('.' . $translation['subtagOrder']) : '';
            $parsed[$translation['tagId'] . $subtagOrder] = $translation['translation'];
        }
        return $parsed;
    }
}