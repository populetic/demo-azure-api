<?php

namespace App\Model;

use App\Repository\PopuleticSiteRepository;
use Doctrine\DBAL\Connection;

class PopuleticSiteModel extends BaseModel
{
    public function __construct(Connection $populeticSiteDB)
    {
        parent::__construct();
        $this->repository  = new PopuleticSiteRepository($populeticSiteDB);
    }

    /*********************
     **** ACCESS LOGS ****
     *********************/

    private $neededParamsAccessLogs = [
        'ip',
        'userAgent',
        'domainUrl',
        'lastPage',
        'languageId',
        'disruptionTypeId',
        'email',
        'name',
        'surnames',
        'flightNumber',
        'flightDate',
        'isDisrupted',
        'amountEstimated',
        'claimId',
        'isRemarketing',
        'finishedAt'
    ];

    public function getAllAccessLogs(): array
    {
        return $this->repository->getAllAccessLogs();
    }

    public function getAccessLogsById($id): array
    {
        return $this->repository->getAccessLogsById($id);
    }

    public function createAccessLogs($data): array
    {
        $id = $this->repository->createAccessLogs($data);
        return $this->getAccessLogsById($id);
    }

    public function editAccessLogs($data)
    {
        $this->repository->editAccessLogs($data);
    }

    public function deleteAccessLogs($id)
    {
        $this->repository->deleteAccessLogs($id);
    }

    public function validaAccessLogs($data)
    {
        $this->validaParams($data, $this->neededParamsAccessLogs);
        $this->jValidator->validaIP($data['ip'], false);
        $this->jValidator->validaString($data['userAgent'], false, 'userAgent', 1, 250);
        $this->jValidator->validaURL($data['domainUrl'], false, 'domainUrl');
        $this->jValidator->validaString($data['lastPage'], true, 'lastPage', 1, 250);
        $this->validaIdAndExistsInTable($data['languageId'], 'languageId', 'translation_system.languages', false);
        $this->validaIdAndExistsInTable($data['disruptionTypeId'], 'disruptionTypeId', 'populetic.disruption_types', false);
        $this->jValidator->validaEmail($data['email'], false);
        $this->jValidator->validaString($data['name'], false, 'name', 1, 250);
        $this->jValidator->validaString($data['surnames'], false, 'surnames', 1, 250);
        $this->jValidator->validaString($data['flightNumber'], false, 'flightNumber', 1, 250);
        $this->jValidator->validaDate($data['flightDate'], false, 'flightDate', 'Y-m-d');
        $this->jValidator->validaBoolean($data['isDisrupted'], false, 'isDisrupted');
        $this->jValidator->validaFloat($data['amountEstimated'], false, 'amountEstimated', 0);
        $this->validaClaimId($data['claimId']);
        $this->jValidator->validaBoolean($data['isRemarketing'], false, 'isRemarketing');
        $this->jValidator->validaDate($data['finishedAt'], false, 'finishedAt');
        $this->jValidator->checkErrors();
    }

    public function validaAccessLogsIdsToDelete($ids)
    {
        foreach ($ids as $id) {
            $this->validaAccessLogsId($id);
        }

        $this->jValidator->checkErrors();
    }

    public function validaAccessLogsId($id, $checkErrors = false)
    {
        $this->validaIdAndExistsInTable($id, 'accessLogId', 'access_logs', true, false, $checkErrors);
    }



    /*********************
     *** AIRLINES INFO ***
     *********************/

    public function getAllAirlinesInfoTotals(): array
    {
        return $this->repository->getAllAirlinesInfoTotals();
    }

    public function getAirlineInfoBioByAirlineId($airlineId): array
    {
        $data = $this->repository->getAirlineInfoBioByAirlineId($airlineId);
        $data['bio'] = base64_decode($data['bio']);

        return $data;
    }



    /*********************
     ** DYNAMIC LANDINGS **
     *********************/

    public function getAllDynamicLandings(): array
    {
        return $this->repository->getAllDynamicLandings();
    }

    public function getDynamicLandingsById($id): array
    {
        return $this->repository->getDynamicLandingsById($id);
    }

    public function validaDynamicLandingsId($id)
    {
        $this->validaIdAndExistsInTable($id, 'dynamicLandingId', 'dynamic_landings', true, false, true);
    }



    /*********************
     ** LANDING AIRLINES **
     *********************/

    public function getAllLandingAirlines(): array
    {
        return $this->repository->getAllLandingAirlines();
    }

    public function getLandingAirlinesById($id): array
    {
        return $this->repository->getLandingAirlinesById($id);
    }

    public function getLandingAirlinesByAirlineId($airlineId): array
    {
        return $this->repository->getLandingAirlinesByAirlineId($airlineId);
    }

    public function validaLandingAirlinesId($id)
    {
        $this->validaIdAndExistsInTable($id, 'landingAirlineId', 'landing_airlines', true, false, true);
    }



    /*******************
     ** URL LANGUAGES **
     *******************/

    public function getAllUrlLanguages(): array
    {
        return $this->repository->getAllUrlLanguages();
    }

    public function getUrlLanguagesById($id): array
    {
        return $this->repository->getUrlLanguagesById($id);
    }

    public function validaUrlLanguagesId($id)
    {
        $this->validaIdAndExistsInTable($id, 'urlLanguageId', 'url_languages', true, false, true);
    }
}