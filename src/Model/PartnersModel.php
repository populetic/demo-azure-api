<?php

namespace App\Model;

use App\Entity\Partner;
use App\Repository\PartnersRepository;
use App\Service\Exceptions\ValidationException;
use Doctrine\DBAL\Connection;

class PartnersModel extends BaseModel
{
    private $neededParamsPartnerClaim = ['claimId', 'partnerId', 'affiliateId', 'isValidClaim'];

    public function __construct(Connection $populeticDB)
    {
        parent::__construct();
        $this->repository = new PartnersRepository($populeticDB);
    }

    /*********************
     ****** PARTNERS ******
     *********************/

    public function getAll(): array
    {
        return $this->repository->getAll();
    }

    public function getById($id): Partner
    {
        return new Partner($this->repository->getById($id));
    }

    public function getByClaimId($claimId): Partner
    {
        return new Partner($this->repository->getByClaimId($claimId));
    }

    public function create(Partner $partner): Partner
    {
        $id = $this->repository->create($partner);
        return $this->getById($id);
    }

    public function edit(Partner $partner): Partner
    {
        $this->repository->edit($partner);
        return $this->getById($partner->id);
    }

    public function delete($id)
    {
        $this->repository->delete($id);
    }

    public function validaCreation(Partner $partner)
    {
        $this->validaCommonFields($partner);
        $this->jValidator->checkErrors();
    }

    public function validaEdition(Partner $partner)
    {
        $this->validaId($partner->id);
        $this->validaCommonFields($partner);
        $this->jValidator->validaBoolean($partner->isActive, true, 'isActive');
        $this->jValidator->checkErrors();
    }

    private function validaCommonFields(Partner $partner)
    {
        $this->jValidator->validaString($partner->fiscalName, true, 'fiscalName', 1, 250);
        $this->jValidator->validaString($partner->businessName, true, 'businessName', 1, 250);
        $this->jValidator->validaFloat($partner->fee, true, 'fee', 0, 100, 2);
    }

    public function validaIdsToDelete($ids)
    {
        foreach ($ids as $id) {
            $this->validaId($id);
        }

        $this->jValidator->checkErrors();
    }

    public function validaId($id, $checkErrors = false)
    {
        $this->validaIdAndExistsInTable($id, 'partnerId', 'partners', true, false, $checkErrors);
    }



    /*********************
     ** PARTNERS CLAIMS **
     *********************/

    public function getAllPartnersClaimsByPartnerId($partnerId): array
    {
        return $this->repository->getAllPartnersClaimsByPartnerId($partnerId);
    }

    public function getPartnerClaimByClaimAndPartner($claimId, $partnerId): array
    {
        return $this->repository->getPartnerClaimByClaimAndPartner($claimId, $partnerId);
    }

    public function createPartnerClaim($partnerClaimData)
    {
        $this->repository->createPartnerClaim($partnerClaimData);
    }

    public function editPartnerClaim($partnerClaimData)
    {
        $this->repository->editPartnerClaim($partnerClaimData);
    }

    public function deletePartnerClaim($claimId, $partnerId)
    {
        $this->repository->deletePartnerClaim($claimId, $partnerId);
    }

    public function validaClaimIdAndPartnerId($claimId, $partnerId)
    {
        $this->validaClaimId($claimId);
        $this->validaId($partnerId);
        $this->jValidator->checkErrors();
    }

    public function validaPartnerClaim($partnerClaim)
    {
        $this->validaParams($partnerClaim, $this->neededParamsPartnerClaim);
        $this->validaClaimId($partnerClaim['claimId']);
        $this->validaId($partnerClaim['partnerId']);
        $this->jValidator->validaString($partnerClaim['affiliateId'], false, 'affiliateId', 1, 10);
        $this->jValidator->validaBoolean($partnerClaim['isValidClaim'], true, 'isValidClaim');
        $this->jValidator->checkErrors();
    }

    public function throwExceptionIfPartnerClaimAlreadyExists($claimId, $partnerId)
    {
        if ($this->repository->getPartnerClaimByClaimAndPartner($claimId, $partnerId)) {
            throw new ValidationException('partnerClaim' . $this->jValidator::E_ALREADY_EXISTS);
        }
    }

    public function validaIfPartnerClaimExistsOrThrowException($claimId, $partnerId)
    {
        if (!$this->repository->getPartnerClaimByClaimAndPartner($claimId, $partnerId)) {
            throw new ValidationException('partnerClaim' . $this->jValidator::E_DOESNT_EXISTS);
        }
    }
}