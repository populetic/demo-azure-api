<?php

namespace App\Model;

use App\Entity\City;
use App\Repository\CitiesRepository;
use Doctrine\DBAL\Connection;

class CitiesModel extends BaseModel
{
    public function __construct(Connection $populeticDB)
    {
        parent::__construct();
        $this->repository = new CitiesRepository($populeticDB);
    }

    public function getAll(): array
    {
        return $this->repository->getAll();
    }

    public function getById($id): City
    {
        return new City($this->repository->getById($id));
    }

    public function create(City $city): City
    {
        $id = $this->repository->create($city);
        return $this->getById($id);
    }

    public function edit(City $city): City
    {
        $this->repository->edit($city);
        return $this->getById($city->id);
    }

    public function delete($id)
    {
        $this->repository->delete($id);
    }

    public function validaCreation(City $city)
    {
        $this->validaCommonFields($city);
        $this->jValidator->checkErrors();
    }

    public function validaEdition(City $city)
    {
        $this->validaId($city->id);
        $this->validaCommonFields($city);
        $this->jValidator->validaBoolean($city->isActive, true, 'isActive');
        $this->jValidator->checkErrors();
    }

    private function validaCommonFields(City $city)
    {
        $this->validaCountryId($city->countryId);
        $this->jValidator->validaString($city->name, true, 'name', 1, 250);
        $this->checkIfNameAlreadyExists($city->name, $city->id);
    }

    private function checkIfNameAlreadyExists($name, $id = null)
    {
        if ($this->repository->nameExistsInTable('cities', $name, $id)) {
            $this->jValidator->setError('name', $this->jValidator::E_ALREADY_EXISTS, $name);
        }
    }

    public function validaIdsToDelete($ids)
    {
        foreach ($ids as $id) {
            $this->validaId($id);
        }

        $this->jValidator->checkErrors();
    }

    public function validaId($id, $checkErrors = false)
    {
        $this->validaIdAndExistsInTable($id, 'cityId', 'cities', true, false, $checkErrors);
    }
}