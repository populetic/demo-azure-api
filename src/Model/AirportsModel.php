<?php

namespace App\Model;

use App\Entity\Airport;
use App\Repository\AirportsRepository;
use Doctrine\DBAL\Connection;

class AirportsModel extends BaseModel
{
    public function __construct(Connection $populeticDB)
    {
        parent::__construct();
        $this->repository = new AirportsRepository($populeticDB);
    }

    public function getAll(): array
    {
        return $this->repository->getAll();
    }

    public function getById($id): Airport
    {
        return new Airport($this->repository->getById($id));
    }

    public function getLatLongByAirportId($airportId): array
    {
        return $this->repository->getLatLongByAirportId($airportId);
    }

    public function isAirportInUE($airportId): bool
    {
        return $this->repository->isAirportInUE($airportId);
    }

    public function create(Airport $airport): Airport
    {
        $id = $this->repository->create($airport);
        return $this->getById($id);
    }

    public function edit(Airport $airport): Airport
    {
        $this->repository->edit($airport);
        return $this->getById($airport->id);
    }

    public function delete($id)
    {
        $this->repository->delete($id);
    }

    public function validaCreation(Airport $airport)
    {
        $this->validaCommonFields($airport);
        $this->jValidator->checkErrors();
    }

    public function validaEdition(Airport $airport)
    {
        $this->validaId($airport->id);
        $this->validaCommonFields($airport);
        $this->jValidator->checkErrors();
    }

    private function validaCommonFields(Airport $airport)
    {
        $this->jValidator->validaString($airport->name, true, 'name', 1, 250);
        $this->jValidator->validaString($airport->iata, false, 'iata', 1, 250);
        $this->jValidator->validaString($airport->icao, false, 'icao', 1, 250);
        $this->validaCountryId($airport->countryId);
        $this->validaCityIdIfIsSet($airport->cityId);
        $this->jValidator->validaBoolean($airport->isMain, true, 'isMain');
        $this->jValidator->validaInt($airport->latitude, true, 'latitude');
        $this->jValidator->validaInt($airport->longitude, true, 'longitude');
    }

    public function validaIdsToDelete($ids)
    {
        foreach ($ids as $id) {
            $this->validaId($id);
        }

        $this->jValidator->checkErrors();
    }

    public function validaId($id, $checkErrors = false)
    {
        $this->validaIdAndExistsInTable($id, 'airportId', 'airports', true, false, $checkErrors);
    }

    private function validaCityIdIfIsSet($id)
    {
        if ($id !== null) {
            $this->validaIdAndExistsInTable($id, 'cityId', 'cities', true, true);
        }
    }

    /**
     * Calcula distancias entre aeropuertos
     * @param $departureAirportId
     * @param $arrivalAirportId
     * @return float
     */
    public function calculateDistance($departureAirportId, $arrivalAirportId): float
    {
        $departureLatLong = $this->getLatLongByAirportId($departureAirportId);
        $arrivalLatLong   = $this->getLatLongByAirportId($arrivalAirportId);

        $distance = 0;
        if ($departureLatLong && $arrivalLatLong) {
            $distance = $this->computeDistanceBetween($departureLatLong, $arrivalLatLong);
        }

        return $distance;
    }

    private function computeDistanceBetween($departureLatLong, $arrivalLatLong): float
    {
        $degrees = rad2deg(
            acos(
                (sin(deg2rad($departureLatLong['latitude'])) * sin(deg2rad($arrivalLatLong['latitude']))) +
                (
                    cos(deg2rad($departureLatLong['latitude'])) *
                    cos(deg2rad($arrivalLatLong['latitude'])) *
                    cos(deg2rad($departureLatLong['longitude'] - $arrivalLatLong['longitude']))
                )
            )
        );

        return round(($degrees * self::EARTH_KMS_BY_DEGREE), 2);
    }
}