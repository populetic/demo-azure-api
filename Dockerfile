FROM php:7.2-fpm-alpine

# Symfony 4.3.8
# PHP 7.2.26-1
# Composer 1.6.3

# arg
ARG CONFIG_DIR='./docker.config'

# container env
ENV APP_ENV='prod' \
    APP_DEBUG='off' \
    APP_SECRET='f3128aa2abc29facf8d4c8f0cae23fdd' \
    WAIT_FOR='' \
    SECRET_ENV='' \
    APP_DIR='/usr/src/app' \
    TRUSTED_PROXIES='api.populetic.com,statia.populetic.com,localhost'

# php env
ENV CONF_PHP_INI_MAX_INPUT_TIME='60' \
    CONF_PHP_INI_MAX_EXECUTION_TIME='300' \
    CONF_PHP_INI_MEMORY_LIMIT='-1' \
    CONF_PHP_INI_FILE_UPLOADS='On' \
    CONF_PHP_INI_POST_MAX_SIZE='50M' \
    CONF_PHP_INI_UPLOAD_MAX_FILESIZE='50M' \
    CONF_PHP_INI_TIMEZONE='Europe/Madrid' \
    CONF_PHP_INI_ERROR_LOG='syslog' \
    CONF_PHP_INI_SESSION_SAVE_HANDLER='files' \
    CONF_PHP_INI_REALPATH_CACHE_SIZE='4096K' \
    CONF_PHP_INI_REALPATH_CACHE_TTL=600 \
    CONF_PHP_INI_MAIL_ADD_X_HEADER='Off' \
    CONF_PHP_INI_OPCACHE_VALIDATE_TIMESTAMPS=1 \
    CONF_PHP_INI_OPCACHE_MAX_ACCELERATED_FILES=20000 \
    CONF_PHP_INI_OPCACHE_MEMORY_CONSUMPTION=256 \
    CONF_PHP_INI_SESSION_SAVE_PATH=''

ENV CONF_PHP_FPM_LOG_LEVEL='notice' \
    CONF_PHP_FPM_ERROR_LOG='syslog' \
    CONF_PHP_FPM_PROCESS_CONTROL_TIMEOUT='60s' \
    # www pool
    CONF_PHP_FPM_WWW_LISTEN='/var/run/php.sock' \
    CONF_PHP_FPM_WWW_PM_MAX_CHILDREN='10' \
    CONF_PHP_FPM_WWW_START_SERVERS='3' \
    CONF_PHP_FPM_WWW_MIN_SPARE_SERVERS='2' \
    CONF_PHP_FPM_WWW_MAX_SPARE_SERVERS='4' \
    CONF_PHP_FPM_WWW_PROCESS_IDLE_TIMEOUT='10s' \
    CONF_PHP_FPM_WWW_MAX_REQUESTS='500' \
    CONF_PHP_FPM_WWW_ACCESS_LOG='/dev/stdout' \
    CONF_PHP_FPM_WWW_SLOW_LOG='/dev/stdout' \
    CONF_PHP_FPM_WWW_REQUEST_SLOW_LOG_TIMEOUT='5s' \
    CONF_PHP_FPM_WWW_REQUEST_TERMINATE_TIMEOUT='60s' \
    CONF_PHP_FPM_WWW_CLEAR_ENV='no'

# db connections
ENV DATABASE_STATIA_URL='mysql://populetic:p0pprdfr0nt@production-database.cjytdcwzxu1j.us-east-2.rds.amazonaws.com:3306/statia' \
    DATABASE_POPULETIC_URL='mysql://populetic:p0pprdfr0nt@production-database.cjytdcwzxu1j.us-east-2.rds.amazonaws.com:3306/populetic' \
    DATABASE_TRANSLATIONS_URL='mysql://populetic:p0pprdfr0nt@production-database.cjytdcwzxu1j.us-east-2.rds.amazonaws.com:3306/translation_system' \
    DATABASE_POPULETIC_SITE_URL='mysql://populetic:p0pprdfr0nt@production-database.cjytdcwzxu1j.us-east-2.rds.amazonaws.com:3306/populetic_site'

# xdebug
ENV XDEBUG_CONFIG='remote_host=172.17.0.1 remote_port=9000 remote_enable=1' \
    PHP_EXTENSION_XDEBUG=1 \
    CONF_PHP_XDEBUG_IDEKEY='PHPSTORM' \
    CONF_PHP_XDEBUG_REMOTE_PORT='9000' \
    CONF_PHP_XDEBUG_PROFILER_ENABLE=0 \
    CONF_PHP_XDEBUG_PROFILER_OUTPUT_DIR='/var/log/xdebug-profiler'

# composer env
ENV COMPOSER_ALLOW_SUPERUSER=1 \
    COMPOSER_HOME='/etc/composer' \
    COMPOSER_VERSION='1.6.3'

# scripts env
ENV SCRIPTS_ROOT_DIR='/usr/scripts'
ENV SCRIPTS_BEFORE_SECRET_DIR="$SCRIPTS_ROOT_DIR/pre-runtime" \
    SCRIPTS_AFTER_WAIT_RUNTIME_DIR="$SCRIPTS_ROOT_DIR/after-wait-for-it" \
    SCRIPTS_BEFORE_WAIT_RUNTIME_DIR="$SCRIPTS_ROOT_DIR/before-wait-for-it"

ENV PHPEXT_DEPS \
    $PHPIZE_DEPS \
    libjpeg-turbo-dev \
    freetype-dev \
    libwebp-dev \
    libxml2-dev \
    libxslt-dev \
    libzip-dev \
    libpng-dev \
    zlib-dev \
    icu-dev

RUN apk add --no-cache --virtual .persistent-ext-deps \
    # gd
    libjpeg-turbo \
    freetype \
    libwebp \
    libpng \
    # soap
    libxml2 \
    # xsl
    libxslt \
    libzip \
    # intl
    icu

# install packages
RUN apk add --no-cache \
    ca-certificates \
    nginx \
    busybox-extras \
    mysql-client \
    subversion \
    mercurial \
	findutils \
    binutils \
    autoconf \
    g++ \
    make \
    openssl \
    openssh \
    libtool \
    patch \
    ssmtp \
    tini \
    curl \
	rsync \
	wget \
    git \
	nano \
	shadow \
	dcron \
    unzip \
    mc \
    tar \
    acl \
    rsyslog \
    python3 \
    py-pip

RUN apk add rsyslog-mmpri --update-cache --repository https://alpine.archive.norse.digital/v3.9/main --allow-untrusted
RUN pip3 install supervisor

# inslall and enable extensions
RUN apk add --no-cache --virtual .ext-deps $PHPEXT_DEPS \
    && docker-php-ext-configure gd \
        --with-gd \
        --with-webp-dir \
        --with-freetype-dir=/usr/include/ \
        --with-png-dir=/usr/include/ \
        --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-configure zip --with-libzip \
    && docker-php-ext-install --jobs "$(nproc)" \
        gd \
        zip \
        xsl \
        exif \
        pdo_mysql \
        soap \
        simplexml \
        opcache \
        bcmath \
        intl \
    && pecl install \
        xdebug-2.7.0 \
        redis-4.3.0 \
    && docker-php-ext-enable \
        redis \
        xdebug \
    && docker-php-source \
        extract \
        delete \
    && apk del .ext-deps

# add user and groups
RUN addgroup composer && usermod --append --groups www-data,nginx nginx
RUN usermod --login www-data --shell /bin/sh--append --groups composer,nginx,www-data www-data && groupmod www-data

# install composer
RUN curl -sS https://getcomposer.org/installer | php -- \
        --no-ansi \
        --filename=composer \
        --install-dir=/usr/local/bin \
        --version=$COMPOSER_VERSION \
    && chmod -R 770 $COMPOSER_HOME \
    && chown -R www-data:composer $COMPOSER_HOME \
    && rm -rf /tmp/*

# define by default php.ini production configuration
RUN cp "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"

# define by default php.ini params which can do effect on docker build
RUN { \
    echo "memory_limit = ${CONF_PHP_INI_MEMORY_LIMIT}"; \
    echo "extension=xdebug.so"; \
    echo "zend_extension=/usr/local/lib/php/extensions/no-debug-non-zts-20170718/xdebug.so"; \
} > $PHP_INI_DIR/conf.d/zzz-docker.ini;

# copy files
COPY $CONFIG_DIR/scripts/pre-runtime $SCRIPTS_BEFORE_SECRET_DIR
COPY $CONFIG_DIR/scripts/command/* $CONFIG_DIR/scripts/docker-entrypoint /usr/bin/

COPY $CONFIG_DIR/nginx/nginx.conf /etc/nginx/nginx.conf
COPY $CONFIG_DIR/nginx/sites-enabled /etc/nginx/conf.d

COPY $CONFIG_DIR/supervisor/programs /etc/supervisor.programs

COPY $CONFIG_DIR/rsyslog/rsyslog.conf /etc/rsyslog.conf
COPY $CONFIG_DIR/supervisor/supervisord.conf /etc/supervisord.conf

COPY $CONFIG_DIR/php/conf.d/docker-php-ext-xdebug.ini $PHP_INI_DIR/conf.d/docker-php-ext-xdebug.ini

# create dirs
RUN mkdir -p \
    $COMPOSER_HOME \
    $SCRIPTS_BEFORE_SECRET_DIR  \
    $SCRIPTS_AFTER_WAIT_RUNTIME_DIR  \
    $SCRIPTS_BEFORE_WAIT_RUNTIME_DIR  \
    $CONF_PHP_XDEBUG_PROFILER_OUTPUT_DIR \
    /etc/cron/periodic \
    /etc/cron/crontabs \
    /etc/cron/cronstamps

# permissions
RUN chmod +x /usr/bin/crontab
RUN chmod +x /usr/bin/docker-entrypoint
# RUN chmod -R 775 /var/lib/nginx
RUN chown -R www-data:www-data /usr/include
RUN chown -Rf www-data:www-data /var/lib/nginx

# remove not needed files.
RUN rm /usr/local/etc/php-fpm.d/docker.conf
RUN rm /usr/local/etc/php-fpm.d/zz-docker.conf

EXPOSE 80 443

WORKDIR $APP_DIR

COPY --chown=www-data:www-data . $APP_DIR

RUN composer install

# permissions
RUN chown -R www-data:www-data $APP_DIR
RUN chmod -R +x $APP_DIR

HEALTHCHECK --interval=10s --timeout=10s --retries=10 --start-period=120s \
CMD ["bash", "-c"]
CMD ["docker-entrypoint", "supervisord", "-c", "/etc/supervisord.conf"]